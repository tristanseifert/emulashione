//#include <catch2/catch_test_macros.hpp>
#include <catch2/catch.hpp>

#include <emulashione.h>

#include <cstring>

/**
 * Reads out various version information.
 */
TEST_CASE("check version methods work without initialization", "[init]") {
    SECTION("short version is valid semver string") {
        auto semStr = libemulashione_version();
        REQUIRE(!!semStr);
        REQUIRE(strlen(semStr) >= 3); // shortest is, for example, 1.0

        // semver must have at least one '.' in it
        REQUIRE(strchr(semStr, '.'));
    }

    SECTION("long version string is valid") {
        auto longStr = libemulashione_version_long();
        REQUIRE(!!longStr);
        REQUIRE(strlen(longStr));
    }
}
