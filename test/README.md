All tests for the core library are contained within this directory. Tests are implemented using the [Catch2](https://github.com/catchorg/Catch2) framework, and each test suite is isolated to its own directory.

## Available Suites
Following is a list of available test suites:

- **setup**: Tests loading the core library and initializing it. This also exercises all methods that can be used to get library information prior to initialization.

