#include <iostream>

#include <emulashione.h>

#include <Info.h>
#include <Log.h>
#include <Registry.h>

#include <cassert>
#include <csignal>
#include <cstdio>
#include <cstring>
#include <list>
#include <string>

#include <getopt.h>

struct emulashione_system *em;

/**
 * Signal handler for Ctrl+C; this will abort the emulation.
 */
static void SigIntHandler(int sig) {
    // ensure this can't happen again
    signal(sig, SIG_IGN);

    // and abort it
    std::cerr << "SIGINT received; aborting emulation!" << std::endl;
    libemulashione_system_abort(em, -42069);
}


/**
 * Prints command line usage instructions.
 */
static void PrintHelp() {
    std::cout << R"(Emulashione test utility

Required Arguments:

- s: Path to the system definition file to load


Optional Arguments:

- l: Log level, where 1 is most (trace logging) and 6 is the least (critical
     messages only)

- p: Specify the path of a directory that contains plugins. May be specified
     multiple times
)";
}

/**
 * Utility entry point
 */
int main(const int argc, char * const *argv) {
    emulashione_status_t err;
    int runStatus;

    emulashione_init_params_t emParams{};

    // parse args
    std::string systemDef;
    bool nextOpt{true};

    std::list<std::string> pluginDirs;

    while(nextOpt) {
        const auto ret = getopt(argc, argv, "s:l:p:h");

        switch(ret) {
            case 's':
                systemDef = optarg;
                break;

            // log level
            case 'l': {
                emParams.logLevel = std::atoi(optarg);
                if(emParams.logLevel <= 0 || emParams.logLevel > 6) {
                    std::cerr << "invalid log level: must be [1, 6] (got " << emParams.logLevel
                        << ")" << std::endl;
                    return -1;
                }
                break;
            }

            // plugin dir
            case 'p':
                pluginDirs.emplace_back(optarg);
                break;

            case '?':
            case 'h':
                PrintHelp();
                return 0;

            case -1:
                nextOpt = false;
                break;
        }
    }

    if(systemDef.empty()) {
        std::cerr << "you must specify a system definition with the -s switch" << std::endl;
        return -1;
    }

    // get version
    std::cout << "Version: " << libemulashione_version() << " (" << libemulashione_version_long()
        << ")" << std::endl;

    // initialize library
    if((err = libemulashione_init(nullptr, &emParams))) {
        std::cerr << "Failed to initialize library: " << err << std::endl;
        return -1;
    }

    for(const auto &path : pluginDirs) {
        const auto st = libemulashione_plugin_load_dir(path.c_str());
        if(st != kEmulashioneSuccess) {
            std::cerr << "Failed to load plugins from directory '" << path << "': " << st
                << std::endl;
            return -1;
        }
    }

    // create an emulation instance and load a definition
    em = libemulashione_system_alloc();
    std::cout << "Emulation instance: $" << std::hex << em << std::dec << std::endl;
    assert(!!em);

    err = libemulashione_system_load_definition(em, systemDef.c_str());
    if(err) {
        std::cerr << "Failed to load definition (from " << systemDef << "): " << err << std::endl;
        goto beach;
    }

    // install signal handler
    signal(SIGINT, SigIntHandler);

    // run the thingie
    runStatus = libemulashione_system_run(em);
    if(runStatus) {
        std::cerr << "Failed to start system: " << runStatus << std::endl;
        goto beach;
    }

beach:;
    // clean up
    if(em && (err = libemulashione_system_free(em))) {
        std::cerr << "Failed to free system: " << err << std::endl;
        goto beach;
    }

    if((err = libemulashione_deinit())) {
        std::cerr << "Failed to deinitialize library: " << err << std::endl;
        return -1;
    }

    return 0;
}
