# Utilities
Various utilities for working with the emulator.

## EmulashioneInfo
Outputs information about the available Emulashione library, such as its version, supported systems and devices, and more.

