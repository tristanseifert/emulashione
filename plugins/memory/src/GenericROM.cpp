#include "GenericROM.h"
#include "version.h"

#include <algorithm>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <iterator>
#include <stdexcept>

#include <Config.h>
#include <Logger.h>
#include <interfaces/Device.h>

using namespace emulashione::plugin;
using namespace emulashione::memory;

const std::string_view GenericROM::kVersion{gVERSION_SHORT};

EMULASHIONE_DEVICE_DESC(GenericROM, kSupportsMemory,
        (kSupportsPerInstanceThread | kEmulationModePassive),
        0xE0,0xAF,0x0B,0xED,0xC1,0xDB,0x46,0xD6,0xB7,0xF9,0x5D,0x41,0x63,0x49,0x15,0xF6);

/**
 * Reserves the memory the ROM is loaded into.
 *
 * The following config keys are mandatory:
 * - `access`: Access time, in nanoseconds.
 *
 * The following config keys may be specified:
 * - `align`: ROM size alignment, in bytes. Default is 0, which disables alignment.
 * - `file`: Path to a file with the contents of the ROM.
 * - `padding`: Byte value to repeat to align the ROM size up to the specified alignment
 */
GenericROM::GenericROM(const std::string_view &name, struct emulashione_config_object *_cfg) :
    MemoryDevice(name, _cfg, MemoryDevice::InitFlags::ReadOnly) {
    if(!_cfg) {
        throw std::invalid_argument("Config required for RAM");
    }
    Config cfg(_cfg);

    // read out access time and alignment/padding parameters
    this->accessTime = cfg.getDouble("access");

    if(cfg.hasKey("align")) {
        this->alignment = cfg.getUnsigned("align");
    } if(cfg.hasKey("padding")) {
        this->padding = static_cast<std::byte>(cfg.getUnsigned("padding"));
    }

    Logger::Debug("Generic ROM: {} ns access, {} byte alignment (fill ${:02x})",
            this->accessTime, this->alignment, this->padding);

    // load from file if specified
    if(cfg.hasKey("file")) {
        const auto filePath = cfg.getString("file");
        std::filesystem::path path(filePath);

        if(!std::filesystem::exists(path)) {
            throw std::runtime_error(fmt::format("failed to open ROM file at '{}'", path.string()));
        }

        std::ifstream in(path, std::ios::binary);

        in.seekg(0, std::ios::end);
        const auto numBytes = in.tellg();
        in.seekg(0, std::ios::beg);

        this->storage.resize(numBytes);
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        in.read(reinterpret_cast<char *>(this->storage.data()), numBytes);

        // do byteswapping, if needed
        if(cfg.hasKey("byteswap")) {
            auto swapMode = cfg.getString("byteswap");

            if(swapMode == "word") {
                for(size_t i = 0; i < (this->storage.size() / 2); i++) {
                    const auto x = this->storage[i * 2], y = this->storage[(i * 2) + 1];

                    this->storage[(i * 2) + 1] = x;
                    this->storage[i * 2] = y;
                }
            } else {
                throw std::runtime_error(fmt::format("unknown byteswap mode '{}'", swapMode));
            }
        }

        this->roundUpStorage();
        Logger::Debug("Generic ROM: Read {} bytes (aligned to {}) from '{}'", numBytes,
                this->storage.size(), path.string());
    }
}

/**
 * Rounds up and pads the current storage size.
 */
void GenericROM::roundUpStorage() {
    if(!this->alignment || !(this->storage.size() % this->alignment)) return;

    const auto bytesNeeded = this->alignment - (this->storage.size() % this->alignment);
    this->storage.reserve(this->storage.size() + bytesNeeded);

    for(size_t i = 0; i < bytesNeeded; i++) {
        this->storage.emplace_back(this->padding);
    }
}

