#include "MemoryDevice.h"

#include <algorithm>
#include <fstream>
#include <iterator>
#include <stdexcept>

#include <Logger.h>

#include <fmt/core.h>

using namespace emulashione::plugin;
using namespace emulashione::memory;

/**
 * Initializes the generic memory.
 *
 * Currently this is a no-op.
 *
 * @param name Short name for this device
 * @param cfg Configuration keys provided in the system description for this device
 * @param flags A combination of InitFlags that define how the memory device behaves
 */
MemoryDevice::MemoryDevice(const std::string_view &name, struct emulashione_config_object *cfg,
        const InitFlags flags) : Device(name, cfg) {
    // register the bus slots to which the device can be connected
    registerBusSlotName("bus");

    // get read/write mode
    this->readOnly = (flags != InitFlags::ReadWrite);
}

/**
 * Attempt to load the contents of the memory from the provided file path.
 *
 * @param path File to open and read in binary mode into the contents of the memory
 *
 * @return Whether the file was opened and read successfully
 */
bool MemoryDevice::loadContents(const std::filesystem::path &path) {
    if(!std::filesystem::exists(path)) return false;
    auto storage = this->getStorage();

    // try to open and read as binary into the storage array
    try {
        std::ifstream in(path, std::ios::binary);
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        in.read(reinterpret_cast<char *>(storage.data()), storage.size());

        Logger::Debug("Memory: Loaded contents from '{}'", path.string());
    } catch(const std::exception &e) {
        Logger::Warning("Failed to load memory contents from '{}': {}", path.string(), e.what());
    }

    // if we get here, success
    return true;
}

/**
 * Writes out the RAM contents to the specified file path.
 *
 * @param path Path to write the memory contents to
 */
void MemoryDevice::writeContents(const std::filesystem::path &path) {
    auto storage = this->getStorage();

    try {
        std::ofstream out(path, std::ios::binary | std::ios::trunc);
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        out.write(reinterpret_cast<const char *>(storage.data()), storage.size());
    } catch(const std::exception &e) {
        Logger::Error("Failed to save memory contents to '{}': {}", path.string(), e.what());
    }
}



/**
 * Reads from the memory device.
 *
 * @param caller Device performing the read transaction
 * @param addressSpace Address space id; only the default memory space `0` is supported
 * @param address Full memory address of the access, taken modulo the memory's size
 * @param width Access width
 * @param outData Location to store the read data
 * @param time Current system time point at which the access is performed
 * @param accessTime Location to store the duration taken for the memory access, in nanoseconds
 *
 * @throw If write would wrap around the memory array, or an unsupported address space is specified
 *
 */
void MemoryDevice::read(struct emulashione_device_instance_header *caller,
        const uint16_t addressSpace, const uint64_t address, const AccessWidth width,
        uint64_t &outData, const double time, double &accessTime) {
    (void) caller, (void) time;

    // validate
    if(addressSpace) {
        throw std::runtime_error(fmt::format("unsupported address space: {}", addressSpace));
    }

    auto storage = this->getStorage();
    const auto offset = address % storage.size();

    // read from storage
    switch(width) {
        case AccessWidth::Byte:
            outData = static_cast<uint64_t>(storage[offset]);
            break;
        case AccessWidth::Word:
            if(offset + 2 > storage.size()) {
                throw std::runtime_error(fmt::format("wrapped read not yet supported (off {})",
                            offset));
            } else {
                uint16_t temp;
                memcpy(&temp, &storage[offset], sizeof(temp));
                outData = temp;
            }
            break;
        case AccessWidth::LongWord:
            if(offset + 4 > storage.size()) {
                throw std::runtime_error(fmt::format("wrapped read not yet supported (off {})",
                            offset));
            } else {
                uint32_t temp;
                memcpy(&temp, &storage[offset], sizeof(temp));
                outData = temp;
            }
            break;
        case AccessWidth::QuadWord:
            if(offset + 8 > storage.size()) {
                throw std::runtime_error(fmt::format("wrapped read not yet supported (off {})",
                            offset));
            } else {
                uint64_t temp;
                memcpy(&temp, &storage[offset], sizeof(temp));
                outData = temp;
            }
            break;
    }

    // access time is fixed
    accessTime = this->getAccessTime(offset, false);
}

/**
 * Performs a write against the memory device.
 *
 * @param caller Device performing the read transaction
 * @param addressSpace Address space id; only the default memory space `0` is supported
 * @param address Full memory address of the access, taken modulo the memory's size
 * @param width Access width
 * @param inData Data to write to memory; only the n least significant bytes (decided by width)
 *        will be written.
 * @param time Current system time point at which the access is performed
 * @param accessTime Location to store the duration taken for the memory access, in nanoseconds
 * @param mask A bitmask to apply against the written data, or 0 to write all bits.
 *
 * @throw If the address space id is invalid.
 */
void MemoryDevice::write(struct emulashione_device_instance_header *caller,
        const uint16_t addressSpace, const uint64_t address, const AccessWidth width,
        const uint64_t inData, const double time, double &accessTime, const uint64_t mask) {
    (void) caller, (void) time;

    // validate
    if(addressSpace) {
        throw std::runtime_error(fmt::format("unsupported address space: {}", addressSpace));
    }
    auto storage = this->getStorage();
    const auto offset = address % storage.size();

    // ignore writes (for read only memories)
    if(this->readOnly) {
        Logger::Warning("Write to read-only memory! (address ${:x})", address);

        accessTime = this->getAccessTime(offset, true);
        return;
    }

    // write to storage
    if(mask) {
        this->writeMasked(storage, offset, width, inData, mask);
    } else {
        this->writeFull(storage, offset, width, inData);
    }

    // access time is fixed
    accessTime = this->getAccessTime(offset, true);

    // set the dirty flag
    this->dirty = true;
}
/**
 * Performs a write where all bits in the word are stored.
 *
 * @throw If write would wrap around the memory array
 */
void MemoryDevice::writeFull(std::span<std::byte> &storage, const size_t offset,
        const AccessWidth width, const uint64_t data) {
    switch(width) {
        case AccessWidth::Byte:
            storage[offset] = static_cast<std::byte>(data);
            break;
        case AccessWidth::Word:
            if(offset + 2 > storage.size()) {
                throw std::runtime_error(fmt::format("wrapped write not yet supported (off {})",
                            offset));
            } else {
                const auto temp = static_cast<uint16_t>(data);
                memcpy(&storage[offset], &temp, sizeof(temp));
            }
            break;
        case AccessWidth::LongWord:
            if(offset + 4 > storage.size()) {
                throw std::runtime_error(fmt::format("wrapped write not yet supported (off {})",
                            offset));
            } else {
                const auto temp = static_cast<uint32_t>(data);
                memcpy(&storage[offset], &temp, sizeof(temp));
            }
            break;
        case AccessWidth::QuadWord:
            if(offset + 8 > storage.size()) {
                throw std::runtime_error(fmt::format("wrapped write not yet supported (off {})",
                            offset));
            } else {
                const auto temp = static_cast<uint64_t>(data);
                memcpy(&storage[offset], &temp, sizeof(temp));
            }
            break;
    }
}
/**
 * Performs a write where only the bits set in the mask are stored.
 *
 * TODO: Should we consider the entire bit mask or only on a byte granularity (like UWS/LWS write
 * strobes on 16-bit memories?)
 *
 * @throw If write would wrap around the memory array
 */
void MemoryDevice::writeMasked(std::span<std::byte> &storage, const size_t offset,
        const AccessWidth width, const uint64_t data, const uint64_t mask) {
    switch(width) {
        case AccessWidth::Byte:
            storage[offset] = static_cast<std::byte>(data & mask);
            break;
        case AccessWidth::Word:
            if(offset + 2 > storage.size()) {
                throw std::runtime_error(fmt::format("wrapped write not yet supported (off {})",
                            offset));
            } else {
                const auto temp = static_cast<uint16_t>(data & mask);
                memcpy(&storage[offset], &temp, sizeof(temp));
            }
            break;
        case AccessWidth::LongWord:
            if(offset + 4 > storage.size()) {
                throw std::runtime_error(fmt::format("wrapped write not yet supported (off {})",
                            offset));
            } else {
                const auto temp = static_cast<uint32_t>(data & mask);
                memcpy(&storage[offset], &temp, sizeof(temp));
            }
            break;
        case AccessWidth::QuadWord:
            if(offset + 8 > storage.size()) {
                throw std::runtime_error(fmt::format("wrapped write not yet supported (off {})",
                            offset));
            } else {
                const auto temp = static_cast<uint64_t>(data & mask);
                memcpy(&storage[offset], &temp, sizeof(temp));
            }
            break;
    }
}

