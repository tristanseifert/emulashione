#include "GenericRAM.h"
#include "version.h"

#include <algorithm>
#include <cstring>
#include <fstream>
#include <iterator>
#include <random>
#include <stdexcept>

#include <fmt/core.h>
#include <Config.h>
#include <Logger.h>
#include <interfaces/Device.h>

using namespace emulashione::plugin;
using namespace emulashione::memory;

const std::string_view GenericRAM::kVersion{gVERSION_SHORT};

EMULASHIONE_DEVICE_DESC(GenericRAM, kSupportsMemory,
        (kSupportsPerInstanceThread | kEmulationModePassive),
        0xBE,0xDE,0x04,0x19,0xFB,0x55,0x48,0x12,0xA9,0xFE,0xE4,0x7E,0x77,0xAD,0x2E,0xA6);

/**
 * Initializes the internal state of the memory, including initializing the memory contents.
 *
 * The following configuration keys are mandatory and must be specified:
 * - `size`: Size of the RAM region, in bytes
 * - `access`: Access time, in nanoseconds.
 * - `contents`: Initial contents of the RAM; may be any of the `FillMode` constants
 *
 * Additionally, the following optional keys can be specified:
 * - `file`: Path to a file that is loaded (if it exists) on startup to fill the RAM instead of the
 *           specified default; and written back when the device is shut down.
 * - `writeback`: When set to `true`, the contents of the RAM are written back to the file path
 *                specified when shutting down.
 */
GenericRAM::GenericRAM(const std::string_view &name, struct emulashione_config_object *_cfg) :
    MemoryDevice(name, _cfg, MemoryDevice::InitFlags::ReadWrite) {
    if(!_cfg) {
        throw std::invalid_argument("Config required for RAM");
    }
    Config cfg(_cfg);

    // allocate backing storage (based on `size`) key
    const auto size = cfg.getUnsigned("size");
    if(!size) throw std::invalid_argument(fmt::format("invalid size {}", size));

    this->storage.resize(size, std::byte{0});

    // read out access time
    this->accessTime = cfg.getDouble("access");

    // load from a file if specified
    bool needsInit{true};
    if(cfg.hasKey("file")) {
        const auto pathStr = cfg.getString("file");
        std::filesystem::path path(pathStr);

        this->persistencePath = path;
        if(this->loadContents(path)) {
            needsInit = false;
        }
    }

    // the file path may only be used for reading
    if(cfg.hasKey("writeback")) {
        this->shouldWriteContents = cfg.getBool("writeback");
    }

    // initialize content
    const auto contentType = cfg.getString("contents");

    if(contentType == "zero") {
        this->storageDefault = FillMode::Zeros;
    } else if(contentType == "ones") {
        this->storageDefault = FillMode::Ones;
    } else if(contentType == "random") {
        this->storageDefault = FillMode::Random;
    }

    if(needsInit) {
        this->initContents();
    }

    Logger::Debug("Generic RAM: size {} bytes, {} ns access, initial data type {} ('{}')",
            this->storage.size(), this->accessTime, (int) this->storageDefault, contentType);
}

/**
 * Initializes the contents of the memory object.
 */
void GenericRAM::initContents() {
    switch(this->storageDefault) {
        case FillMode::Zeros:
            std::fill(this->storage.begin(), this->storage.end(), std::byte{0});
            break;
        case FillMode::Ones:
            std::fill(this->storage.begin(), this->storage.end(), std::byte{0xFF});
            break;
        case FillMode::Random: {
            std::random_device rd;
            std::mt19937 gen(rd());
            std::uniform_int_distribution<uint8_t> dist;
            std::generate(this->storage.begin(), this->storage.end(), [&]() {
                return std::byte{dist(gen)};
            });
            break;
        }
    }
}

/**
 * Saves the RAM contents if requested.
 */
GenericRAM::~GenericRAM() {
    if(this->shouldWriteContents && !this->persistencePath.empty()) {
        if(this->isDirty()) {
            Logger::Debug("Generic RAM: Writing contents to '{}'", this->persistencePath.string());
            this->writeContents(this->persistencePath);
            this->clearDirty();
        } else {
            Logger::Debug("Generic RAM: Skipping writing contents to '{}'; contents NOT dirty", this->persistencePath.string());
        }
    }
}

/**
 * Allows reading the contents of the memory size and contents.
 */
size_t GenericRAM::copyOutInfo(const std::string_view &key, std::span<std::byte> &outBuf) {
    if(key == "size") {
        const size_t size = this->storage.size();
        const auto toCopy = std::min(sizeof(size_t), outBuf.size());
        memcpy(outBuf.data(), &size, toCopy);
        return toCopy;
    } else if(key == "contents") {
        const auto toCopy = std::min(this->storage.size(), outBuf.size());
        memcpy(outBuf.data(), this->storage.data(), toCopy);
        return toCopy;
    }

    return MemoryDevice::copyOutInfo(key, outBuf);
}

/**
 * Allow writing to the memory contents. Any writes larger than the size of the array are
 * truncated and zero byte writes are ignored.
 */
bool GenericRAM::copyInInfo(const std::string_view &key, const std::span<std::byte> &inBuf) {
    if(key == "size") {
        throw Device::ReadOnlyInfoKey();
    } else if(key == "contents") {
        const auto toCopy = std::min(this->storage.size(), inBuf.size());
        memcpy(this->storage.data(), inBuf.data(), toCopy);
        return true;
    }

    return MemoryDevice::copyInInfo(key, inBuf);
}
