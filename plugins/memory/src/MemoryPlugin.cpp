#include "MemoryPlugin.h"
#include "version.h"

#include "GenericRAM.h"
#include "GenericROM.h"

#include <Logger.h>
#include <interfaces/Info.h>

using namespace emulashione::plugin;
using namespace emulashione::memory;

const std::string_view MemoryPlugin::kVersion{gVERSION_SHORT};
/**
 * Initializes the plugin; we register our device types here.
 */
void MemoryPlugin::init() {
    Plugin::RegisterDevice<GenericRAM>();
    Plugin::RegisterDevice<GenericROM>();
}

/// Define the plugin info struct
EMULASHIONE_PLUGIN_DESC(MemoryPlugin,0x01,0x6B,0xF3,0x49,0xA7,0x42,0x4A,0xB1,0xB9,0x1F,0xF2,0x7E,
        0x8B,0xA2,0x48,0xB8);


