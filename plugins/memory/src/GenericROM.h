#ifndef GENERICROM_H
#define GENERICROM_H

#include "MemoryDevice.h"

#include <cstddef>
#include <string_view>
#include <vector>

namespace emulashione::memory {
/**
 * @brief Generic read only memory with a fixed access time.
 */
class GenericROM: public MemoryDevice {
    // lifecycle
    public:
        GenericROM(const std::string_view &name, struct emulashione_config_object *cfg);

        /// ROM access time is always fixed.
        double getAccessTime(const uint64_t, const bool) override {
            return this->accessTime;
        }

    protected:
        /// Provide the storage pointer for the MemoryDevice internals.
        std::span<std::byte> getStorage() override {
            return this->storage;
        }

    // required device fields
    public:
        static const struct emulashione_device_descriptor kDeviceDescriptor;

        constexpr static const std::string_view kDisplayName{"Generic ROM"};
        constexpr static const std::string_view kShortName{"rom"};
        constexpr static const std::string_view kAuthors{"Tristan Seifert"};
        constexpr static const std::string_view kDescription{
            "A generic ROM block"
        };
        constexpr static const std::string_view kInfoUrl{"https://gitlab.trist.network/mega-drive-stuff/emulashione/-/tree/main/plugins/memory"};
        static const std::string_view kVersion;

    private:
        void roundUpStorage();

    private:
        /// Data stroage
        std::vector<std::byte> storage;
        /// Constant access time, in nanoseconds
        double accessTime{0};

        /// Desired alignment of loaded ROM image
        size_t alignment{0};
        /// Byte to pad the loaded image with, if needed
        std::byte padding{0xFF};
};
}

#endif
