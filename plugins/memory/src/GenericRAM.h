#ifndef GENERICRAM_H
#define GENERICRAM_H

#include "MemoryDevice.h"

#include <cstddef>
#include <filesystem>
#include <string_view>
#include <vector>

namespace emulashione::memory {
/**
 * @brief Generic read/write memory with a fixed access time.
 *
 * Simulates a generic random access memory device with a fixed access time. It is assumed that
 * accesses to the device do not cause any side effects.
 */
class GenericRAM: public MemoryDevice {
    public:
        enum class FillMode {
            /// All 0 bits
            Zeros                       = 0,
            /// All 1 bits
            Ones                        = 1,
            /// Random bits
            Random                      = 2,
        };

    // lifecycle
    public:
        GenericRAM(const std::string_view &name, struct emulashione_config_object *cfg);
        virtual ~GenericRAM();

        /// Generic RAM has a fixed access time for all transactions.
        double getAccessTime(const uint64_t, const bool) override {
            return this->accessTime;
        }

        size_t copyOutInfo(const std::string_view &key, std::span<std::byte> &outBuf) override;
        bool copyInInfo(const std::string_view &key, const std::span<std::byte> &inBuf) override;

    protected:
        /// Provide the storage pointer for the MemoryDevice internals.
        std::span<std::byte> getStorage() override {
            return this->storage;
        }

    // required device fields
    public:
        static const struct emulashione_device_descriptor kDeviceDescriptor;

        constexpr static const std::string_view kDisplayName{"Generic RAM"};
        constexpr static const std::string_view kShortName{"ram"};
        constexpr static const std::string_view kAuthors{"Tristan Seifert"};
        constexpr static const std::string_view kDescription{
            "A generic RAM block"
        };
        constexpr static const std::string_view kInfoUrl{"https://gitlab.trist.network/mega-drive-stuff/emulashione/-/tree/main/plugins/memory"};
        static const std::string_view kVersion;

    private:
        void initContents();

    private:
        /// Data stroage
        std::vector<std::byte> storage;
        /// Default value of storage
        FillMode storageDefault{FillMode::Zeros};

        /// Constant access time, in nanoseconds
        double accessTime{0};

        /// file path to read/write RAM contents to
        std::filesystem::path persistencePath;
        /// when set, the RAM's contents are written out on quit
        bool shouldWriteContents{true};
};
}

#endif
