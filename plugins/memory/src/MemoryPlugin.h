#ifndef MEMORYPLUGIN_H
#define MEMORYPLUGIN_H

#include <Plugin.h>

#include <string_view>

/**
 * @brief Implementations of generic memory devices
 *
 * Devices exported by the `memory` plugin, which provides various generic read-only and read-write
 * memory types.
 * 
 * \section contents_sec Contents
 *
 * All memory devices exported by this plugin derive from MemoryDevice, an abstract base class that
 * implements some common behavior. More concretely, memory devices derive from either GenericROM
 * or GenericRAM, both of which can be used as-is if no special behavior is needed, while allowing
 * some customization by overriding particular methods.
 */
namespace emulashione::memory {
/**
 * @brief Plugin class for the memory plugin
 */
class MemoryPlugin: public emulashione::plugin::Plugin {
    public:
        /**
         * @name Lifecycle
         *
         * Initialization of the plugin and registration of devices exported by it
         *
         * @{
         */
        MemoryPlugin(struct emulashione_plugin_context *ctx) : Plugin(ctx) {}

        void init() override;
        /// @}

        /**
         * @name Required plugin fields
         *
         * These fields are mandatory to identify the plugin and to build the plugin descriptor
         * that is exported and read by the core library.
         *
         * @{
         */
        constexpr static const std::string_view kDisplayName{"Memory Devices"};
        constexpr static const std::string_view kShortName{"memory"};
        constexpr static const std::string_view kAuthors{"Tristan Seifert"};
        constexpr static const std::string_view kDescription{
            "Various types of memory devices."
        };
        constexpr static const std::string_view kInfoUrl{"https://gitlab.trist.network/mega-drive-stuff/emulashione/-/tree/main/plugins/memory"};
        constexpr static const std::string_view kLicense{"ISC"};
        static const std::string_view kVersion;
        /// @}
};
}

#endif
