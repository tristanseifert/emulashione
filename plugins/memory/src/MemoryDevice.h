#ifndef MEMORYDEVICE_H
#define MEMORYDEVICE_H

#include <cstddef>
#include <cstdint>
#include <filesystem>
#include <span>

#include <Device.h>

struct emulashione_config_object;

namespace emulashione::memory {
/**
 * A generic base class that implements basic read/write logic for memory devices.
 *
 * When subclassing this method, simply provide the data buffer.
 *
 * @brief Base class for memory devices
 */
class MemoryDevice: public emulashione::plugin::Device {
    public:
        /// Flags that define how a memory device is initialized
        enum class InitFlags {
            /// Mask for the read/write mode flag
            RWModeMask                  = (0x01 << 0),
            /// Memory is read only
            ReadOnly                    = (0 << 0),
            /// Memory is read/write
            ReadWrite                   = (1 << 0),
        };

    public:
        MemoryDevice(const std::string_view &name, struct emulashione_config_object *cfg,
                const InitFlags flags);
        virtual ~MemoryDevice() = default;

        void read(struct emulashione_device_instance_header *caller, const uint16_t addressSpace,
                const uint64_t address, const AccessWidth width, uint64_t &outData,
                const double time, double &accessTime) override;
        void write(struct emulashione_device_instance_header *caller, const uint16_t addressSpace,
                const uint64_t address, const AccessWidth width, const uint64_t inData,
                const double time, double &accessTime, const uint64_t mask) override;

    protected:
        bool loadContents(const std::filesystem::path &path);
        void writeContents(const std::filesystem::path &path);

        /**
         * Method that will return a span containing the storage of the device.
         *
         * @return Span encompassing the entire storage of the memory device
         */
        virtual std::span<std::byte> getStorage() = 0;

        /**
         * Returns the access time for a memory access to the given address. For most
         * implementations, it is sufficient to return a fixed value here.
         *
         * @remark Note that the returned value may be altered by any busses between the requestor
         * and this device, which may introduce their own delays.
         *
         * @param address Address that is being accessed
         * @param write Whether the access was a read (`false`) or write (`true`)
         *
         * @return Time required to complete this memory access, in nanoseconds.
         */
        virtual double getAccessTime(const uint64_t address, const bool write) = 0;

        /**
         * Is the device read only?
         *
         * @remark A read only device may ignore writes or it may signal an error condition on
         * writes. This is implementation defined.
         *
         * @return Whether the device is read only.
         */
        virtual bool isReadOnly() const {
            return this->readOnly;
        }

        /**
         * Are the contents of the device dirty?
         *
         * The dirty flag is set whenever a write is posted to the device's contents. It is cleared
         * at the request of the implementation; that is, it's up to the subclass to clear it.
         */
        virtual bool isDirty() const {
            return this->dirty;
        }

        /**
         * Clears the dirty flag.
         */
        virtual void clearDirty() {
            this->dirty = false;
        }

    private:
        void writeFull(std::span<std::byte> &, const size_t, const AccessWidth, const uint64_t);
        void writeMasked(std::span<std::byte> &, const size_t, const AccessWidth, const uint64_t,
                const uint64_t);

    private:
        /// Is the memory device read only? (Ignores writes)
        bool readOnly{false};
        /// when set, the device has been written to
        bool dirty{false};
};
}

#endif
