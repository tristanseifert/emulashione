#ifndef XR68C681_DEVICE_H
#define XR68C681_DEVICE_H

#include <memory>

#include <Device.h>

namespace emulashione::io {
class OutputPort;

/**
 * @brief XR68C681 DUART Device
 *
 * Emulates the
 * [MaxLinear XR68C681](https://www.maxlinear.com/product/interface/uarts/8-bit-vlio-uarts/xr68c681)
 * dual UART. In addition to providing two UARTs, it provides a timer, 8 general purpose outputs
 * and 6 general purpose inputs.
 *
 * Most of this is unimplemented; there's just enough to get some characters in and out, but the
 * timings are completely fucked and we do not support interrupts yet.
 *
 * It's assumed that only the low byte of any read/write transactions will be meaningful, and that
 * only address bits 4..1 are used in identifying which register to access.
 */
class XR68C681: public plugin::Device {
    private:
        using super = plugin::Device;

    public:
        /// IDs of the strobes exposed by the device. Their string names match the enum keys.
        enum Strobes: unsigned int {
            /// Interrupt output
            INTR                        = 1,
            /// Interrupt acknowledge input
            IACK                        = 2,
        };

    public:
    /// @{ @name Lifecycle

        XR68C681(const std::string_view &name, struct emulashione_config_object *cfg);

        void willStartEmulation() override;
        void willShutDown() override;

        void reset(const bool hard) override;
    /// @};

        void read(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, const AccessWidth width,
                uint64_t &outData, const double time, double &accessTime) override;
        void write(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, const AccessWidth width,
                const uint64_t inData, const double time, double &accessTime,
                const uint64_t mask) override;

    /**
     * @name Required device fields
     *
     * These fields are mandatory to identify the device type, and to build its device descriptor
     * that's registered during plugin initialization.
     *
     * @{
     */
    public:
        static const struct emulashione_device_descriptor kDeviceDescriptor;

        constexpr static const std::string_view kDisplayName{"MaxLinear XR68C681"};
        constexpr static const std::string_view kShortName{"xr68c681"};
        constexpr static const std::string_view kAuthors{"Tristan Seifert"};
        constexpr static const std::string_view kDescription{
            "A MC68681 compatible dual UART with timers and GPIO"
        };
        constexpr static const std::string_view kInfoUrl{"https://gitlab.trist.network/mega-drive-stuff/emulashione/-/tree/main/plugins/io"};
        static const std::string_view kVersion;
    /// @}

    private:
        /// Device connected to port A, if any
        std::shared_ptr<OutputPort> portA;
        /// When set, port A must be connected before starting emulation
        bool waitPortA{false};

        /// Device connected to port B, if any
        std::shared_ptr<OutputPort> portB;
        /// When set, port A must be connected before starting emulation
        bool waitPortB{false};

        /// Counter/timer set value
        uint16_t counterSet{0};

        /// Interrupt mask register: which IRQs are enabled
        uint8_t irqMask{0};
        /// Interrupt vector assigned to the device
        uint8_t irqVector{0x0F};
};
}

#endif
