#include "Device.h"

#include "version.h"

#include "outputs/OutputPort.h"
#include "outputs/ListenSocket.h"

#include <Config.h>
#include <Logger.h>

#include <interfaces/Device.h>
#include <stdexcept>

using namespace emulashione::plugin;
using namespace emulashione::io;

const std::string_view XR68C681::kVersion{gVERSION_SHORT};

EMULASHIONE_DEVICE_DESC(XR68C681, kSupportsMemory, 0,
        0x66,0xC4,0x05,0x06,0xB5,0x54,0x4F,0x2B,0x93,0x38,0xE3,0x62,0xE0,0x63,0xD1,0x80);




/**
 * Initializes a new device. Reads from the configuration to determine how to deal with the two
 * UARTs; they'll go either to a file or we'll set up a listening socket.
 */
XR68C681::XR68C681(const std::string_view &name, struct emulashione_config_object *_cfg) :
    Device(name, _cfg) {
    registerStrobe(Strobes::INTR, "INTR", 1);
    registerStrobe(Strobes::IACK, "IACK", 1);

    Config cfg(_cfg);

    // handle device on port A
    if(cfg.hasKey("porta") && cfg.getBool("porta.connected")) {
        const auto type = cfg.getString("porta.type");

        if(type == "socket") {
            this->waitPortA = cfg.getBool("porta.wait");
            const auto port = cfg.getUnsigned("porta.port");

            Logger::Info("Port A listen on localhost:{}", port);
            this->portA = std::make_shared<ListenSocket>(port);
        } else {
            throw std::runtime_error(fmt::format("unsupported port type {}", type));
        }
    }
    // handle device on port B
    if(cfg.hasKey("portb") && cfg.getBool("portb.connected")) {
        const auto type = cfg.getString("portb.type");

        if(type == "socket") {
            this->waitPortB = cfg.getBool("portb.wait");
            const auto port = cfg.getUnsigned("portb.port");

            Logger::Info("Port B listen on localhost:{}", port);
            this->portB = std::make_shared<ListenSocket>(port);
        } else {
            throw std::runtime_error(fmt::format("unsupported port type {}", type));
        }
    }
}

/**
 * Resets the device's register state.
 *
 * @param hard A soft reset is toggling the /RESET line, whereas a hard reset is a power cycle. For
 *        this device, they are functionally identical.
 */
void XR68C681::reset(const bool hard) {
    (void) hard;

    // counter/timer
    this->counterSet = 0;

    // interrupts
    this->irqMask = 0;
    this->irqVector = 0xF;
}

/**
 * When the device emulation is about to begin, wait for connections to any UARTs that require
 * one before proceeding.
 */
void XR68C681::willStartEmulation() {
    if(this->waitPortA) {
        this->portA->waitReady();
    }
    if(this->waitPortB) {
        this->portB->waitReady();
    }
}

/**
 * Disconnects all UART clients.
 */
void XR68C681::willShutDown() {
    Logger::Debug("Disconnecting UARTs");
}



/**
 * Reads from one of the registers on the device.
 */
void XR68C681::read(struct emulashione_device_instance_header *caller,
        const uint16_t addressSpace, const uint64_t address, const AccessWidth width,
        uint64_t &outData, const double time, double &accessTime) {
    (void) time, (void) width, (void) caller, (void) addressSpace;

    // get register number
    const auto reg = (address >> 1) & 0b1111;
    uint8_t temp{0};

    switch(reg) {
        /*
         * Channel A status register
         *
         * No errors are emulated so the high 4 bits are fixed as zero. Likewise, the TX FIFO is
         * of "unlimited" size. The RX FIFO bits are emulated based on the port's returned
         * values.
         */
        case 0x1: {
            // TXRDY is always set
            temp |= (1 << 2);
            // FFULL set when >= 3 chars pending; RXRDY if any are
            const auto rxPending = this->portA ? this->portA->pendingChars() : 0;
            if(rxPending) {
                temp |= (1 << 0);
            }
            if(rxPending >= 3) {
                temp |= (1 << 1);
            }
            break;
        }
        /*
         * Channel A RX holding register
         *
         * Read from the associated port, or return 0 if there is no port. This means the behavior
         * is undefined if there's no data pending; probably how real hardware behaves too.
         */
        case 0x03:
            temp = this->portA ? this->portA->getChar() : 0;
            break;

        /*
         * Channel B status register
         *
         * Same disclaimers as for Channel A apply.
         */
        case 0x9: {
            // TXRDY is always set
            temp |= (1 << 2);
            // FFULL set when >= 3 chars pending; RXRDY if any are
            const auto rxPending = this->portB ? this->portB->pendingChars() : 0;
            if(rxPending) {
                temp |= (1 << 0);
            }
            if(rxPending >= 3) {
                temp |= (1 << 1);
            }
            break;
        }
        /*
         * Channel B RX holding register
         *
         * Read from the associated port, or return 0 if there is no port. This means the behavior
         * is undefined if there's no data pending; probably how real hardware behaves too.
         */
        case 0x0B:
            temp = this->portB ? this->portB->getChar() : 0;
            break;

        /**
         * Interrupt vector register
         */
        case 0x0C:
            temp = this->irqVector;
            break;

        default:
            Logger::Warning("Read from unimplemented register: ${:x}", reg);
            break;
    }

    outData = temp;

    // XXX: figure out the proper access time
    accessTime = 100;
}

/**
 * Writes to one of the registers on the device.
 */
void XR68C681::write(struct emulashione_device_instance_header *caller,
        const uint16_t addressSpace, const uint64_t address, const AccessWidth width,
        const uint64_t inData, const double time, double &accessTime,
        const uint64_t mask) {
    (void) time, (void) width, (void) caller, (void) addressSpace;

    // get register number and data
    const auto reg = (address >> 1) & 0b1111;
    const uint8_t data = inData & (mask ? mask : 0xFF);

    switch(reg) {
        /*
         * Channel A TX holding register
         *
         * Data written here is immediately sent to the output port, or discarded if there is no
         * device connected.
         */
        case 0x03:
            if(this->portA) this->portA->putChar(data);
            break;

        /*
         * Channel B TX holding register
         */
        case 0x0B:
            if(this->portB) this->portB->putChar(data);
            break;

        /**
         * Interrupt mask register
         */
        case 0x05:
            this->irqMask = data;
            break;
        /**
         * Upper byte of the counter/timer set value
         */
        case 0x06:
            this->counterSet = (this->counterSet & 0xFF) | (static_cast<uint16_t>(data & 0xFF) << 8);
            break;
        /**
         * Lower byte of the counter/timer set value
         */
        case 0x07:
            this->counterSet = (this->counterSet & 0xFF00) | (data & 0xFF);
            break;
        /**
         * Interrupt vector register
         */
        case 0x0C:
            this->irqVector = data;
            break;

        default:
            Logger::Trace("Write to unimplemented register: ${:x} = ${:02x}", reg, data);
            break;
    }

    // XXX: figure out the proper access time
    accessTime = 100;
}

