#ifndef IOPLUGIN_H
#define IOPLUGIN_H

#include <Plugin.h>

#include <string_view>

/**
 * @brief Implementations of system IO devices
 *
 * Devices exported by the `io` plugin, which provides various generic system IO peripherals, many
 * from lines of common IO controllers for a particular processor architecture.
 * 
 * \section devices_sec Implemented Devices
 *
 * Currently, all peripheral devices are Motorola 68000 family style devices:
 *
 * - MaxLinear XR68C681: Dual UART with timer and GPIO (MC68681 compatible)
 */
namespace emulashione::io {
/**
 * @brief Plugin class for the system peripherals plugin
 */
class IoPlugin: public emulashione::plugin::Plugin {
    public:
        /**
         * @name Lifecycle
         *
         * Initialization of the plugin and registration of devices exported by it
         *
         * @{
         */
        IoPlugin(struct emulashione_plugin_context *ctx) : Plugin(ctx) {}

        void init() override;
        /// @}

        /**
         * @name Required plugin fields
         *
         * These fields are mandatory to identify the plugin and to build the plugin descriptor
         * that is exported and read by the core library.
         *
         * @{
         */
        constexpr static const std::string_view kDisplayName{"System Peripherals"};
        constexpr static const std::string_view kShortName{"io"};
        constexpr static const std::string_view kAuthors{"Tristan Seifert"};
        constexpr static const std::string_view kDescription{
            "Various types of memory devices."
        };
        constexpr static const std::string_view kInfoUrl{"https://gitlab.trist.network/mega-drive-stuff/emulashione/-/tree/main/plugins/io"};
        constexpr static const std::string_view kLicense{"ISC"};
        static const std::string_view kVersion;
        /// @}
};
}

#endif
