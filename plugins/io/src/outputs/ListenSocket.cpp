#include "ListenSocket.h"

#include <Logger.h>

#include <cerrno>
#include <cstring>
#include <system_error>

#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <magic_enum.hpp>

using namespace emulashione::plugin;
using namespace emulashione::io;

/**
 * Initializes the listening socket.
 */
ListenSocket::ListenSocket(const uint16_t port) : port(port) {
    int err;

    // set up listen pipe
    err = pipe(this->workerPipe);
    if(err) {
        throw std::system_error(errno, std::generic_category(), "failed to create worker pipe");
    }

    // create listening socket and bind it to the given port
    this->listenFd = socket(PF_INET, SOCK_STREAM, 0);
    if(this->listenFd == -1) {
        throw std::system_error(errno, std::generic_category(), "failed to create listening socket");
    }

    int yes = 1;
    if(setsockopt(this->listenFd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0) {
        throw std::system_error(errno, std::generic_category(), "failed to enable SO_REUSEADDR");
    }

    struct sockaddr_in saddr{};
    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    saddr.sin_port = htons(port);

    err = bind(this->listenFd, reinterpret_cast<struct sockaddr *>(&saddr), sizeof(saddr));
    if(err == -1) {
        throw std::system_error(errno, std::generic_category(), "failed to bind listening socket");
    }

    err = listen(this->listenFd, 5);
    if(err == -1) {
        throw std::system_error(errno, std::generic_category(), "failed to listen on socket");
    }

    // create listener thread
    this->listener = std::make_unique<std::thread>(&ListenSocket::listenerMain, this);
}

/**
 * Ensures the worker thread is shut down and closes the listener sockets.
 */
ListenSocket::~ListenSocket() {
    // notify worker to stop
    this->listenerRun = false;

    WorkerCommand cmd{WorkerCommand::Type::NoOp};
    this->listenerSendCmd(cmd);

    close(this->workerPipe[1]);
    close(this->listenFd);

    // wait for worker to join then clean up sockets/pipes
    this->listener->join();

    close(this->workerPipe[1]);
}

/**
 * Waits for the socket to be ready, that is, for a client to connect.
 *
 * This works by waiting on a condition variable and checking whether the value of the
 * client fd changed from what it was at the start of the loop.
 */
void ListenSocket::waitReady() {
    int currentFd = this->clientFd;
    if(currentFd) return;

    Logger::Info("Waiting for connection on localhost:{}", this->port);

    std::unique_lock lg(this->clientFdLock);
    this->clientFdCv.wait(lg, [&]{
        return (this->clientFd && (this->clientFd != currentFd)) || !this->listenerRun;
    });
}



/**
 * Entry point for the listener function.
 *
 * This will accept a client, then handle it until it disconnects.
 */
void ListenSocket::listenerMain() {
    int err;

    // main loop
    while(this->listenerRun) {
        struct sockaddr_in clientAddr;
        socklen_t clientAddrLen{sizeof(clientAddr)};

        // wait for accept of a client
        // TODO: can we multiplex this with worker message pipe? avoids the "connection aborted"
        err = accept(this->listenFd, reinterpret_cast<struct sockaddr *>(&clientAddr),
                &clientAddrLen);
        if(err == -1) {
            Logger::Critical("Failed to accept client connection: {}", strerror(errno));
            continue;
        }

        // handle that client
        this->listenerHandleClient(err);
    }

    // clean up
    if(this->clientFd) {
        close(this->clientFd);
    }
}

/**
 * Handles a listener client. This waits to receive data on either the command pipe or the socket,
 * and puts it in the respective places.
 */
void ListenSocket::listenerHandleClient(int client) {
    int err;

    // notify anyone waiting
    {
        std::unique_lock lg(this->clientFdLock);
        this->clientFd = client;
    }
    this->clientFdCv.notify_all();

    // handle loop
    while(this->listenerRun) {
        // wait for client message or pipe message
        int maxfd = std::max(client, this->workerPipe[1]) + 1;
        fd_set rd;

        FD_SET(this->workerPipe[0], &rd);
        FD_SET(client, &rd);

        err = select(maxfd, &rd, nullptr, nullptr, nullptr);
        if(err == -1) {
            return Logger::Critical("select() failed: {}", strerror(errno));
        }

        // is there a pipe message?
        if(FD_ISSET(this->workerPipe[0], &rd)) {
            this->listenerProcessMessage();
        }
        // is there data from the client?
        if(FD_ISSET(client, &rd)) {
            // TODO: does this work?
            char ch;
            err = read(client, &ch, sizeof(ch));

            if(err == -1) {
                return Logger::Critical("Failed to read from client: {}", strerror(errno));
            }

            std::lock_guard lg(this->rxFifoLock);
            this->rxFifo.push(ch);
        }
    }

    // clean up client
    close(client);
    this->clientFd = 0;
}

/**
 * Reads a message from the worker pipe.
 */
void ListenSocket::listenerProcessMessage() {
    int err;
    WorkerCommand cmd;

    // read the command
    err = read(this->workerPipe[0], &cmd, sizeof(cmd));
    if(err == -1) {
        Logger::Critical("Failed to read listener command: {}", strerror(errno));
        return;
    } else if(err != sizeof(cmd)) {
        Logger::Warning("Failed to receive full listener command: {}", err);
        return;
    }

    // process it
    if(kLogCommands) {
        Logger::Trace("Listener command: {}", magic_enum::enum_name(cmd.type));
    }

    switch(cmd.type) {
        case WorkerCommand::Type::SendChar:
            err = write(this->clientFd, &cmd.ch, sizeof(cmd.ch));
            if(err == -1) {
                return Logger::Warning("Failed to send to client: {}", strerror(errno));
            }
            break;

        case WorkerCommand::Type::NoOp:
            break;
    }
}

/**
 * Writes a command to the listener thread's notification pipe.
 */
void ListenSocket::listenerSendCmd(const WorkerCommand &cmd) {
    int err = write(this->workerPipe[1], &cmd, sizeof(cmd));

    if(err == -1) {
        Logger::Critical("Failed to send listener command: {}", strerror(errno));
    } else if(err != sizeof(cmd)) {
        Logger::Warning("Failed to send full listener command: {}", err);
    }
}

