#ifndef OUTPUTS_OUTPUTPORT_H
#define OUTPUTS_OUTPUTPORT_H

#include <cstddef>

namespace emulashione::io {
/**
 * @brief Abstract device to be connected to an output port, such as an UART
 *
 * This defines various methods to be implemented by concrete implementations so that output can
 * be sent somewhere, and input provided from elsewhere.
 */
class OutputPort {
    public:
        virtual ~OutputPort() = default;

        /**
         * Waits for the device to be ready to accept data. This call should block until later
         * calls to `putchar` can function correctly.
         */
        virtual void waitReady() = 0;

        /**
         * Writes a character to the output port.
         *
         * @param ch Character to write
         */
        virtual void putChar(const char ch) = 0;

        /**
         * Gets the number of characters of input data pending to be read from this port.
         *
         * @return Number of pending characters
         */
        virtual size_t pendingChars() = 0;

        /**
         * Get the topmost character on the receive FIFO, if any. If there are no characters
         * available (pendingChars is zero) the value is undefined.
         *
         * @return A pending character from receive FIFO
         */
        virtual char getChar() = 0;
};
}

#endif
