#ifndef OUTPUTS_LISTENSOCKET_H
#define OUTPUTS_LISTENSOCKET_H

#include "OutputPort.h"

#include <atomic>
#include <condition_variable>
#include <cstdint>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>

namespace emulashione::io {
/**
 * @brief Output port that accepts a client over TCP
 *
 * Listens on a local TCP socket with a specified port, accepting an user client. This client will
 * receive all characters written to the socket, and likewise all characters sent by the client are
 * written into the receive FIFO.
 */
class ListenSocket: public OutputPort {
    public:
        ListenSocket(const uint16_t port);
        ~ListenSocket();

        void waitReady() override;

        /**
         * Enqueues a character for transmission.
         */
        void putChar(const char ch) override {
            WorkerCommand cmd{
                WorkerCommand::Type::SendChar, ch
            };
            this->listenerSendCmd(cmd);
        }

        /**
         * Get the number of characters pending in the receive queue.
         */
        size_t pendingChars() override {
            std::lock_guard lg(this->rxFifoLock);
            return this->rxFifo.size();
        }

        /**
         * Read a char out of the receive queue.
         */
        char getChar() override {
            char ch;
            {
                std::lock_guard lg(this->rxFifoLock);
                if(this->rxFifo.empty()) {
                    return 0xFF;
                }

                ch = this->rxFifo.front();
                this->rxFifo.pop();
            }
            return ch;
        }

    private:
        struct WorkerCommand {
            enum class Type: uint8_t {
                NoOp, SendChar,
            };

            /// Command type
            Type type;

            /// Payload (character)
            char ch{0};
        };

    private:
        void listenerMain();
        void listenerHandleClient(int);
        void listenerProcessMessage();

        void listenerSendCmd(const WorkerCommand &);

    private:
        /// Listening socket
        int listenFd{0};

        /// Lock for the client socket
        std::mutex clientFdLock;
        /// Condition variable for indicating the client fd changed
        std::condition_variable clientFdCv;
        /// Client socket
        int clientFd{0};

        /// Lock for the receive FIFO
        std::mutex rxFifoLock;
        /// receive FIFO
        std::queue<char> rxFifo;

        /// Flag to indicate listener shall run
        std::atomic_bool listenerRun{true};
        /// Worker thread (for listening for a connection)
        std::unique_ptr<std::thread> listener;

        /// pipes to communicate with worker with
        /// 0 = read, 1 = write
        int workerPipe[2];

        /// Port we're listening on
        uint16_t port;

    private:
        /// Log commands received by the listener thread
        constexpr static const bool kLogCommands{false};
};
}

#endif
