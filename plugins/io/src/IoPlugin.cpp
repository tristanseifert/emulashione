#include "IoPlugin.h"
#include "version.h"

#include "DebugTrap.h"
#include "xr68c681/Device.h"

#include <Logger.h>
#include <interfaces/Info.h>

using namespace emulashione::plugin;
using namespace emulashione::io;

const std::string_view IoPlugin::kVersion{gVERSION_SHORT};
/**
 * Register all supported devices.
 */
void IoPlugin::init() {
    Plugin::RegisterDevice<DebugTrap>();
    Plugin::RegisterDevice<XR68C681>();
}

/// Define the plugin info struct
EMULASHIONE_PLUGIN_DESC(IoPlugin,0xB0,0x87,0xBD,0xF3,0x35,0x47,0x47,0xB3,0x92,0xB0,0x18,0xA0,
        0x6A,0xB0,0x10,0xB3);

