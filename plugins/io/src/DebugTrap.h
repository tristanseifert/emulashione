#ifndef DEBUGTRAP_H
#define DEBUGTRAP_H

#include <cstdint>

#include <Device.h>
#include <stdexcept>

namespace emulashione::io {
/**
 * @brief Device that aborts execution when accessed
 *
 * This is mostly intended for use in automated testing and other such scenarios. Reads are ignored
 * and will cause a bus error, while writes will abort execution with the byte value ORed with a
 * constant as the abort code.
 */
class DebugTrap: public plugin::Device {
    private:
        using super = plugin::Device;

    public:
        DebugTrap(const std::string_view &name, struct emulashione_config_object *cfg);

        void write(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, const AccessWidth width,
                const uint64_t inData, const double time, double &accessTime,
                const uint64_t mask) override;

    /**
     * @name Required device fields
     *
     * These fields are mandatory to identify the device type, and to build its device descriptor
     * that's registered during plugin initialization.
     *
     * @{
     */
    public:
        static const struct emulashione_device_descriptor kDeviceDescriptor;

        constexpr static const std::string_view kDisplayName{"Emulation Abort Trap"};
        constexpr static const std::string_view kShortName{"debugtrap"};
        constexpr static const std::string_view kAuthors{"Tristan Seifert"};
        constexpr static const std::string_view kDescription{
            "Virtual device that aborts the current emulation context when it is accessed"
        };
        constexpr static const std::string_view kInfoUrl{"https://gitlab.trist.network/mega-drive-stuff/emulashione/-/tree/main/plugins/io"};
        static const std::string_view kVersion;
    /// @};


    private:
        uint32_t baseValue{0x80000000};
};
}

#endif
