#include "DebugTrap.h"
#include "version.h"

#include <Config.h>
#include <Logger.h>

#include <interfaces/Device.h>

using namespace emulashione::plugin;
using namespace emulashione::io;

const std::string_view DebugTrap::kVersion{gVERSION_SHORT};

EMULASHIONE_DEVICE_DESC(DebugTrap, kSupportsMemory, 0, 0x86,0x55,0xFD,0xFC,0xAD,0xE5,0x47,0x02,0x8A,
        0xEC,0x2A,0xB8,0x76,0x3C,0x60,0x55
);

/**
 * Initialize a new debug trap device. Only one parameter, `base` can be speciied, which is an
 * integer value to use as the base to OR against any byte written to it.
 */
DebugTrap::DebugTrap(const std::string_view &name, struct emulashione_config_object *_cfg) :
    Device(name, _cfg) {
    if(!_cfg) return;

    Config cfg(_cfg);

    if(cfg.hasKey("base")) {
        this->baseValue = cfg.getUnsigned("base");
    }
}

/**
 * Handles an abort of the emulation.
 */
void DebugTrap::write(struct emulashione_device_instance_header *caller,
        const uint16_t addressSpace, const uint64_t address, const AccessWidth width,
        const uint64_t inData, const double time, double &accessTime,
        const uint64_t mask) {
    (void) time, (void) width, (void) caller, (void) addressSpace, (void) address;

    // abort the access
    uint32_t why{this->baseValue};
    why |= (inData & (mask ? mask : 0xFF)) & 0xFF;

    Logger::Info("DebugTrap: Aborting with status ${:08x}", why);
    GetDeviceInterface()->abort(this->getInstancePtr(), static_cast<int>(why));

    // this access is "free"
    accessTime = 0;
}
