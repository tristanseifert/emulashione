# Plugins
This directory contains some plugins that are developed in step with the emulator itself, as well as some support code that can be used in plugins to take care of a lot of boilerplate work.

At this time, plugins exist to provide devices required to emulate the Sega Genesis, 68komputer, as well as a few generic and test plugins:

- `io`: Various system peripheral devices, both real and virtual
- `m68k`: Motorola 68000 CPU
- `memory`: Generic RAM and ROM devices

A plugin support library (which basically amounts to a C++ shim to translate between the C interfaces provided by the library and exported by plugins) is available in `support`, exported as the `Emulashione::PluginSupport` target.
