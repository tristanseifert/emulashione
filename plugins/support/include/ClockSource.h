#ifndef EMULASHIONE_PLUGINSUPPORT_CLOCKSOURCE_H
#define EMULASHIONE_PLUGINSUPPORT_CLOCKSOURCE_H

#include <cstddef>
#include <string>

struct emulashione_clock_source_interface;
struct emulashione_clock_source_instance;

namespace emulashione::plugin {
/**
 * @brief Wrapper around a clock source provided by the core library
 */
class ClockSource {
    friend class WrapperCache;

    public:
        ClockSource(struct emulashione_clock_source_instance *);
        virtual ~ClockSource() = default;

        /// Get the name of the bus
        virtual std::string getName() const;
        /// Frequency of the clock source, in Hz
        virtual double getFrequency() const;
        /// Period of the clock source, in nanoseconds
        virtual double getPeriod() const;

        /// Get the underlying raw clock source handler
        constexpr auto getInstancePtr() {
            return this->instance;
        }

    private:
        static void ResolveInterface();

    private:
        /// Instance handle for the clock source
        struct emulashione_clock_source_instance *instance{nullptr};

        /// Cached interface pointer for the clock source interface
        static const struct emulashione_clock_source_interface *gInterface;
};
}

#endif
