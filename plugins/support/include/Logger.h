#ifndef EMULASHIONE_PLUGINSUPPORT_LOGGER_H
#define EMULASHIONE_PLUGINSUPPORT_LOGGER_H

struct emulashione_plugin_context;
struct emulashione_logger_interface;
struct emulashione_logger_handle;

#include <string_view>

#include <fmt/core.h>

namespace emulashione::plugin {
/**
 * @brief Per plugin logger instance
 *
 * @remark You should not try to allocate an instance; invoke the static log methods instead. The
 * instance is allocated by the core library during plugin load, and when using the Plugin base
 * class, the logger will be automatically initialized with it during load before any user provided
 * code is executed.
 */
class Logger {
    friend class Plugin;

    public:
        enum class Level {
            Trace, Debug, Info, Warning, Error, Critical
        };

    public:
        /// Write a log message with the given severity.
        void log(const Level l, const std::string_view &msg);

        /// Get the current log level
        Level getLogLevel() const;
        /// Set the current minimum log level
        void setLogLevel(const Level l);

        /// Get the shared instance
        auto The() {
            return gShared;
        }

        /// Trace level logging
        template<typename... Args>
        static inline void Trace(fmt::format_string<Args...> fmt, Args &&...args) {
            const auto msg = fmt::format(fmt, std::forward<Args>(args)...);
            gShared->log(Level::Trace, msg);
        }
        /// Debug level logging
        template<typename... Args>
        static inline void Debug(fmt::format_string<Args...> fmt, Args &&...args) {
            const auto msg = fmt::format(fmt, std::forward<Args>(args)...);
            gShared->log(Level::Debug, msg);
        }
        /// Info level logging
        template<typename... Args>
        static inline void Info(fmt::format_string<Args...> fmt, Args &&...args) {
            const auto msg = fmt::format(fmt, std::forward<Args>(args)...);
            gShared->log(Level::Info, msg);
        }
        /// Warning level logging
        template<typename... Args>
        static inline void Warning(fmt::format_string<Args...> fmt, Args &&...args) {
            const auto msg = fmt::format(fmt, std::forward<Args>(args)...);
            gShared->log(Level::Warning, msg);
        }
        /// Error level logging
        template<typename... Args>
        static inline void Error(fmt::format_string<Args...> fmt, Args &&...args) {
            const auto msg = fmt::format(fmt, std::forward<Args>(args)...);
            gShared->log(Level::Error, msg);
        }
        /// Critical level logging
        template<typename... Args>
        static inline void Critical(fmt::format_string<Args...> fmt, Args &&...args) {
            const auto msg = fmt::format(fmt, std::forward<Args>(args)...);
            gShared->log(Level::Critical, msg);
        }

    private:
        Logger(struct emulashione_plugin_context *ctx);

        static Level ConvertLogLevel(const unsigned int);
        static unsigned int ConvertLogLevel(const Level);

    private:
        /// Function pointers for logger
        const struct emulashione_logger_interface *interface{nullptr};
        /// Logger instance (created by library on load)
        struct emulashione_logger_handle *handle{nullptr};

        static Logger *gShared;
};
}

/**
 * Define formatters for a few commonly used types.
 */
#include <uuid.h>

template <> struct fmt::formatter<uuids::uuid> {
    template <typename FormatContext>
    auto format(const uuids::uuid &u, FormatContext &ctx) -> decltype(ctx.out()) {
        return format_to(ctx.out(), "{}", uuids::to_string(u));
    }
};

#endif
