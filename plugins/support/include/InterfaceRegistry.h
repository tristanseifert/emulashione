#ifndef EMULASHIONE_PLUGINSUPPORT_INTERFACEREGISTRY_H
#define EMULASHIONE_PLUGINSUPPORT_INTERFACEREGISTRY_H

#include <uuid.h>

struct emulashione_plugin_context;
struct emulashione_registry_interface;
struct emulashione_interface_header;

namespace emulashione::plugin {
/**
 * @brief Look up interfaces exported by the core library (and other plugins) by UUID
 */
class InterfaceRegistry {
    friend class Plugin;

    public:
        /// Return the global interface registry object; available on plugin load
        static auto The() {
            return gShared;
        }

        /// Get the interface with the given uuid.
        const struct emulashione_interface_header *get(const uuids::uuid &id);

    private:
        InterfaceRegistry(struct emulashione_plugin_context *ctx);

    private:
        static InterfaceRegistry *gShared;

        /// Registry interface; allows looking up of interfaces
        const struct emulashione_registry_interface *registry{nullptr};
};
};

#endif
