#ifndef EMULASHIONE_PLUGINSUPPORT_CONFIG_H
#define EMULASHIONE_PLUGINSUPPORT_CONFIG_H

#include <cstdint>
#include <string>
#include <string_view>

struct emulashione_config_interface;
struct emulashione_config_object;

namespace emulashione::plugin {
/**
 * @brief Represents a config object provided via the core library's config interface
 */
class Config {
    friend class Plugin;

    public:
        Config(struct emulashione_config_object *cfg);

        bool hasKey(const std::string_view &key) const;

        std::string getString(const std::string_view &key) const;
        int64_t getSigned(const std::string_view &key) const;
        uint64_t getUnsigned(const std::string_view &key) const;
        double getDouble(const std::string_view &key) const;
        bool getBool(const std::string_view &key) const;

    private:
        void ResolveInterface();

    private:
        /// Shared config interface
        static const struct emulashione_config_interface *gInterface;

        /// Config object instance
        struct emulashione_config_object *config{nullptr};
};
}

#endif
