#ifndef EMULASHIONE_PLUGINSUPPORT_TRACEMANAGER_H
#define EMULASHIONE_PLUGINSUPPORT_TRACEMANAGER_H

#include <cstddef>
#include <cstdint>
#include <functional>
#include <mutex>
#include <span>
#include <string_view>

struct emulashione_trace_manager_instance;
struct emulashione_trace_manager_interface;
struct emulashione_trace_manager_mem_output;

namespace emulashione::plugin {
class Device;

/**
 * @brief Interface to a trace manager in the core library
 *
 * This is a wrapper around the interface exported by trace managers in the core library, allowing
 * plugin devices to easily create a trace manager for themselves.
 */
class TraceManager {
    public:
        TraceManager(const uint16_t ns, const std::string_view &name,
                const size_t maxPayloadSize = 0);
        TraceManager(const uint16_t ns, const Device &device, const size_t maxPayloadSize = 0);
        ~TraceManager();

        void addMemoryOutput(const size_t maxChunks = 0, const size_t chunkSize = 0);

        void putMessage(const double time, const uint64_t id,
                const std::span<std::byte> &payload = {});

        /// Get the memory output, if there is one
        constexpr inline auto getMemOutput() {
            return this->memOutput;
        }

        size_t iterateRecords(const std::function<bool(double time, uint16_t ns, uint64_t id,
                std::span<std::byte> &payload)> &callback);

    private:
        static void ResolveInterface();

    private:
        /// Flag to indicate whether the trace manager interface has been found
        static std::once_flag gInterfaceAllocated;
        /// Trace manager interface
        static const struct emulashione_trace_manager_interface *gInterface;

        /// Allocated trace manager handle
        struct emulashione_trace_manager_instance *instance{nullptr};
        /// Handle to the memory output (if any)
        struct emulashione_trace_manager_mem_output *memOutput{nullptr};
};
}

#endif
