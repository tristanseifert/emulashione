#ifndef EMULASHIONE_PLUGINSUPPORT_BUS_H
#define EMULASHIONE_PLUGINSUPPORT_BUS_H

#include <cstddef>
#include <cstdint>
#include <string>

#include <fmt/core.h>

struct emulashione_bus_interface;
struct emulashione_bus_instance;
struct emulashione_device_instance_header;

namespace emulashione::plugin {
/**
 * @brief Wrapper around a bus provided by the core library
 */
class Bus {
    friend class WrapperCache;

    public:
        /// Defines the width of a memory access
        enum class AccessWidth: uint8_t {
            Byte, Word, LongWord, QuadWord
        };

        /// Indicates a bus transaction failed
        class BusError: public std::exception {
            friend class Bus;

            private:
                /// Create an error with no detail string.
                BusError() = default;

                /// Create an error with the given format string.
                template<typename... Args>
                BusError(fmt::format_string<Args...> fmt, Args &&...args) {
                    this->message = fmt::format(fmt, std::forward<Args>(args)...);
                }

            public:
                const char *what() const noexcept override {
                    return this->message.c_str();
                }

            private:
                std::string message;
        };

    public:
        Bus(struct emulashione_bus_instance *);
        virtual ~Bus() = default;

        virtual std::string getName() const;
        virtual size_t getDataWidth() const;
        virtual size_t getAddressWidth() const;

        /// @{ @name Reads
        virtual void read(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, uint8_t &outData,
                double &accessTime);
        virtual void read(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, uint16_t &outData,
                double &accessTime);
        virtual void read(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, uint32_t &outData,
                double &accessTime);
        virtual void read(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, uint64_t &outData,
                double &accessTime);
        /// @}

        /// @{ @name Writes
        virtual void write(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, const uint8_t inData,
                double &accessTime, const uint8_t mask = 0);
        virtual void write(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, const uint16_t inData,
                double &accessTime, const uint16_t mask = 0);
        virtual void write(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, const uint32_t inData,
                double &accessTime, const uint32_t mask = 0);
        virtual void write(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, const uint64_t inData,
                double &accessTime, const uint64_t mask = 0);
        /// @}

    private:
        static void ResolveInterface();

    private:
        /// Instance handle for the bus
        struct emulashione_bus_instance *instance{nullptr};

        /// Cached interface pointer for the bus interface
        static const struct emulashione_bus_interface *gInterface;
};
}

#endif
