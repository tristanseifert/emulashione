#ifndef EMULASHIONE_PLUGINSUPPORT_COOPERATIVEDEVICE_H
#define EMULASHIONE_PLUGINSUPPORT_COOPERATIVEDEVICE_H

#include <Device.h>

#include <cstddef>
#include <memory>
#include <string>
#include <string_view>

#include <interfaces/Scheduler.h>

struct emulashione_device_scheduler_context;
struct emulashione_scheduler_interface;
struct emulashione_scheduler_instance;

namespace emulashione::plugin {
/**
 * This is a device type class, which may be implemented by device implementations in plugins, that
 * is specifically designed for those types of devices that will be emulated using the cooperative
 * scheduling mode of emulation. Much of the boilerplate to deal with this is implemented in this
 * class.
 *
 * Concrete device implementations will override the main() method, which should contain the device
 * run loop. Any time the device has advanced a certain number of ticks (e.g. an instruction has
 * finished execution) it calls into the scheduler (methods prefixed with `sched`) to notify it of
 * this. The scheduler may choose to switch to another device to emulate, or it may continue to
 * emulate this device. Either way, the device can be sure that once these scheduler methods return
 * back to it, the clock cycles have been accounted for and synchronization with other devices, as
 * required, hsa been performed.
 *
 * @remark Since the scheduler operates in terms of clock cycles, you need to specify a clock
 *         source this is relative to. If only one clock slot is specified during initialization,
 *         we will automatically select that clock. Otherwise, you should manually set it when a
 *         clock source connection is made.
 *
 * @brief Abstract base class for a device emulated with cooperative scheduling
 */
class CooperativeDevice: public Device {
    friend class Device;

    private:
        /**
         * An exception type that indicates that the emulation is to be aborted. It carries as its payload
         * a status code.
         */
        struct EmulationAborted: public std::exception {
            EmulationAborted(const int status) : status(status) {}

            /// Get the status code associated with this abort.
            constexpr auto getStatus() const noexcept {
                return status;
            }

            private:
                int status{0};
        };

    public:
        CooperativeDevice(const std::string_view &name, struct emulashione_config_object *cfg);

    // reimplemented device methods
    protected:
        void connectClockSource(const std::string &name,
                const std::shared_ptr<ClockSource> &clock) override;
        void disconnectClockSource(const std::string &name) override;

    // methods for use by subclasses
    protected:
        void setSchedulerClock(const std::shared_ptr<ClockSource> &clock);
        /// Gets the current scheduler clock used by the device, if any.
        constexpr auto &getSchedulerClock() {
            return this->schedClk;
        }

        /**
         * Notifies the scheduler that the device has emulated the specified number of clock ticks.
         *
         * The scheduler is free to switch to another device (to maintain synchronization) or
         * simply return control immediately.
         *
         * @param ticks Number of ticks of the device's scheduler clock that were executed
         * @param resumedTime Variable to receive the system time at which the device resumes
         */
        inline void schedDidAdvanceBy(const size_t ticks, double &resumedTime) {
            this->schedInterface->yieldTicks(this->scheduler, this->getInstancePtr(), ticks,
                    &resumedTime);
        }

        /**
         * Notifies the scheduler that the device has emulated the specified number of clock ticks.
         *
         * The scheduler is free to switch to another device (to maintain synchronization) or
         * simply return control immediately.
         *
         * @param ticks Number of ticks of the device's scheduler clock that were executed
         */
        inline void schedDidAdvanceBy(const size_t ticks) {
            this->schedInterface->yieldTicks(this->scheduler, this->getInstancePtr(), ticks, nullptr);
        }

        void schedAbort(const int status = -1);

    // methods to be implemented by subclasses
    protected:
        /**
         * Main function for this device's emulation thread.
         *
         * The device should enter a permanent loop where it processes instructions or whatever
         * else it needs to be doing. It can keep the scheduler updated on its progress via the
         * various `sched` methods on this class.
         *
         * @note Do not return from this method; this will cause the emulation thread for this
         *       device to exit unexpectedly, which can result in an abnormal program termination.
         */
        virtual void main() = 0;

    // overrides for device methods
    public:
        static const struct emulashione_device_cothread_callbacks cbCothread;

    // internal methods
    private:
        void schedPrepare(struct emulashione_device_scheduler_context *);

    private:
        /// Interface pointer for scheduler methods
        const struct emulashione_scheduler_interface *schedInterface{nullptr};
        /// Scheduler responsible for emulating this device
        struct emulashione_scheduler_instance *scheduler{nullptr};

        /// Clock source used for scheduler calls
        std::shared_ptr<ClockSource> schedClk;
};
}

#endif

