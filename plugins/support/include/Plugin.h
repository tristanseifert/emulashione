#ifndef EMULASHIONE_PLUGINSUPPORT_PLUGIN_H
#define EMULASHIONE_PLUGINSUPPORT_PLUGIN_H

struct emulashione_plugin_context;
struct emulashione_device_interface;
struct emulashione_device_descriptor;

/**
 * This namespace contains various classes to ease plugin development. This includes base classes
 * for plugin and devices, as well as wrappers for some objects exported by the core library.
 *
 * Additionally, a few helpers useful for emulating devices are provided.
 *
 * @remark This is built as a static library as the `Emulashione::PluginSupport` target. You should
 *         link against it in your plugin, and ensure it's built with the same toolchain and
 *         compile options as the rest of your plugin.
 *
 * @brief Emulashione plugin support library
 */
namespace emulashione::plugin {
/**
 * Base plugin class; you should subclass this and implement the required methods.
 *
 * Note that you _can_ have multiple plugin subclasses; these just have to be registered manually,
 * as the macro approach can only support a single plugin descriptor.
 *
 * @remark For the automatic registration to work, you must place exactly one instance of the
 * EMULASHIONE_PLUGIN_DESC() macro somewhere in your plugin. This will define the plugin
 * descriptor parsed by the core library.
 *
 * @brief Base class for plugins
 */
class Plugin {
    public:
        /// Return the shared plugin instance
        static auto The() {
            return gShared;
        }

        /// Static plugin instance, used by `EMULASHIONE_PLUGIN_DESC()`
        static Plugin *gShared;

        /// Create a new plugin.
        Plugin(struct emulashione_plugin_context *ctx);
        virtual ~Plugin();

        /**
         * The plugin is being initialized during initial load.
         *
         * Plugin implementations should override this method and register their exported device
         * types and allocate any shared resources.
         */
        virtual void init() = 0;
        /**
         * The plugin is being shut down.
         *
         * This can be overridden to perform any sort of last minute cleanup of shared resources
         * allocated by the plugin. Devices registered by the plugin will be automatically removed
         * from the registry so most plugins do not need to do anything here.
         */
        virtual void shutdown() {};

        /**
         * Registers a device (with its associated descriptor) with the core library.
         *
         * @tparam DevClass Class name of a concrete device subclass to register
         */
        template<class DevClass>
        static void RegisterDevice() {
            gShared->_registerDevice(DevClass::kDeviceDescriptor);
        }

        /**
         * Unregisters the device descriptor associated with the given device.
         *
         * @tparam DevClass Class name of a concrete device subclass to unregister
         */
        template<class DevClass>
        static void UnregisterDevice() {
            gShared->_unregisterDevice(DevClass::kDeviceDescriptor);
        }

    protected:
        void _registerDevice(const struct emulashione_device_descriptor &);
        void _unregisterDevice(const struct emulashione_device_descriptor &);

        const struct emulashione_device_interface *getDeviceInterface();

    protected:
        /// Plugin context (provided by library at init time)
        struct emulashione_plugin_context *ctx{nullptr};
        /// @noref Device interface (looked up as needed)
        const struct emulashione_device_interface *devInterface{nullptr};
};
};


// TODO: handle for non-gcc compatible compilers
#define EMULASHIONE_PLUGIN_USED __attribute__((used))
#define EMULASHIONE_PLUGIN_EXPORTED __attribute__((visibility("default")))

/**
 * Outputs the plugin information structure, with all of the fields populated properly to point to
 * the appropriate fields for the provided class name.
 *
 * This requires that the string keys are available as static `std::string_view` objects on the
 * class, and that the class inherits from Plugin.
 *
 * @param ClassName fully qualified name of the plugin subclass
 * @param ... Plugin UUID bytes
 */
#define EMULASHIONE_PLUGIN_DESC(ClassName, ...) struct emulashione_plugin_info EMULASHIONE_PLUGIN_USED EMULASHIONE_PLUGIN_EXPORTED _EmulashionePluginInfo={\
    .magic = kEmulashionePluginMagic,\
    .infoVersion = 1,\
    .minPluginApi = 1,\
    .uuid = {__VA_ARGS__},\
    .strings = {\
        .displayName = ClassName::kDisplayName.data(),\
        .shortName = ClassName::kShortName.data(),\
        .authors = ClassName::kAuthors.data(),\
        .description = ClassName::kDescription.data(),\
        .infoUrl = ClassName::kInfoUrl.data(),\
        .version = ClassName::kVersion.data(),\
        .license = ClassName::kLicense.data(),\
    },\
    .initCallback = [](struct emulashione_plugin_context *ctx) -> int {\
        try {\
            ClassName::gShared = new ClassName(ctx);\
            ClassName::gShared->init();\
        } catch(const std::exception &e) {\
            return -1;\
        }\
        return 0;\
    },\
    .shutdownCallback = []() -> int {\
        try {\
            ClassName::gShared->shutdown();\
            delete ClassName::gShared;\
        } catch(const std::exception &e) {\
            Logger::Error("Failed to shut down plugin: {}", e.what());\
            return -1;\
        }\
        ClassName::gShared = nullptr;\
        return 0;\
    },\
};\

#endif
