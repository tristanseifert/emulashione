#ifndef EMULASHIONE_PLUGINSUPPORT_DEVICE_H
#define EMULASHIONE_PLUGINSUPPORT_DEVICE_H

#include <cstring>
#include <memory>
#include <mutex>
#include <optional>
#include <stdexcept>
#include <span>
#include <string>
#include <string_view>
#include <unordered_map>
#include <unordered_set>

#include <uuid.h>

#include "Bus.h"

struct emulashione_device_descriptor;
struct emulashione_device_instance_header;
struct emulashione_config_object;
struct emulashione_device_interface;
struct emulashione_device_info_callbacks;
struct emulashione_device_emulation_callbacks;
struct emulashione_device_slice_callbacks;
struct emulashione_device_step_callbacks;
struct emulashione_device_cothread_callbacks;
struct emulashione_device_memory_callbacks;
struct emulashione_device_strobe_callbacks;
struct emulashione_device_connection_callbacks;


namespace emulashione::plugin {
class Bus;
class ClockSource;

/**
 * This class provides base implementations of many methods, as well as the infrastructure to
 * support simple registration.
 *
 * @remark The translation between the methods defined in this class and the appropriate instance,
 * and the function pointers and raw pointers provided to the core library are transparently
 * handled by this class. The EMULASHIONE_DEVICE_DESC() macro is required to define the appropriate
 * structures; see its documentation for more.
 *
 * @brief Abstract base class for a plugin provided device
 */
class Device {
    friend class CooperativeDevice;

    public:
        /// Provided to C API
        struct Wrapper {
            const struct emulashione_device_descriptor *descriptor{nullptr};
            std::shared_ptr<Device> p;
        };

    // Life cycle methods
    protected:
        /**
         * The plugin has just been loaded.
         */
        static void DidLoad() {}
        /**
         * This device is about to be removed from the device registry.
         */
        static void WillUnload() {}

    // @{ @name Information methods
    protected:
        /// Indicates an info key is unknown
        struct UnknownInfoKey final: public std::exception {
            UnknownInfoKey() {}
        };

        /// Indicates an info key is not writeable
        struct ReadOnlyInfoKey final: public std::exception {
            ReadOnlyInfoKey() {}
        };


        /**
         * Check whether the device supports the interface with the given uuid.
         *
         * @remark When implementing this method in a subclass, you should always fall back to this
         *         superclass implementation for unknown ids.
         *
         * @param interfaceId UUID of the interface to test
         *
         * @return Whether the interface is supported.
         */
        virtual bool supportsInterface(const uuids::uuid &interfaceId);

        /**
         * Copy out information specified by the given key.
         *
         * @remark When implementing this method in a subclass, you should always fall back to this
         *         superclass implementation for unknown keys.
         *
         * @param key Information key to query
         * @param outBuf Output buffer to contain the result
         *
         * @return Actual number of bytes copied out
         *
         * @throw UnknownInfoKey The specified key was not found
         */
        virtual size_t copyOutInfo(const std::string_view &key, std::span<std::byte> &outBuf);

        /**
         * Copy in information specified by the given key, overwriting it.
         *
         * @remark When implementing this method in a subclass, you should always fall back to this
         *         superclass implementation for unknown keys.
         *
         * @param key Information key to write
         * @param inBuf Input buffer with the new value; a size of 0 truncates the existing value.
         *
         * @return Whether the value was written
         *
         * @throw ReadOnlyInfoKey The specified key is read-only
         * @throw UnknownInfoKey The specified key was not found
         */
        virtual bool copyInInfo(const std::string_view &key, const std::span<std::byte> &inBuf);

    // @}

    // Emulation callbacks
    protected:
        /**
         * The system containing this device is about to be powered on and emulation will begin;
         * subclasses should allocate any worker threads or other resources here.
         */
        virtual void willStartEmulation();

        /**
         * The system containing this device is about to be stopped.
         */
        virtual void willStopEmulation() {}

        /**
         * The system containing this device is about to be paused; it may be resumed at a later
         * time. Background work should be stopped.
         */
        virtual void willPauseEmulation() {}

        /**
         * The system containing this device is about to transition from the paused state back to
         * the running state.
         */
        virtual void willResumeEmulation() {}

        /**
         * The system containing this device is being shut down, and the device will not receive
         * any further time slices.
         *
         * Previously allocated workers and background processing should be halted here. It should
         * signal these workers and any other threads that may be blocked on resources exposed by
         * this device.
         */
        virtual void willShutDown() {}

        /**
         * Resets the internal state of the device.
         *
         * @param hard Whether the device is undergoing a hard (`true`) or soft reset.
         */
        virtual void reset(const bool) {}

        /**
         * Indicates that all devices in the system have executed up to at least the specified time
         * point.
         *
         * @param Absolute system time point, in nanoseconds
         */
        virtual void commitTo(const double) {}

    // Time slice emulation callbacks
    protected:
        /**
         * A time slice is about to begin.
         *
         * @param time Current system clock at the start of the time slice
         * @param length Length of the time slice, in nanoseconds
         */
        virtual void timeSliceWillBegin(const double time, const double length) {
            (void) time, (void) length;
        }

        /**
         * Indicates that the time slice of the given duration should be executed.
         *
         * @param time Current system clock, in nanoseconds, at the start of the time slice
         * @param length Length of the time slice, in nanoseconds
         */
        virtual void executeTimeSlice(const double time, const double length) {
            (void) time, (void) length;
            throw std::runtime_error("Not implemented");
        }

        /**
         * A time slice has ended.
         *
         * @param time Current system clock at the end of the time slice
         */
        virtual void timeSliceDidEnd(const double time) {
            (void) time;
        }

        /**
         * Returns the progress into the time slice, in nanoseconds.
         *
         * Subclasses must override this method with an implementation that suits their emulation
         * strategy.
         */
        virtual double getTimeSliceProgress() {
            throw std::runtime_error("Not implemented");
        }

    // Time step emulation callbacks
    protected:
        /**
         * Emulates a single atomic step of the device.
         *
         * @param time Current system time point
         *
         * @return Time taken to execute this step, in nanoseconds.
         *
         * @throws In case of error, throw an exception.
         */
        virtual double executeStep(const double time) {
            (void) time;
            throw std::runtime_error("Not implemented");
        }

    // Memory access
    protected:
        using AccessWidth = Bus::AccessWidth;

        /**
         * Gets all address spaces supported by the device.
         */
        virtual size_t getSupportedAddressSpaces(std::span<uint16_t> &out);

        /**
         * Performs a read.
         *
         * @param caller Device performing the read
         * @param addressSpace Address space id the read is going to
         * @param address Address of the read
         * @param width Size of the read
         * @param outData Data that was read
         * @param time System timestamp the read is occurring at
         * @param accessTime Time, in nanoseconds, required for the read
         */
        virtual void read(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, const AccessWidth width,
                uint64_t &outData, const double time, double &accessTime) {
            (void) addressSpace, (void) address, (void) width, (void) outData, (void) time,
            (void) accessTime, (void) caller;
            throw std::runtime_error("Not implemented");
        }

        /**
         * Performs a write to the device.
         *
         * @param caller Device performing the write
         * @param addressSpace Address space id the write is going to
         * @param address Address of the write
         * @param width Size of the write
         * @param inData Data to be written
         * @param time System timestamp the write is occurring at
         * @param accessTime Time, in nanoseconds, required for the write
         * @param mask Indicates which bits of the write are effective, or 0 for all
         */
        virtual void write(struct emulashione_device_instance_header *caller,
                const uint16_t addressSpace, const uint64_t address, const AccessWidth width,
                const uint64_t inData, const double time, double &accessTime,
                const uint64_t mask = 0) {
            (void) addressSpace, (void) address, (void) width, (void) inData, (void) time,
            (void) accessTime, (void) caller, (void) mask;
            throw std::runtime_error("Not implemented");
        }

    // Strobes
    protected:
        /**
         * Returns the maximum strobe id in use by this device, or 0 if no strobes are used.
         */
        virtual unsigned int maxStrobeId();

        /**
         * Returns the string name for a strobe with the given id.
         */
        virtual std::optional<std::string> getStrobeName(const unsigned int id);

        /**
         * Returns the id of a strobe by its name.
         *
         * @note The name comparison is exact, including case sensitivity. It's suggested that all
         *       strobe names are limited to uppercase ASCII for this reason.
         */
        virtual std::optional<unsigned int> getStrobeId(const std::string &name);

        /**
         * Returns the width, in bits, of a strobe.
         */
        virtual std::optional<size_t> getStrobeWidth(const unsigned int id);

        /**
         * Sets the state of a strobe exposed by this device.
         *
         * @param caller Device that the request is coming from
         * @param id Strobe id
         * @param value New strobe value
         * @param time Time point at which the change takes effect
         */
        virtual void setStrobeState(const std::shared_ptr<Device> &device, const unsigned int id,
                const uint64_t value, const double time) {
            (void) device, (void) id, (void) value, (void) time;
            throw std::runtime_error("Not implemented");
        }

        /**
         * Indicates a device is no longer driving a strobe line.
         *
         * @param caller Device that the request is coming from
         * @param id Strobe id
         * @param time Time point at which the change takes effect
         */
        virtual void stopDrivingStrobe(const std::shared_ptr<Device> &device,
                const unsigned int id, const double time) {
            (void) device, (void) id, (void) time;
            throw std::runtime_error("Not implemented");
        }

        /**
         * Waits for the strobe to change to the specified value.
         *
         * @param caller Device that the request is coming from
         * @param id Strobe id
         * @param value Value we're waiting for on the strobe
         * @param mask An optional mask to apply against the strobe; 0 for no mask
         * @param time Time point at which the wait begins
         *
         * @return Whether the wait succeeded or not
         */
        virtual bool waitForStrobeChange(const std::shared_ptr<Device> &device,
                const unsigned int id, const uint64_t value, const double time,
                const uint64_t mask = 0) {
            (void) device, (void) id, (void) value, (void) time, (void) mask;
            throw std::runtime_error("Not implemented");
        }

    public:
        /// Returns the device's instance pointer (for C interop)
        inline auto getInstancePtr() {
            return reinterpret_cast<struct emulashione_device_instance_header *>(this->wrapper);
        }

        /// Returns the device descriptor (from wrapper)
        virtual const struct emulashione_device_descriptor *getDescriptor() {
            return this->wrapper->descriptor;
        }

        /// Return the device's name
        constexpr auto &getName() const {
            return this->name;
        }

    // Constructors and destructors
    protected:
        /**
         * Instantiate a new device.
         *
         * Subclasses should implement their own constructor and use that to register any strobes
         * and address spaces, as needed if the built in implementations for enumerating them.
         *
         * @param name Short name of this device, as specified in system definition
         * @param cfg Optional configuration of this device
         */
        Device(const std::string_view &name, struct emulashione_config_object *cfg = nullptr);
        /// Deallocates the device.
        virtual ~Device() = default;

        static const struct emulashione_device_interface *GetDeviceInterface();

        static const struct emulashione_device_info_callbacks cbInfo;
        static const struct emulashione_device_emulation_callbacks cbEmulation;
        static const struct emulashione_device_slice_callbacks cbSlice;
        static const struct emulashione_device_step_callbacks cbStep;
        static const struct emulashione_device_cothread_callbacks cbCothread;
        static const struct emulashione_device_memory_callbacks cbMemory;
        static const struct emulashione_device_strobe_callbacks cbStrobe;
        static const struct emulashione_device_connection_callbacks cbConnection;

        /// Short name of the device
        std::string name;

        /// Wrapper object
        Wrapper *wrapper{nullptr};
        /// Do we own the wrapper object, e.g. did we allocate it?
        bool ownsWrapper{true};

    // Connected busses, clock sources and devices
    protected:
        std::mutex connectedBussesLock;
        std::unordered_map<std::string, std::shared_ptr<Bus>> connectedBusses;

        virtual void connectBus(const std::string &name, const std::shared_ptr<Bus> &bus);
        virtual void disconnectBus(const std::string &name);

        std::mutex connectedClockSourcesLock;
        std::unordered_map<std::string, std::shared_ptr<ClockSource>> connectedClockSources;

        virtual void connectClockSource(const std::string &name,
                const std::shared_ptr<ClockSource> &clock);
        virtual void disconnectClockSource(const std::string &name);

        std::mutex connectedDevicesLock;
        std::unordered_map<std::string, std::shared_ptr<Device>> connectedDevices;

        virtual void connectDevice(const std::string &name, const std::shared_ptr<Device> &device);
        virtual void disconnectDevice(const std::string &name);

        /// Adds the given slot name to the allow list for bus slots
        void registerBusSlotName(const std::string &slot) {
            std::lock_guard<std::mutex> lg(this->connectedBussesLock);
            this->busSlotNames.emplace(slot);
        }
        /// Adds the given slot name to the allow list for clock source slots
        void registerClockSourceSlotName(const std::string &slot) {
            std::lock_guard<std::mutex> lg(this->connectedClockSourcesLock);
            this->clockSourceSlotNames.emplace(slot);
        }
        /// Adds the given slot name to the allow list for device slots
        void registerDeviceSlotName(const std::string &slot) {
            std::lock_guard<std::mutex> lg(this->connectedDevicesLock);
            this->deviceSlotNames.emplace(slot);
        }

    // allowed slot names for the connections
    private:
        std::unordered_set<std::string> busSlotNames;
        std::unordered_set<std::string> clockSourceSlotNames;
        std::unordered_set<std::string> deviceSlotNames;

    // Device helpers
    protected:
        static std::shared_ptr<Device> GetDeviceFor(struct emulashione_device_instance_header *);

    // Helpers to register strobes
    protected:
        virtual void registerStrobe(const unsigned int id, const std::string &name,
                const size_t width);

    // Helpers to register address spaces
    protected:
        virtual void registerAddressSpace(const uint16_t id, const std::string &name);

    private:
        static const struct emulashione_device_interface *gDeviceInterface;

    // Data and types for internal mapping of strobes
    private:
        struct StrobeInfo {
            std::string name;
            size_t width;
        };

        std::mutex strobesLock;
        std::unordered_map<unsigned int, StrobeInfo> strobes;

        void validateStrobesList();

    // Data types for internal mapping of address spaces
    private:
        struct AddressSpaceInfo {
            std::string name;
        };

        std::mutex addressSpacesLock;
        std::unordered_map<uint16_t, AddressSpaceInfo> addressSpaces;
};
};

/**
 * Generates the device descriptor for a particular device class, including the required stubs to
 * translate between the C style methods exported to the core interface.
 *
 * @remark Invoke this macro inside the compilation unit of a device subclass to generate the
 * device descriptor for the device as `kDeviceDescriptor` constant on the class.
 *
 * The last argument is 16 uuid bytes.
 *
 * @param ClassName Fully qualified class name of the device subclass
 * @param Methods Combination of emulashione_device_supported_methods flags for the device
 * @param ThreadingMode Combination of emulashione_device_threading_mode flags for the device
 * @param ... Device UUID bytes
 */
#define EMULASHIONE_DEVICE_DESC(ClassName, Methods, ThreadingMode, ...) \
const struct emulashione_device_descriptor ClassName::kDeviceDescriptor={\
    .magic = kEmulashioneDeviceMagic,\
    .infoVersion = 1,\
    .reserved = 0,\
    .uuid = {__VA_ARGS__},\
    .strings = {\
        .displayName = ClassName::kDisplayName.data(),\
        .shortName = ClassName::kShortName.data(),\
        .authors = ClassName::kAuthors.data(),\
        .description = ClassName::kDescription.data(),\
        .infoUrl = ClassName::kInfoUrl.data(),\
        .version = ClassName::kVersion.data(),\
    },\
    .supported = static_cast<enum emulashione_device_supported_methods>(Methods),\
    .threadingMode = static_cast<enum emulashione_device_threading_mode>(ThreadingMode),\
    .lifecycle = {\
        .didLoad = []() -> int {\
            try {\
                ClassName::DidLoad();\
            } catch(const std::exception &e) {\
                Logger::Error("Device load callback failed: {}", e.what());\
                return -1;\
            }\
            return 0;\
        },\
        .willUnload = []() -> int {\
            try {\
                ClassName::WillUnload();\
            } catch(const std::exception &e) {\
                Logger::Error("Device unload callback failed: {}", e.what());\
                return -1;\
            }\
            return 0;\
        },\
        .alloc = [](auto name, auto cfg) \
            -> struct emulashione_device_instance_header * {\
            try {\
                auto obj = std::make_shared<ClassName>(name, cfg);\
                auto wrapper = new ClassName::Wrapper;\
                wrapper->descriptor = &ClassName::kDeviceDescriptor;\
                wrapper->p = obj;\
                obj->wrapper = wrapper;\
                return reinterpret_cast<struct emulashione_device_instance_header *>(wrapper);\
            } catch(const std::exception &e) {\
                Logger::Error("Failed to allocate device: {}", e.what());\
                return nullptr;\
            }\
        },\
        .free = [](struct emulashione_device_instance_header *ctx) -> int {\
            try {\
                delete reinterpret_cast<ClassName::Wrapper *>(ctx);\
            } catch(const std::exception &e) {\
                Logger::Error("Failed to free device: {}", e.what());\
                return -1;\
            }\
            return 0;\
        },\
    },\
    .info = ClassName::cbInfo,\
    .emulation = ClassName::cbEmulation,\
    .slice = ClassName::cbSlice,\
    .step = ClassName::cbStep,\
    .cothread = ClassName::cbCothread,\
    .memory = ClassName::cbMemory,\
    .strobe = ClassName::cbStrobe,\
    .connections = ClassName::cbConnection,\
\
}\

#endif

