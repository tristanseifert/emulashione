#include "Config.h"
#include "InterfaceRegistry.h"
#include "Logger.h"

#include <stdexcept>

#include <fmt/core.h>
#include <interfaces/Config.h>

using namespace emulashione::plugin;

const struct emulashione_config_interface *Config::gInterface{nullptr};

/**
 * Creates the config object
 */
Config::Config(struct emulashione_config_object *_config) : config(_config) {
    if(!_config) throw std::invalid_argument("Config ptr may not be null");
    if(!gInterface) {
        ResolveInterface();
    }
}

/**
 * Resolves the config interface.
 */
void Config::ResolveInterface() {
    static const uuids::uuid kInterfaceUuid{{0xAB, 0xAB, 0x5F, 0xF9, 0xDE, 0x9A, 0x42, 0xB7, 0xBD,
        0x64, 0x18, 0xA4, 0x16, 0x59, 0x81, 0x21
    }};

    auto intf = InterfaceRegistry::The()->get(kInterfaceUuid);
    if(!intf) {
        throw std::runtime_error("failed to get config interface");
    }

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    gInterface = reinterpret_cast<const struct emulashione_config_interface *>(intf);
}

bool Config::hasKey(const std::string_view &key) const {
    return gInterface->keyExists(this->config, key.data());
}

std::string Config::getString(const std::string_view &key) const {
    size_t length{0};

    // get the string length first
    if(!gInterface->getStringLength(this->config, key.data(), &length)) {
        throw std::runtime_error(fmt::format("failed to get string length for key '{}'", key));
    }

    // allocate a string and copy into it
    std::string val(length, '\0');

    if(!gInterface->getString(this->config, key.data(), val.data(), val.size())) {
        throw std::runtime_error(fmt::format("failed to get string value for key '{}'", key));
    }

    return val;
}

int64_t Config::getSigned(const std::string_view &key) const {
    int64_t out{0};
    if(!gInterface->getSigned(this->config, key.data(), &out)) {
        throw std::runtime_error(fmt::format("failed to get signed value for key '{}'", key));
    }
    return out;
}

uint64_t Config::getUnsigned(const std::string_view &key) const {
    uint64_t out{0};
    if(!gInterface->getUnsigned(this->config, key.data(), &out)) {
        throw std::runtime_error(fmt::format("failed to get unsigned value for key '{}'", key));
    }
    return out;
}

double Config::getDouble(const std::string_view &key) const {
    double out{0};
    if(!gInterface->getDouble(this->config, key.data(), &out)) {
        throw std::runtime_error(fmt::format("failed to get double value for key '{}'", key));
    }
    return out;
}

bool Config::getBool(const std::string_view &key) const {
    bool out{false};
    if(!gInterface->getBool(this->config, key.data(), &out)) {
        throw std::runtime_error(fmt::format("failed to get bool value for key '{}'", key));
    }
    return out;
}

