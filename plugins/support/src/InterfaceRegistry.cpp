#include "InterfaceRegistry.h"

#include <interfaces/Info.h>
#include <interfaces/Registry.h>

using namespace emulashione::plugin;

InterfaceRegistry *InterfaceRegistry::gShared{nullptr};

/**
 * Initializes the interface registry.
 */
InterfaceRegistry::InterfaceRegistry(struct emulashione_plugin_context *ctx) :
    registry(ctx->registryInterface) {

}

/**
 * Query the library for an interface with the given id.
 */
const struct emulashione_interface_header *InterfaceRegistry::get(const uuids::uuid &id) {
    return this->registry->getInterface(reinterpret_cast<const uint8_t *>(id.as_bytes().data()),
            0);
}

