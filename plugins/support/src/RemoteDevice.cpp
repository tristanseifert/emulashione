#include "RemoteDevice.h"
#include "Logger.h"

#include <interfaces/Device.h>

#include <cmath>

using namespace emulashione::plugin;

/**
 * Initialize the remote device. This simply sets the wrapper pointer to the same instance as was
 * passed in.
 */
RemoteDevice::RemoteDevice(struct emulashione_device_instance_header *instance) : Device("(remote device)"){
    this->ownsWrapper = false;
    this->wrapper = reinterpret_cast<Wrapper *>(instance);
}

void RemoteDevice::willStartEmulation() {
    auto meth = this->getDescriptor()->emulation.willStartEmulation;
    if(!meth) return;

    int err = meth(this->getInstancePtr());
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
}
void RemoteDevice::willStopEmulation() {
    auto meth = this->getDescriptor()->emulation.willStopEmulation;
    if(!meth) return;

    int err = meth(this->getInstancePtr());
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
}
void RemoteDevice::willPauseEmulation() {
    auto meth = this->getDescriptor()->emulation.willPauseEmulation;
    if(!meth) return;

    int err = meth(this->getInstancePtr());
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
}
void RemoteDevice::willResumeEmulation() {
    auto meth = this->getDescriptor()->emulation.willResumeEmulation;
    if(!meth) return;

    int err = meth(this->getInstancePtr());
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
}
void RemoteDevice::willShutDown() {
    auto meth = this->getDescriptor()->emulation.willShutDown;
    if(!meth) return;

    int err = meth(this->getInstancePtr());
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
}
void RemoteDevice::reset(const bool hard) {
    auto meth = this->getDescriptor()->emulation.reset;
    if(!meth) return;

    int err = meth(this->getInstancePtr(), hard);
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
}
void RemoteDevice::commitTo(const double time) {
    auto meth = this->getDescriptor()->emulation.commitTo;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    int err = meth(this->getInstancePtr(), time);
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
}



void RemoteDevice::timeSliceWillBegin(const double time, const double length) {
    auto meth = this->getDescriptor()->slice.timeSliceWillBegin;
    if(!meth) return;

    int err = meth(this->getInstancePtr(), time, length);
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
}
void RemoteDevice::executeTimeSlice(const double time, const double length) {
    auto meth = this->getDescriptor()->slice.executeTimeSlice;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    int err = meth(this->getInstancePtr(), time, length);
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
}
void RemoteDevice::timeSliceDidEnd(const double time) {
    auto meth = this->getDescriptor()->slice.timeSliceDidEnd;
    if(!meth) return;

    int err = meth(this->getInstancePtr(), time);
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
}
double RemoteDevice::getTimeSliceProgress() {
    auto meth = this->getDescriptor()->slice.getTimeSliceProgress;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    return meth(this->getInstancePtr());
}



double RemoteDevice::executeStep(const double currentTime) {
    auto meth = this->getDescriptor()->step.step;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    double time{NAN};
    int err = meth(this->getInstancePtr(), currentTime, &time);
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
    return time;
}



size_t RemoteDevice::getSupportedAddressSpaces(std::span<uint16_t> &out) {
    auto meth = this->getDescriptor()->memory.getSupportedAddressSpaces;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    int err = meth(this->getInstancePtr(), out.data(), out.size());
    if(err < 0) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
    return err;
}
void RemoteDevice::read(struct emulashione_device_instance_header *caller,
        const uint16_t addressSpace, const uint64_t address, const AccessWidth width,
        uint64_t &outData, const double time, double &accessTime) {
    auto meth = this->getDescriptor()->memory.read;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    // build request
    struct emulashione_device_memory_access access{};
    access.addressSpace = addressSpace;

    switch(width) {
        case AccessWidth::Byte:
            access.flags  = kAccessWidthByte;
            break;
        case AccessWidth::Word:
            access.flags  = kAccessWidthWord;
            break;
        case AccessWidth::LongWord:
            access.flags  = kAccessWidthLongWord;
            break;
        case AccessWidth::QuadWord:
            access.flags  = kAccessWidthQuadWord;
            break;
    }

    // make request
    int err = meth(this->getInstancePtr(), caller, address, time, &access);
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }

    // extract info
    switch(width) {
        case AccessWidth::Byte:
            outData = access.d8;
            break;
        case AccessWidth::Word:
            outData = access.d16;
            break;
        case AccessWidth::LongWord:
            outData = access.d32;
            break;
        case AccessWidth::QuadWord:
            outData = access.d64;
            break;
    }
    accessTime = access.time;
}
void RemoteDevice::write(struct emulashione_device_instance_header *caller,
        const uint16_t addressSpace, const uint64_t address, const AccessWidth width,
        const uint64_t inData, const double time, double &accessTime, const uint64_t mask) {
    auto meth = this->getDescriptor()->memory.write;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    // build request
    struct emulashione_device_memory_access access{};
    access.addressSpace = addressSpace;

    switch(width) {
        case AccessWidth::Byte:
            access.flags  = kAccessWidthByte;
            access.d8 = static_cast<uint8_t>(inData);
            access.mask8 = static_cast<uint8_t>(mask);
            break;
        case AccessWidth::Word:
            access.flags  = kAccessWidthWord;
            access.d16 = static_cast<uint16_t>(inData);
            access.mask16 = static_cast<uint16_t>(mask);
            break;
        case AccessWidth::LongWord:
            access.flags  = kAccessWidthLongWord;
            access.d32 = static_cast<uint32_t>(inData);
            access.mask32 = static_cast<uint32_t>(mask);
            break;
        case AccessWidth::QuadWord:
            access.flags  = kAccessWidthQuadWord;
            access.d64 = static_cast<uint64_t>(inData);
            access.mask64 = static_cast<uint64_t>(mask);
            break;
    }

    // make request
    int err = meth(this->getInstancePtr(), caller, address, time, &access);
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }

    // extract info
    accessTime = access.time;
}



unsigned int RemoteDevice::maxStrobeId() {
    auto meth = this->getDescriptor()->strobe.maxId;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    return meth(this->getInstancePtr());
}
std::optional<std::string> RemoteDevice::getStrobeName(const unsigned int id) {
    auto meth = this->getDescriptor()->strobe.nameFor;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    auto ptr = meth(this->getInstancePtr(), id);
    if(!ptr) return std::nullopt;
    return std::string(ptr);
}
std::optional<unsigned int> RemoteDevice::getStrobeId(const std::string &name) {
    auto meth = this->getDescriptor()->strobe.idFor;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    auto id = meth(this->getInstancePtr(), name.c_str());
    if(!id) return std::nullopt;
    else if(id > 0) return id;
    else {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, id);
    }
}
std::optional<size_t> RemoteDevice::getStrobeWidth(const unsigned int id) {
    auto meth = this->getDescriptor()->strobe.widthFor;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    auto width = meth(this->getInstancePtr(), id);
    if(!width) return std::nullopt;
    else if(width > 0) return width;
    else {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, width);
    }
}
void RemoteDevice::setStrobeState(const std::shared_ptr<Device> &device, const unsigned int id,
        const uint64_t value, const double time) {
    auto meth = this->getDescriptor()->strobe.setState;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    int err = meth(this->getInstancePtr(), device->getInstancePtr(), id, value, time);
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

void RemoteDevice::stopDrivingStrobe(const std::shared_ptr<Device> &device, const unsigned int id,
        const double time) {
    auto meth = this->getDescriptor()->strobe.stopDriving;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    // TODO: what is the value param for?
    int err = meth(this->getInstancePtr(), device->getInstancePtr(), id, 0, time);
    if(err) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

bool RemoteDevice::waitForStrobeChange(const std::shared_ptr<Device> &device,
        const unsigned int id, const uint64_t value, const double time,
        const uint64_t mask) {
    auto meth = this->getDescriptor()->strobe.waitForChange;
    if(!meth) throw Error("missing function pointer for {}!", __PRETTY_FUNCTION__);

    int err = meth(this->getInstancePtr(), device->getInstancePtr(), id, value, mask, time);
    if(err < 0) {
        throw Error("{} failed: {}", __PRETTY_FUNCTION__, err);
    }

    return (err == 1);
}

