#include "Bus.h"
#include "Logger.h"
#include "InterfaceRegistry.h"

#include <stdexcept>

#include <interfaces/Bus.h>
#include <interfaces/Device.h>

#include <fmt/core.h>
#include <uuid.h>

using namespace emulashione::plugin;

const struct emulashione_bus_interface *Bus::gInterface{nullptr};

/**
 * Creates a new bus wrapper for the given instance.
 */
Bus::Bus(struct emulashione_bus_instance *_instance) : instance(_instance) {
    if(!gInterface) {
        ResolveInterface();
    }
}

/**
 * Resolves the bus interface.
 */
void Bus::ResolveInterface() {
    static const uuids::uuid kInterfaceUuid{{0xBE, 0xA4, 0x6E, 0xC1, 0xE4, 0xF4, 0x48, 0xB3, 0x84,
        0xF8, 0x05, 0x5F, 0xD9, 0x3E, 0xBA, 0xAD
    }};

    auto intf = InterfaceRegistry::The()->get(kInterfaceUuid);
    if(!intf) {
        throw std::runtime_error("failed to get bus interface");
    }

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    gInterface = reinterpret_cast<const struct emulashione_bus_interface *>(intf);
}

/**
 * Gets the name of the bus.
 *
 * @return String representation of bus name
 */
std::string Bus::getName() const {
    auto ptr = gInterface->getName(this->instance);
    if(ptr) {
        return std::string(ptr);
    }
    return "";
}

/**
 * Get the bus' data width.
 *
 * @return Width of the data component of the bus, in bits
 */
size_t Bus::getDataWidth() const {
    auto width = gInterface->getDataWidth(this->instance);
    if(width <= 0) {
        throw std::runtime_error(fmt::format("{} failed: {}", __PRETTY_FUNCTION__, width));
    }
    return width;
}

/**
 * Get the bus' address width.
 *
 * @return Width of the address component of the bus, in bits
 */
size_t Bus::getAddressWidth() const {
    auto width = gInterface->getAddressWidth(this->instance);
    if(width <= 0) {
        throw std::runtime_error(fmt::format("{} failed: {}", __PRETTY_FUNCTION__, width));
    }
    return width;
}



/**
 * Reads a byte from the given address on the bus.
 *
 * @param caller Calling device performing this read
 * @param addressSpace Identifier for the address space
 * @param address Physical address to access
 * @param outData Location to store the read data in
 * @param accessTime Time taken to perform this bus access, in nanoseconds
 *
 * @throw BusError An error occurred while performing the IO.
 */
void Bus::read(struct emulashione_device_instance_header *caller,
        const uint16_t addressSpace, const uint64_t address, uint8_t &outData,
        double &accessTime) {
    int err{0};
    struct emulashione_device_memory_access info{};

    info.addressSpace = addressSpace;
    info.flags = kAccessWidthByte;

    err = gInterface->read(this->instance, caller, address, &info);
    if(err) {
        throw BusError("Read from ${:x}:{:016x} failed: {}", addressSpace, address, err);
    }

    outData = info.d8;
    accessTime = info.time;
}

/**
 * Reads a word from the given address on the bus.
 *
 * @param caller Calling device performing this read
 * @param addressSpace Identifier for the address space
 * @param address Physical address to access
 * @param outData Location to store the read data in
 * @param accessTime Time taken to perform this bus access, in nanoseconds
 *
 * @throw BusError An error occurred while performing the IO.
 */
void Bus::read(struct emulashione_device_instance_header *caller,
        const uint16_t addressSpace, const uint64_t address, uint16_t &outData,
        double &accessTime) {
    int err{0};
    struct emulashione_device_memory_access info{};

    info.addressSpace = addressSpace;
    info.flags = kAccessWidthWord;

    err = gInterface->read(this->instance, caller, address, &info);
    if(err) {
        throw BusError("Read from ${:x}:{:016x} failed: {}", addressSpace, address, err);
    }

    outData = info.d16;
    accessTime = info.time;
}

/**
 * Reads a longword from the given address on the bus.
 *
 * @param caller Calling device performing this read
 * @param addressSpace Identifier for the address space
 * @param address Physical address to access
 * @param outData Location to store the read data in
 * @param accessTime Time taken to perform this bus access, in nanoseconds
 *
 * @throw BusError An error occurred while performing the IO.
 */
void Bus::read(struct emulashione_device_instance_header *caller,
        const uint16_t addressSpace, const uint64_t address, uint32_t &outData,
        double &accessTime) {
    int err{0};
    struct emulashione_device_memory_access info{};

    info.addressSpace = addressSpace;
    info.flags = kAccessWidthLongWord;

    err = gInterface->read(this->instance, caller, address, &info);
    if(err) {
        throw BusError("Read from ${:x}:{:016x} failed: {}", addressSpace, address, err);
    }

    outData = info.d32;
    accessTime = info.time;
}

/**
 * Reads a quadword from the given address on the bus.
 *
 * @param caller Calling device performing this read
 * @param addressSpace Identifier for the address space
 * @param address Physical address to access
 * @param outData Location to store the read data in
 * @param accessTime Time taken to perform this bus access, in nanoseconds
 *
 * @throw BusError An error occurred while performing the IO.
 */
void Bus::read(struct emulashione_device_instance_header *caller,
        const uint16_t addressSpace, const uint64_t address, uint64_t &outData,
        double &accessTime) {
    int err{0};
    struct emulashione_device_memory_access info{};

    info.addressSpace = addressSpace;
    info.flags = kAccessWidthQuadWord;

    err = gInterface->read(this->instance, caller, address, &info);
    if(err) {
        throw BusError("Read from ${:x}:{:016x} failed: {}", addressSpace, address, err);
    }

    outData = info.d64;
    accessTime = info.time;
}


/**
 * Writes a byte the given address on the bus.
 *
 * @param caller Calling device performing this read
 * @param addressSpace Identifier for the address space
 * @param address Physical address to access
 * @param inData Data to be written to the device
 * @param accessTime Time taken to perform this bus access, in nanoseconds
 * @param mask If nonzero, a bitmask to apply against the data to indicate that only part of the
 *        bus is being actively driven.
 *
 * @throw BusError An error occurred while performing the IO.
 */
void Bus::write(struct emulashione_device_instance_header *caller, const uint16_t addressSpace,
        const uint64_t address, const uint8_t inData, double &accessTime, const uint8_t mask) {
    int err{0};
    struct emulashione_device_memory_access info{};

    info.addressSpace = addressSpace;
    info.flags = kAccessWidthByte;
    info.d8 = inData;
    info.mask8 = mask;

    err = gInterface->write(this->instance, caller, address, &info);

    if(err) {
        throw BusError("Write to ${:x}:{:016x} failed: {}", addressSpace, address, err);
    }

    // pull out information about the access
    accessTime = info.time;
}

/**
 * Writes a word the given address on the bus.
 *
 * @param caller Calling device performing this read
 * @param addressSpace Identifier for the address space
 * @param address Physical address to access
 * @param inData Data to be written to the device
 * @param accessTime Time taken to perform this bus access, in nanoseconds
 * @param mask If nonzero, a bitmask to apply against the data to indicate that only part of the
 *        bus is being actively driven.
 *
 * @throw BusError An error occurred while performing the IO.
 */
void Bus::write(struct emulashione_device_instance_header *caller, const uint16_t addressSpace,
        const uint64_t address, const uint16_t inData, double &accessTime, const uint16_t mask) {
    int err{0};
    struct emulashione_device_memory_access info{};

    info.addressSpace = addressSpace;
    info.flags = kAccessWidthWord;
    info.d16 = inData;
    info.mask16 = mask;

    err = gInterface->write(this->instance, caller, address, &info);

    if(err) {
        throw BusError("Write to ${:x}:{:016x} failed: {}", addressSpace, address, err);
    }

    // pull out information about the access
    accessTime = info.time;
}

/**
 * Writes a longword the given address on the bus.
 *
 * @param caller Calling device performing this read
 * @param addressSpace Identifier for the address space
 * @param address Physical address to access
 * @param inData Data to be written to the device
 * @param accessTime Time taken to perform this bus access, in nanoseconds
 * @param mask If nonzero, a bitmask to apply against the data to indicate that only part of the
 *        bus is being actively driven.
 *
 * @throw BusError An error occurred while performing the IO.
 */
void Bus::write(struct emulashione_device_instance_header *caller, const uint16_t addressSpace,
        const uint64_t address, const uint32_t inData, double &accessTime, const uint32_t mask) {
    int err{0};
    struct emulashione_device_memory_access info{};

    info.addressSpace = addressSpace;
    info.flags = kAccessWidthLongWord;
    info.d32 = inData;
    info.mask32 = mask;

    err = gInterface->write(this->instance, caller, address, &info);

    if(err) {
        throw BusError("Write to ${:x}:{:016x} failed: {}", addressSpace, address, err);
    }

    // pull out information about the access
    accessTime = info.time;
}

/**
 * Writes a quadword the given address on the bus.
 *
 * @param caller Calling device performing this read
 * @param addressSpace Identifier for the address space
 * @param address Physical address to access
 * @param inData Data to be written to the device
 * @param accessTime Time taken to perform this bus access, in nanoseconds
 * @param mask If nonzero, a bitmask to apply against the data to indicate that only part of the
 *        bus is being actively driven.
 *
 * @throw BusError An error occurred while performing the IO.
 */
void Bus::write(struct emulashione_device_instance_header *caller, const uint16_t addressSpace,
        const uint64_t address, const uint64_t inData, double &accessTime, const uint64_t mask) {
    int err{0};
    struct emulashione_device_memory_access info{};

    info.addressSpace = addressSpace;
    info.flags = kAccessWidthQuadWord;
    info.d64 = inData;
    info.mask64 = mask;

    err = gInterface->write(this->instance, caller, address, &info);

    if(err) {
        throw BusError("Write to ${:x}:{:016x} failed: {}", addressSpace, address, err);
    }

    // pull out information about the access
    accessTime = info.time;
}
