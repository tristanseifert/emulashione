#include "TraceManager.h"
#include "Device.h"
#include "InterfaceRegistry.h"
#include "Logger.h"

#include <interfaces/TraceManager.h>

#include <fmt/format.h>

#include <stdexcept>

using namespace emulashione::plugin;

std::once_flag TraceManager::gInterfaceAllocated;
const struct emulashione_trace_manager_interface *TraceManager::gInterface{nullptr};

/**
 * Initializes a new trace manager with the given short name.
 *
 * @param ns Namespace id for trace message type codes
 * @param name A short identifier string for this trace manager
 * @param maxPayloadSize Maximum size of trace message payloads, in bytes, or 0 to disable
 *
 * @throw std::runtime_error An unknown error occurred while initializing the trace manager
 */
TraceManager::TraceManager(const uint16_t ns, const std::string_view &name,
        const size_t maxPayloadSize) {
    std::call_once(gInterfaceAllocated, TraceManager::ResolveInterface);

    auto err = gInterface->alloc(ns, name.data(), maxPayloadSize, &this->instance);
    if(err) {
        throw std::runtime_error(fmt::format("Failed to allocate trace manager: {}", err));
    }
}

/**
 * Initializes a trace manager that takes a device's short name.
 *
 * @param device Device to take the short name from
 *
 * @throw std::runtime_error An unknown error occurred while initializing the trace manager
 */
TraceManager::TraceManager(const uint16_t ns, const Device &dev, const size_t maxPayloadSize) :
    TraceManager(ns, dev.getName(), maxPayloadSize) {

}

/**
 * Release the underlying trace manager instance.
 */
TraceManager::~TraceManager() {
    gInterface->free(this->instance);
}

/**
 * Get the trace manager interface.
 */
void TraceManager::ResolveInterface() {
    static const uuids::uuid kInterfaceUuid{{0x05, 0xC0, 0xFE, 0x3F, 0x84, 0x02, 0x40, 0xDE, 0xB4,
        0xC5, 0xD1, 0x72, 0x2D, 0xEC, 0x6C, 0x02
    }};

    auto intf = InterfaceRegistry::The()->get(kInterfaceUuid);
    if(!intf) {
        throw std::runtime_error("failed to get trace manager interface");
    }

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    gInterface = reinterpret_cast<const struct emulashione_trace_manager_interface *>(intf);
}



/**
 * Adds a memory output to store trace messages.
 *
 * @param maxChunks Maximum number of chunks to allocate, or 0 for no limit
 * @param chunkSize Size of each chunk, in bytes, or 0 to use system default
 *
 * @remark While the underlying interface supports multiple memory backed outputs, we will only
 *         support one in this wrapper. Any more than that is likely not necessary.
 *
 * @throw std::runtime_error Failed to create the memory output
 */
void TraceManager::addMemoryOutput(const size_t maxChunks, const size_t chunkSize) {
    auto err = gInterface->addMemoryOutput(this->instance, maxChunks, chunkSize, &this->memOutput);
    if(err) {
        throw std::runtime_error(fmt::format("Failed to add memory output: {}", err));
    }
}


/**
 * Writes a trace message to the outputs of the trace manager.
 *
 * @param time System timestamp to associate with this message
 * @param id Message identifier
 * @param payload An optional blob to associate with the message
 */
void TraceManager::putMessage(const double time, const uint64_t id,
        const std::span<std::byte> &payload) {
    gInterface->putMessage(this->instance, time, id, payload.data(), payload.size());
}

/**
 * Invoke a method for each record sent to the trace manager.
 *
 * @remark This calls through to a method on the memory output, so it will not work unless
 *         the trace manager has a memory output added to it during initialization.
 *
 * @param callback Method to invoke for each record, with that record's data. It can return `false`
 *        to abort the iteration process early.
 *
 * @return Total number of records iterated over before reaching end or terminating
 *
 * @throw std::logic_error No memory output is associated with the trace manager
 */
size_t TraceManager::iterateRecords(const std::function<bool(double time, uint16_t ns, uint64_t id,
            std::span<std::byte> &payload)> &callback) {
    if(!this->memOutput) {
        throw std::logic_error("no memory output associated with device!");
    }

    struct ctx {
        const std::function<bool(double time, uint16_t ns, uint64_t id,
            std::span<std::byte> &payload)> &cb;
    };
    ctx c{callback};

    return gInterface->memIterateRecords(this->memOutput, [](auto time, auto ns, auto id,
                auto payload, auto payloadSz, auto inCtx) -> bool {
        auto ctx = reinterpret_cast<struct ctx *>(inCtx);
        std::span<std::byte> payloadSpan;
        if(payload && payloadSz) {
            payloadSpan = {reinterpret_cast<std::byte *>(const_cast<void *>(payload)), payloadSz};
        }

        return ctx->cb(time, ns, id, payloadSpan);
    }, &c);
}

