#include "Logger.h"

#include <array>
#include <interfaces/Info.h>
#include <interfaces/Log.h>
#include <stdexcept>

using namespace emulashione::plugin;

Logger *Logger::gShared{nullptr};

/**
 * Initializes the logger instance; this stores the context and instance pointers.
 */
Logger::Logger(struct emulashione_plugin_context *ctx) : interface(ctx->loggerInterface),
    handle(ctx->logger) {

}

/**
 * Outputs a log message.
 */
void Logger::log(const Level l, const std::string_view &msg) {
    switch(l) {
        case Level::Trace:
            return this->interface->logTrace(this->handle, msg.data());
        case Level::Debug:
            return this->interface->logDebug(this->handle, msg.data());
        case Level::Info:
            return this->interface->logInfo(this->handle, msg.data());
        case Level::Warning:
            return this->interface->logWarning(this->handle, msg.data());
        case Level::Error:
            return this->interface->logError(this->handle, msg.data());
        case Level::Critical:
            return this->interface->logCritical(this->handle, msg.data());

        default:
            throw std::invalid_argument("Invalid log level");
    }
}

/**
 * Returns the current log level.
 */
Logger::Level Logger::getLogLevel() const {
    int err = this->interface->getLogLevel(this->handle);
    if(err < 0) {
        throw std::runtime_error(fmt::format("Failed to query log level: {}", err));
    }

    return ConvertLogLevel(static_cast<unsigned int>(err));
}

/**
 * Converts an integer log level to the enum value.
 */
Logger::Level Logger::ConvertLogLevel(unsigned int x) {
    static const std::array<Level, 6> kLevels{{
        Level::Trace, Level::Debug, Level::Info, Level::Warning, Level::Error, Level::Critical
    }};
    if(x >= kLevels.size()) {
        throw std::runtime_error(fmt::format("Invalid log level value: {}", x));
    }

    return kLevels[x];
}
