#include "Plugin.h"

#include "Config.h"
#include "InterfaceRegistry.h"
#include "Logger.h"
#include "WrapperCache.h"

#include <interfaces/Info.h>
#include <interfaces/Device.h>

#include <cassert>

#include <fmt/core.h>
#include <stdexcept>
#include <uuid.h>

using namespace emulashione::plugin;

/// Global plugin object instance
Plugin *Plugin::gShared{nullptr};

/**
 * Instantiate the plugin and several other support classes.
 */
Plugin::Plugin(struct emulashione_plugin_context *_ctx) : ctx(_ctx) {
    InterfaceRegistry::gShared = new InterfaceRegistry(_ctx);
    Logger::gShared = new Logger(_ctx);
    WrapperCache::gShared = new WrapperCache;
}

/**
 * Clean up the initialized resources.
 */
Plugin::~Plugin() {
    delete WrapperCache::gShared;
    WrapperCache::gShared = nullptr;

    delete InterfaceRegistry::gShared;
    InterfaceRegistry::gShared = nullptr;

    // kill the logger last, so we can catch errors during shutdown
    delete Logger::gShared;
    Logger::gShared = nullptr;

    Config::gInterface = nullptr;
}

/**
 * Returns the device interface, looking it up if required.
 *
 * @return Address of the device interface exposed by the core library
 */
const struct emulashione_device_interface *Plugin::getDeviceInterface() {
    // return if already loaded
    if(this->devInterface) return this->devInterface;

    // we should acquire it
    static const uuids::uuid kDeviceInterfaceUuid{{0x79, 0x4F, 0xAA, 0x0F, 0x26, 0x94, 0x47, 0xCF,
    0xAB, 0x40, 0xF5, 0xB8, 0x4E, 0x4F, 0x8B, 0x51}};

    auto interface = InterfaceRegistry::The()->get(kDeviceInterfaceUuid);
    assert(!!interface);

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    this->devInterface = reinterpret_cast<const struct emulashione_device_interface *>(interface);
    return this->devInterface;
}

/**
 * Registers the given device.
 *
 * @param desc Device descriptor to register
 */
void Plugin::_registerDevice(const struct emulashione_device_descriptor &desc) {
    int err =  this->getDeviceInterface()->registerDevice(this->ctx, &desc,
            sizeof(struct emulashione_device_descriptor));
    if(err) {
        throw std::runtime_error(fmt::format("Failed to register device: {}", err));
    }
}

/**
 * Unregisters the device.
 *
 * @param desc Device descriptor to unregister
 */
void Plugin::_unregisterDevice(const struct emulashione_device_descriptor &desc) {
    int err =  this->getDeviceInterface()->unregisterDevice(this->ctx, desc.uuid);
    if(err) {
        throw std::runtime_error(fmt::format("Failed to unregister device: {}", err));
    }
}

