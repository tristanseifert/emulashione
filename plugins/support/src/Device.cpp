#include "Device.h"
#include "CooperativeDevice.h"
#include "Logger.h"
#include "InterfaceRegistry.h"
#include "WrapperCache.h"

#include <uuid.h>
#include <fmt/core.h>
#include <interfaces/Device.h>

#define EMULASHIONE_TYPES_ONLY 1
#include <emulashione.h>

#include <cmath>
#include <mutex>
#include <optional>
#include <stdexcept>

using namespace emulashione::plugin;

#ifndef DOXYGEN_SHOULD_SKIP_THIS

/**
 * @brief Device interface exported by emulation library
 *
 * This variable holds a reference to the device interface and is looked up lazily.
 */
const struct emulashione_device_interface *Device::gDeviceInterface{nullptr};

/// Information callbacks
const struct emulashione_device_info_callbacks Device::cbInfo{
    .supportsInterface = [](auto ctx, auto interfaceId, auto interfaceIdLen) -> bool {
        try {
            const uuids::uuid uuid{interfaceId, interfaceId + interfaceIdLen};

            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            return reinterpret_cast<Wrapper *>(ctx)->p->supportsInterface(uuid);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return false;
        }
    },
    .getInfo = [](auto ctx, auto key, auto outBuf, auto outBufSize) -> int {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        std::span<std::byte> buf(reinterpret_cast<std::byte *>(outBuf), outBufSize);

        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            return reinterpret_cast<Wrapper *>(ctx)->p->copyOutInfo(key, buf);
        } catch(const UnknownInfoKey &) {
            return kEmulashioneUnknownKey;
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return kEmulashioneUnknownKey;
        }
    },
    .setInfo = [](auto ctx, auto key, auto valueBuf, auto valueBufSize) -> int {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        std::span<std::byte> buf(reinterpret_cast<std::byte *>(const_cast<void *>(valueBuf)), valueBufSize);

        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->copyInInfo(key, buf);
        } catch(const UnknownInfoKey &) {
            return kEmulashioneUnknownKey;
        } catch(const ReadOnlyInfoKey &) {
            return kEmulashioneReadOnlyKey;
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return kEmulashioneUnknownError;
        }

        return kEmulashioneSuccess;
    }
};

/// Life cycle callbacks
const struct emulashione_device_emulation_callbacks Device::cbEmulation{
    .willStartEmulation = [](struct emulashione_device_instance_header *ctx) -> int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->willStartEmulation();
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .willStopEmulation = [](struct emulashione_device_instance_header *ctx) -> int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->willStopEmulation();
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .willPauseEmulation = [](struct emulashione_device_instance_header *ctx) -> int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->willPauseEmulation();
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .willResumeEmulation = [](struct emulashione_device_instance_header *ctx) -> int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->willResumeEmulation();
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .willShutDown = [](struct emulashione_device_instance_header *ctx) -> int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->willShutDown();
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .reset = [](struct emulashione_device_instance_header *ctx, const bool hard) -> int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->reset(hard);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .commitTo = [](struct emulashione_device_instance_header *ctx, const double time) -> int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->commitTo(time);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
};

#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
#elif defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

/// Time slice callbacks
const struct emulashione_device_slice_callbacks Device::cbSlice{
    .timeSliceWillBegin = [](auto ctx, auto time, auto length) -> int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->timeSliceWillBegin(time, length);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .executeTimeSlice = [](auto ctx, auto time, auto length) -> int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->executeTimeSlice(time, length);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .timeSliceDidEnd = [](auto ctx, auto time) -> int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->timeSliceDidEnd(time);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .getTimeSliceProgress = [](auto ctx) -> double {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            return reinterpret_cast<Wrapper *>(ctx)->p->getTimeSliceProgress();
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return NAN;
        }
    }
};

/// Time step callbacks
const struct emulashione_device_step_callbacks Device::cbStep{
    .step = [](auto ctx, auto currentTime, auto outTime) {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            *outTime = reinterpret_cast<Wrapper *>(ctx)->p->executeStep(currentTime);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
};

/// Cooperative threading callbacks (no-op for base device class)
const struct emulashione_device_cothread_callbacks Device::cbCothread{
    .prepare = [](auto device, auto schedCtx) -> int {
        Logger::Error("cothread emulation mode not implemented for base device class; subclass",
                "CooperativeDevice instead");
        return -1;
    },
    .main = [](auto device, auto schedCtx) -> int {
        Logger::Error("cothread emulation mode not implemented for base device class; subclass",
                "CooperativeDevice instead");
        return -1;
    },
};

/// Memory callbacks
const struct emulashione_device_memory_callbacks Device::cbMemory{
    .getSupportedAddressSpaces = [](auto ctx, auto outBuf, auto numElements) -> int {
        if(!ctx || !outBuf || !numElements) return -1;

        try {
            std::span<uint16_t> span{outBuf, numElements};

            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            return reinterpret_cast<Wrapper *>(ctx)->p->getSupportedAddressSpaces(span);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
    },
    .read = [](auto ctx, auto caller, auto address, auto time, auto info) -> int {
        if(!ctx || !info) return -1;

        try {
            uint64_t data{0};
            AccessWidth width{AccessWidth::Byte};

            switch(info->flags & kAccessWidthMask) {
                case kAccessWidthByte:
                    width = AccessWidth::Byte;
                    break;
                case kAccessWidthWord:
                    width = AccessWidth::Word;
                    break;
                case kAccessWidthLongWord:
                    width = AccessWidth::LongWord;
                    break;
                case kAccessWidthQuadWord:
                    width = AccessWidth::QuadWord;
                    break;
                default:
                    return -1;
            }

            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->read(caller, info->addressSpace, address,
                    width, data, time, info->time);

            switch(width) {
                case AccessWidth::Byte:
                    info->d8 = static_cast<uint8_t>(data);
                    break;
                case AccessWidth::Word:
                    info->d16 = static_cast<uint16_t>(data);
                    break;
                case AccessWidth::LongWord:
                    info->d32 = static_cast<uint32_t>(data);
                    break;
                case AccessWidth::QuadWord:
                    info->d64 = static_cast<uint64_t>(data);
                    break;
            }
        } catch(const CooperativeDevice::EmulationAborted &) {
            throw;
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", "device bus read", e.what());
            return -1;
        }
        return 0;
    },

    .write = [](auto ctx, auto caller, auto address, auto time, auto info) -> int {
        if(!ctx || !info) return -1;

        try {
            uint64_t data{0}, mask{0};
            AccessWidth width{AccessWidth::Byte};

            switch(info->flags & kAccessWidthMask) {
                case kAccessWidthByte:
                    width = AccessWidth::Byte;
                    data = info->d8;
                    mask = info->mask8;
                    break;
                case kAccessWidthWord:
                    width = AccessWidth::Word;
                    data = info->d16;
                    mask = info->mask16;
                    break;
                case kAccessWidthLongWord:
                    width = AccessWidth::LongWord;
                    data = info->d32;
                    mask = info->mask32;
                    break;
                case kAccessWidthQuadWord:
                    width = AccessWidth::QuadWord;
                    data = info->d64;
                    mask = info->mask64;
                    break;
                default:
                    return -1;
            }

            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->write(caller, info->addressSpace, address,
                    width, data, time, info->time, mask);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", "device bus write", e.what());
            return -1;
        }
        return 0;
    },
};

/// Strobe callbacks
const struct emulashione_device_strobe_callbacks Device::cbStrobe{
    .maxId = [](auto ctx) -> unsigned int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            return reinterpret_cast<Wrapper *>(ctx)->p->maxStrobeId();
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return 0;
        }
    },
    .nameFor = [](auto ctx, auto id) -> const char * {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            auto name = reinterpret_cast<Wrapper *>(ctx)->p->getStrobeName(id);
            if(name) return name->c_str();
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
        }
        return nullptr;
    },
    .idFor = [](auto ctx, auto name) -> int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            auto id = reinterpret_cast<Wrapper *>(ctx)->p->getStrobeId(name);
            if(id) return *id;
            return 0;
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
    },
    .widthFor = [](auto ctx, auto id) -> int {
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            auto width = reinterpret_cast<Wrapper *>(ctx)->p->getStrobeWidth(id);
            if(width) return *width;
            return 0;
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
    },
    .setState = [](auto ctx, auto caller, auto id, auto value, auto time) -> int {
        try {
            auto callerDevice = GetDeviceFor(caller);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->setStrobeState(callerDevice, id, value, time);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    // the value (unlabeled arg) is unused, i don't remember why i added it lmao
    .stopDriving = [](auto ctx, auto caller, auto id, auto, auto time) -> int {
        try {
            auto callerDevice = GetDeviceFor(caller);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->stopDrivingStrobe(callerDevice, id, time);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .waitForChange = [](auto ctx, auto caller, auto id, auto value, auto mask, auto time) -> int {
        try {
            auto callerDevice = GetDeviceFor(caller);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            auto success = reinterpret_cast<Wrapper *>(ctx)->p->waitForStrobeChange(callerDevice,
                    id, value, time, mask);
            return success ? 1 : 0;
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
    },
};

/// Connection callbacks
const struct emulashione_device_connection_callbacks Device::cbConnection{
    .addBus = [](auto ctx, auto slot, auto bus) -> int {
        if(!ctx || !slot ||!bus) return -1;
        try {
            auto busObj = WrapperCache::The()->getBus(bus);
            if(!busObj) throw std::runtime_error("failed to get wrapper for bus");

            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->connectBus(slot, busObj);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .removeBus = [](auto ctx, auto slot) -> int {
        if(!ctx || !slot) return -1;
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->disconnectBus(slot);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .addClockSource = [](auto ctx, auto slot, auto clock) -> int {
        if(!ctx || !slot ||!clock) return -1;
        try {
            auto clockSourceObj = WrapperCache::The()->getClockSource(clock);
            if(!clockSourceObj) throw std::runtime_error("failed to get wrapper for clock source");

            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->connectClockSource(slot, clockSourceObj);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .removeClockSource = [](auto ctx, auto slot) -> int {
        if(!ctx || !slot) return -1;
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->disconnectClockSource(slot);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .addDevice = [](auto ctx, auto slot, auto device) -> int {
        if(!ctx || !slot ||!device) return -1;
        try {
            auto deviceObj = WrapperCache::The()->getDevice(device);
            if(!deviceObj) throw std::runtime_error("failed to get wrapper for device");

            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->connectDevice(slot, deviceObj);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .removeDevice = [](auto ctx, auto slot) -> int {
        if(!ctx || !slot) return -1;
        try {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<Wrapper *>(ctx)->p->disconnectDevice(slot);
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
};

#if defined(__clang__)
#pragma clang diagnostic pop
#elif defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

#endif


/**
 * Initializes the base device instance.
 */
Device::Device(const std::string_view &_name, struct emulashione_config_object *) : name(_name) {

}

bool Device::supportsInterface(const uuids::uuid &interfaceId) {
    // device interface uuid
    static const uuids::uuid kDeviceInterfaceUuid{{0x79, 0x4F, 0xAA, 0x0F, 0x26, 0x94, 0x47, 0xCF,
    0xAB, 0x40, 0xF5, 0xB8, 0x4E, 0x4F, 0x8B, 0x51}};
    if(interfaceId == kDeviceInterfaceUuid) return true;

    return false;
}


/**
 * Validate that the device state is consistent. This entails the following:
 *
 * - Ensure that strobe IDs are sequentially allocated from [1..max].
 */
void Device::willStartEmulation() {
    this->validateStrobesList();
}



/**
 * Attempts to convert a device instance to a device object pointer.
 *
 * The way this works is by means of another internal device subclass, which simply forwards all
 * calls back through the C function pointers in the descriptor.
 */
std::shared_ptr<Device> Device::GetDeviceFor(struct emulashione_device_instance_header *inst) {
    return WrapperCache::The()->getDevice(inst);
}


/**
 * Registers a new strobe. This should be called during the initialization phase by the concrete
 * subclasses.
 */
void Device::registerStrobe(const unsigned int id, const std::string &name, const size_t width) {
    std::lock_guard<std::mutex> lg(this->strobesLock);
    if(this->strobes.contains(id)) {
        throw std::runtime_error(fmt::format("Duplicate strobe id {}", id));
    }

    this->strobes.emplace(id, StrobeInfo{name, width});
}

/**
 * Ensures that the strobe IDs are sequential starting at 1.
 */
void Device::validateStrobesList() {
    std::lock_guard<std::mutex> lg(this->strobesLock);
    if(this->strobes.empty()) return;

    unsigned int max{0};
    for(const auto &[id, info] : this->strobes) {
        max = std::max(id, max);
    }

    if(max != this->strobes.size()) {
        throw std::runtime_error(fmt::format("Strobes invalid; have {} strobes but max id is {}",
                    this->strobes.size(), max));
    }
}

unsigned int Device::maxStrobeId() {
    std::lock_guard<std::mutex> lg(this->strobesLock);
    return this->strobes.size();
}

std::optional<std::string> Device::getStrobeName(const unsigned int id) {
    std::lock_guard<std::mutex> lg(this->strobesLock);
    if(!this->strobes.contains(id)) return nullptr;
    return this->strobes.at(id).name;
}

std::optional<unsigned int> Device::getStrobeId(const std::string &name) {
    std::lock_guard<std::mutex> lg(this->strobesLock);
    for(const auto &[id, info] : this->strobes) {
        if(info.name == name) return id;
    }
    return std::nullopt;
}

std::optional<size_t> Device::getStrobeWidth(const unsigned int id) {
    std::lock_guard<std::mutex> lg(this->strobesLock);
    if(!this->strobes.contains(id)) return std::nullopt;
    return this->strobes.at(id).width;
}



/**
 * Registers an address space.
 */
void Device::registerAddressSpace(const uint16_t id, const std::string &name) {
    std::lock_guard<std::mutex> lg(this->addressSpacesLock);
    if(this->addressSpaces.contains(id)) {
        throw std::runtime_error(fmt::format("Duplicate address space id {}", id));
    }

    this->addressSpaces.emplace(id, AddressSpaceInfo{name});
}

/**
 * Copies out the supported address spaces.
 */
size_t Device::getSupportedAddressSpaces(std::span<uint16_t> &out) {
    size_t copied{0};

    std::lock_guard<std::mutex> lg(this->addressSpacesLock);
    for(const auto &[id, info] : this->addressSpaces) {
        out[copied++] = id;
        if(copied == out.size()) goto beach;
    }

beach:;
    return copied;
}



/**
 * Connects a bus.
 *
 * Subclasses can override this method, but they should first always call the super implementation
 * to retain the managed behavior.
 */
void Device::connectBus(const std::string &name, const std::shared_ptr<Bus> &bus) {
    std::lock_guard<std::mutex> lg(this->connectedBussesLock);

    if(!this->busSlotNames.contains(name)) {
        throw std::runtime_error(fmt::format("unknown bus slot '{}'", name));
    }
    else if(this->connectedBusses.contains(name)) {
        throw std::runtime_error(fmt::format("duplicate bus connection for slot '{}'", name));
    }
    this->connectedBusses.emplace(name, bus);
}
/**
 * Disconnects a bus.
 *
 * Subclasses can override this method, but should always invoke this implementation at the end.
 */
void Device::disconnectBus(const std::string &name) {
    std::lock_guard<std::mutex> lg(this->connectedBussesLock);

    if(!this->busSlotNames.contains(name)) {
        throw std::runtime_error(fmt::format("unknown bus slot '{}'", name));
    }
    else if(!this->connectedBusses.erase(name)) {
        throw std::runtime_error(fmt::format("no bus connected to slot '{}'", name));
    }
}

/**
 * Connects a clock source.
 *
 * Subclasses can override this method, but they should first always call the super implementation
 * to retain the managed behavior.
 */
void Device::connectClockSource(const std::string &name,
        const std::shared_ptr<ClockSource> &clockSource) {
    std::lock_guard<std::mutex> lg(this->connectedClockSourcesLock);

    if(!this->clockSourceSlotNames.contains(name)) {
        throw std::runtime_error(fmt::format("unknown clock source slot '{}'", name));
    }
    else if(this->connectedClockSources.contains(name)) {
        throw std::runtime_error(fmt::format("duplicate clock source connection for slot '{}'",
                    name));
    }
    this->connectedClockSources.emplace(name, clockSource);
}
/**
 * Disconnects a clock source.
 *
 * Subclasses can override this method, but should always invoke this implementation at the end.
 */
void Device::disconnectClockSource(const std::string &name) {
    std::lock_guard<std::mutex> lg(this->connectedClockSourcesLock);

    if(!this->clockSourceSlotNames.contains(name)) {
        throw std::runtime_error(fmt::format("unknown clock source slot '{}'", name));
    }
    else if(!this->connectedClockSources.erase(name)) {
        throw std::runtime_error(fmt::format("no clock source connected to slot '{}'", name));
    }
}
/**
 * Connects a device.
 *
 * Subclasses can override this method, but they should first always call the super implementation
 * to retain the managed behavior.
 */
void Device::connectDevice(const std::string &name, const std::shared_ptr<Device> &device) {
    std::lock_guard<std::mutex> lg(this->connectedDevicesLock);

    if(!this->deviceSlotNames.contains(name)) {
        throw std::runtime_error(fmt::format("unknown device slot '{}'", name));
    }
    else if(this->connectedDevices.contains(name)) {
        throw std::runtime_error(fmt::format("duplicate device connection for slot '{}'", name));
    }
    this->connectedDevices.emplace(name, device);
}
/**
 * Disconnects a device.
 *
 * Subclasses can override this method, but should always invoke this implementation at the end.
 */
void Device::disconnectDevice(const std::string &name) {
    std::lock_guard<std::mutex> lg(this->connectedDevicesLock);

    if(!this->deviceSlotNames.contains(name)) {
        throw std::runtime_error(fmt::format("unknown device slot '{}'", name));
    }
    else if(!this->connectedDevices.erase(name)) {
        throw std::runtime_error(fmt::format("no device connected to slot '{}'", name));
    }
}

/*
 * Copy out information, based on the fixed keys defined during initialization.
 */
size_t Device::copyOutInfo(const std::string_view &key, std::span<std::byte> &outBuf) {
    (void) key, (void) outBuf;

    Logger::Warning("Unhandled device info read: '{}'", key);
    throw UnknownInfoKey();
}

/*
 * Copy in information, based on the fixed keys defined during initialization. The info is written
 * to those variables.
 */
bool Device::copyInInfo(const std::string_view &key, const std::span<std::byte> &inBuf) {
    (void) key, (void) inBuf;
    throw UnknownInfoKey();
}

/**
 * Look up the device interface, if needed.
 *
 * @return Device interface exported by core library
 *
 * @throw std::runtime_error An error occurred looking up the device interface
 */
const struct emulashione_device_interface *Device::GetDeviceInterface() {
    const uuids::uuid kDeviceInterfaceUuid{{0x79, 0x4F, 0xAA, 0x0F, 0x26, 0x94, 0x47, 0xCF,
        0xAB, 0x40, 0xF5, 0xB8, 0x4E, 0x4F, 0x8B, 0x51}};

    if(!gDeviceInterface) {
        auto intf = InterfaceRegistry::The()->get(kDeviceInterfaceUuid);
        if(!intf) {
            throw std::runtime_error("failed to resolve device interface");
        }

        gDeviceInterface = reinterpret_cast<const struct emulashione_device_interface *>(intf);
    }
    return gDeviceInterface;
}

