#ifndef WRAPPERCACHE_H
#define WRAPPERCACHE_H

#include <memory>
#include <mutex>
#include <unordered_map>

struct emulashione_bus_instance;
struct emulashione_clock_source_instance;
struct emulashione_device_instance_header;

namespace emulashione::plugin {
class Bus;
class ClockSource;
class Device;

/**
 * Various plain C structs passed by the library for objects can be wrapped by C++ classes for use
 * inside plugin code; this cache will automatically allocate these objects, and keep a hold of
 * them for later dispensing.
 *
 * @brief A cache to keep track of the internal wrappers for core library objects
 */
class WrapperCache {
    friend class Plugin;

    public:
        /// Get the shared cache instance.
        static WrapperCache *The() {
            return gShared;
        }

        ~WrapperCache();

        /// Get a bus object for the given pointer.
        std::shared_ptr<Bus> getBus(struct emulashione_bus_instance *);
        /// Get a clock source object for the given pointer.
        std::shared_ptr<ClockSource> getClockSource(struct emulashione_clock_source_instance *);
        /// Get a device object for the given pointer.
        std::shared_ptr<Device> getDevice(struct emulashione_device_instance_header *);

    private:
        static WrapperCache *gShared;

        std::mutex bussesLock;
        std::unordered_map<struct emulashione_bus_instance *, std::weak_ptr<Bus>> busses;

        std::mutex clockSourcesLock;
        std::unordered_map<struct emulashione_clock_source_instance *,
            std::weak_ptr<ClockSource>> clockSources;

        std::mutex devicesLock;
        std::unordered_map<struct emulashione_device_instance_header *,
            std::weak_ptr<Device>> devices;
};
}

#endif
