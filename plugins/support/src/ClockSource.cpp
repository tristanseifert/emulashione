#include "ClockSource.h"
#include "InterfaceRegistry.h"

#include <stdexcept>

#include <fmt/core.h>
#include <interfaces/ClockSource.h>
#include <uuid.h>

using namespace emulashione::plugin;

const struct emulashione_clock_source_interface *ClockSource::gInterface{nullptr};

/**
 * Creates a new clock source wrapper for the given instance.
 */
ClockSource::ClockSource(struct emulashione_clock_source_instance *_instance):instance(_instance) {
    if(!gInterface) {
        ResolveInterface();
    }
}

/**
 * Resolves the clock source interface.
 */
void ClockSource::ResolveInterface() {
    static const uuids::uuid kInterfaceUuid{{0xC5, 0xF6, 0xCB, 0x2A, 0x26, 0x31, 0x45, 0xE4, 0x94,
        0x5D, 0x4E, 0x02, 0x25, 0x92, 0x92, 0x87
    }};


    auto intf = InterfaceRegistry::The()->get(kInterfaceUuid);
    if(!intf) {
        throw std::runtime_error("failed to get clock source interface");
    }

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    gInterface = reinterpret_cast<const struct emulashione_clock_source_interface *>(intf);
}

/**
 * Return the clock source name.
 */
std::string ClockSource::getName() const {
    auto ptr = gInterface->getName(this->instance);
    if(ptr) {
        return std::string(ptr);
    }
    return "";
}

/**
 * Return the frequency of the clock source.
 */
double ClockSource::getFrequency() const {
    return gInterface->getFrequency(this->instance);
}

/**
 * Return the period of the clock source.
 */
double ClockSource::getPeriod() const {
    return gInterface->getPeriod(this->instance);
}
