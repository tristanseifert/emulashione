#include "CooperativeDevice.h"
#include "ClockSource.h"
#include "InterfaceRegistry.h"
#include "Logger.h"
#include "WrapperCache.h"

#include <fmt/core.h>
#include <interfaces/Device.h>
#include <interfaces/Scheduler.h>

#include <stdexcept>

using namespace emulashione::plugin;



/// Cooperative threading callbacks (no-op for base device class)
const struct emulashione_device_cothread_callbacks CooperativeDevice::cbCothread{
    .prepare = [](auto ctx, auto schedCtx) -> int {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        auto dev = std::dynamic_pointer_cast<CooperativeDevice>(
                reinterpret_cast<Wrapper *>(ctx)->p);
        dev->schedPrepare(schedCtx);
        return 0;
    },
    .main = [](auto ctx, auto) -> int {
        /*
         * We read the raw value of the underlying smart pointer, and operate on that, rather than
         * casting the smart pointer directly; this ensures we don't create an extra reference
         * here, which may not get deallocated correctly (since the main method may never return,
         * especially if emulation is aborted!) otherwise.
         *
         * This introduces a small window during which the device could get deallocated due to all
         * references being dropped.
         */
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        auto dev = dynamic_cast<CooperativeDevice *>(reinterpret_cast<Wrapper *>(ctx)->p.get());

        try {
            dev->main();
        } catch(const CooperativeDevice::EmulationAborted &e) {
            return e.getStatus();
        } catch(const std::exception &e) {
            Logger::Error("{} failed: {}", "device comain", e.what());
            return -1;
        }

        return 0;
    },
};



/**
 * Resolves the interface to communicate with the emulator scheduler at initialization time.
 */
CooperativeDevice::CooperativeDevice(const std::string_view &name,
        struct emulashione_config_object *cfg) : Device(name, cfg) {
    static const uuids::uuid kSchedulerInterfaceUuid{{
        0xF1, 0x2F, 0x67, 0xEC, 0xC1, 0xCC, 0x4B, 0xE2, 0x81, 0x55, 0x1C, 0xFE,
        0x9A, 0xC6, 0x07, 0xC3
    }};
    auto intf = InterfaceRegistry::The()->get(kSchedulerInterfaceUuid);

    if(!intf) {
        throw std::runtime_error("failed to get scheduler interface");
    }

    this->schedInterface = reinterpret_cast<const struct emulashione_scheduler_interface *>(intf);
}

/**
 * Handles clock source connections; if only a single clock source slot is registered, we take this
 * clock source as the scheduler clock.
 *
 * @remark If the device exports more than one clock slot, you must manually set the scheduler
 *         reference clock.
 *
 * @param name Clock source slot
 * @param clock Clock source to connect
 */
void CooperativeDevice::connectClockSource(const std::string &name,
        const std::shared_ptr<ClockSource> &clock) {
    Device::connectClockSource(name, clock);

    if(this->clockSourceSlotNames.size() == 1) {
        this->setSchedulerClock(clock);
    }
}

/**
 * If this device only exports one clock slot, and that clock is being removed, also clear out the
 * scheduler clock.
 *
 * @param name Clock slot to clear
 */
void CooperativeDevice::disconnectClockSource(const std::string &name) {
    if(this->clockSourceSlotNames.size() == 1) {
        // XXX: we should probably validate the slot name matches...
        this->setSchedulerClock(nullptr);
    }

    Device::disconnectClockSource(name);
}

/**
 * Sets the new scheduler clock. All time ticks passed to scheduler methods will be relative to
 * this clock's frequency.
 *
 * @param clock New scheduler clock
 */
void CooperativeDevice::setSchedulerClock(const std::shared_ptr<ClockSource> &clock) {
    this->schedClk = clock;

    const auto err = this->schedInterface->setRefClock(this->scheduler, this->getInstancePtr(),
            clock ? clock->getInstancePtr() : nullptr);
    if(err) {
        throw std::runtime_error(fmt::format("failed to set device refclk: {}", err));
    }
}



/**
 * Prepares the device's state with the provided scheduler context.
 */
void CooperativeDevice::schedPrepare(struct emulashione_device_scheduler_context *schedCtx) {
    this->scheduler = schedCtx->scheduler;
}

/**
 * Aborts the system's emulation because of an unrecoverable error.
 *
 * This will result in the scheduler stopping emulation of all devices, and all other devices in
 * the system are likewise stopped.
 *
 * @param status A status code indicating more information why the emulation was aborted.
 */
void CooperativeDevice::schedAbort(const int status) {
    throw EmulationAborted(status);
}
