#include "WrapperCache.h"
#include "RemoteDevice.h"

#include "Bus.h"
#include "ClockSource.h"
#include "Device.h"

#include <mutex>

using namespace emulashione::plugin;

WrapperCache *WrapperCache::gShared{nullptr};

/**
 * Clears any cached interface pointers in wrapper classes.
 */
WrapperCache::~WrapperCache() {
    Bus::gInterface = nullptr;
    ClockSource::gInterface = nullptr;
}

/**
 * Retrieves a cached bus object, creating one and storing it if we don't already have one.
 */
std::shared_ptr<Bus> WrapperCache::getBus(struct emulashione_bus_instance *inst) {
    std::lock_guard<std::mutex> lg(this->bussesLock);
    // check if we've an instance
    if(this->busses.contains(inst)) {
        auto ptr = this->busses.at(inst).lock();
        if(ptr) return ptr;

        // the pointer has become invalidated
        this->busses.erase(inst);
    }

    // nope; create one and save it
    auto ptr = std::make_shared<Bus>(inst);
    this->busses.emplace(inst, ptr);

    return ptr;
}

/**
 * Retrieves a cached clock source object, creating one and storing it if we don't already have
 * one.
 */
std::shared_ptr<ClockSource> WrapperCache::getClockSource(struct emulashione_clock_source_instance *inst) {
    std::lock_guard<std::mutex> lg(this->clockSourcesLock);
    // check if we've an instance
    if(this->clockSources.contains(inst)) {
        auto ptr = this->clockSources.at(inst).lock();
        if(ptr) return ptr;

        // the pointer has become invalidated
        this->clockSources.erase(inst);
    }

    // nope; create one and save it
    auto ptr = std::make_shared<ClockSource>(inst);
    this->clockSources.emplace(inst, ptr);

    return ptr;
}
/**
 * Retrieves a cached device object, creating one and storing it if we don't already have one.
 */
std::shared_ptr<Device> WrapperCache::getDevice(struct emulashione_device_instance_header *inst) {
    std::lock_guard<std::mutex> lg(this->devicesLock);
    // check if we've an instance
    if(this->devices.contains(inst)) {
        auto ptr = this->devices.at(inst).lock();
        if(ptr) return ptr;

        // the pointer has become invalidated
        this->devices.erase(inst);
    }

    // nope; create one and save it
    auto ptr = std::make_shared<RemoteDevice>(inst);
    this->devices.emplace(inst, ptr);

    return ptr;
}

