#ifndef REMOTEDEVICE_H
#define REMOTEDEVICE_H

#include "Device.h"

#include <exception>

#include <fmt/core.h>

struct emulashione_device_instance_header;
struct emulashione_device_descriptor;

namespace emulashione::plugin {
/**
 * Device subclass that forwards all method calls through the function pointers defined in the
 * instance's descriptor.
 *
 * @brief Wrapper around a remote device instance
 */
class RemoteDevice: public Device {
    public:
        /// Indicates a device method failed
        class Error: public std::exception {
            friend class RemoteDevice;

            private:
                /// Create a load error with no detail string.
                Error() = default;

                /// Create a load error with the given format string.
                template<typename... Args>
                Error(fmt::format_string<Args...> fmt, Args &&...args) {
                    this->message = fmt::format(fmt, std::forward<Args>(args)...);
                }

            public:
                const char *what() const noexcept override {
                    return this->message.c_str();
                }

            private:
                std::string message;
        }; 

    public:
        RemoteDevice(struct emulashione_device_instance_header *instance);

    protected:
        void willStartEmulation() override;
        void willStopEmulation() override;
        void willPauseEmulation() override;
        void willResumeEmulation() override;
        void willShutDown() override;
        void reset(const bool hard) override;
        void commitTo(const double time) override;

        void timeSliceWillBegin(const double time, const double length) override;
        void executeTimeSlice(const double time, const double length) override;
        void timeSliceDidEnd(const double time) override;
        double getTimeSliceProgress() override;

        double executeStep(const double time) override;

        size_t getSupportedAddressSpaces(std::span<uint16_t> &out) override;
        void read(struct emulashione_device_instance_header *caller, const uint16_t addressSpace,
                const uint64_t address, const AccessWidth width, uint64_t &outData,
                const double time, double &accessTime) override;
        void write(struct emulashione_device_instance_header *caller, const uint16_t addressSpace,
                const uint64_t address, const AccessWidth width, const uint64_t inData,
                const double time, double &accessTime, const uint64_t mask) override;

        unsigned int maxStrobeId() override;
        std::optional<std::string> getStrobeName(const unsigned int id) override;
        std::optional<unsigned int> getStrobeId(const std::string &name) override;
        std::optional<size_t> getStrobeWidth(const unsigned int id) override;
        void setStrobeState(const std::shared_ptr<Device> &device, const unsigned int id,
                const uint64_t value, const double time) override;
        void stopDrivingStrobe(const std::shared_ptr<Device> &device,
                const unsigned int id, const double time) override;
        bool waitForStrobeChange(const std::shared_ptr<Device> &device,
                const unsigned int id, const uint64_t value, const double time,
                const uint64_t mask) override;
};
}

#endif
