#ifndef TESTER_H
#define TESTER_H

#include <cstdint>
#include <cstddef>
#include <filesystem>
#include <functional>
#include <span>

#include "Comparinator.h"

/**
 * Encapsulates logic to run a single test program on the system.
 */
class Tester {
    public:
        /**
         * Status code returned by the emulation run method if a test was successful; this is the
         * base value OR 1 written to the debug trap.
         */
        constexpr static const int kSuccessReturn{static_cast<int>(0xFFFFF000 | 1)};

    public:
        /**
         * Executes the provided test program, and compares its result against the specified trace
         * state buffer.
         *
         * @param program Buffer to a program to load at address $0 in the device's memory
         * @param trace A trace buffer to compare execution against
         */
        static void RunTest(const std::span<std::byte> &program,
                const std::span<std::byte> &trace) {
             Run(program, [&](auto traceIntf, auto traceMem) {
                REQUIRE_NOTHROW(Comparinator::ValidateTrace(traceIntf, traceMem, trace));
            });
        }

        /**
         * Writes out the trace state after running the specified program. This is used to create
         * the trace files we compare against later.
         *
         * @param program Buffer to a program to load at address $0 in the device's memory
         * @param path Filesystem path to store the output file at
         */
        static void WriteTraceResult(const std::span<std::byte> &program,
                const std::filesystem::path &path) {
             Run(program, [&](auto traceIntf, auto traceMem) {
                Comparinator::WriteTrace(traceIntf, traceMem, path);
            });
        }

    private:
        static void Run(const std::span<std::byte> &program, const std::function<void(
                    const struct emulashione_trace_manager_interface *traceIntf,
                    struct emulashione_trace_manager_mem_output *traceMem
                    )> &postRun);
};

#endif
