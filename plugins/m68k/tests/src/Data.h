#ifndef DATA_H
#define DATA_H

#include <cstddef>
#include <cstdint>
#include <span>

/**
 * @brief Container for test program data
 */
struct Data {
    static const std::span<std::byte> kRomInstructionArithmetic;
    static const std::span<std::byte> kRomInstructionBitwise;
    static const std::span<std::byte> kRomInstructionMove;
    static const std::span<std::byte> kRomInstructionComparison;
    static const std::span<std::byte> kRomProgramLz4Dec;

    static const std::span<std::byte> kRefInstructionArithmetic;
    static const std::span<std::byte> kRefInstructionBitwise;
    static const std::span<std::byte> kRefInstructionMove;
    static const std::span<std::byte> kRefInstructionComparison;
    static const std::span<std::byte> kRefProgramLz4Dec;
};

#endif
