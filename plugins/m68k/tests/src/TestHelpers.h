#ifndef HELPERS_H
#define HELPERS_H

#include <plugin/interface/Device.h>
#include <plugin/interface/TraceManager.h>

#include <fmt/core.h>
#include <emulashione.h>

#include <array>
#include <cstddef>
#include <cstdint>
#include <span>
#include <stdexcept>

/**
 * Provides an interface to make working with the processors for tests easier. This consists
 * mostly of helper methods to read/write stuff.
 *
 * To make things less annoying for tests, there's no state kept between invocations; any
 * interfaces are always looked up for each call, so this isn't particularly fast.
 */
class Helpers {
    public:
        /**
         * Reads a device information key.
         *
         * @param device The device instance to get the info key from
         * @param key Information key to access
         * @param outBuf A buffer to receive the read data
         *
         * @return Actual number of bytes read
         *
         * @throw std::runtime_error An error occurred while performing the access
         */
        static size_t ReadDeviceInfo(struct emulashione_device_instance_header *device,
                const std::string_view &key, std::span<std::byte> &outBuf) {
            auto err = device->descriptor->info.getInfo(device, key.data(), outBuf.data(), outBuf.size());
            if(err < 0) {
                throw std::runtime_error(fmt::format("failed to get info key: {}", err));
            }

            return err;
        }

        /**
         * Reads a pointer sized information key.
         *
         * @param device Device to read the info key from
         * @param key Information key to access
         *
         * @return The read pointer value
         *
         * @throw std::runtime_error An error occurred while performing the access
         */
        static void *ReadDeviceInfoPtr(struct emulashione_device_instance_header *device,
                const std::string_view &key) {
            std::array<std::byte, sizeof(void *)> temp;
            std::span tempSpan{temp.data(), temp.size()};

            auto val = ReadDeviceInfo(device, key, tempSpan);
            if(val != sizeof(void *)) {
                throw std::runtime_error(fmt::format("failed to read full pointer: got {} bytes",
                            val));
            }

            // copy it out into a pointer
            void *shit;
            memcpy(&shit, temp.data(), sizeof(shit));
            return shit;
        }

        /**
         * Writes a device information key.
         *
         * @param device The device instance to set the info key on
         * @param key Information key to access
         * @param buf Buffer of data to be written to this information key
         *
         * @throw std::runtime_error An error occurred while performing the access
         */
        static void WriteDeviceInfo(struct emulashione_device_instance_header *device,
                const std::string_view &key, const std::span<std::byte> &buf) {
            auto err = device->descriptor->info.setInfo(device, key.data(), buf.data(), buf.size());
            if(err < 0) {
                throw std::runtime_error(fmt::format("failed to set info key: {}", err));
            }
        }

        /**
         * Get the trace manager interface.
         */
        static const struct emulashione_trace_manager_interface *GetTraceInterface() {
            constexpr static const std::array<uint8_t, 16> kTraceInterfaceUuid{{
                0x05, 0xC0, 0xFE, 0x3F, 0x84, 0x02, 0x40, 0xDE, 0xB4, 0xC5, 0xD1, 0x72, 0x2D,
                0xEC, 0x6C, 0x02
            }};

            const struct emulashione_interface_header *outIntf;
            auto err = libemulashione_interface_get(kTraceInterfaceUuid.data(),
                    kTraceInterfaceUuid.size(), &outIntf);

            if(err != kEmulashioneSuccess) {
                throw std::runtime_error(fmt::format("failed to get trace manager interface: {}",
                            err));
            }

            return reinterpret_cast<const struct emulashione_trace_manager_interface *>(outIntf);
        }
        /**
         * Get the device interface.
         */
        static const struct emulashione_device_interface *GetDeviceInterface() {
            constexpr static const std::array<uint8_t, 16> kDeviceInterfaceUuid{{
                0x79, 0x4F, 0xAA, 0x0F, 0x26, 0x94, 0x47, 0xCF, 0xAB, 0x40, 0xF5, 0xB8, 0x4E,
                    0x4F, 0x8B, 0x51
            }};

            const struct emulashione_interface_header *outIntf;
            auto err = libemulashione_interface_get(kDeviceInterfaceUuid.data(),
                    kDeviceInterfaceUuid.size(), &outIntf);

            if(err != kEmulashioneSuccess) {
                throw std::runtime_error(fmt::format("failed to get device interface: {}", err));
            }

            return reinterpret_cast<const struct emulashione_device_interface *>(outIntf);
        }
};

#endif
