/**
 * @brief Tests of individual instructions
 *
 * This file defines test cases for individual instructions or groups of instructions to be tested
 * in isolation.
 */
#include <catch2/catch.hpp>

#include "Data.h"
#include "Flags.h"
#include "Tester.h"

TEST_CASE("check correctness of arithmetic instructions", "[instruction]") {
    if(kGenerateTestData) Tester::WriteTraceResult(Data::kRomInstructionArithmetic, "arithmetic.bin");
    else Tester::RunTest(Data::kRomInstructionArithmetic, Data::kRefInstructionArithmetic);
}

TEST_CASE("check correctness of bitwise instructions", "[instruction]") {
    if(kGenerateTestData) Tester::WriteTraceResult(Data::kRomInstructionBitwise, "bitwise.bin");
    else Tester::RunTest(Data::kRomInstructionBitwise, Data::kRefInstructionBitwise);
}

TEST_CASE("check correctness of move instructions", "[instruction]") {
    if(kGenerateTestData) Tester::WriteTraceResult(Data::kRomInstructionMove, "move.bin");
    else Tester::RunTest(Data::kRomInstructionMove, Data::kRefInstructionMove);
}

TEST_CASE("check correctness of comparison instructions", "[instruction]") {
    if(kGenerateTestData) Tester::WriteTraceResult(Data::kRomInstructionComparison, "compare.bin");
    else Tester::RunTest(Data::kRomInstructionComparison, Data::kRefInstructionComparison);
}

