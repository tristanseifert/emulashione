#include "Tester.h"

#include <string_view>

#include <catch2/catch.hpp>
#include <emulashione.h>
#include <TraceTypes.h>

#include <unistd.h>

#include "Comparinator.h"
#include "TestHelpers.h"
#include "PluginPaths.h"
#include "Serialization.h"

using namespace emulashione::m68k;

/**
 * System definition for a minimal 68000 system to execute these tests on. The system has only
 * one peripheral, a 512K RAM spanning $000000 to $07FFFF. The exact binary to execute will be
 * loaded in here once the system is created.
 *
 * We "abuse" the RESET instruction to exit the emulation.
 */
constexpr static const std::string_view kSystemDef{R"(
[system]
name = "m68k module test"
description = "System to test the 68000 core"
manufacturer = "Tristan Seifert"

[[clocks]]
name = "MCLK"

[[clocks.freqs]]
name = "default"
freq = 10000000

[[schedulers]]
name = "main"

[[schedulers.devices]]
target = "maincpu"

[[busses]]
name = "68k"
dataWidth = 16
addrWidth = 24
pulldown = 0xFFFF
abortOnUnhandledAccess = true

[[busses.map]]
range = [ 0x000000, 0x07FFFF ]
target = "fastram"

[[busses.map]]
range = [ 0xFFFF00, 0xFFFFFF ]
target = "trap"

# Main CPU
[[devices]]
type = "m68000"
#type = "musashi68000"
name = "maincpu"

# output instruction traces to memory
[devices.trace]
enable = true
memory = true

[[devices.connections]]
name = "bus"
target = "68k"
type = "bus"
[[devices.connections]]
name = "clock"
target = "MCLK"
type = "clock"

[[devices]]
type = "ram"
name = "fastram"
size = 524288
contents = "zero"
access = 70

[[devices]]
type = "debugtrap"
name = "trap"
base = 0xFFFFF000
)"};

/**
 * Set up and run the emulated system, loading the specified program into it. Once the
 * program has completed execution, the given callback is invoked.
 *
 * @param program Buffer to a program to load at address $0 in the device's memory
 * @param postRun Method to call once the execution loop terminates
 */
void Tester::Run(const std::span<std::byte> &program, const std::function<void(
            const struct emulashione_trace_manager_interface *traceIntf,
            struct emulashione_trace_manager_mem_output *traceMem
            )> &postRun) {
    emulashione_status_t err;

    // set up a system
    err = libemulashione_init(nullptr, nullptr);
    REQUIRE(err == kEmulashioneSuccess);

    err = libemulashione_plugin_load_file(PluginPaths::IoPluginPath.data());
    REQUIRE(err == kEmulashioneSuccess);
    err = libemulashione_plugin_load_file(PluginPaths::MemoryPluginPath.data());
    REQUIRE(err == kEmulashioneSuccess);
    err = libemulashione_plugin_load_file(PluginPaths::M68kPluginPath.data());
    REQUIRE(err == kEmulashioneSuccess);

    auto traceIntf = Helpers::GetTraceInterface();
    REQUIRE(traceIntf);

    auto em = libemulashione_system_alloc();
    REQUIRE(em);

    err = libemulashione_system_parse_definition(em, kSystemDef.data());
    REQUIRE(err == kEmulashioneSuccess);

    // get references to devices
    struct emulashione_device_instance_header *ramDev{nullptr}, *cpuDev{nullptr};

    err = libemulashione_system_get_device(em, "maincpu", &cpuDev);
    REQUIRE(err == kEmulashioneSuccess);
    REQUIRE(cpuDev);
    err = libemulashione_system_get_device(em, "fastram", &ramDev);
    REQUIRE(err == kEmulashioneSuccess);
    REQUIRE(ramDev);

    auto tracer = reinterpret_cast<struct emulashione_trace_manager_instance *>
        (Helpers::ReadDeviceInfoPtr(cpuDev, "tracer"));
    REQUIRE(tracer);
    auto traceMem = reinterpret_cast<struct emulashione_trace_manager_mem_output *>
        (Helpers::ReadDeviceInfoPtr(cpuDev, "traceMemOut"));
    REQUIRE(traceMem);

    // load binary and execute it
    Helpers::WriteDeviceInfo(ramDev, "contents", program);

    auto ret = libemulashione_system_run(em);
    REQUIRE(ret == kSuccessReturn);

    // read out trace and compare
    /*
     * there's a bug that crashes stuff because catch2 does fork-y stuff and that apparently
     * spoils thread ids to the point where concurrentqueue gets angerey so just wait long
     * enough for the trace manager's work queue to drain
     *
     * if we start getting intermittent test failures we need to just increase this lmao
     */
    usleep(1000 * 200);

    postRun(traceIntf, traceMem);

    // clean up
    err = libemulashione_system_free(em);
    REQUIRE(err == kEmulashioneSuccess);
    err = libemulashione_deinit();
    REQUIRE(err == kEmulashioneSuccess);
}
