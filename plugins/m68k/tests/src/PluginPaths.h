#ifndef PLUGINPATHS_H
#define PLUGINPATHS_H

#include <string_view>

struct PluginPaths {
    /// Path of the dylib for the IO devices plugin
    static const std::string_view IoPluginPath;
    /// Path of the dylib for the memory devices plugin
    static const std::string_view MemoryPluginPath;
    /// Path of the dylib for the m68k plugin
    static const std::string_view M68kPluginPath;
};

#endif
