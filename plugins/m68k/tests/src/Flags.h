#ifndef FLAGS_H
#define FLAGS_H

/**
 * @brief Whether test cases output reference trace files or compare against them
 *
 * When set to `true`, each test case will instead output, to the current working directory, a
 * binary trace file suitable for inclusion into the test as reference data.
 */
static constexpr bool kGenerateTestData{false};

#endif
