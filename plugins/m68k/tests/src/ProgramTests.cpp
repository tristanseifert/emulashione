/**
 * @brief Tests executing entire programs
 *
 * A few slightly more complicated programs are executed as part of this test and their execution
 * states compared
 */
#include <catch2/catch.hpp>

#include "Data.h"
#include "Flags.h"
#include "Tester.h"

TEST_CASE("testing lz4_dec test program", "[program]") {
    if(kGenerateTestData) Tester::WriteTraceResult(Data::kRomProgramLz4Dec, "lz4_dec.bin");
    else Tester::RunTest(Data::kRomProgramLz4Dec, Data::kRefProgramLz4Dec);
}
