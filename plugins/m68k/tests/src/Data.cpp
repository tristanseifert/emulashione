#include "Data.h"

#include "refdata/arithmetic.bin.h"
#include "refdata/bitwise.bin.h"
#include "refdata/compare.bin.h"
#include "refdata/move.bin.h"

#include "refdata/lz4_dec.bin.h"

#include "roms/arithmetic.bin.h"
#include "roms/bitwise.bin.h"
#include "roms/compare.bin.h"
#include "roms/move.bin.h"

#include "roms/lz4_dec.bin.h"

const std::span<std::byte> Data::kRomInstructionArithmetic{
    reinterpret_cast<std::byte *>(arithmetic_bin), arithmetic_bin_len};
const std::span<std::byte> Data::kRomInstructionBitwise{
    reinterpret_cast<std::byte *>(bitwise_bin), bitwise_bin_len};
const std::span<std::byte> Data::kRomInstructionMove{
    reinterpret_cast<std::byte *>(move_bin), move_bin_len};
const std::span<std::byte> Data::kRomInstructionComparison{
    reinterpret_cast<std::byte *>(compare_bin), compare_bin_len};
const std::span<std::byte> Data::kRomProgramLz4Dec{
    reinterpret_cast<std::byte *>(lz4_dec_bin), lz4_dec_bin_len};

const std::span<std::byte> Data::kRefInstructionArithmetic{
    reinterpret_cast<std::byte *>(ref_arithmetic_bin), ref_arithmetic_bin_len};
const std::span<std::byte> Data::kRefInstructionBitwise{
    reinterpret_cast<std::byte *>(ref_bitwise_bin), ref_bitwise_bin_len};
const std::span<std::byte> Data::kRefInstructionMove{
    reinterpret_cast<std::byte *>(ref_move_bin), ref_move_bin_len};
const std::span<std::byte> Data::kRefInstructionComparison{
    reinterpret_cast<std::byte *>(ref_compare_bin), ref_compare_bin_len};
const std::span<std::byte> Data::kRefProgramLz4Dec{
    reinterpret_cast<std::byte *>(ref_lz4_dec_bin), ref_lz4_dec_bin_len};


