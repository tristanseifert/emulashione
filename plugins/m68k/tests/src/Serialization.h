#ifndef SERIALIZATION_H
#define SERIALIZATION_H

#include <TraceTypes.h>

#include <fstream>
#include <arpa/inet.h>

/**
 * @brief Helpers to read/write register state to files
 */
class Serialization {
    public:
        /// Number of bytes per record
        constexpr static const size_t kBytesPerRecord{(8 * 4) + (8 * 4) + 4 + 4 + 2};

        /**
         * Write the given register state out to the file.
         *
         * Note that only the values of the registers are written; the abort flags and other flags
         * for the processor state aren't yet.
         *
         * @param file File to write the register state to, at the current file offset
         * @param reg Register state to write
         */
        static void WriteState(std::ostream &file, const emulashione::m68k::TraceRegisterState &reg) {
            // write each data and address register
            for(size_t i = 0; i < 8; i++) {
                const uint32_t val = htonl(reg.d[i]);
                file.write(reinterpret_cast<const char *>(&val), sizeof(val));
            }
            for(size_t i = 0; i < 8; i++) {
                const uint32_t val = htonl(reg.a[i]);
                file.write(reinterpret_cast<const char *>(&val), sizeof(val));
            }
            // then ssp, pc and status
            const uint32_t ssp = htonl(reg.ssp);
            file.write(reinterpret_cast<const char *>(&ssp), sizeof(ssp));

            const uint32_t pc = htonl(reg.pc);
            file.write(reinterpret_cast<const char *>(&pc), sizeof(pc));

            const uint16_t status = htons(reg.status);
            file.write(reinterpret_cast<const char *>(&status), sizeof(status));
        }

        /**
         * Read out a register state record from the current position in the stream.
         *
         * @param file File stream to read from
         * @param reg Register state to receive the read data
         */
        static void ReadState(std::istream &file, emulashione::m68k::TraceRegisterState &reg) {
            // read each data and address register
            for(size_t i = 0; i < 8; i++) {
                uint32_t temp;
                file.read(reinterpret_cast<char *>(&temp), sizeof(temp));
                reg.d[i] = ntohl(temp);
            }
            for(size_t i = 0; i < 8; i++) {
                uint32_t temp;
                file.read(reinterpret_cast<char *>(&temp), sizeof(temp));
                reg.a[i] = ntohl(temp);
            }

            uint32_t ssp, pc;
            file.read(reinterpret_cast<char *>(&ssp), sizeof(ssp));
            reg.ssp = ntohl(ssp);

            file.read(reinterpret_cast<char *>(&pc), sizeof(pc));
            reg.pc = ntohl(pc);

            uint16_t status;
            file.read(reinterpret_cast<char *>(&status), sizeof(status));
            reg.status = ntohs(status);
        }
};

#endif
