#ifndef COMPARINATOR_H
#define COMPARINATOR_H

#include <catch2/catch.hpp>

#include "Serialization.h"

#include <plugin/interface/TraceManager.h>
#include <TraceTypes.h>

#include <fmt/core.h>

#include <array>
#include <cstddef>
#include <filesystem>
#include <iostream>
#include <string_view>
#include <span>
#include <sstream>

/**
 * @brief Comparison of execution traces
 *
 * Allows serializing traces and comparing them against a reference that was previously serialized.
 */
class Comparinator {
    public:
        /**
         * Iterates over all trace state records in the given memory output, and compares them
         * against the sequence of provided in the given buffer.
         *
         * @param traceIntf Trace interface to call methods on
         * @param mem Trace manager to read out the records of
         * @param reference A byte array containing individual records from a trace file
         */
        static void ValidateTrace(const struct emulashione_trace_manager_interface *traceIntf,
            struct emulashione_trace_manager_mem_output *mem,
            const std::span<std::byte> &reference) {
            using namespace emulashione::m68k;
            // prepare input buffer from reference
            struct ReadCtx {
                std::stringstream file;
                size_t read{0}, mismatches{0};
            };
            struct ReadCtx ctx;

            ctx.file.str(std::string(reinterpret_cast<const char *>(reference.data()),
                        reference.size()));

            // iterate over all records
            traceIntf->memIterateRecords(mem, [](auto time, auto ns, auto id,
                    auto payloadPtr, auto payloadSz, auto inCtx) -> bool {
                (void) time;
                // only care about the trace events
                if(ns != 0x0001 || id != kTraceMessageInstruction) return true;
                REQUIRE(payloadSz >= sizeof(TraceRegisterState));

                // ensure file is not at end
                auto info = reinterpret_cast<struct ReadCtx *>(inCtx);
                if(info->file.eof()) {
                    throw std::runtime_error("reached end of reference buffer!");
                }

                // get the actual expected regs and the actual regs
                TraceRegisterState expectedRegs{};
                Serialization::ReadState(info->file, expectedRegs);

                auto actualRegs = reinterpret_cast<const TraceRegisterState *>(payloadPtr);

                // compare them, abort if different
                if(*actualRegs != expectedRegs) {
                    std::cerr
                        << fmt::format(" ***** Mismatched processor state (record {:5}) ***** ",
                                info->read) << std::endl;
                    PrintRegDiff(*actualRegs, expectedRegs);
                    info->mismatches++;
                }

                // only print the first n mismatches
                constexpr static const size_t kMaxMismatches{50};
                if(info->mismatches >= kMaxMismatches) return false;

                // continue to next
                info->read++;
                return true;
            }, &ctx);

            if(ctx.mismatches) {
                throw std::runtime_error(fmt::format("mismatched processor state ({} records "
                            "failed)", ctx.mismatches));
            }
        }

        /**
         * Dumps the contents of the given memory trace manager (all instruction trace records) to
         * the file with the given path.
         *
         * @param traceIntf Trace interface to call methods on
         * @param mem Trace manager to read out
         * @param path File path to receive the contents; it will be truncated on open
         *
         * @return Number of register states written
         */
        static size_t WriteTrace(const struct emulashione_trace_manager_interface *traceIntf,
                struct emulashione_trace_manager_mem_output *mem,
                const std::filesystem::path &path) {
            using namespace emulashione::m68k;

            // prepare context by opening the file
            struct WriteCtx {
                std::ofstream file;
                size_t written{0};
            };
            struct WriteCtx ctx;
            ctx.file.open(path, std::ofstream::trunc);

            // then iterate pls
            traceIntf->memIterateRecords(mem, [](auto time, auto ns, auto id,
                    auto payloadPtr, auto payloadSz, auto inCtx) -> bool {
                (void) time;
                // only care about the trace events
                if(ns != 0x0001 || id != kTraceMessageInstruction) return true;
                REQUIRE(payloadSz >= sizeof(TraceRegisterState));

                // get info
                auto info = reinterpret_cast<struct WriteCtx *>(inCtx);
                info->written++;

                auto expectedRegs = reinterpret_cast<const TraceRegisterState *>(payloadPtr);

                Serialization::WriteState(info->file, *expectedRegs);
                return true;
            }, &ctx);

            return ctx.written;
        }

        /**
         * Prints the two register states, highlighting any registers that differ in the two; the
         * actual registers are printed the first row, with the expected below.
         */
        static void PrintRegDiff(const emulashione::m68k::TraceRegisterState &actual,
                const emulashione::m68k::TraceRegisterState &expected) {
            std::stringstream str;
            uint32_t actVal, expVal;
            bool differs;

            // which register to read for a given iteration
            constexpr static const std::array<size_t, 16> kActualRegOffset{{
                0, 1, 2, 3, 0, 1, 2, 3, 4, 5, 6, 7, 4, 5, 6, 7
            }};
            // whether to print the actual (1) or expected (0) value
            constexpr static const std::array<size_t, 16> kRowType{{
                1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0
            }};
            // whether a newline is produced after an iteration
            constexpr static const std::array<bool, 16> kNewline{{
                false, false, false, true, false, false, false, true, false, false, false, true,
                false, false, false, true
            }};

            // data registers
            for(size_t i = 0; i < 16; i++) {
                actVal = actual.d[kActualRegOffset[i]];
                expVal = expected.d[kActualRegOffset[i]];
                differs = (actVal != expVal);

                if(differs) str << "\x1b[1;37;41m";
                else str << "\x1b[1;97;42m";

                if(kRowType[i] == 1) str << fmt::format(" D{:1d} ${:08x}", kActualRegOffset[i],
                        actVal);
                else str << fmt::format("    ${:08x}", expVal);

                str << "\x1b[0m";
                if(kNewline[i]) str << std::endl;
                else str << " ";
            }

            // address registers
            for(size_t i = 0; i < 16; i++) {
                actVal = actual.a[kActualRegOffset[i]];
                expVal = expected.a[kActualRegOffset[i]];
                differs = (actVal != expVal);

                if(differs) str << "\x1b[1;37;41m";
                else str << "\x1b[1;97;42m";

                if(kRowType[i] == 1) str << fmt::format(" A{:1d} ${:08x}", kActualRegOffset[i],
                        actVal);
                else str << fmt::format("    ${:08x}", expVal);

                str << "\x1b[0m";
                if(kNewline[i]) str << std::endl;
                else str << " ";
            }

            // SSP, PC, status
            std::array<uint32_t, 3> actualRegs{{
                actual.ssp, actual.pc, actual.status
            }}, expectedRegs{{
                expected.ssp, expected.pc, expected.status
            }};
            static constexpr const std::array<std::string_view, 3> kRegNames{{
                "SSP", " PC", " SR"
            }};

            constexpr static const std::array<size_t, 6> kActualRegOffset2{{
                0, 1, 2, 0, 1, 2
            }};
            constexpr static const std::array<size_t, 6> kRowType2{{
                1, 1, 1, 0, 0, 0
            }};
            constexpr static const std::array<bool, 6> kNewline2{{
                false, false, true, false, false, true
            }};

            for(size_t i = 0; i < 6; i++) {
                actVal = actualRegs[kActualRegOffset2[i]];
                expVal = expectedRegs[kActualRegOffset2[i]];
                differs = (actVal != expVal);

                if(differs) str << "\x1b[1;37;41m";
                else str << "\x1b[1;97;42m";

                if(kRowType2[i] == 1) str << fmt::format("{} ${:08x}", kRegNames[i%3],
                        actVal);
                else str << fmt::format("    ${:08x}", expVal);

                str << "\x1b[0m";
                if(kNewline2[i]) str << std::endl;
                else str << " ";
            }

            // output directly to stderr
            std::cerr << str.str() << std::endl;
        }
};

#endif
