Here are tests for the 68000 plugin, which work by allocating a whole system and executing some test plugins.

Many of the tests work by comparing the results of these programs against instruction traces, captured on real hardware or other emulators. These traces are stored in the `ref` directory.