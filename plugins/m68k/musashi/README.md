This directory contains files necessary to build the [Musashi](https://github.com/kstenerud/Musashi) 68000 emulation core. It's automagically fetched from GitHub, and the opcode table generator is invoked as part of its build process; this then results in a static library the plugin links against.

In the `config` directory is a header file that defines configuration options for the library.