#ifndef MUSASHI_CONF_H
#define MUSASHI_CONF_H

// include the original config file (for some sane defaults) and constants
#include "m68kconf.h"

// limit to just the 68000 for now (reduce binary size)
#define M68K_EMULATE_EC020              OPT_OFF
#define M68K_EMULATE_020                OPT_OFF
#define M68K_EMULATE_030                OPT_OFF
#define M68K_EMULATE_040                OPT_OFF

// use callbacks for interrupts and resets
#define M68K_EMULATE_INT_ACK            OPT_OFF
#define M68K_EMULATE_RESET              OPT_ON

// instruction hooks (for logging)
#define M68K_INSTRUCTION_HOOK           OPT_ON

// XXX: enable support trace exceptions
#define M68K_EMULATE_TRACE              OPT_OFF

// XXX: enable prefetch and address errors
#define M68K_EMULATE_PREFETCH           OPT_OFF
#define M68K_EMULATE_ADDRESS_ERROR      OPT_OFF

#endif
