# Motorola 68000 Plugin
This plugin provides the 68000 emulation.

Several 68k assembly test programs are located in the `testrom` directory. The [vasm](http://sun.hasenbraten.de/vasm/) assembler is suggested for assembling these as needed.

## Emulation Cores
Multiple 68k cores are provided, primarily for testing purposes. The following cores are available:

- Custom 68000 core: This was written from scratch, based off of patents, microcode dumps, and analysis of real hardware. It is intended to be hardware accurate to the bus cycle level, and aims to implement correct timing for all instructions.
- [Musashi](https://github.com/kstenerud/Musashi): A mature, widely-used 68k core with an emphasis on speed.
	- *Note*: Because Musashi internally uses global static data, only one core can execute at any given time. Multiple cores can be created, and will be correctly arbitrated between, but the actual instruction execution will be serial between all instances.

## Tests
When building of tests is enabled globally, we'll build some tests to verify the correctness of the 68000 cores. These work by instantiating systems and emulating some test binaries, and comparing instruction traces against what is expected.