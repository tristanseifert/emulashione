#include "M68kPlugin.h"
#include "version.h"

#include "68000/Processor.h"

#include "musashi/Musashi68000.h"

#include <Logger.h>
#include <interfaces/Info.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k;

const std::string_view M68kPlugin::kVersion{gVERSION_SHORT};

/**
 * Initializes the plugin; we register our device types here.
 */
void M68kPlugin::init() {
    Plugin::RegisterDevice<m68000::Processor>();

    Plugin::RegisterDevice<musashi::Musashi68000>();
}

/// Define the plugin info struct
EMULASHIONE_PLUGIN_DESC(M68kPlugin,0x36,0xBD,0xCF,0x6C,0x88,0x9F,0x4E,0x6C,0x8E,0x38,0x4F,0xBD,
        0xA3,0x8C,0xC8,0x9B);

