#ifndef OPCODETABLE_H
#define OPCODETABLE_H

#include <algorithm>
#include <array>
#include <cstddef>
#include <cstdint>
#include <locale>
#include <numeric>
#include <span>
#include <stdexcept>
#include <string>
#include <string_view>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <ctre.hpp>
#include <fmt/core.h>

namespace emulashione::m68k {
/**
 * @brief Jump table for opcode dispatching
 *
 * This holds an array that has the handler functions for all possible instructions registered, and
 * provides many convenience accessors for accomplishing this registration.
 *
 * @tparam Callback Type of the instruction processing method. Its first parameter _must_ be
 *         OpcodeType and will contain the opcode executed.
 * @tparam Bits Size of an opcode, in bits
 * @tparam OpcodeType Integer type to use for opcodes; must be at least Bits wide
 */
template<typename Callback, size_t Bits = 16, typename OpcodeType = uint16_t>
class OpcodeTable {
    protected:
        /**
         * @brief Values to be substituted into a pattern
         *
         * Defines all values taken on by a substitution token, as well as its bit width.
         */
        struct Substitution {
            /// Number of bits in the substitution
            size_t width{0};
            /// All values that this substitution token carries
            std::unordered_set<OpcodeType> values;
        };

        /**
         * @brief Piece of a pattern with a common type
         *
         * Defines a single component (that is, a consecutive region of one or more bits of the
         * same type -- literal, wildcard, or substitution word) of an opcode's encoding.
         */
        struct Component {
            enum class Type: uint8_t {
                Unknown                 = 0,
                Literal                 = 1,
                Wildcard                = 2,
                Substitution            = 3,
            };

            /// Type of this component, indicates how it is processed
            Type type{Type::Unknown};
            /// For substitution, the substitution character
            char substitution{0};
            /// For literal, the value
            OpcodeType value{0};
            /// Width of this component
            size_t width{0};

            /*
             * Calculated prefixes for this component. These are generated when the component is
             * read in. Note that they are not shifted; only the low `width` bits are significant.
             */
            std::vector<OpcodeType> prefixes;

            /**
             * Get the component type corresponding to the given character.
             *
             * @param ch Character of a pattern string
             *
             * @return Corresponding component type
             */
            constexpr static inline auto TypeFor(const char ch) {
                switch(ch) {
                    // wildcard
                    case '*':
                        return Type::Wildcard;
                    // literal
                    case '0':
                    case '1':
                        return Type::Literal;
                    // anything else is assumed to be a substitution
                    default:
                        return Type::Substitution;
                }
            }

            /**
             * @brief Resolve the component's values
             *
             * Generate all valid prefix values, given a list of substitutions. Multiple values
             * may be generated.
             *
             * @remark When invoking this method, you must ensure that any referenced
             *         substitutions already exist.
             *
             * @param subs Map of all substitutions
             */
            void resolve(const std::unordered_map<char, Substitution> &subs) {
                this->prefixes.clear();

                switch(this->type) {
                    case Type::Wildcard:
                        this->prefixes.reserve(1UL << this->width);
                        for(size_t i = 0; i < (1UL << this->width); i++) {
                            this->prefixes.emplace_back(i);
                        }
                        break;

                    case Type::Literal:
                        this->prefixes.emplace_back(this->value);
                        break;

                    case Type::Substitution: {
                        const auto &sub = subs.at(this->substitution);
                        this->prefixes.reserve(sub.values.size());
                        std::copy(sub.values.begin(), sub.values.end(),
                                std::back_inserter(this->prefixes));
                        break;
                    }

                    default:
                        throw std::logic_error("cannot resolve component of unknown type");
                }
            }
        };

    public:
        /// Total number of instructions in this table
        constexpr static const size_t kNumInstructions{1 << Bits};

        /**
         * Ensure that all opcodes are cleared when the opcode table is initialized first, so we
         * can easily catch any unregistered opcodes by crashing.
         */
        OpcodeTable() {
            this->reset();
        }

        /**
         * @brief Run handler for an opcode
         *
         * Executes an instruction by invoking its handler from the table.
         *
         * @param opcode Opcode to execute a handler for
         */
        template<class... Args>
        void operator()(const OpcodeType opcode, Args&&... args) {
            handlers[opcode](opcode, std::forward<Args>(args)...);
        }

        /**
         * @brief Return the installed handler for an opcode.
         *
         * @param opcode Opcode value
         */
        auto &operator[](const OpcodeType opcode) {
            return handlers[opcode];
        }

        /**
         * @brief Resets all currently installed handlers to `nullptr`.
         */
        void reset() {
            std::fill(handlers.begin(), handlers.end(), nullptr);
        }

        /**
         * @brief Add instruction handler with literal opcode
         *
         * Registers an instruction with a single constant value in the opcode table.
         *
         * @param cb Callback function to install
         * @param opcode Opcode value to insert a handler for
         */
        void add(Callback cb, const OpcodeType opcode) {
            handlers[opcode] = cb;
        }

        /**
         * @brief Add instruction handler with bit pattern
         *
         * Registers an instruction based on a bit pattern. This represents ranges of instructions
         * better than just a fixed mask based comparison.
         *
         * @param cb Callback function to install
         * @param pattern Bit pattern for this instruction. This must have as many characters as
         *        the opcode table has Bits, and is read left (MSB) to right (LSB). This pattern
         *        can have various components at each bit position:
         *        - **Literal:** A character of either `0` or `1` indicates this bit position is
         *          fixed at the given value.
         *        - **Wildcard:** This bit is "don't care," and can be either 0 or 1. It is
         *          indicated with the asterisk (*) character.
         *        - **Substitution:** One or more consecutive alphabetic characters (A-Z) that
         *          will be substituted with all values of this substitution variable. See the
         *          substitutions parameter. Any combination of characters is allowed.
         *        Note that any spaces or apostrophes are ignored, so they can be used for
         *        formatting and readability purposes.
         * @param substitutionStr Defines values for the named substitutions (A-Z); this is a space
         *        separated list in the format of `An=a,b,c`. `An` refers to the substitution
         *        character, and the number of occurrences of the character indicates how many bits
         *        the substitution fits. After the equals sign can be one of two token types:
         *        - **Literal:** Binary strings that are copied as-is into the allowed values for
         *          this substitution (`AAAA=0010`)
         *        - **Range:** Defines a range of binary values; all values in this range are added
         *          to the substitution. For example, `AAAA=1000-1010` is equivalent to
         *          `AAAA=1000,1001,1010`.
         *         
         *        Both literals and ranges can be combined in the same definition, but care
         *        should be taken to ensure no value is specified twice.
         *
         * @remark When using substitutions, the number of times the character occurs is relevant
         *         in both the pattern (obviously) but also in the definition of the
         *         substitution; if they do not match exactly, the substitution will fail.
         *
         *         Any space or apostrophe (') characters in the pattern are ignored.
         *
         * @note It's not permitted to add an opcode in a slot that already has an opcode
         *       registered through this mechanism.
         *
         * @return Number of instructions installed
         *
         * @throw std::invalid_argument If the pattern or substitution values are invalid
         * @throw std::runtime_error An opcode handler has already been registered
         * @throw std::logic_error Something went wrong in the parsing logic; this should never
         *        happen with valid inputs.
         */
        size_t add(Callback cb, const std::string &pattern,
                const std::string &substitutionStr = "") {
            size_t added{0};

            std::unordered_map<char, Substitution> subs;
            if(!substitutionStr.empty()) {
                ParseSubstitutions(subs, substitutionStr);
            }

            /*
             * Parse the bit pattern, character by character, greedily coalescing it into the
             * smallest number of components.
             *
             * Once that's done, validate it and resolve its component values. Doing that now
             * helps avoid the overhead of repeating this every time we recurse.
             */
            std::vector<Component> components;

            size_t bitOff{0};
            while(bitOff < pattern.length()) {
                // skip this character if it's a whitespace/ignored type
                const auto typeChar = pattern[bitOff];
                if(typeChar == ' ' || typeChar == '\'') {
                    bitOff++;
                    continue;
                }

                // otherwise, get substring and parse it
                const auto remaining = pattern.substr(bitOff);
                const auto comp = GetNextComponent(remaining);
                bitOff += comp.width;
                components.emplace_back(std::move(comp));
            }

            // validate the pattern and resolve the components
            ValidatePattern(components, subs);

            for(auto &c : components) {
                c.resolve(subs);
            }

            /*
             * Recursively process the components. This works by having each component generate a
             * list of prefixes, and for each prefix, invoking recursively the processing method of
             * the next component; or, if it's the last component, inserting the accumulated input
             * prefix, and its value.
             *
             * This is kind of a fucked explanation but I think this works lmao it breaks if there
             * are fewer than two components but that's the minimum for this to really be useful
             * anyhow so meh.
             */
            this->processComponent(0, components, 0, subs, added, cb);

            return added;
        }

        /**
         * @brief Install the given handler for all unallocated opcodes
         *
         * Installs a handler for all unmapped instructions. This can be used to trap any
         * unimplemented instructions to processor exceptions, for example.
         *
         * @param cb Handler function to install
         *
         * @return Number of opcodes the trap handler was installed for
         */
        size_t addUnmappedTrap(Callback cb) {
            size_t illegals{0};
            for(auto it = handlers.begin(); it != handlers.end(); it++) {
                if(*it) continue;
                *it = cb;

                illegals++;
            }

            return illegals;
        }


    protected:
        /**
         * @brief Recursively register an opcode.
         *
         * Recurses through all components, building up a tree of prefixes, and eventually
         * inserting the handler specified for all valid opcodes.
         *
         * @param offset Offset from the leftmost bit (MSB) of the expression
         * @param components All components in this opcode
         * @param componentIdx Index of the current component
         * @param substitutions Any substitution values defined
         * @param outNumAdded A counter to increment for every added opcode
         * @param handler Opcode handler function to insert
         * @param inPrefix Prefix value to OR to the generated value when inserting recursively
         */
        void processComponent(size_t offset, const std::span<Component> &components,
                const size_t componentIdx,
                const std::unordered_map<char, Substitution> &substitutions,
                size_t &outNumAdded, Callback handler, const OpcodeType inPrefix = 0) {
            const auto &c = components[componentIdx];

            // recursive case; invoke the next handler with all of this one's prefixes
            if(componentIdx != (components.size() - 1)) {
                for(const auto prefix : c.prefixes) {
                    // calculate the prefix shifted appropriately
                    auto shiftedPrefix = inPrefix;
                    shiftedPrefix |= (prefix << (Bits - c.width - offset));

                    // recurse
                    const auto nextOff = offset + c.width;
                    this->processComponent(nextOff, components, componentIdx + 1, substitutions,
                            outNumAdded, handler, shiftedPrefix);
                }
            }
            /*
             * End case; insert. We can simply OR the prefixes of the last component in, since they
             * should align to the least significant bit.
             */
            else {
                for(const auto prefix : c.prefixes) {
                    // calculate opcode and validate
                    auto opcode = inPrefix;
                    opcode |= prefix;

                    if(this->handlers[opcode]) {
                        throw std::logic_error(fmt::format("invalid pattern; there's already an "
                                    "instruction for opcode ${:04x}!", opcode));
                    }

                    // add it
                    this->handlers[opcode] = handler;
                    outNumAdded++;
                }
            }
        }

        /**
         * @brief Get next component from start of string
         *
         * Given a substring of a bit pattern, extract from it the longest consecutive component of
         * the same type, extracting the required payload data.
         */
        static Component GetNextComponent(const std::string &pattern) {
            Component c{};

            /*
             * The type of the component is determined by the first character of the pattern; and
             * for substitutions, this also defines the source of its value.
             */
            c.type = Component::TypeFor(pattern[0]);
            if(c.type == Component::Type::Substitution) {
                c.substitution = pattern[0];
            }

            /*
             * Read until we find a character that indicates a different type, to determine the
             * length of this group.
             *
             * Once we've established that, and the type is a literal, get the substring that this
             * region defines, and convert it to an integer value.
             */
            for(size_t i = 0; i < pattern.length(); i++) {
                auto ch = pattern[i];

                // if this character is for something else, bail
                if(Component::TypeFor(ch) != c.type) {
                    break;
                }
                // also skip if it's a whitespace type
                else if(ch == ' ' || ch == '\'') {
                    // XXX: do we need to skip them for strtoul too?
                    continue;
                }
                // did a different substitution start?
                else if(c.type == Component::Type::Substitution && ch != c.substitution) {
                    break;
                }

                // increment its width
                c.width++;
            }

            if(c.type == Component::Type::Literal) {
                const auto substr = pattern.substr(0, c.width);
                c.value = strtoul(substr.c_str(), nullptr, 2);
            }

            return c;
        }

        /**
         * @brief Validate parsed pattern
         *
         * Ensures that the specified pattern, after being converted to components, is valid and
         * can be resolved.
         *
         * @param components All components making up the bit pattern
         * @param subs All known substitutions
         *
         * @throw std::invalid_argument Bit pattern or components are invalid
         */
        static void ValidatePattern(const std::span<Component> &components,
                const std::unordered_map<char, Substitution> &subs) {
            // ensure bit length matches
            const size_t totalBits = std::accumulate(components.begin(), components.end(), 0,
                    [](size_t bits, auto &component) -> size_t {
                return bits + component.width;
            });

            if(totalBits != Bits) {
                throw std::logic_error(fmt::format("Failed to parse bit pattern, expected "
                            "{} bits, got {}", Bits, totalBits));
            }

            // next, check substitution names (and their lengths)
            for(const auto &c : components) {
                if(c.type != Component::Type::Substitution) continue;

                try {
                    const auto &sub = subs.at(c.substitution);
                    if(sub.width != c.width) {
                        throw std::invalid_argument(fmt::format("Width mismatch for substitution "
                                    "group '{}': pattern specified {} bits, but group is {}",
                                    c.substitution, c.width, sub.width));
                    }
                }
                // there's no such group
                catch(const std::out_of_range &) {
                    throw std::invalid_argument(fmt::format("Unknown substitution group '{}' "
                                "specified in pattern", c.substitution));
                }
            }
        }

        /**
         * @brief Parse substitutions string
         *
         * Extracts all of the valid values from the substitution string and builds up substitution
         * structs from it.
         */
        static void ParseSubstitutions(std::unordered_map<char, Substitution> &outSubs,
                const std::string &in) {
            for(auto [whole, unused2, _name, _contents]: ctre::range<"(([A-Za-z]+)(?:=)([\\d\\-,]*)(?:\\s*))">(in)) {
                const std::string_view name{_name}, contents{_contents};

                Substitution s;
                s.width = name.length();

                ParseSubstitutionValues(s, contents);

                if(s.values.empty()) {
                    throw std::runtime_error(
                            fmt::format("failed to parse substitutions for key '{}'", name));
                }

                outSubs.emplace(name[0], std::move(s));

                (void) whole, (void) unused2;
            }
        }

        /**
         * @brief Parse value string of substitution
         *
         * Parses the payload string, which is a comma separated list of either literals (`010`)
         * or ranges (`000-011`). For each of these, one or more entries in the substitution
         * is created.
         */
        static void ParseSubstitutionValues(Substitution &sub, const std::string_view &content) {
            // handle the no comma case
            if(content.find(',') == std::string::npos) {
                InsertSubstitutionsFor(sub, content);
            }
            // split by commas and handle each one
            else {
                std::string::size_type prevPos{0}, pos{0};
                while((pos = content.find(',', pos)) != std::string::npos) {
                    const std::string token(content.substr(prevPos, pos-prevPos));
                    InsertSubstitutionsFor(sub, token);
                    prevPos = ++pos;
                }

                // don't forget the last one
                std::string token(content.substr(prevPos, pos-prevPos));
                InsertSubstitutionsFor(sub, token);
            }
        }

        /**
         * @brief Inserts all substitution values for the given string.
         *
         * @param value A string containing either a literal or range
         *
         * @remark Ranges are inclusive of both the start and end values.
         */
        static void InsertSubstitutionsFor(Substitution &sub, const std::string_view &value) {
            // is it a literal?
            if(value.find('-') == std::string::npos) {
                const auto literal = strtoul(value.data(), nullptr, 2);
                sub.values.emplace(literal);
            }
            // otherwise, handle it as a range
            else {
                // get range bounds
                const auto startStr = value.substr(0, value.find('-')),
                      endStr = value.substr(value.find('-') + 1);

                const auto start = strtoul(startStr.data(), nullptr, 2),
                      end = strtoul(endStr.data(), nullptr, 2);

                if(end < start) {
                    throw std::runtime_error(fmt::format("invalid range for substitution: "
                                "start {:x} end {:x}", start, end));
                }

                // loop through the range and insert each value
                OpcodeType val = start;
                do {
                    sub.values.emplace(val);

                    val++;
                } while(val <= end);
            }
        }


    protected:
        /**
         * Instruction processing function storage
         *
         * This array is indexed directly by the opcode.
         */
        std::array<Callback, kNumInstructions> handlers;
};
}

#endif
