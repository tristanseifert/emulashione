#include "MusashiCore.h"
#include "TraceTypes.h"

#include <algorithm>
#include <cmath>
#include <utility>

#include <arpa/inet.h>

#include <m68k.h>

#include <Bus.h>
#include <Config.h>
#include <ClockSource.h>
#include <Logger.h>
#include <TraceManager.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k;
using namespace emulashione::m68k::musashi;

/**
 * @brief Lock protecting access to Musashi functions
 *
 * The Musashi emulator's design only allows using a single processor at a time, because it
 * relies on a global processor context. To prevent devices in different threads from trampling on
 * each other, use this lock to serialize access to the core.
 *
 * Performance of using a mutex here is probably pretty shit; we might look into using some more
 * lightweight primitives.
 */
std::mutex MusashiCore::gCoreLock;

/**
 * @brief Currently executing CPU core instance
 *
 * We keep a pointer to this to avoid the context switching overhead if no other cores have been
 * executed since this core's state was switched out.
 *
 * This plays a role in lazy context switching, which takes place any time the core executes a
 * certain number of clock cycles.
 */
MusashiCore *MusashiCore::gActiveCore{nullptr};


/**
 * Initialize the base core class.
 *
 * It will reserve memory for the context buffer.
 */
MusashiCore::MusashiCore(const std::string_view &name, struct emulashione_config_object *_cfg) :
    CooperativeDevice(name, _cfg) {
    registerAddressSpace(0, "Memory");

    registerClockSourceSlotName("clock");
    registerBusSlotName("bus");

    this->context.resize(m68k_context_size(), std::byte{0});

    // allocate a tracer if needed
    if(_cfg) {
        plugin::Config cfg(_cfg);
        if(cfg.hasKey("trace") && cfg.getBool("trace.enable")) {
            // TODO: replace max payload size with proper size
            this->tracer = std::make_unique<plugin::TraceManager>(0x0001, *this, 256);

            if(cfg.getBool("trace.memory")) {
                this->tracer->addMemoryOutput();
            }
        }
    }
}

/**
 * Saves the state of the currently active Musashi processor core into this core's context buffer.
 */
void MusashiCore::saveContext() {
     m68k_get_context(this->context.data());
}

/**
 * Restore the context of this core's context buffer into Musashi.
 */
void MusashiCore::restoreContext() {
    m68k_set_context(this->context.data());
}

/**
 * Begins emulation. This is where we validate the required connections have been made.
 */
void MusashiCore::willStartEmulation() {
    if(!this->bus) throw std::runtime_error("required connection `bus` missing");
    else if(!this->clk) throw std::runtime_error("required connection `clock` missing");

    // set up the core state
    std::lock_guard lg(gCoreLock);
    this->saveStateIfNeeded();

    m68k_init();
    m68k_set_reset_instr_callback(MusashiCore::HandleResetCallback);

    if(this->tracer) {
        Logger::Debug("Installing instruction callback for m68k core {}", (void *) this);
        m68k_set_instr_hook_callback(MusashiCore::InstructionHookCallback);
    }

    this->performCoreInit();

    this->saveContext();
}

/**
 * Handle a reset. This just sets a flag to be checked the next run through the main loop.
 */
void MusashiCore::reset(const bool) {
    bool no{false};
    if(!this->resetPending.compare_exchange_strong(no, true, std::memory_order_acquire,
                std::memory_order_relaxed)) {
        Logger::Warning("Attempt to reset Musashi instance ${} while reset is still pending",
                (void *) this);
    }
}



/**
 * Connects a bus; this stores a pointer to the bus on which the CPU sits, connected to slot `bus`.
 *
 * @param name Connection slot name
 * @param bus Bus to connect to this slot
 */
void MusashiCore::connectBus(const std::string &name, const std::shared_ptr<plugin::Bus> &bus) {
    super::connectBus(name, bus);
    if(name == "bus") {
        this->bus = bus;
    }
}

/**
 * Disconnects a bus; this resets the pointer to the CPU bus.
 *
 * @param name Connection slot name
 */
void MusashiCore::disconnectBus(const std::string &name) {
    if(name == "bus") {
        this->bus.reset();
    }
    super::disconnectBus(name);
}

/**
 * Connects a clock source; this stores the pointer to the CPU clock, connected to slot `clock`.
 *
 * @param name Connection slot name
 * @param clock Clock source to connect to this slot
 */
void MusashiCore::connectClockSource(const std::string &name,
        const std::shared_ptr<plugin::ClockSource> &clock) {
    super::connectClockSource(name, clock);
    if(name == "clock") {
        this->clk = clock;
        Logger::Trace("New CPU clock: {} Hz", clock->getFrequency());
        // TODO: cache period with callback (for bus cycle calculation)
        this->clkPeriod = clock->getPeriod();
    }
}

/**
 * Disconnects a clock source; it resets the pointer to the CPU clock.
 *
 * @param name Connection slot name
 */
void MusashiCore::disconnectClockSource(const std::string &name) {
    if(name == "clock") {
        this->clk.reset();
    }
    super::disconnectClockSource(name);
}



/**
 * Main emulation loop of the core.
 *
 * This will attempt to acquire the global lock on entry (so the first go through the work loop
 * has the lock) and then just loop forever to process instructions.
 */
void MusashiCore::main() {
    // take lock and try to restore state and reset CPU
    gCoreLock.lock();
    this->restoreStateIfNeeded();

    this->doReset();

    // main opcode loop
    while(1) {

        const auto cycles = m68k_execute(2);
        this->didConsumeTicks(cycles);
    }
}

/**
 * Perform a reset of the core.
 *
 * @remark The core must be currently executing.
 */
void MusashiCore::doReset() {
    // log the reset event
    if(this->tracer) {
        this->tracer->putMessage(this->systemTime, kTraceMessageReset);
    }

    m68k_pulse_reset();
}

/**
 * An instruction has been fetched.
 */
void MusashiCore::instructionHook(const uint32_t pc) {
    TraceRegisterState trs{};
    trs.pc = pc;
    this->copyOutRegState(trs);

    this->tracer->putMessage(this->systemTime, kTraceMessageInstruction,
            {reinterpret_cast<std::byte *>(&trs), sizeof(trs)});
}

/**
 * Yeets the state of the processor's registers into the given struct.
 */
void MusashiCore::copyOutRegState(TraceRegisterState &trs) {
    trs.d[0] = m68k_get_reg(nullptr, M68K_REG_D0);
    trs.d[1] = m68k_get_reg(nullptr, M68K_REG_D1);
    trs.d[2] = m68k_get_reg(nullptr, M68K_REG_D2);
    trs.d[3] = m68k_get_reg(nullptr, M68K_REG_D3);
    trs.d[4] = m68k_get_reg(nullptr, M68K_REG_D4);
    trs.d[5] = m68k_get_reg(nullptr, M68K_REG_D5);
    trs.d[6] = m68k_get_reg(nullptr, M68K_REG_D6);
    trs.d[7] = m68k_get_reg(nullptr, M68K_REG_D7);

    trs.a[0] = m68k_get_reg(nullptr, M68K_REG_A0);
    trs.a[1] = m68k_get_reg(nullptr, M68K_REG_A1);
    trs.a[2] = m68k_get_reg(nullptr, M68K_REG_A2);
    trs.a[3] = m68k_get_reg(nullptr, M68K_REG_A3);
    trs.a[4] = m68k_get_reg(nullptr, M68K_REG_A4);
    trs.a[5] = m68k_get_reg(nullptr, M68K_REG_A5);
    trs.a[6] = m68k_get_reg(nullptr, M68K_REG_A6);
    //trs.a[7] = m68k_get_reg(nullptr, M68K_REG_A7);
    trs.a[7] = m68k_get_reg(nullptr, M68K_REG_USP);

    trs.ssp = m68k_get_reg(nullptr, M68K_REG_A7);
    trs.status = m68k_get_reg(nullptr, M68K_REG_SR);
}

/**
 * Calls back into the core emulation library to notify the scheduler the core has consumed the
 * given number of ticks. It ensures the core lock is released before the call, and re-acquired
 * when we return.
 *
 * When returning, we'll restore our context (if the current core context is different) and save
 * the previous core's state.
 *
 * @param ticks Number of clock cycles consumed
 */
void MusashiCore::didConsumeTicks(const size_t ticks) {
    gCoreLock.unlock();

    const auto memTicks = std::exchange(this->memoryWaitCycles, 0);
    this->schedDidAdvanceBy(ticks + memTicks, this->systemTime);

    // relock and perform the lazy context switch
    gCoreLock.lock();
    this->restoreStateIfNeeded();
}

/**
 * Convert the given time required for a bus access (in nanoseconds) to clock ticks.
 *
 * @param time Time taken for a bus cycle, in nanoseconds
 */
void MusashiCore::addBusCycles(const double time) {
    // number of clock cycles (fractional)
    const auto cycles = time / this->clkPeriod;
    // round up to nearest multiple of 2
    const size_t clocks = std::min(2., std::ceil(cycles / 2.) * 2.);

    this->memoryWaitCycles += (clocks > 2) ? clocks : 0;
}

/**
 * The core executed a reset instruction.
 *
 * @todo Actually do stuff here; right now it just aborts execution
 */
void MusashiCore::handleReset() {
    Logger::Warning("Encountered RESET (pc = ${:08x})", m68k_get_reg(nullptr, M68K_REG_PPC));
    this->schedAbort(-420);
}

/**
 * Allow copying out of both the trace handler and the trace handler's memory output under the
 * `tracer` and `traceMemOut` keys. Both of these are pointer sized.
 */
size_t MusashiCore::copyOutInfo(const std::string_view &key, std::span<std::byte> &outBuf) {
    if(key == "tracer") {
        void *ptr = this->tracer.get();
        const auto copied = std::min(sizeof(ptr), outBuf.size());
        memcpy(outBuf.data(), &ptr, copied);
        return copied;
    } else if(key == "traceMemOut") {
        void *ptr = this->tracer->getMemOutput();
        const auto copied = std::min(sizeof(ptr), outBuf.size());
        memcpy(outBuf.data(), &ptr, copied);
        return copied;
    }

    return super::copyOutInfo(key, outBuf);
}


/**
 * Log information about a bus error.
 */
void MusashiCore::logBusError(const uint32_t addr, const bool read, const size_t size) {
    TraceRegisterState trs{};
    trs.pc = m68k_get_reg(nullptr, M68K_REG_PPC);
    this->copyOutRegState(trs);

    Logger::Warning("Bus error on {} from ${:08x} ({})\n{}", read ? "read" : "write", addr,
            (size == 2) ? "word" : "byte", trs);
}


/*
 * Bus access handlers for Musashi
 *
 * These always perform the access directly against the currently executing core's bus.
 *
 * @remark Currently, we do not emulate any function codes (all accesses go to the default address
 *         space 0,) nor do we properly emulate longword accesses.
 */
extern "C" {
/**
 * @brief Read an 8-bit byte from memory
 *
 * @param address Physical memory address
 */
unsigned int m68k_read_memory_8(unsigned int address) {
    uint8_t data;
    double time{0};
    auto active = MusashiCore::gActiveCore;

    try {
        active->bus->read(active->getInstancePtr(), 0, address, data, time);
    }
    catch(const Bus::BusError &e) {
        active->logBusError(address, true, 1);
        m68k_pulse_bus_error();
    }

    if(MusashiCore::kLogReads) Logger::Trace("Read byte from ${:08x} = ${:02x} ({} ns)", address,
            data, time);
    active->addBusCycles(time);
    return data;
}

/**
 * @brief Write an 8-bit byte to memory
 *
 * @param address Physical memory address
 * @param value Value to write; only the low 8 bits are relevant.
 */
void m68k_write_memory_8(unsigned int address, unsigned int value) {
    double time{0};
    auto active = MusashiCore::gActiveCore;

    try {
        const auto temp = static_cast<uint8_t>(value);
        active->bus->write(active->getInstancePtr(), 0, address, temp, time);
    }
    catch(const Bus::BusError &e) {
        active->logBusError(address, false, 1);
        m68k_pulse_bus_error();
    }

    if(MusashiCore::kLogWrites) Logger::Trace("Write byte to ${:08x} = ${:02x}", address, value);
    active->addBusCycles(time);
}

/**
 * @brief Read a 16-bit word from memory
 *
 * @param address Physical memory address
 */
unsigned int m68k_read_memory_16(unsigned int address) {
    uint16_t data;
    double time{0};
    auto active = MusashiCore::gActiveCore;

    try {
        active->bus->read(active->getInstancePtr(), 0, address, data, time);
    }
    catch(const Bus::BusError &e) {
        active->logBusError(address, true, 2);
        m68k_pulse_bus_error();
    }

    // data must be byteswapped
    data = ntohs(data);

    if(MusashiCore::kLogReads) Logger::Trace("Read word from ${:08x} = ${:04x}", address, data,
            time);
    active->addBusCycles(time);
    return data;
}

/**
 * @brief Write a 16-bit word to memory
 *
 * @param address Physical memory address
 * @param value Value to write; only the low 16 bits are relevant.
 */
void m68k_write_memory_16(unsigned int address, unsigned int value) {
    double time{0};
    auto active = MusashiCore::gActiveCore;

    try {
        // data must be byteswapped
        const auto temp = htons(static_cast<uint16_t>(value));
        active->bus->write(active->getInstancePtr(), 0, address, temp, time);
    }
    catch(const Bus::BusError &e) {
        active->logBusError(address, false, 2);
        m68k_pulse_bus_error();
    }

    if(MusashiCore::kLogWrites) Logger::Trace("Write word to ${:08x} = ${:04x}", address, value);
    active->addBusCycles(time);
}

/**
 * @brief Read a 32-bit longword from memory
 *
 * @param address Physical memory address
 */
unsigned int m68k_read_memory_32(unsigned int address) {
    uint32_t temp{0};

    temp = (m68k_read_memory_16(address) & 0xFFFF) << 16;
    temp |= (m68k_read_memory_16(address + 2) & 0xFFFF);

    if(MusashiCore::kLogReads) Logger::Trace("Read longword from ${:08x} = ${:08x}", address,
            temp);
    return temp;
}

/**
 * @brief Write a 32-bit longword to memory
 *
 * @param address Physical memory address
 * @param value Value to write; only the low 32 bits are relevant.
 */
void m68k_write_memory_32(unsigned int address, unsigned int value) {
    m68k_write_memory_16(address, (value >> 16));
    m68k_write_memory_16(address + 2, (value & 0xFFFF));

    if(MusashiCore::kLogWrites) Logger::Trace("Write longword to ${:08x} = ${:08x}", address, value);
}
}
