#include "Musashi68000.h"

#include <m68k.h>

#include <Logger.h>
#include <interfaces/Device.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k::musashi;

/// Indicates the version of Musashi in use
/// TODO: update this automatically when the code changes
const std::string_view Musashi68000::kVersion{"4.60"};

EMULASHIONE_DEVICE_DESC(Musashi68000, 0, kEmulationModeCoThread,
        0xA9,0x84,0xC3,0xCE,0x9F,0x73,0x47,0xA4,0xB0,0x17,0x48,0x29,0x51,0x40,0x27,0xB4);



/**
 * Ensures the core is initialized as a 68000.
 */
void Musashi68000::performCoreInit() {
    m68k_set_cpu_type(M68K_CPU_TYPE_68000);
}
