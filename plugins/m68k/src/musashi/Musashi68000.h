#ifndef MUSASHI_68000_H
#define MUSASHI_68000_H

#include <string_view>

#include "MusashiCore.h"

namespace emulashione::m68k::musashi {
/**
 * @brief 68000 processor using the Musashi backend
 */
class Musashi68000: public MusashiCore {
    public:
        Musashi68000(const std::string_view &name, struct emulashione_config_object *cfg) :
            MusashiCore(name, cfg) {};

    protected:
        void performCoreInit() override;

    public:
        static const struct emulashione_device_descriptor kDeviceDescriptor;

        constexpr static const std::string_view kDisplayName{"Motorola 68000 (Musashi)"};
        constexpr static const std::string_view kShortName{"musashi68000"};
        constexpr static const std::string_view kAuthors{"Karl Stenerud, Tristan Seifert"};
        constexpr static const std::string_view kDescription{
            "Motorola 68000 emulation using the Musashi backend"
        };
        constexpr static const std::string_view kInfoUrl{"https://github.com/kstenerud/Musashi"};
        static const std::string_view kVersion;
};
}

#endif
