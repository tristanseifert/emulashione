#ifndef MUSASHI_MUSASHICORE_H
#define MUSASHI_MUSASHICORE_H

#include <atomic>
#include <cstddef>
#include <memory>
#include <mutex>
#include <string_view>
#include <vector>

#include <CooperativeDevice.h>
#include <TraceManager.h>

#include <m68k.h>

namespace emulashione::m68k {
struct TraceRegisterState;
}

namespace emulashione::m68k::musashi {
/**
 * @brief Base class for Musashi-based devices
 *
 * This abstract class implements most of the behaviors to implement a Musashi based emulation
 * device. It implements the shims to translate to/from device calls and provides the callbacks for
 * bus access and other events for the Musashi core.
 *
 * Subclasses should implement this class and customize the initialization behavior.
 *
 * \section sec_connections
 *
 * The following connections can be made to this device:
 *
 * - Busses
 *   - `bus`: Main bus all memory read/write operations take place against
 * - Clock
 *   - `clk`: Processor clock input
 * - Devices: None
 */
class MusashiCore: public plugin::CooperativeDevice {
    friend unsigned int ::m68k_read_memory_8(unsigned int);
    friend unsigned int ::m68k_read_memory_16(unsigned int);
    friend unsigned int ::m68k_read_memory_32(unsigned int);
    friend void ::m68k_write_memory_8(unsigned int, unsigned int);
    friend void ::m68k_write_memory_16(unsigned int, unsigned int);
    friend void ::m68k_write_memory_32(unsigned int, unsigned int);

    friend class Common;

    using super = plugin::CooperativeDevice;

    public:
        MusashiCore(const std::string_view &name, struct emulashione_config_object *cfg);
        ~MusashiCore() = default;

        void willStartEmulation() override;
        void reset(const bool isHard) override;

        void connectBus(const std::string &name,
                const std::shared_ptr<plugin::Bus> &bus) override;
        void disconnectBus(const std::string &name) override;
        void connectClockSource(const std::string &name,
                const std::shared_ptr<plugin::ClockSource> &clock) override;
        void disconnectClockSource(const std::string &name) override;

        void main() override;

        size_t copyOutInfo(const std::string_view &, std::span<std::byte> &) override;

    protected:
        void didConsumeTicks(const size_t ticks);

        /**
         * @brief Perform Musashi core initialization
         *
         * Called during device startup to initialize the Musashi core's state; the subclass shall
         * implement this method, and perform any processor state setup necessary.
         *
         * Once it returns, the current processor context is stored into the context buffer.
         */
        virtual void performCoreInit() = 0;

        void doReset();

    private:
        void saveContext();
        void restoreContext();

        void logBusError(const uint32_t addr, const bool read, const size_t size);

        void addBusCycles(const double);

        void copyOutRegState(TraceRegisterState &);

        /**
         * Save the state of the previously executing core, if any.
         */
        inline void saveStateIfNeeded() {
            if(gActiveCore && this != gActiveCore) {
                gActiveCore->saveContext();
            }
        }

        /**
         * Save the state of the previously executing core, and restore this core's context. The
         * operation is a no-op if the last core executing was this one.
         *
         * Additionally, gActiveCore is updated here.
         */
        inline void restoreStateIfNeeded() {
            if(this != gActiveCore) {
                if(gActiveCore) {
                    gActiveCore->saveContext();
                }

                gActiveCore = this;
                this->restoreContext();
            }
        }

        void handleReset();
        static void HandleResetCallback() {
            gActiveCore->handleReset();
        }

        void instructionHook(const uint32_t pc);
        static void InstructionHookCallback(unsigned int pc) {
            gActiveCore->instructionHook(pc);
        }

    protected:
        /**
         * @brief Processor context storage
         *
         * To support multiple simultaneous cores, Musashi allows different context buffers which
         * hold an instance's state. This buffer will hold this.
         */
        std::vector<std::byte> context;

        /**
         * @brief Indicates whether the core is currently executing
         *
         * This is set by the common code whenever code is executed in the context of this core,
         * and the Musashi emulation state corresponds to this core's context buffer.
         */
        bool isActive{false};

        /// Bus on which the CPU sits
        std::shared_ptr<plugin::Bus> bus;
        /// Main CPU clock
        std::shared_ptr<plugin::ClockSource> clk;

        /// Current system time point
        double systemTime{0};

    private:
        /**
         * @brief Indicates a reset operation is pending.
         *
         * This is set whenever the reste function is invoked; we check it on every loop through
         * the opcode handler since the core currently executing may not be this one.
         */
        std::atomic_bool resetPending{false};

        /// Period of the main CPU clock
        double clkPeriod{-1};

        /**
         * @brief Clock cycles used by bus wait state cycles
         *
         * Musashi includes the "standard" 2 clock cycle bus access timing in all instructions
         * (TODO: verify this) so we just have to count any bus cycles beyond that incurred by
         * memory accesses.
         */
        size_t memoryWaitCycles{0};

        /// If tracing is requested, the trace manager that receives the requests
        std::unique_ptr<plugin::TraceManager> tracer;

        static std::mutex gCoreLock;
        static MusashiCore *gActiveCore;

        /// Are read accesses logged?
        static constexpr const bool kLogReads{false};
        /// Are write accesses logged?
        static constexpr const bool kLogWrites{false};
};
}

#endif
