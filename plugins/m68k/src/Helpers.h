#ifndef M68000_HELPERS_H
#define M68000_HELPERS_H

#include <bitset>
#include <cstddef>
#include <cstdint>

namespace emulashione::m68k {

/**
 * Gets the most significant bit of an integer type.
 *
 * @param T Integer type; currently uint8_t, uint16_t, uint32_t are supported.
 *
 * @return A value of type `T` with only the most significant bit set.
 */
template <typename T>
inline constexpr T GetMsb();

template <>
inline constexpr uint8_t GetMsb<uint8_t>() {
    return (1 << 7);
}
template <>
inline constexpr uint16_t GetMsb<uint16_t>() {
    return (1 << 15);
}
template <>
inline constexpr uint32_t GetMsb<uint32_t>() {
    return (1 << 31);
}

/**
 * @brief Sign extends a number.
 *
 * @tparam T Type to extend to
 * @tparam B Number of bits of the original number, including sign
 *
 * @param x Original value to sign extend
 */
template <typename T, unsigned B>
inline T SignExtend(const T x) {
    struct {T x:B;} s;
    return s.x = x;
}

/**
 * Extracts a range of bits from a larger integral type.
 *
 * @tparam Msb Most significant bit of the value (0 based)
 * @tparam Length Number of bits in the field (1 based)
 * @tparam T Integer type to use
 *
 * @param val Value to extract the value from (must be at least `M` bits wide)
 *
 * @return Extracted value
 */
template<size_t Msb, size_t Length, typename T>
static inline constexpr T GetBits(const T val) {
    return (val >> (Msb - (Length - 1))) & ((1 << Length) - 1);
}

template<typename T>
static inline constexpr T GetBits(const T val, const size_t Msb, const size_t Length) {
    return (val >> (Msb - (Length - 1))) & ((1 << Length) - 1);
}

/**
 * Reverses a bitset in place.
 *
 * @tparam N size of the bitset
 *
 * @param b Bitset to reverse
 */
template<std::size_t N>
static inline void Reverse(std::bitset<N> &b) {
    for(std::size_t i = 0; i < N/2; ++i) {
        bool t = b[i];
        b[i] = b[N-i-1];
        b[N-i-1] = t;
    }
}

}

#endif
