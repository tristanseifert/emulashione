/*
 * Arithmetic - Bitwise AND: ANDI to CCR, ANDI to SR, ANDI, AND
 */
#include "Decoder.h"
#include "Decoder+Fmt.h"
#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k;
using namespace emulashione::m68k::m68000;

using namespace magic_enum::bitwise_operators;


/**
 * @brief AND
 *
 * Performs an AND between source and destination, and stores the result in the destination.
 */
void Decoder::And(const uint16_t opcode, Processor &proc) {
    // get some opcode info
    const auto storeToEa = !!GetBits<8,1>(opcode);
    const auto regNum = GetRegIndex<11>(opcode);
    const auto size = GetSize(opcode);
    const auto addrMode = GetAddrMode<5,2>(opcode);

    // resolve effective address and read from it
    auto resolved = ResolveEffectiveAddr(proc, addrMode, size, kAddrModeDataAlterable);
    const auto readData = ReadEffectiveAddr(proc, resolved, size);

    // perform operation, update flags, prefetch
    const uint32_t result = readData & proc.regs.getDn(regNum);

    proc.regs.clearCcrBits(Ccr::Overflow | Ccr::Carry);
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));

    PrefetchCycle(proc);

    // write back
    if(storeToEa) {
        WriteEffectiveAddr(proc, resolved, size, result);
    } else {
        switch(size) {
            case Size::Byte:
                proc.regs.storeData(regNum, static_cast<uint8_t>(result));
                break;
            case Size::Word:
                proc.regs.storeData(regNum, static_cast<uint16_t>(result));
                break;
            case Size::Longword:
                proc.regs.storeData(regNum, static_cast<uint32_t>(result));
                break;
            default: proc.abort(Processor::AbortReason::InternalError);
        }
    }

    /*
     * When storing into a register, _and_ it is a longword, add an extra no-op cycle. If the
     * source was _also_ a data register (or immediate), add another cycle.
     */
    if(!storeToEa && size == Size::Longword) {
        proc.microcycleNop();
        if(addrMode.isDataRegister() || addrMode.isImmediate()) {
            proc.microcycleNop();
        }
    }
}

/**
 * @brief ANDI
 *
 * Read an immediate value (one or two words, of which either 8, 16 or 32 bits are used) then AND
 * it against the contents of a specified effective address.
 *
 * @todo Validate when flags get updated during execution
 */
void Decoder::AndImmediate(const uint16_t opcode, Processor &proc) {
    // get opcode parameters and immediate word
    const auto size = GetSize(opcode);
    const auto addrMode = GetAddrMode<5,2>(opcode);

    const auto imm = ReadImmediate(proc, size);

    // resolve addressing mode and read data source; perform the operation
    auto resolved = ResolveEffectiveAddr(proc, addrMode, size, kAddrModeDataAlterable);
    const auto readData = ReadEffectiveAddr(proc, resolved, size);
    const uint32_t result = readData & imm;

    // update flags and perform prefetch
    proc.regs.clearCcrBits(Ccr::Overflow | Ccr::Carry);
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));

    PrefetchCycle(proc);

    // write back to destination
    WriteEffectiveAddr(proc, resolved, size, result);

    /*
     * If this is a longword op with a data reg destination, add two extra no-op cycles at the end
     *
     * This behavior is common with EORI, ORI, ANDI, SUBI, ADDI
     */
    if(size == Size::Longword && addrMode.isDataRegister()) {
        proc.microcycleNop();
        proc.microcycleNop();
    }
}

/**
 * @brief ANDI to SR, ORI to CCR
 *
 * AND the low 16 or 8 bits of the immediate (depending on encoding) to the status register.
 *
 * The only difference in handling is that ANDI to CCR only cosiders the low byte and does not
 * perform a privilege level check.
 */
void Decoder::AndImmediateToStatus(const uint16_t opcode, Processor &proc) {
    // encoding of the ANDI to SR opcode
    constexpr static const uint16_t kAndiToSr{0b0000'0010'0111'1100};

    // get immediate operand
    const auto imm = ReadImmediate(proc, Size::Word);

    // ensure supervisor mode for ANDI to SR
    if(opcode == kAndiToSr &&
            proc.regs.getPrivilegeLevel() != Processor::PrivilegeLevel::Supervisor) {
        PrefetchCycle(proc);
        return proc.setEvent(Processor::EventFlags::PrivilegeViolation);
    }

    // perform the AND operation
    auto val = proc.regs.getStatus();
    val = (opcode == kAndiToSr) ? (val & imm) : (((val & imm) & 0xFF) | (val & ~0xFF));
    proc.regs.setStatus(val);

    // there are four nop microcycles here, ostensibly when the operation is performed?
    for(size_t i = 0; i < 4; i++) {
        proc.microcycleNop();
    }

    // perform final prefetch
    PrefetchCycle(proc);
}

