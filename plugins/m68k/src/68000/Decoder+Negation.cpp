/*
 * Arithmetic - Sign extension and negation: EXT, NOT, NEG
 */
#include "Decoder.h"
#include "Decoder+Fmt.h"
#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k;
using namespace emulashione::m68k::m68000;

using namespace magic_enum::bitwise_operators;

/**
 * @brief NOT
 *
 * Calculate the one's complement (inverse) of the destination operand and write the result back to
 * it.
 *
 * V, C are always cleared, N and Z are set as normal.
 */
void Decoder::LogicalComplement(const uint16_t opcode, Processor &proc) {
    // resolve the effective address
    const auto size = GetSize(opcode);
    const auto addrMode = GetAddrMode<5,2>(opcode);

    const auto resolved = ResolveEffectiveAddr(proc, addrMode, size, kAddrModeDataAlterable);

    // read the original data, negate it and perform prefetch
    const auto result = ~ReadEffectiveAddr(proc, resolved, size);

    PrefetchCycle(proc);

    // then write it back, and update the flags
    WriteEffectiveAddr(proc, resolved, size, result);

    proc.regs.clearCcrBits(Ccr::Overflow | Ccr::Carry);
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));

    // longword data register implies an extra noop
    if(size == Size::Longword && addrMode.isDataRegister()) {
        proc.microcycleNop();
    }
}

/**
 * @brief NEG
 *
 * Calculate the two's complement of the destination operand, and write the result back to it; this
 * is equivalent to doing (0 - dest).
 *
 * The N, Z, V, C bits are updated.
 */
void Decoder::Negate(const uint16_t opcode, Processor &proc) {
    // resolve the effective address
    const auto size = GetSize(opcode);
    const auto addrMode = GetAddrMode<5,2>(opcode);

    const auto resolved = ResolveEffectiveAddr(proc, addrMode, size, kAddrModeDataAlterable);

    // read original data and do prefetch
    const auto source = ReadEffectiveAddr(proc, resolved, size);
    PrefetchCycle(proc);

    // calculate result, write it back and update flags
    const auto result = 0 - source;
    WriteEffectiveAddr(proc, resolved, size, result);

    proc.regs.setCcr(Ccr::Extend | Ccr::Carry, !!result);
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));
    proc.regs.setCcr(Ccr::Overflow, (result & SignBitFor(size)) && (source & SignBitFor(size)));

    // longword data register implies an extra noop
    if(size == Size::Longword && addrMode.isDataRegister()) {
        proc.microcycleNop();
    }
}

/**
 * @brief EXT
 *
 * Sign extend a byte to word, or a word to longword, by replicating its sign bit.
 *
 * V, C are always cleared, and N and Z are updated.
 */
void Decoder::ExtendSign(const uint16_t opcode, Processor &proc) {
    uint32_t result{0};
    const auto regIndex = GetRegIndex<2>(opcode);
    const auto Dn = proc.regs.getDn(regIndex);
    Size size{Size::Word};

    // extend word to longword?
    if(GetBits<6,1>(opcode)) {
        size = Size::Longword;
        result = SignExtend<int32_t,16>(Dn & 0xFFFF);
        proc.regs.storeData(regIndex, result);
    }
    // extend byte to word
    else {
        result = SignExtend<int32_t,8>(Dn & 0xFF);
        proc.regs.storeData(regIndex, static_cast<uint16_t>(result));
    }

    // update flags and prefetch
    proc.regs.setCcr(Ccr::Overflow | Ccr::Carry, false);
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));

    PrefetchCycle(proc);
}
