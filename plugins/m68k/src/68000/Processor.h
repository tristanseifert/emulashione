#ifndef M68000_PROCESSOR_H
#define M68000_PROCESSOR_H

#include "TraceTypes.h"

#include <array>
#include <atomic>
#include <cstddef>
#include <cstdint>
#include <exception>
#include <mutex>
#include <stdexcept>
#include <string_view>

#include <CooperativeDevice.h>
#include <TraceManager.h>

#include <magic_enum.hpp>

/**
 * @brief Namespace for implementations of 68000 and 68010 family processors.
 */
namespace emulashione::m68k::m68000 {
/**
 * Emulation device for a Motorola 68000 processor.
 *
 * \section sec_timing Timings
 *
 * The timings of this core should match real hardware. They are derived from official Motorola
 * documentation, as well as YACHT document from Atari-Forum. While the exact internal mechanisms
 * for some of these cycles (such as the multiplication/division algorithms) may not be implemented
 * exactly, the timings and externally visible behavior should match.
 *
 * @brief Instance of a 68000 processor
 */
class Processor: public plugin::CooperativeDevice {
    friend struct Decoder;

    private:
        using super = plugin::CooperativeDevice;

    protected:
        /**
         * @brief Indicates the processor has detected a reset condition.
         *
         * Processor resets are asynchronous and are checked at the end of every microcycle; since
         * these completely clear the processor state it's easiest to throw this exception when
         * they're detected, and then the reset handler is invoked at the top of the main
         * processing loop.
         */
        struct ResetException: public std::exception {};

        /**
         * @brief Function codes output by the processor
         *
         * These function codes correspond to the type of memory access being made.
         */
        enum class FunctionCode {
            /// Accessing data while executing in user mode
            UserData                    = 0b001,
            /// Prefetching an instruction fetch in user mode
            UserProgram                 = 0b010,
            /// Accessing data while executing in supervisor mode
            SupervisorData              = 0b101,
            /// Performing an instruction fetch in supervisor mode
            SupervisorProgram           = 0b110,
            /// Accessing CPU space (breakpoint, interrupt acknowledge, etc.)
            CpuSpace                    = 0b111,
        };

        /**
         * @brief Various events caused by instruction execution and decoding
         *
         * These events correspond to Group 1 exceptions, which are taken at instruction
         * boundaries.
         */
        enum class EventFlags {
            /// No pending events
            None                        = 0,

            ExtIrq                      = (1 << 0),
            /// The most recently decoded instruction is invalid
            IllegalInstruction          = (1 << 1),
            /// An instruction that requires supervisor mode was executed in user mode
            PrivilegeViolation          = (1 << 2),
        };

        /**
         * @brief Error codes indicating why the processor aborted
         */
        enum class AbortReason {
            /// An internal error of unknown origin
            InternalError,
            /// This feature is currently not implemented
            Unimplemented,
            /**
             * An opcode that should be implemented, wasn't. This means that there's some issue
             * in the instruction decoder.
             */
            UnknownOpcode,
            /**
             * The addressing mode requested is either invalid, or not supported with the current
             * instruction.
             */
            InvalidAddressingMode,
            /**
             * A precondition was violated. This indicates a serious programming error inside the
             * core.
             */
            PreconditionViolated,
            /**
             * A size (as part of an instruction) is invalid for the requested operation. This
             * usually indicates incorrect decoding of an instruction.
             */
            InvalidSize,
        };

    public:
        /// IDs of the strobes exposed by the CPU. Their string names match the enum keys.
        enum Strobes: unsigned int {
            RESET                       = 1,
            HALT                        = 2,
            BR                          = 3,
            BG                          = 4,
            IPL                         = 5,
            VPA                         = 6,
        };

        /// IDs of address spaces exposed by the CPU.
        enum AddressSpaces: unsigned int {
            /// Regular memory mapped data
            Memory                      = 0,
            /// Interrupt acknowledge cycle
            InterruptAck                = 1,
        };

        /**
         * This enum defines all of the exceptions handled by the processor core. Each exception
         * corresponds to a vector number; this is combined with the auxiliary data associated with
         * an exception in some cases before forming the final vector that's executed for a
         * particular exception.
         *
         * These exceptions are divided into groups 0 through 2. Each group has different
         * properties that determine when they are handled. Roughly speaking, exceptions in lower
         * groups have a higher priority than exceptions in higher numbered groups:
         *
         * - **Group 0:** Aborts instruction execution at the next microcycle, at which point
         *                exception processing begins.
         * - **Group 1:** Allows the current instruction to finish executing, if they are the
         *                result of instruction execution (trace, interrupts;) other types are
         *                raised during instruction decoding (privilege violation, illegal
         *                instruction.)
         * - **Group 2:** Occurs as a result of an executed instruction rather than in response to
         *                an external event; this encopasses things like traps and divide by zero.
         *
         * @brief Kinds of processor exceptions
         */
        enum class ExceptionType {
            /// @{
            /**
             * The processor is reset. This can happen either from a power-on reset, a hard reset,
             * or when the processor encounters a fault while executing a fault handler.
             */
            Reset,
            /**
             * Attempt to read word or longword sized data from an unaligned address.
             */
            AddressError,
            /**
             * A bus error was raised by an external device during a bus transaction. The bus
             * cycle is aborted.
             */
            BusError,
            /// @}

            /// @{
            /**
             * The current instruction has completed execution, and the trace bit in the status
             * register is set.
             */
            Trace,
            /**
             * External device delivered an interrupt to the device.
             */
            Interrupt,
            /**
             * The most recently prefetched and decoded instruction is not allowed in the current
             * privilege mode.
             */
            PrivilegeViolation,
            /**
             * The most recently prefetched and decoded instruction is not valid and does not
             * decode to an useful instruction.
             */
            IllegalInstruction,
            /// @}

            /// @{
            /**
             * The bounds check associated with a `CHK` instruction was executed and failed.
             */
            CHK,
            /**
             * The divisor for a division operation (`DIVU`/`DIVS`) was zero.
             */
            DivideByZero,
            /**
             * A `TRAP` instruction was executed.
             */
            TRAP,
            /**
             * A `TRAPV` instruction was executed and the overflow bit (`V`) in the status register
             * is set.
             */
            TRAPV,
            /// @}
        };

        /**
         * Privilege level of the processor; see the 68000 PRM and URM for precisely the
         * differences between user and supervisor modes.
         */
        enum class PrivilegeLevel {
            /// Unprivileged mode
            User                        = 0,
            /// Privileged mode
            Supervisor                  = 1,
        };

        /**
         * This contains the definition of the user visible processor registers, both the user and
         * supervisor mode versions.
         *
         * @remark By convention, `A7` is the same as the current stack pointer.
         *
         * @brief User visible processor registers
         */
        struct Registers {
            /// Bits in the condition code register
            enum class ConditionCode: uint8_t {
                /// Convenience for indicating no flags are set
                None                    = 0,

                /// **C**: Carry flag
                Carry                   = (1 << 0),
                /// **V**: Overflow flag
                Overflow                = (1 << 1),
                /// **Z**: Zero flag
                Zero                    = (1 << 2),
                /// **N**: Negative flag
                Negative                = (1 << 3),
                /// **X**: Extend flag
                Extend                  = (1 << 4),

                /// Mask of all valid condition codes
                Mask                    = (Carry | Overflow | Zero | Negative | Extend),
            };

            /// Various conditional tests available, using the state of the ccr
            enum class Condition: uint8_t {
                /// **T**: Always true
                True                    = 0b0000,
                /// **F**: Always false
                False                   = 0b0001,
                /// **HI**: Higher than
                HigherThan              = 0b0010,
                /// **LS**: Lower than or same
                LowerOrSame             = 0b0011,
                /// **CC**: Carry bit is clear
                CarryClear              = 0b0100,
                /// **CS**: Carry bit is set
                CarrySet                = 0b0101,
                /// **NE**: Not equal
                NotEqual                = 0b0110,
                /// **EQ**: Equal
                Equal                   = 0b0111,
                /// **VC**: Overflow bit is clear
                OverflowClear           = 0b1000,
                /// **VS**: Overflow bit is set
                OverflowSet             = 0b1001,
                /// **PL**: Value is positive
                Plus                    = 0b1010,
                /// **MI**: Value is negative
                Minus                   = 0b1011,
                /// **GE**: Greater than or equal
                GreaterOrEqual          = 0b1100,
                /// **LT**: Less than
                LessThan                = 0b1101,
                /// **GT**: Greater than
                GreaterThan             = 0b1110,
                /// **LE**: Less than or equal
                LessOrEqual             = 0b1111,
            };

            /// Data registers
            std::array<uint32_t, 8> d{0};
            /// Address registers (including A7, for user stack pointer)
            std::array<uint32_t, 8> a{0};
            /// Supervisor stack pointer
            uint32_t ssp{0};

            /**
             * @brief Program counter
             *
             * Address of the instruction that is being executed. In actuality, this points up to
             * two words ahead of the current instruction, depending on how many extension words
             * it has and when it is read during the course of an instruction.
             */
            uint32_t pc{0};

            /**
             * @brief Condition code register
             *
             * This register contains the condition codes that are set as a result of certain
             * operations, and used as an input for certain others. It is accessible from both
             * user and supervisor mode, and forms the low byte of the 16-bit status register.
             */
            ConditionCode ccr{ConditionCode::None};

            /**
             * @brief System status register
             *
             * Contains the "system byte" of the status register; this is only accessible in
             * supervisor mode, and forms the high byte of the 16-bit status register.
             */
            uint8_t system{0};

            /**
             * Gets a reference to a data register.
             *
             * @param Dn Index of the data register
             *
             * @return Reference to the data register
             */
            inline constexpr uint32_t &getDn(const uint8_t Dn) {
                return this->d[Dn];
            }
            /**
             * Store a value into the low 8 bits of the data register. The remaining bits are
             * left untouched.
             */
            inline constexpr void storeData(const uint8_t Dn, const uint8_t val) {
                this->d[Dn] = (this->d[Dn] & ~0xFF) | static_cast<uint32_t>(val);
            }
            /**
             * Store a value into the low 16 bits of the data register. The remaining bits are
             * left untouched.
             */
            inline constexpr void storeData(const uint8_t Dn, const uint16_t val) {
                this->d[Dn] = (this->d[Dn] & ~0xFFFF) | static_cast<uint32_t>(val);
            }
            /**
             * Store a value into the data register.
             */
            inline constexpr void storeData(const uint8_t Dn, const uint32_t val) {
                this->d[Dn] = val;
            }

            /**
             * Gets a reference to an address register. For A7, this will either be A7 (user) or
             * SSP (supervisor) depending on the current processor state.
             *
             * @param An Index of the address register
             *
             * @return Reference to address register for current processor mode
             */
            inline constexpr uint32_t &getAn(const uint8_t An) {
                constexpr const uint8_t kSupervisorBit{(1 << 5)};
                if(An == 7) {
                    if(this->system & kSupervisorBit) {
                        return this->ssp;
                    }
                }
                return this->a[An];
            }

            /// Return a reference to the condition code part of the status register.
            inline constexpr ConditionCode &getCcr() {
                return this->ccr;
            }
            /// Return the current condition codes.
            inline constexpr ConditionCode getCcr() const {
                return this->ccr;
            }

            /// Is the given condition code bit set?
            inline constexpr bool testConditionCode(const ConditionCode c) const {
                using namespace magic_enum::bitwise_operators;
                return (this->ccr & c) == c;
            }

            /**
             * Overwrite the condition code register.
             *
             * @param newCcr New value of the condition code register
             */
            inline void setCcr(const ConditionCode newCcr) {
                this->ccr = newCcr;
            }

            /**
             * Sets and clears certain condition code bits.
             *
             * @param set Condition code bits to set
             * @param clear Condition code bits to clear
             */
            inline void setCcr(const ConditionCode set, const ConditionCode clear) {
                using namespace magic_enum::bitwise_operators;

                auto temp = this->ccr;
                temp &= ~clear;
                temp |= set;
                this->ccr = temp;
            }

            /**
             * Sets the specified condition code if the condition is true, clears it otherwise.
             *
             * @param bits Condition code bits to set or clear
             * @param condition When true, the bits are set; they will be cleared otherwise.
             */
            inline void setCcr(const ConditionCode bits, const bool condition) {
                using namespace magic_enum::bitwise_operators;
                if(condition) {
                    this->ccr |= bits;
                } else {
                    this->ccr &= ~bits;
                }
            }

            /**
             * Sets all of the specified bits in the condition code register.
             *
             * @param set Values to set
             */
            inline void setCcrBits(const ConditionCode set) {
                using namespace magic_enum::bitwise_operators;
                this->ccr |= set;
            }

            /**
             * Clears all of the specified bits in the condition code register.
             *
             * @param clear Values to clear
             */
            inline void clearCcrBits(const ConditionCode clear) {
                using namespace magic_enum::bitwise_operators;
                this->ccr &= ~clear;
            }

            /**
             * Gets the full 16 bit value of the status register. This is the concatenation of
             * the condition codes (low) and system status (high) bytes.
             */
            inline constexpr uint16_t getStatus() const {
                return (this->system << 8) | static_cast<uint8_t>(this->ccr);
            }

            /**
             * Sets the the condition code and system bytes of the status register from a full 16-
             * bit status register value.
             *
             * @param status 16-bit status register value; high byte is system byte, low is CCR.
             */
            inline void setStatus(const uint16_t status) {
                this->ccr = static_cast<ConditionCode>(status &
                        static_cast<uint8_t>(ConditionCode::Mask));
                this->system = (status >> 8);
            }

            /**
             * Sets the state of the trace bit in the status register.
             *
             * @param enable Whether instruction tracing should be enabled.
             */
            inline void setTrace(const bool enable) {
                constexpr const uint8_t kTraceBit{(1 << 7)};
                if(enable) {
                    this->system |= kTraceBit;
                } else {
                    this->system &= ~kTraceBit;
                }
            }
            /**
             * Gets whether instruction tracing is enabled.
             *
             * @return State of the trace flag in the status register.
             */
            inline constexpr bool isTraceEnabled() const {
                constexpr const uint8_t kTraceBit{(1 << 7)};
                return !!(this->system & kTraceBit);
            }

            /**
             * Sets the interrupt mask in the status register.
             *
             * @param mask New interrupt mask to apply; interrupt levels lower than this value will
             *        be masked.
             */
            inline void setInterruptMask(const uint8_t mask) {
                constexpr const uint8_t kMask{0b111};
                this->system = (this->system & ~kMask) | (mask & kMask);
            }
            /**
             * Gets the current interrupt mask in the status register.
             *
             * @return Current interrupt mask
             */
            inline constexpr uint8_t getInterruptMask() const {
                constexpr const uint8_t kMask{0b111};
                return this->system & kMask;
            }

            /**
             * Sets the current privilege level in the status register.
             *
             * @param level New privilege level to set.
             */
            inline void setPrivilegeLevel(const PrivilegeLevel level) {
                constexpr const uint8_t kSupervisorBit{(1 << 5)};
                if(level == PrivilegeLevel::Supervisor) {
                    this->system |= kSupervisorBit;
                } else {
                    this->system &= ~kSupervisorBit;
                }
            }
            /**
             * Gets the current privilege level set in the status register.
             *
             * @return Current processor privilege level
             */
            inline constexpr PrivilegeLevel getPrivilegeLevel() const {
                constexpr const uint8_t kSupervisorBit{(1 << 5)};
                if(this->system & kSupervisorBit) {
                    return PrivilegeLevel::Supervisor;
                } else {
                    return PrivilegeLevel::User;
                }
            }

            /// Get the state of the carry bit
            inline constexpr bool getC() const {
                using namespace magic_enum::bitwise_operators;
                return (this->ccr & ConditionCode::Carry) == ConditionCode::Carry;
            }
            /// Get the state of the overflow bit
            inline constexpr bool getV() const {
                using namespace magic_enum::bitwise_operators;
                return (this->ccr & ConditionCode::Overflow) == ConditionCode::Overflow;
            }
            /// Get the state of the zero bit
            inline constexpr bool getZ() const {
                using namespace magic_enum::bitwise_operators;
                return (this->ccr & ConditionCode::Zero) == ConditionCode::Zero;
            }
            /// Get the state of the negative bit
            inline constexpr bool getN() const {
                using namespace magic_enum::bitwise_operators;
                return (this->ccr & ConditionCode::Negative) == ConditionCode::Negative;
            }
            /// Get the state of the extend bit
            inline constexpr bool getX() const {
                using namespace magic_enum::bitwise_operators;
                return (this->ccr & ConditionCode::Extend) == ConditionCode::Extend;
            }

            /**
             * Performs a conditional test, according to the current state of the ccr.
             *
             * @param test The conditional test to execute
             *
             * @return Whether the conditional test passed
             */
            inline constexpr bool test(const Condition test) {
                using namespace magic_enum::bitwise_operators;
                switch(test) {
                    case Condition::False:
                        return false;
                    case Condition::True:
                        return true;

                    case Condition::HigherThan:
                        return !(this->getC() || this->getZ());
                    case Condition::LowerOrSame:
                        return (this->getC() || this->getZ());
                    case Condition::CarryClear:
                        return !this->getC();
                    case Condition::CarrySet:
                        return this->getC();
                    case Condition::NotEqual:
                        return !this->getZ();
                    case Condition::Equal:
                        return this->getZ();
                    case Condition::OverflowClear:
                        return !this->getV();
                    case Condition::OverflowSet:
                        return this->getV();
                    case Condition::Plus:
                        return !this->getN();
                    case Condition::Minus:
                        return this->getN();
                    case Condition::GreaterOrEqual:
                        return (this->getN() == this->getV());
                    case Condition::LessThan:
                        return (this->getN() != this->getV());
                    case Condition::GreaterThan:
                        return (!this->getZ() && ((this->getN() == this->getV())));
                    case Condition::LessOrEqual:
                        return (this->getZ() || (this->getN() != this->getV()));

                    // TODO: implement remainder
                    default:
                        throw std::runtime_error(fmt::format("unimplemented conditional mode: {}",
                                    magic_enum::enum_name(test)));
                }
            }

            /**
             * Copy the register state into the specified trace message structure.
             *
             * @remark This does not copy PC; you should copy it yourself
             */
            inline void copyToTraceState(TraceRegisterState &state) {
                std::copy(this->d.begin(), this->d.end(), state.d);
                std::copy(this->a.begin(), this->a.end(), state.a);
                state.ssp = this->ssp;
                state.status = this->getStatus();
            }

            /// Bitmask of all implemented status register bits
            constexpr static const uint16_t kStatusMask{0b1010'0111'0001'1111};
        };

        /**
         * The 68k has a small three word prefetch queue that is used for all internal memory
         * accesses. It is divided into three stages, from back to front:
         *
         * \dot "Diagram of the prefetch queue"
         * digraph prefetch {
         *      node [shape=box, fontname=LiberationSans];
         *
         *      irc [label=<<B>IRC</B><BR/>Most recently prefetched word> URL="\ref irc"];
         *      ir [label=<<B>IR</B><BR/>Instruction being decoded> URL="\ref ir"];
         *      ird [label=<<B>IRD</B><BR/>Current instruction or extension word> URL="\ref ird"];
         *
         *      irc -> ir [];
         *      ir -> ird [];
         * }
         * \enddot
         *
         * Of these registers, the only externally visible (by means of the "current instruction"
         * value pushed as part of stack frames of address or bus errors) is IRD; it is otherwise
         * not directly accessible.
         *
         * @brief Internal processor prefetch queue
         */
        struct PrefetchQueue {
            /// Storage for the queue
            std::array<uint16_t, 3> storage;

            /// Reference to the most recently prefetched word
            inline uint16_t &irc() {
                return this->storage[0];
            }
            /// Reference to the most recently prefetched word
            inline const uint16_t &irc() const {
                return this->storage[0];
            }

            /// Reference to the currently decoding instruction
            inline uint16_t &ir() {
                return this->storage[1];
            }
            /// Reference to the currently decoding instruction
            inline const uint16_t &ir() const {
                return this->storage[1];
            }

            /// Reference to the currently executing instruction
            inline uint16_t &ird() {
                return this->storage[2];
            }
            /// Reference to the currently executing instruction
            inline const uint16_t &ird() const {
                return this->storage[2];
            }

            /**
             * Pushes the given value into the prefetch queue. This will transfer IRC to IR, store
             * the value into IRC, then transfer IR to IRD. The current value of IRD is lost.
             *
             * @param data 16 bit value to insert into the queue
             */
            inline void push(const uint16_t data) {
                std::shift_right(this->storage.begin(), this->storage.end(), 1);
                this->storage[0] = data;
            }

            /**
             * Pops the current instruction from the head of the queue.
             *
             * @return 16 bit value in IRD
             */
            inline uint16_t pop() {
                const auto ird = this->storage[2];
                this->storage[1] = this->storage[0];
                this->storage[2] = this->storage[1];
                return ird;
            }
        };

    /**
     * @name Lifecycle
     *
     * Initialization and deinitialization of the processor core
     *
     * @{
     */
    public:
        Processor(const std::string_view &name, struct emulashione_config_object *cfg);
        ~Processor() = default;

        void willStartEmulation() override;
        void willShutDown() override;
        void reset(const bool isHard = true) override;

        static void DidLoad();

        size_t copyOutInfo(const std::string_view &, std::span<std::byte> &) override;
    /// @}

    /**
     * @name Connections
     *
     * Overrides of device connection methods, so that we can store a reference to all the clocks
     * and busses the processor is connected to.
     *
     * @{
     */
    public:
        void connectBus(const std::string &name,
                const std::shared_ptr<plugin::Bus> &bus) override;
        void disconnectBus(const std::string &name) override;
        void connectClockSource(const std::string &name,
                const std::shared_ptr<plugin::ClockSource> &clock) override;
        void disconnectClockSource(const std::string &name) override;
    /// @}

    /// @{ @name Emulation
    public:
        void main() override;

    /// @} @{ @name Exceptions
    protected:
        void abort(const AbortReason cause);

        void doReset();

        void processType1Exception(const ExceptionType type);
        void processType2Exception(const ExceptionType type, const uint8_t vectorOffset = 0);
    /// @}

    /**
     * @name Microcycles
     * Implementations of some common microcycle types' associated timings.
     * @{
     */
    protected:
        /**
         * Test whether a reset has been received, and if so, generate the appropriate exception
         * to process it. It atomically checks the reset flag, clearing it if it's set, and
         * resetting the processor if so.
         *
         * @throw ResetException The processor should be reset.
         */
        inline void checkForReset() {
            bool yes{true};
            if(this->resetPending.compare_exchange_strong(yes, false, std::memory_order_release,
                        std::memory_order_relaxed)) {
                throw ResetException();
            }
        }

        /// Clock cycles used by the current instruction
        size_t ticks{0};
        /// Current system time
        double systemTime{0};

        /// No-op
        inline void microcycleNop() {
            this->advanceClockBy(2);
            ticks += 2;
            this->checkForReset();
        }
        /// No-op with wait state (length of two microcycles)
        inline void microcycleNopWait() {
            // XXX: is this indivisible as we pretend it is here?
            this->advanceClockBy(4);
            ticks += 4;
            this->checkForReset();
        }
        /**
         * @brief Program fetch
         *
         * Fetches a word from the next consecutive program address, and inserts it at the end of
         * the prefetch queue. The program counter is incremented by two.
         */
        inline void microcyclePrefetch() {
            this->prefetch();
            this->checkForReset();
        }

    /// @}
    /// @name Bus Cycles
    /// @{
        uint32_t fetchVector(const size_t vec);

        void prefetch();
        uint16_t prefetchWord();

        /**
         * Reads the first extension word for the instruction. It will have already been read into
         * the prefetch queue.
         *
         * @return First extension word
         */
        inline uint16_t getExtensionWord() {
            auto word = this->queue.irc();
            return word;
        }

        /**
         * Reads the second and subsequent extension word for the instruction. These words bypass
         * the prefetch queue.
         *
         * @param nop Whether the prefetch is preceded by a two cycle no-op.
         *
         * @return Extension word
         */
        inline uint16_t getExtensionWordBonus(const bool nop = true) {
            if(nop) this->microcycleNop();
            return this->prefetchWord();
        }

        /**
         * Function code to use for accessing the stack based on the current processor mode
         */
        inline constexpr auto stackGetFc() {
            if(this->regs.getPrivilegeLevel() == PrivilegeLevel::Supervisor) {
                return FunctionCode::SupervisorData;
            } else {
                return FunctionCode::UserData;
            }
        }

        /**
         * Pushes a byte value to the stack. This will be "rounded up" into a word.
         *
         * @param value Byte to push to the stack
         */
        inline void stackPush(const uint8_t value) {
            this->stackPush(static_cast<uint16_t>(value));
        }
        /**
         * Pushes a word to the stack, and updates the stack pointer.
         *
         * @param value Word to write to the stack.
         */
        void stackPush(const uint16_t value) {
            this->microcycleNop();

            auto &sp = this->regs.getAn(7);
            sp -= 2;
            this->writeWord(this->stackGetFc(), sp, value);
        }
        /**
         * Pushes a longword to the stack, and updates the stack pointer.
         *
         * @param value Longword to write to the stack
         * @param reverse When set, the LSW is written first.
         */
        inline void stackPush(const uint32_t value, const bool reverse = false) {
            // TODO: implement reversed pushing
            (void) reverse;
            this->stackPush(static_cast<uint16_t>(value & 0xFFFF));
            this->stackPush(static_cast<uint16_t>(value >> 16));
        }

        /**
         * Pops a byte from the stack. This reads a word, but then only returns the low byte.
         *
         * @return Byte read from stack.
         */
        inline uint8_t stackPopByte() {
            return static_cast<uint8_t>(this->stackPopWord() & 0xFF);
        }
        /**
         * Pops a word from the stack, and updates the stack pointer.
         *
         * @return Word read from stack
         */
        inline uint16_t stackPopWord() {
            uint16_t data;
            this->microcycleNop();

            auto &sp = this->regs.getAn(7);
            data = this->readWord(this->stackGetFc(), sp);
            sp += 2;

            return data;
        }
        /**
         * Pops a longword from the stack, and updates the stack pointer.
         *
         * @param reverse When set, the LSW is read first.
         *
         * @return Longword read from stack.
         */
        inline uint32_t stackPopLongWord(const bool reverse = false) {
            uint32_t data{0};
            // TODO: implement reversed popping
            (void) reverse;
            data = static_cast<uint32_t>(this->stackPopWord()) << 16;
            data |= this->stackPopWord();

            return data;
        }

        /**
         * Gets the function code to be used for data accesses.
         */
        constexpr inline auto getDataFc() const {
            return (this->regs.getPrivilegeLevel() == PrivilegeLevel::Supervisor) ?
                FunctionCode::SupervisorData : FunctionCode::UserData;
        }

        uint8_t readByte(const FunctionCode fc, const uint32_t address);
        uint16_t readWord(const FunctionCode fc, const uint32_t address);

        /**
         * Reads a 32-bit longword from the bus.
         *
         * @param fc Function code to use for this bus access
         * @param address Memory address to read from
         * @param reverse When set, the lower word is read first
         */
        uint32_t readLongword(const FunctionCode fc, const uint32_t address,
                const bool reverse = false) {
            uint32_t temp;
            if(reverse) abort(AbortReason::Unimplemented);

            this->microcycleNop();
            temp = this->readWord(fc, address) << 16;
            this->microcycleNop();
            temp |= this->readWord(fc, address + 2);

            return temp;
        }

        void writeByte(const FunctionCode fc, const uint32_t address, const uint8_t data);
        void writeWord(const FunctionCode fc, const uint32_t address, const uint16_t data);

        /**
         * Writes a 32-bit longword to the bus.
         *
         * @param fc Function code to use for this bus access
         * @param address Memory address to write to
         * @param data 32 bit value to write
         * @param reverse When set, the lower word is written first
         */
        void writeLongword(const FunctionCode fc, const uint32_t address, const uint32_t data,
                const bool reverse = false) {
            if(reverse) abort(AbortReason::Unimplemented);

            this->microcycleNop();
            this->writeWord(fc, address, (data >> 16));

            this->microcycleNop();
            this->writeWord(fc, address + 2, (data & 0xFFFF));
        }

    /// @}

    /// @{ @name Helpers
        void setEvent(const EventFlags flags);

    /// }

    private:
        /**
         * Ensures that sufficient nop microcycles are inserted for a memory access that took the
         * specified number of nanoseconds. This means at least 2 clocks (one microcycle) are
         * always used, increasing in multiples of 2.
         *
         * @param nanos Time required by the bus cycle, in nanoseconds
         */
        inline void handleBusWaits(const double nanos) {
            // number of clock cycles (fractional)
            const auto cycles = nanos / this->clkPeriod;
            // round up to nearest multiple of 2
            const size_t clocks = std::min(2., std::ceil(cycles / 2.) * 2.);
            this->advanceClockBy(clocks);
        }

        /**
         * Advances the emulated clock by the given number of cycles, and updates the internal
         * instruction cycle counter as well.
         */
        inline void advanceClockBy(const size_t clocks) {
            this->schedDidAdvanceBy(clocks, this->systemTime);
            this->ticks += clocks;
        }

    /**
     * @name Required device fields
     *
     * These fields are mandatory to identify the device type, and to build its device descriptor
     * that's registered during plugin initialization.
     *
     * @{
     */
    public:
        static const struct emulashione_device_descriptor kDeviceDescriptor;

        constexpr static const std::string_view kDisplayName{"Motorola 68000"};
        constexpr static const std::string_view kShortName{"m68000"};
        constexpr static const std::string_view kAuthors{"Tristan Seifert"};
        constexpr static const std::string_view kDescription{
            "Cycle accurate emulation of the Motorola 68000 CPU"
        };
        constexpr static const std::string_view kInfoUrl{"https://gitlab.trist.network/mega-drive-stuff/emulashione/-/tree/main/plugins/m68k"};
        static const std::string_view kVersion;
    /// @}

    /// @name External device connections
    /// @{
    protected:
        /// Bus on which the CPU sits
        std::shared_ptr<plugin::Bus> bus;
        /// Main CPU clock
        std::shared_ptr<plugin::ClockSource> clk;
        /// Period of the main CPU clock
        double clkPeriod{-1};

        /// If tracing is requested, the trace manager that receives the requests
        std::unique_ptr<plugin::TraceManager> tracer;
    /// @}


    /**
     * Various asynchronous event flags that can be set outside of the step function. This includes
     * things such as interrupts, bus errors, and an external reset.
     *
     * @name Event flags
     *
     * @{
     */
    protected:
        /// Is a reset pending?
        std::atomic_bool resetPending{false};
    /// @}

    /**
     * @name Processor state
     * @{
     */
    protected:
        /// Current programmer visible registers
        Registers regs;
        /// Prefetch queue
        PrefetchQueue queue;

        /// Event flags (checked at the end of each instruction)
        EventFlags events{EventFlags::None};

    /// @}

    /// Whether read accesses are logged
    static constexpr bool gLogReads{false};
    /// Whether write accesses are logged
    static constexpr bool gLogWrites{false};

    /// Output every decoded instruction
    static constexpr bool kLogInstructions{false};
    /// Output the clocks taken for each instruction
    static constexpr bool kLogInstructionTimings{false};

    /**
     * @name Constants
     * @{
     */
    protected:
        /// Memory address of the vector table
        static constexpr const uint32_t kVectorBase{0x00000000};

        /// Vector number for the reset stack
        static constexpr const size_t kVectorResetStack{0};
        /// Vector number for the reset program counter
        static constexpr const size_t kVectorResetPc{1};
        /// Vector number for bus error
        static constexpr const size_t kVectorBusError{2};
        /// Address error exception vector
        static constexpr const size_t kVectorAddressError{3};
        /// Illegal instruction vector
        static constexpr const size_t kVectorIllegalInstruction{4};
        /// Attempt to divide by zero
        static constexpr const size_t kVectorDivideByZero{5};
        /// CHK instruction
        static constexpr const size_t kVectorChkInstruction{6};
        /// TRAPV instruction
        static constexpr const size_t kVectorTrapvInstruction{7};
        /// Vector number for privilege violation
        static constexpr const size_t kVectorPrivilegeViolation{8};
        /// Vector number for the trace exception
        static constexpr const size_t kVectorTrace{9};
        /// Vector number for instructions starting with `0b1010`
        static constexpr const size_t kVectorLine1010Emulator{10};
        /// Vector number for instructions starting with `0b1111`
        static constexpr const size_t kVectorLine1111Emulator{11};
        /// Vector number for a spurious (interrupt acknowledge followed by /BERR) interrupt
        static constexpr const size_t kVectorSpuriousInterrupt{24};
        /// Vector number for the first autovector (Level 1)
        static constexpr const size_t kVectorInterruptAutoBase{25};
        /// Offset of the first of the 16 TRAP vectors
        static constexpr const size_t kVectorTrapBase{32};

        /// Bitmask for which physical address bits exist
        static constexpr const uint32_t kAddressMask{0x00FFFFFF};

    /// @}
};
}

#endif
