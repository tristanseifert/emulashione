/*
 * Comparison operations: CMP, CMPA, CMPI, CMPM, TST
 */
#include "Decoder.h"
#include "Decoder+Fmt.h"
#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k;
using namespace emulashione::m68k::m68000;
using namespace magic_enum::bitwise_operators;


/**
 * Calculates the overflow (V) flag for compares.
 *
 * @tparam T Integer type (must be one of `uint8_t`, `uint16_t`, `uint32_t`)
 *
 * @param source Value of the source
 * @param destination Value at the destination
 * @param result Result of the subtraction done during the comparison
 *
 * @return State of the overflow bit
 */
template<typename T>
static constexpr inline bool GetOverflow(const T source, const T destination, const T result) {
    const bool Sm = source & GetMsb<T>(), Dm = destination & GetMsb<T>(),
          Rm = result & GetMsb<T>();
    //return (!Sm && Dm && !Rm) || (Sm && !Dm && Rm);
    return (Sm == Rm) && (Dm != Sm);
}

/**
 * Calculates the carry (C) flag for compares. It indicates whether a borrow occurred.
 *
 * @tparam T Integer type (must be one of `uint8_t`, `uint16_t`, `uint32_t`)
 *
 * @param source Value of the source
 * @param destination Value at the destination
 * @param result Result of the subtraction done during the comparison
 *
 * @return State of the carry bit
 */
template<typename T>
static constexpr inline bool GetBorrow(const T source, const T destination, const T result) {
    const bool Sm = source & GetMsb<T>(), Dm = destination & GetMsb<T>(),
          Rm = result & GetMsb<T>();
    //return (Sm && !Dm) || (Rm && !Dm) || (Sm && Rm);
    return (Sm && Rm) || (!Dm && (Sm || Rm));
}

/**
 * Updates the condition code register after a comparison operation.
 *
 * The N and Z bits are updated according to the standard rules. V and C follow the
 * implementations defined above.
 *
 * @tparam T Integer type (must be one of `uint8_t`, `uint16_t`, `uint32_t`)
 *
 * @param source Value of the source
 * @param destination Value at the destination
 * @param result Result of the subtraction done during the comparison
 * @param set CCR bits that should be set
 * @param clear CCR bits that should be cleared
 */
template<typename T>
static constexpr inline void UpdateCcr(const T source, const T destination, const T result,
        Processor::Registers::ConditionCode &set, Processor::Registers::ConditionCode &clear) {
    using Ccr = Processor::Registers::ConditionCode;

    // negative bit: if MSB is set
    if(GetMsb<T>() & result) {
        set |= Ccr::Negative;
    } else {
        clear |= Ccr::Negative;
    }
    // zero bit
    if(result) {
        clear |= Ccr::Zero;
    } else {
        set |= Ccr::Zero;
    }
    // overflow bit
    if(GetOverflow(source, destination, result)) {
        set |= Ccr::Overflow;
    } else {
        clear |= Ccr::Overflow;
    }
    // carry bit (borrow)
    if(GetBorrow(source, destination, result)) {
        set |= Ccr::Carry;
    } else {
        clear |= Ccr::Carry;
    }
}



/**
 * @brief CMP
 *
 * Subtract the source operand from the value of the destination register, and update the
 * condition codes based on the result. The destination register is not modified.
 *
 * All addressing modes are allowed. N, Z, V, C flags are updated.
 *
 * @todo Ensure timings are correct.
 */
void Decoder::Compare(const uint16_t opcode, Processor &proc) {
    // read EA
    const auto size = GetSize(opcode);
    const auto srcMode = GetAddrMode<5,2>(opcode);

    const auto source = ReadEffectiveAddr(proc, srcMode, size);

    // get the register and perform the subtraction (in a scratch variable) and update ccr
    Ccr set{Ccr::None}, clear{Ccr::None};

    const auto &Dn = proc.regs.getDn(GetRegIndex<11>(opcode));

    switch(size) {
        case Size::Byte: {
            uint8_t src = source, dst = Dn, result = dst - src;
            UpdateCcr(src, dst, result, set, clear);
            break;
        }
        case Size::Word: {
            uint16_t src = source, dst = Dn, result = dst - src;
            UpdateCcr(src, dst, result, set, clear);
            break;
        }
        case Size::Longword: {
            uint32_t src = source, dst = Dn, result = dst - src;
            UpdateCcr(src, dst, result, set, clear);
            break;
        }
        default:
            proc.abort(Processor::AbortReason::InternalError);
    }

    proc.regs.setCcr(set, clear);
    PrefetchCycle(proc);

    if(kLogCompare) {
        Logger::Trace("CMP values: src = ${:08x}, dst = ${:08x}, size {} -> CCR  ${:02x}", source,
                Dn, magic_enum::enum_name(size), proc.regs.ccr);
    }
}

/**
 * @brief CMPA
 *
 * Subtract the source operand from the value of the destination address register, and update the
 * condition codes based on the result. The destination register is not modified. If the source
 * operand is word sized, it is sign extended to longword.
 *
 * All addressing modes are allowed. N, Z, V, C flags are updated.
 *
 * @todo Ensure timings are correct.
 */
void Decoder::CompareAddress(const uint16_t opcode, Processor &proc) {
    // read EA
    const auto size = !!GetBits<8,1>(opcode) ? Size::Longword : Size::Word;
    const auto srcMode = GetAddrMode<5,2>(opcode);

    auto value = ReadEffectiveAddr(proc, srcMode, size);

    if(size == Size::Word) {
        value = SignExtend<int32_t,16>(value);
    }

    // get the register and perform the subtraction (in a scratch variable) and update ccr
    Ccr set{Ccr::None}, clear{Ccr::None};

    const uint32_t An = proc.regs.getAn(GetRegIndex<11>(opcode));

    uint32_t src = value, dst = An, result = dst - src;
    UpdateCcr(src, dst, result, set, clear);

    proc.regs.setCcr(set, clear);
    PrefetchCycle(proc);

    if(kLogCompare) {
        Logger::Trace("CMPA value: ${:08x}, size {} -> CCR  ${:02x}", value,
                magic_enum::enum_name(size), proc.regs.ccr);
    }
}

/**
 * @brief CMPI
 *
 * Subtract the immediate value from the destination value, and update the condition codes; the
 * destination is not modified.
 *
 * All addressing modes are allowed. N, Z, V, C flags are updated.
 *
 * @todo Ensure timings are correct.
 */
void  Decoder::CompareImmediate(const uint16_t opcode, Processor &proc) {
    // read immediate value
    const auto size = GetSize(opcode);
    const auto srcMode = GetAddrMode<5,2>(opcode);

    const auto imm = ReadImmediate(proc, size);

    // read <ea> (destination)
    const auto destValue = ReadEffectiveAddr(proc, srcMode, size);

    // perform the operation and set flags
    Ccr set{Ccr::None}, clear{Ccr::None};

    switch(size) {
        case Size::Byte: {
            uint8_t src = imm, dst = destValue, result = dst - src;
            UpdateCcr(src, dst, result, set, clear);
            break;
        }
        case Size::Word: {
            uint16_t src = imm, dst = destValue, result = dst - src;
            UpdateCcr(src, dst, result, set, clear);
            break;
        }
        case Size::Longword: {
            uint32_t src = imm, dst = destValue, result = dst - src;
            UpdateCcr(src, dst, result, set, clear);
            break;
        }
        default:
            proc.abort(Processor::AbortReason::InternalError);
    }

    proc.regs.setCcr(set, clear);
    PrefetchCycle(proc);

    if(kLogCompare) {
        Logger::Trace("CMPI immediate ${:08x}, value ${:08x}, size {} -> CCR  ${:02x}", imm,
                destValue, magic_enum::enum_name(size), proc.regs.ccr);
    }
}

/**
 * @brief CMPM
 *
 * Subtract the source from the destination operand and update the condition codes. Only address
 * register postincrement is supported.
 *
 * X is not affected. N, Z, V, C flags are updated.
 *
 * @todo Ensure timings are correct.
 */
void Decoder::CompareMemory(const uint16_t opcode, Processor &proc) {
    // build addressing modes for source and dest
    const auto size = GetSize(opcode);
    const AddressingMode srcMode{0b011, GetRegIndex<2>(opcode)},
          dstMode{0b011, GetRegIndex<11>(opcode)};

    // perform reads
    const auto source = ReadEffectiveAddr(proc, srcMode, size);
    const auto destination = ReadEffectiveAddr(proc, dstMode, size);

    // compare and update flags
    Ccr set{Ccr::None}, clear{Ccr::None};

    switch(size) {
        case Size::Byte: {
            uint8_t src = source, dst = destination, result = dst - src;
            UpdateCcr(src, dst, result, set, clear);
            break;
        }
        case Size::Word: {
            uint16_t src = source, dst = destination, result = dst - src;
            UpdateCcr(src, dst, result, set, clear);
            break;
        }
        case Size::Longword: {
            uint32_t src = source, dst = destination, result = dst - src;
            UpdateCcr(src, dst, result, set, clear);
            break;
        }
        default:
            proc.abort(Processor::AbortReason::InternalError);
    }

    proc.regs.setCcr(set, clear);
    PrefetchCycle(proc);

    if(kLogCompare) {
        Logger::Trace("CMPM {} {}, size {} -> CCR  ${:02x}", srcMode, dstMode,
                magic_enum::enum_name(size), proc.regs.ccr);
    }
}

/**
 * @brief TST
 *
 * Read the operand specified by the effective address, then set the condition codes based on it.
 *
 * V, C are always cleared; N, Z set based on the value read.
 */
void Decoder::Test(const uint16_t opcode, Processor &proc) {
    // get info out of opcode and read ea
    const auto size = GetSize(opcode);
    const auto srcMode = GetAddrMode<5,2>(opcode);

    const auto value = ReadEffectiveAddr(proc, srcMode, size);

    //update flags and prefetch
    proc.regs.clearCcrBits(Ccr::Overflow | Ccr::Carry);
    proc.regs.setCcr(Ccr::Zero, !(value & MaskFor(size)));
    proc.regs.setCcr(Ccr::Negative, !!(value & SignBitFor(size)));

    PrefetchCycle(proc);
}
