/*
 * Arithmetic - Bitwise EOR (XOR): EORI to CCR, EORI to SR, EORI, EOR
 */
#include "Decoder.h"
#include "Decoder+Fmt.h"
#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k;
using namespace emulashione::m68k::m68000;

using namespace magic_enum::bitwise_operators;

/**
 * @brief EOR
 *
 * Perform an XOR on the destination, using the specified register as a source, and st ore the
 * result in the effective address.
 *
 * @remark Unlike OR or AND, memory-to-register is _not_ supported.
 */
void Decoder::ExclusiveOr(const uint16_t opcode, Processor &proc) {
    // get some opcode info
    const auto regNum = GetRegIndex<11>(opcode);
    const auto size = GetSize(opcode);
    const auto addrMode = GetAddrMode<5,2>(opcode);

    // resolve effective address and read from it
    auto resolved = ResolveEffectiveAddr(proc, addrMode, size, kAddrModeDataAlterable);
    const auto readData = ReadEffectiveAddr(proc, resolved, size);

    // perform operation, update flags, prefetch
    const uint32_t result = readData ^ proc.regs.getDn(regNum);

    proc.regs.clearCcrBits(Ccr::Overflow | Ccr::Carry);
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));

    PrefetchCycle(proc);

    // write back
    WriteEffectiveAddr(proc, resolved, size, result);

    // a longword data register destination incurs 2 extra cycles
    if(size == Size::Longword && addrMode.isDataRegister()) {
        proc.microcycleNop();
        proc.microcycleNop();
    }
}

/**
 * @brief EORI
 *
 * Read an immediate value (one or two words, of which either 8, 16 or 32 bits are used) and XOR
 * it with the contents of a specified effective address, then write it back.
 *
 * @todo Validate when flags get updated during execution
 */
void Decoder::ExclusiveOrImmediate(const uint16_t opcode, Processor &proc) {
    // get opcode parameters and immediate word
    const auto size = GetSize(opcode);
    const auto addrMode = GetAddrMode<5,2>(opcode);

    const auto imm = ReadImmediate(proc, size);

    // resolve addressing mode and read data source; perform the operation
    auto resolved = ResolveEffectiveAddr(proc, addrMode, size, kAddrModeDataAlterable);
    const auto readData = ReadEffectiveAddr(proc, resolved, size);

    const uint32_t result = readData ^ imm;

    // update flags, do prefetch
    proc.regs.clearCcrBits(Ccr::Overflow | Ccr::Carry);
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));

    PrefetchCycle(proc);

    // write back to destination
    WriteEffectiveAddr(proc, resolved, size, result);

    /*
     * If this is a longword op with a data reg destination, add two extra no-op cycles at the end
     *
     * This behavior is common with EORI, ORI, ANDI, SUBI, ADDI
     */
    if(size == Size::Longword && addrMode.isDataRegister()) {
        proc.microcycleNop();
        proc.microcycleNop();
    }
}

/**
 * @brief EORI to SR, EORI to CCR
 *
 * XOR the low 16 or 8 bits of the immediate (depending on encoding) to the status register.
 *
 * The only difference in handling is that EORI to CCR only cosiders the low byte and does not
 * perform a privilege level check.
 */
void Decoder::ExclusiveOrImmediateToStatus(const uint16_t opcode, Processor &proc) {
    // encoding of the EORI to SR opcode
    constexpr static const uint16_t kEoriToSr{0b0000'1010'0111'1100};

    // get immediate operand
    const auto imm = ReadImmediate(proc, Size::Word);

    // ensure supervisor mode for EORI to SR
    if(opcode == kEoriToSr &&
            proc.regs.getPrivilegeLevel() != Processor::PrivilegeLevel::Supervisor) {
        PrefetchCycle(proc);
        return proc.setEvent(Processor::EventFlags::PrivilegeViolation);
    }

    // perform the XOR operation
    auto val = proc.regs.getStatus();
    val = (opcode == kEoriToSr) ? (val ^ imm) : (((val ^ imm) & 0xFF) | (val & ~0xFF));
    proc.regs.setStatus(val);

    // there are four nop microcycles here, ostensibly when the operation is performed?
    for(size_t i = 0; i < 4; i++) {
        proc.microcycleNop();
    }

    // perform final prefetch
    PrefetchCycle(proc);
}

