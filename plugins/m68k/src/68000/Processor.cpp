#include "Processor.h"
#include "Processor+Fmt.h"
#include "Decoder.h"
#include "../TraceTypes.h"

#include "version.h"

#include <arpa/inet.h>

#include <atomic>
#include <stdexcept>

#include <Bus.h>
#include <Config.h>
#include <ClockSource.h>
#include <Logger.h>
#include <interfaces/Device.h>

#include <magic_enum.hpp>

using namespace emulashione::plugin;
using namespace emulashione::m68k::m68000;

const std::string_view Processor::kVersion{gVERSION_SHORT};

EMULASHIONE_DEVICE_DESC(Processor, kSupportsStrobe, kEmulationModeCoThread,
        0x8D,0xB3,0xD6,0xD5,0xEC,0x90,0x4D,0xA3,0xBF,0x20,0x25,0xBE,0x5A,0xEB,0x67,0x8D);

/**
 * Initializes the internal state of the processor.
 */
Processor::Processor(const std::string_view &name, struct emulashione_config_object *_cfg) :
    CooperativeDevice(name, _cfg) {
    // register strobes
    registerStrobe(Strobes::RESET, "RESET", 1);
    registerStrobe(Strobes::HALT, "HALT", 1);
    registerStrobe(Strobes::BR, "BR", 1);
    registerStrobe(Strobes::BG, "BG", 1);
    registerStrobe(Strobes::IPL, "IPL", 3);
    registerStrobe(Strobes::VPA, "VPA", 1);

    // register address spaces
    registerAddressSpace(AddressSpaces::Memory, "Memory");
    registerAddressSpace(AddressSpaces::InterruptAck, "Interrupt Acknowledge");

    // register slots
    registerClockSourceSlotName("clock");
    registerBusSlotName("bus");

    // allocate a tracer if needed
    if(_cfg) {
        plugin::Config cfg(_cfg);
        if(cfg.hasKey("trace") && cfg.getBool("trace.enable")) {
            // TODO: replace max payload size with proper size
            this->tracer = std::make_unique<plugin::TraceManager>(0x0001, *this, 256);

            if(cfg.getBool("trace.memory")) {
                this->tracer->addMemoryOutput();
            }
        }
    }
}

/**
 * The processor device type has been registered; initialize some resources that will be shared
 * by all instances.
 */
void Processor::DidLoad() {
    Decoder::InitMap();
}

/**
 * Connects a bus; this stores a pointer to the bus on which the CPU sits, connected to slot `bus`.
 *
 * @param name Connection slot name
 * @param bus Bus to connect to this slot
 */
void Processor::connectBus(const std::string &name, const std::shared_ptr<plugin::Bus> &bus) {
    super::connectBus(name, bus);
    if(name == "bus") {
        this->bus = bus;
    }
}

/**
 * Disconnects a bus; this resets the pointer to the CPU bus.
 *
 * @param name Connection slot name
 */
void Processor::disconnectBus(const std::string &name) {
    if(name == "bus") {
        this->bus.reset();
    }
    super::disconnectBus(name);
}

/**
 * Connects a clock source; this stores the pointer to the CPU clock, connected to slot `clock`.
 *
 * @param name Connection slot name
 * @param clock Clock source to connect to this slot
 */
void Processor::connectClockSource(const std::string &name,
        const std::shared_ptr<plugin::ClockSource> &clock) {
    super::connectClockSource(name, clock);
    if(name == "clock") {
        this->clk = clock;
        Logger::Trace("New CPU clock: {} Hz", clock->getFrequency());
        // TODO: cache period with callback (for bus cycle calculation)
        this->clkPeriod = clock->getPeriod();
    }
}

/**
 * Disconnects a clock source; it resets the pointer to the CPU clock.
 *
 * @param name Connection slot name
 */
void Processor::disconnectClockSource(const std::string &name) {
    if(name == "clock") {
        this->clk.reset();
    }
    super::disconnectClockSource(name);
}



/**
 * Begins emulation. This is where we validate the required connections have been made.
 */
void Processor::willStartEmulation() {
    if(!this->bus) throw std::runtime_error("required connection `bus` missing");
    else if(!this->clk) throw std::runtime_error("required connection `clock` missing");
}

/**
 * Resets the processor.
 *
 * This will begin processing the reset exception.
 *
 * @param isHard Whether this is a hard or soft reset. This makes no difference to the processor,
 *        as any reset is emulated as both /RESET and /HALT going low.
 */
void Processor::reset(const bool isHard) {
    bool no{false};
    if(!this->resetPending.compare_exchange_strong(no, true, std::memory_order_acquire,
                std::memory_order_relaxed)) {
        Logger::Warning("Attempt to reset m68k instance ${} while reset is still pending",
                (void *) this);
    }
    (void) isHard;
}

/**
 * Shuts down the processor.
 */
void Processor::willShutDown() {
    // TODO: is there anything we need to do here?
}

/**
 * Allow copying out of both the trace handler and the trace handler's memory output under the
 * `tracer` and `traceMemOut` keys. Both of these are pointer sized.
 */
size_t Processor::copyOutInfo(const std::string_view &key, std::span<std::byte> &outBuf) {
    if(key == "tracer") {
        void *ptr = this->tracer.get();
        const auto copied = std::min(sizeof(ptr), outBuf.size());
        memcpy(outBuf.data(), &ptr, copied);
        return copied;
    } else if(key == "traceMemOut") {
        void *ptr = this->tracer->getMemOutput();
        const auto copied = std::min(sizeof(ptr), outBuf.size());
        memcpy(outBuf.data(), &ptr, copied);
        return copied;
    }

    return super::copyOutInfo(key, outBuf);
}


/**
 * Main emulation loop of the CPU. We'll continue to process instructions forever here.
 */
void Processor::main() {
    using namespace magic_enum::bitwise_operators;
    TraceRegisterState trs{};

    /*
     * Reset the initial processor state. On real hardware, we'd wait for both /RESET and /HALT to
     * be asserted before initializing, but we assume that's done as part of the setup of the
     * system we're in.
     */
    this->resetPending = false;
    this->doReset();

    // enter emulation loop
    while(1) {
        /*
         * Since resets can happen at any time, and are always accepted at the end of a microcycle,
         * it's easiest to just throw a particular exception whenever a reset is detected to
         * totally unwind the stack.
         *
         * This incurs the usual exception overhead, but we assume there's not a whole lot of
         * resets anyhow.
         */
        try {
            auto inst = this->queue.ir();

            if(kLogInstructions) {
                Logger::Trace("Decode [${:08x}]: ${:04x}", this->regs.pc-2-2, inst);
            }
            trs.pc = this->regs.pc-2-2;

            this->ticks = 0;

            // write trace state
            if(this->tracer) {
                this->regs.copyToTraceState(trs);
                this->tracer->putMessage(this->systemTime, kTraceMessageInstruction,
                        {reinterpret_cast<std::byte *>(&trs), sizeof(trs)});
            }

            // perform execution
            Decoder::Execute(*this, inst);

            if(kLogInstructionTimings) {
                Logger::Trace("Clocks: {}", this->ticks);
            }

            /*
             * Check for Group 1 exception conditions: that is, if trace interrupts are enabled,
             * an interrupt is pending, an illegal instruction was executed, or a privilege
             * violation encountered.
             *
             * These exceptions are processed in high to low priority order.
             */
            if(this->regs.isTraceEnabled()) {
                this->processType2Exception(ExceptionType::Trace);
            }
            // TODO: interrupts
            else if((this->events & EventFlags::IllegalInstruction) == EventFlags::IllegalInstruction) {
                this->processType2Exception(ExceptionType::IllegalInstruction);
                this->events &= ~EventFlags::IllegalInstruction;
            }
            else if((this->events & EventFlags::PrivilegeViolation) == EventFlags::PrivilegeViolation) {
                this->processType2Exception(ExceptionType::PrivilegeViolation);
                this->events &= ~EventFlags::PrivilegeViolation;
            }
        }
        // reset the processor
        catch(const ResetException &) {
            this->doReset();
        }
    }
}

/**
 * Aborts emulation because of a fatal error inside the core itself. This should ideally never
 * happen.
 *
 * @param cause Indicates why 68k machine broke
 */
void Processor::abort(const AbortReason cause) {
    Logger::Critical("68000 core failure: {}\n"
            "-------- Register Dump --------\n{}\n"
            "-------- Prefetch Queue -------\n{}",
            magic_enum::enum_name(cause), this->regs, this->queue);
    this->schedAbort(-420);
}

/**
 * Implements the reset vector handling. The processor's state is reset and execution begins at
 * the address specified in the vector table.
 *
 * @brief Process a reset exception
 */
void Processor::doReset() {
    // log the reset event
    if(this->tracer) {
        this->tracer->putMessage(this->systemTime, kTraceMessageReset);
    }

    // start out with 5x nops with idle wait
    for(size_t i = 0; i < 5; i++) {
        this->microcycleNop();
    }
    // then, there are two nop microcycles
    for(size_t i = 0; i < 2; i++) {
        this->microcycleNopWait();
    }

    /*
     * On reset, the status register is reset; the trace bit is cleared, and the supervisor bit is
     * set. All interrupts are masked; execution will resume in supervisor mode. The condition
     * codes will have the zero bit set.
     *
     * The content of all other registers is indeterminate. On powerup on real hardware, it appears
     * to be roughly random; we zero them though for reproducibility. (TODO: make this optional)
     */
    std::fill(this->regs.d.begin(), this->regs.d.end(), 0);
    std::fill(this->regs.a.begin(), this->regs.a.end(), 0);

    this->regs.setTrace(false);
    this->regs.setPrivilegeLevel(PrivilegeLevel::Supervisor);
    this->regs.setInterruptMask(0x7); // all masked
    this->regs.setCcr(Registers::ConditionCode::Zero);

    /*
     * Fetch the initial stack pointer (longword from offset $0 in the vector table) and the
     * initial program counter (longword from offset $4) and load them into the processor.
     *
     * Once they are loaded, perform two prefetch cycles against the new program counter so we can
     * begin execution once this returns back to the main loop.
     */
    this->regs.ssp = this->fetchVector(kVectorResetStack);
    this->regs.pc = this->fetchVector(kVectorResetPc);

    Logger::Debug("Reset Exception: Initial SSP ${:08x} PC ${:08x}", this->regs.ssp, this->regs.pc);

    this->microcycleNop();
    this->microcyclePrefetch();
    this->microcycleNop();
    this->microcycleNop();
    this->microcyclePrefetch();
}

/**
 * Processes a "Type 2" exception; that is, an exception that has the compact three-word stack
 * frame. All Group 1 and 2 exceptions fall into this category.
 *
 * The pushed stack frame is as follows, with the top being the lowest address (the stack grows
 * upward):
 *
 * \dot "Type 2 Stack Frame"
 * digraph stackframe {
 * graph [
 * rankdir="LR";
 * ];
 * frame [
 * label = "<f0> Status Register| <f1> Program Counter (high)| <f2> Program Counter (low)"
 * shape = "record"
 * ];
 *
 * }
 * \enddot
 *
 * @param type Exception type to begin processing; this determines the vector number base, and
 *        enables certain quirks (based on microcode bugs)
 * @param vectorOffset An offset to apply to the vector number. For interrupts, this indicates the
 *        interrupt level; while for TRAP, it indicates the trap literal.
 *
 * @brief Process a "Type 2" exception with a short stack frame
 */
void Processor::processType2Exception(const ExceptionType type, const uint8_t vectorOffset) {
    /*
     * On exception entry, the processor transitions to supervisor mode, and the trace function is
     * disabled. We save the status register, as it was at the start of the exception before we
     * molest it here.
     */
    const auto status = this->regs.getStatus();
    this->regs.setPrivilegeLevel(PrivilegeLevel::Supervisor);
    // XXX: is trace disabled fr?
    this->regs.setTrace(false);

    // figure out the vector number
    uint8_t vector{0};
    bool isGroup2{false};

    switch(type) {
        case ExceptionType::Trace:
            vector = kVectorTrace;
            break;
        //case ExceptionType::Interrupt:
        //    // TODO: implement
        //    break;
        case ExceptionType::IllegalInstruction:
            vector = kVectorIllegalInstruction;
            break;
        case ExceptionType::PrivilegeViolation:
            vector = kVectorPrivilegeViolation;
            break;

        case ExceptionType::TRAP:
            vector = kVectorTrapBase + (vectorOffset % 16);
            isGroup2 = true;
            break;
        case ExceptionType::TRAPV:
            vector = kVectorTrapvInstruction;
            isGroup2 = true;
            break;
        case ExceptionType::CHK:
            vector = kVectorChkInstruction;
            isGroup2 = true;
            break;
        case ExceptionType::DivideByZero:
            vector = kVectorDivideByZero;
            isGroup2 = true;
            break;

        default:
            throw std::runtime_error(fmt::format("Exception '{}' is not a known Type 2!",
                        magic_enum::enum_name(type)));
    }

    /*
     * Handle timings (and the emulated bus cycles) of the exception. This is roughly handled by
     * splitting between Group 1 and Group 2 exceptions.
     *
     * For Group 1 exceptions, they all start off with three noops and stacking of the status
     * word. Interrupts then perform an interrupt acknowledge cycle, but otherwise all Group 1
     * exceptions are relatively similar. The only difference appears to be in the order that
     * the PC is stacked.
     */
    if(!isGroup2) {
        for(size_t i = 0; i < 3; i++) this->microcycleNop();

        // perform the interrupt acknowledge cycles, if needed
        if(type == ExceptionType::Interrupt) {
            // TODO: implement
        }

        // stack PC (reversed for trace)
        if(type == ExceptionType::Trace) {
            this->stackPush(this->regs.pc - 2, true);
        }
        // stack PC (regular order for all others)
        else {
            this->stackPush(this->regs.pc - 2);
        }

        // stack the status register
        this->stackPush(status);
    }
    /*
     * Timings for Group 2 exceptions vary so we special case each of the exceptions here.
     *
     * XXX: This is technically wrong, since the real order of bus cycles here is the low word of
     * the PC (SP-2), the status register (SP-6,) then finally the high word of PC (SP-4)
     */
    else {
        // divide by zero has two more nops here
        if(type == ExceptionType::DivideByZero) {
            this->microcycleNop();
            this->microcycleNop();
        }
        // all except trapv have two nop cycles now
        if(type != ExceptionType::TRAPV) {
            this->microcycleNop();
            this->microcycleNop();
        }

        // build stack frame
        this->stackPush(this->regs.pc - 2);
        this->stackPush(status);

        // TODO: anything else?
        // throw std::runtime_error("Group 2 exceptions not yet implemented");
    }

    // update PC with the appropriate vector
    this->regs.pc = this->fetchVector(vector);

    Logger::Debug("Type 2 exception: {} vector ${:02x} -> ${:08x}", magic_enum::enum_name(type),
            vector, this->regs.pc);

    this->microcycleNop();
    this->microcyclePrefetch();
    this->microcycleNop();
    this->microcycleNop();
    this->microcyclePrefetch();
}



/**
 * Reads the nth entry in the vector table as a longword and returns its value. As with all 68k
 * long word accesses, this first reads the high word (at offset $0) and then the low word (at
 * offset $2.)
 *
 * @todo Handle the difference between supervisor program (reset SSP/PC) and data (all other)
 *
 * @param vec Index into the vector table
 *
 * @brief Reads a longword value out of the vector table
 */
uint32_t Processor::fetchVector(const size_t vec) {
    uint32_t temp{0};

    // all vectors, except reset SSP/PC, are fetched as supervisor data
    FunctionCode fc{FunctionCode::SupervisorData};

    if(vec < 2) {
        fc = FunctionCode::SupervisorProgram;
    }

    // calculate the memory address
    const uint32_t addr{kVectorBase + (static_cast<uint32_t>(vec) * 4)};

    temp |= static_cast<uint32_t>(this->readWord(fc, addr + 0)) << 16;
    temp |= static_cast<uint32_t>(this->readWord(fc, addr + 2));

    return temp;
}

/**
 * Prefetches a word from the current program counter and inserts it into the prefetch queue, so
 * that it eventually gets decoded.
 *
 * This is used when the prefetched word is part of an instruction; it is NOT an extension word
 * to an existing instruction. (These extension words should instead use the prefetchExtension()
 * call, which doesn't actually insert it into the prefetch queue.)
 *
 * @brief Prefetch a word and insert it into the queue to be decoded later.
 *
 * @todo Determine whether the supervisor/user mode is determined directly by the status register
 *       or if there are other pieces that affect it.
 */
void Processor::prefetch() {
    const auto data = this->prefetchWord();
    this->queue.push(data);
}

/**
 * Performs a prefetch, including incrementing PC, but does not write the fetched value into the
 * prefetch queue; instead return it. PC is incremented by 2. This is intended to be used for
 * fetching extension words of multiword instructions.
 *
 * @return Value prefetched from current PC
 *
 * @brief Prefetch a word from the next program counter value
 */
uint16_t Processor::prefetchWord() {
    auto fc{FunctionCode::SupervisorProgram};
    if(this->regs.getPrivilegeLevel() == PrivilegeLevel::User) {
        fc = FunctionCode::UserProgram;
    }

    const auto data = this->readWord(fc, this->regs.pc);
    this->regs.pc += 2;

    return data;
}

/**
 * Reads an 8-bit byte from the bus.
 *
 * @todo This doesn't really implement the USW/LSW strobes; it rather does an 8 bit bus transaction
 *       directly and expects the bus layer to do the Right Thing™.
 *
 * @param fc Function code indicating the type of bus access
 * @param address Memory address to read from
 *
 * @return Data read from the bus
 */
uint8_t Processor::readByte(const FunctionCode fc, const uint32_t address) {
    uint8_t data;
    double time;

    // perform read from our associated bus
    try {
        this->bus->read(this->getInstancePtr(),
                (fc==FunctionCode::CpuSpace) ? AddressSpaces::InterruptAck : AddressSpaces::Memory,
                address & kAddressMask, data, time);
    }
    // raise bus error, if access failed
    catch(const Bus::BusError &e) {
        Logger::Warning("Read byte ({}) failed: {}", magic_enum::enum_name(fc), e.what());
        // TODO: start bus error vector processing
    }

    if(gLogReads) Logger::Trace("Read {}:${:08x} = ${:02x}", magic_enum::enum_name(fc), address,
            data);

    this->handleBusWaits(time);

    return data;
}

/**
 * Reads a 16-bit word from the bus.
 *
 * @remark This call takes care of advancing the processor clock by the correct number of cycles.
 *         All bus cycles are at least 2 clock cycles long, with additional wait states (2 cycles
 *         each) inserted as needed.
 *
 *         It also handles the byteswapping required; memories would contain data in big endian
 *         order, but we need to work with it in little endian here.
 *
 * @todo Implement address error handling, if the address is odd.
 *
 * @param fc Function code indicating the type of bus access
 * @param address Memory address to read from
 *
 * @return Data read from the bus
 */
uint16_t Processor::readWord(const FunctionCode fc, const uint32_t address) {
    uint16_t data;
    double time;

    // perform read from our associated bus
    try {
        this->bus->read(this->getInstancePtr(),
                (fc==FunctionCode::CpuSpace) ? AddressSpaces::InterruptAck : AddressSpaces::Memory,
                address & kAddressMask, data, time);
    }
    // raise bus error, if access failed
    catch(const Bus::BusError &e) {
        Logger::Warning("Read word ({}) failed: {}", magic_enum::enum_name(fc), e.what());
        // TODO: start bus error vector processing
    }

    // byteswap if host isn't big endian
    data = ntohs(data);

    if(gLogReads) Logger::Trace("Read {}:${:08x} = ${:04x}", magic_enum::enum_name(fc), address,
            data);

    this->handleBusWaits(time);

    return data;
}

/**
 * Write a 8 bit byte to the bus.
 *
 * @todo This doesn't really implement the USW/LSW strobes; it rather does an 8 bit bus transaction
 *       directly and expects the bus layer to do the Right Thing™.
 *
 * @param fc Function code indicating the type of bus access
 * @param address Memory address to write to
 * @param data 8 bit value to write into this word
 */
void Processor::writeByte(const FunctionCode fc, const uint32_t address, const uint8_t data) {
    double time;

    if(gLogWrites) Logger::Trace("Write {}:${:08x} = ${:02x}", magic_enum::enum_name(fc), address,
            data);

    // perform write to our associated bus
    try {
        this->bus->write(this->getInstancePtr(),
                (fc==FunctionCode::CpuSpace) ? AddressSpaces::InterruptAck : AddressSpaces::Memory,
                address & kAddressMask, data, time);
    }
    // raise bus error, if access failed
    catch(const Bus::BusError &e) {
        Logger::Warning("Write byte ({}) failed: {}", magic_enum::enum_name(fc), e.what());
        // TODO: start bus error vector processing
    }

    // update timings
    this->handleBusWaits(time);
}

/**
 * Write a 16-bit word to the bus.
 *
 * @todo Implement address error handling, if the address is odd.
 *
 * @param fc Function code indicating the type of bus access
 * @param address Memory address to write to
 * @param data 16 bit value to write into this word
 */
void Processor::writeWord(const FunctionCode fc, const uint32_t address, const uint16_t data) {
    double time;

    if(gLogWrites) Logger::Trace("Write {}:${:08x} = ${:04x}", magic_enum::enum_name(fc), address,
            data);

    // perform write to our associated bus
    try {
        this->bus->write(this->getInstancePtr(),
                (fc==FunctionCode::CpuSpace) ? AddressSpaces::InterruptAck : AddressSpaces::Memory,
                address & kAddressMask, htons(data), time);
    }
    // raise bus error, if access failed
    catch(const Bus::BusError &e) {
        Logger::Warning("Write word ({}) failed: {}", magic_enum::enum_name(fc), e.what());
        // TODO: start bus error vector processing
    }

    // update timings
    this->handleBusWaits(time);
}



/**
 * Marks a particular event as pending.
 *
 * @param flags Event bits to set
 */
void Processor::setEvent(const EventFlags flags) {
    using namespace magic_enum::bitwise_operators;
    this->events |= flags;
}
