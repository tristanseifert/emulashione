/*
 * Implementations of branch type instructions: BRA/BSR, RTS/RTE.
 */
#include "Decoder.h"
#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k::m68000;

/**
 * @brief BRA.B #imm / BRA.W #imm
 *
 * Handles an unconditional branch instruction.
 */
void Decoder::Branch(const uint16_t opcode, Processor &proc) {
    proc.microcycleNop();
    BranchCommon(opcode, proc);
}

/**
 * @brief BSR.B #imm / BSR.W #imm
 *
 * Handles an unconditional branch subroutine instruction. Before transfering control to the new
 * function, a longword is written to the stack with the PC.
 */
void Decoder::BranchSub(const uint16_t opcode, Processor &proc) {
    proc.microcycleNop();

    uint32_t pc = proc.regs.pc;
    if(GetBits<7,8>(opcode)) {
        pc -= 2;
    }

    proc.stackPush(pc);

    BranchCommon(opcode, proc);
}

/**
 * Code common to both the BRA and BSR instructions; namely, the part that actually updates PC.
 *
 * Branch instructions may have either an 8 bit displacement (immediate, as part of the opcode) or
 * one extension word containing a 16-bit displacement. In both cases, the displacement is signed
 * and is sign extended to 32 bits before adding to PC.
 *
 * We will select the 16-bit displacement extension word if the 8-bit displacement is zero.
 */
void Decoder::BranchCommon(const uint16_t opcode, Processor &proc) {
    const auto pc = proc.regs.pc;
    int32_t disp;

    // if the 8-bit displacement is nonzero, use it directly
    const auto disp8 = static_cast<uint8_t>(GetBits<7,8>(opcode));
    if(disp8) {
        disp = SignExtend<int32_t,8>(disp8);
    }
    // fetch the extension word; note this does not require any extra bus cycles
    else {
        const auto disp16 = proc.getExtensionWord();
        disp = SignExtend<int32_t,16>(disp16);
    }

    // perform the PC update and prefetch
    proc.regs.pc = (pc - 2) + disp;

    for(size_t i = 0; i < 2; i++) {
        PrefetchCycle(proc);
    }
}


/**
 * @brief JMP
 *
 * Jumps to the address specified by the effective address.
 */
void Decoder::Jump(const uint16_t opcode, Processor &proc) {
    // calculate address
    const auto destination = CalculateEffectiveAddr(proc, GetAddrMode<5,2>(opcode), Size::None,
            kAddrModeControl, false);

    // update PC and perform prefetches
    proc.regs.pc = destination;
    if(kLogBranch) Logger::Trace("JMP to: ${:08x}", proc.regs.pc);

    for(size_t i = 0; i < 2; i++) {
        PrefetchCycle(proc);
    }
}

/**
 * @brief JSR
 *
 * Jumps to the address specified by the effective address, and pushes the old program counter
 * value on the stack. The PC is stacked between the two prefetch cycles
 */
void Decoder::JumpSub(const uint16_t opcode, Processor &proc) {
    // calculate address and return address
    const auto addrMode = GetAddrMode<5,2>(opcode);
    const auto destination = CalculateEffectiveAddr(proc, addrMode, Size::None,
            kAddrModeControl, false);

    auto oldPc = proc.regs.pc;
    if(!addrMode.hasExtensionWords()) {
        // for (An), we do not have extension words, so the return PC needs to be tweaked
        oldPc -= 2;
    }

    // update PC and perform first prefetch
    proc.regs.pc = destination;
    if(kLogBranch) Logger::Trace("JSR to: ${:08x}, return to ${:08x}", proc.regs.pc, oldPc);

    PrefetchCycle(proc);

    // stack PC and perform last prefetch
    proc.stackPush(oldPc);

    PrefetchCycle(proc);
}


/**
 * @brief RTS
 *
 * Returns from a subroutine; this pops a longword off the stack and reloads that into PC.
 */
void Decoder::ReturnSub(const uint16_t, Processor &proc) {
    // read a long from the stack
    uint32_t returnTo;
    returnTo = proc.stackPopLongWord();

    if(kLogBranch) Logger::Trace("RTS to: ${:08x}", returnTo);

    // update PC and prefetch
    proc.regs.pc = returnTo;

    for(size_t i = 0; i < 2; i++) {
        PrefetchCycle(proc);
    }
}

/**
 * @brief RTE
 *
 * Returns from an exception; this pops the status register, then the program counter.
 *
 * @todo Determine whether the order of pops is right.
 */
void Decoder::ReturnException(const uint16_t, Processor &proc) {
    // read status register and PC
    const auto status = proc.stackPopWord();
    const auto returnTo = proc.stackPopLongWord();

    // then restore them and perform prefetch
    proc.regs.setStatus(status);
    proc.regs.pc = returnTo;

    if(kLogBranch) Logger::Trace("RTE to: ${:08x}, SR ${:04x}", returnTo, status);

    for(size_t i = 0; i < 2; i++) {
        PrefetchCycle(proc);
    }
}

/**
 * @brief TRAP
 *
 * Transitions to supervisor mode with the specified trap vector.
 */
void Decoder::Trap(const uint16_t opcode, Processor &proc) {
    const auto offset = GetBits<3,4>(opcode);

    if(kLogTraps) {
        Logger::Trace("TRAP #{}", offset);
    }

    proc.processType2Exception(Processor::ExceptionType::TRAP, offset);
}

/**
 * @brief TRAPV
 *
 * Performs identically to "TRAP #7" if the overflow bit is set; otherwise this behaves exactly
 * like a no-op.
 */
void Decoder::TrapV(const uint16_t opcode, Processor &proc) {
    // no-op bebavhor if oferflow is _not_ set
    if(!proc.regs.testConditionCode(Ccr::Overflow)) {
        if(kLogTraps) Logger::Trace("TRAPV: no trap");
        return PrefetchCycle(proc);
    }

    // otherwise, take a trap
    if(kLogTraps) Logger::Trace("TRAPV: trap");
    proc.processType2Exception(Processor::ExceptionType::TRAP, 7);
}
