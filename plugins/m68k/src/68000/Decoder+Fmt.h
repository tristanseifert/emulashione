#ifndef M68000_DECODER_FMT_H
#define M68000_DECODER_FMT_H

#include "Decoder.h"

#include <array>
#include <string_view>

#include <fmt/core.h>
#include <magic_enum.hpp>

/// Formatter for effective addressing mode
template <> struct fmt::formatter<emulashione::m68k::m68000::Decoder::AddressingMode> {
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const emulashione::m68k::m68000::Decoder::AddressingMode mode,
            FormatContext& ctx) -> decltype(ctx.out()) {
        auto it = ctx.out();
        // get a nice name for it
        switch(mode.M) {
            case 0b000: // Dn
                it = format_to(it, "D{:1}", mode.Xn & 0b111);
                break;
            case 0b001: // An
                it = format_to(it, "A{:1}", mode.Xn & 0b111);
                break;
            case 0b010: // (An)
                it = format_to(it, "(A{:1})", mode.Xn & 0b111);
                break;
            case 0b011: // (An)+
                it = format_to(it, "(A{:1})+", mode.Xn & 0b111);
                break;
            case 0b100: // -(An)
                it = format_to(it, "-(A{:1})", mode.Xn & 0b111);
                break;
            case 0b101: // (d16, An)
                it = format_to(it, "(d16, A{:1})", mode.Xn & 0b111);
                break;
            case 0b110: // (d8, An, Xn)
                it = format_to(it, "(d8, A{:1}, Xn)", mode.Xn & 0b111);
                break;
            case 0b111: // special
                switch(mode.Xn) {
                    case 0b000:
                        it = format_to(it, "(xxx).W");
                        break;
                    case 0b001:
                        it = format_to(it, "(xxx).L");
                        break;
                    case 0b010:
                        it = format_to(it, "(d16, PC)");
                        break;
                    case 0b011:
                        it = format_to(it, "(d8, PC, Xn)");
                        break;
                    case 0b100:
                        it = format_to(it, "#imm");
                        break;

                    default:
                        it = format_to(it, "unknown special mode");
                }
                break;

            default:
                it = format_to(it, "unknown");
        }

        // write literal values
        it = format_to(it, " [{:1x},{:1x}]", mode.M, mode.Xn);
        return it;
    }
};

/// Formatter for a resolved effective addressing mode
template <> struct fmt::formatter<emulashione::m68k::m68000::Decoder::ResolvedAddressingMode> {
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const emulashione::m68k::m68000::Decoder::ResolvedAddressingMode &r,
            FormatContext& ctx) -> decltype(ctx.out()) {
        auto it = ctx.out();

        // write the actual mode
        it = format_to(it, "{}", r.mode);

        // and its address if any
        if(r.mode.isMemory()) {
            it = format_to(it, " = ${:08x}", r.address);
        }

        return it;
    }
};

#endif
