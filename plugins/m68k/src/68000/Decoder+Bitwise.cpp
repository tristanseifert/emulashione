/*
 * Bitwise operations: BTST, BCHG, BCLR, BSET
 */
#include "Decoder.h"
#include "Decoder+Fmt.h"
#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k;
using namespace emulashione::m68k::m68000;

/**
 * Common code to either set, clear, or toggle a bit in either memory or a register.
 *
 * The zero flag is loaded with the inverse of the original value of the bit prior to updating the
 * destination.
 *
 * @todo Validate timings
 *
 * @param op Bit operation to perform
 */
void Decoder::BitChangeCommon(const uint16_t opcode, Processor &proc, const BitChangeOp op) {
    // determine bit number and the destination
    uint8_t bitNumber{0};

    if(GetBits<8,1>(opcode)) { // it is in a register
        bitNumber = static_cast<uint8_t>(proc.regs.getDn(GetRegIndex<11>(opcode)));
    } else { // immediate word
        const auto imm = proc.getExtensionWord();
        bitNumber = (imm & 0xFF);

        PrefetchCycle(proc);
    }

    // prepare destination effective addr
    const auto destEaMode = GetAddrMode<5,2>(opcode);
    const auto destMode = ResolveEffectiveAddr(proc, destEaMode, Size::Byte, kAddrModeDataAlterable);

    if(kLogBitwiseChange) {
        Logger::Trace("{}: Bit {:2}, dest {}", magic_enum::enum_name(op), bitNumber, destMode);
    }

    // read the previous value
    uint32_t oldValue{0};

    if(destMode.isDataRegister()) {
        bitNumber %= 32;
        oldValue = proc.regs.getDn(destMode.getXn());
    } else if(destMode.isMemory()) {
        bitNumber %= 8;
        oldValue = ReadEffectiveAddr(proc, destMode, Size::Byte);
    } else {
        Logger::Warning("{}: Invalid destination mode: {}", magic_enum::enum_name(op), destMode);

        // prefetch so that the pc points to the END of this instruction
        PrefetchCycle(proc);
        return proc.setEvent(Processor::EventFlags::IllegalInstruction);
    }

    // change the bit, and update flags with old value
    uint32_t newValue = oldValue;
    const uint32_t bit{1U << bitNumber};

    switch(op) {
        case BitChangeOp::BSET:
            newValue |= bit;
            break;
        case BitChangeOp::BCLR:
            newValue &= ~bit;
            break;
        case BitChangeOp::BCHG:
            newValue ^= bit;
            break;
    }

    const bool zeroFlag = !(oldValue & bit);
    proc.regs.setCcr(Ccr::Zero, zeroFlag);

    if(kLogBitwiseChange) {
        Logger::Trace("{}: old ${:08x} new ${:08x} Z {}", magic_enum::enum_name(op), oldValue,
                newValue, zeroFlag);
    }

    // perform the final prefetch cycle
    PrefetchCycle(proc);

    /*
     * When performing operations against registers, and the bit index is beyond 15, we have to add
     * an additional wait cycle. Additionally, when CLEARING a bit only, there is yet another wait
     * cycle that is added.
     *
     * This is the only special case if the bit operation is different. Timings are otherwise
     * identical.
     */
    if(destMode.isDataRegister()) {
        proc.microcycleNop();
        if(op == BitChangeOp::BCLR) {
            proc.microcycleNop();
        }

        if(bitNumber > 15) {
            proc.microcycleNop();
        }
    }

    // write back
    if(destMode.isDataRegister()) {
        proc.regs.getDn(destMode.getXn()) = newValue;
    } else {
        WriteEffectiveAddr(proc, destMode, Size::Byte, newValue);
    }
}

/**
 * @brief BTST
 *
 * Tests the specified bit and writes the inverse of the bit to the Z flag.
 *
 * @todo Validate timing
 */
void Decoder::BitTest(const uint16_t opcode, Processor &proc) {
    // determine bit number and the destination
    uint8_t bitNumber{0};

    if(GetBits<8,1>(opcode)) { // it is in a register
        bitNumber = static_cast<uint8_t>(proc.regs.getDn(GetRegIndex<11>(opcode)));
    } else { // immediate word
        const auto imm = proc.getExtensionWord();
        bitNumber = (imm & 0xFF);

        PrefetchCycle(proc);
    }

    // figure out the destination
    const auto destEaMode = GetAddrMode<5,2>(opcode);
    const auto destMode = ResolveEffectiveAddr(proc, destEaMode, Size::Byte, kAddrModeData);

    if(kLogBitwiseChange) {
        Logger::Trace("BTST: Bit {:2}, dest {}", bitNumber, destMode);
    }

    // read the previous value
    uint32_t oldValue{0};

    if(destMode.isDataRegister()) {
        bitNumber %= 32;
        oldValue = proc.regs.getDn(destMode.getXn());
    } else if(destMode.isMemory()) {
        bitNumber %= 8;
        oldValue = ReadEffectiveAddr(proc, destMode, Size::Byte);
    } else {
        Logger::Warning("BTST: Invalid destination mode: {}", destMode);

        // prefetch so that the pc points to the END of this instruction
        PrefetchCycle(proc);
        return proc.setEvent(Processor::EventFlags::IllegalInstruction);
    }

    // update flags and perform prefetch
    const uint32_t bit{1U << bitNumber};
    const bool zeroFlag = !(oldValue & bit);
    proc.regs.setCcr(Ccr::Zero, zeroFlag);

    PrefetchCycle(proc);

    if(destMode.isDataRegister()) {
        proc.microcycleNop();
    }
}
