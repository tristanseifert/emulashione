/*
 * Arithmetic - Bit shifts: ASL/ASR, LSL/LSR
 */
#include "Decoder.h"
#include "Decoder+Fmt.h"
#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

#include <bitset>

using namespace emulashione::plugin;
using namespace emulashione::m68k;
using namespace emulashione::m68k::m68000;

using namespace magic_enum::bitwise_operators;

/**
 * @brief LSL
 *
 * Shift the value in the destination (either a register or memory) left by the specified number of
 * bits, then writes the result back.
 *
 * V is always cleared, C and X are set according to the last bit shifted out of the operand, and
 * N and Z are updated as normal.
 *
 * @todo Look at the behavior of shifts ≥ 32 bits on x86 platform; the shift count is modulus 32
 *       here and might break this. It's worth looking into this for other platforms too.
 */
void Decoder::LogicalShiftLeft(const uint16_t opcode, Processor &proc) {
    uint8_t shiftCount{0};
    Size size;

    /*
     * Figure out whether this instruction shifts the contents of a data register, or if it uses
     * memory. Then either decode the effective addressing mode specified in the instruction or
     * fake one to represent a data register.
     *
     * If memory is used, the size is fixed at word, and the shift count is fixed at 1.
     *
     * For register based operations, the shift count can either be a value in the instruction
     * field (where 1-7 = 1-7, and 0 = 8 bits) or in another data register.
     *
     * In both cases, the shift count is modulus 64.
     */
    constexpr static const uint16_t kMemoryMask{0b1111111111000000},
              kMemoryMaskValue{0b1110001111000000};
    const bool toMemory = (opcode & kMemoryMask) == kMemoryMaskValue;
    AddressingMode addrMode;

    if(toMemory) {
        shiftCount = 1;
        size = Size::Word;

        addrMode = GetAddrMode<5,2>(opcode);
    } else {
        const bool ir = !!GetBits<5,1>(opcode);
        size = GetSize(opcode);

        // shift count is in a data register
        if(ir) {
            const auto shiftCountReg = GetRegIndex<11>(opcode);
            shiftCount = proc.regs.getDn(shiftCountReg);
        }
        // shift count is in bits 11-9
        else {
            shiftCount = GetBits<11,3>(opcode);
            if(!shiftCount) shiftCount = 8;
        }

        // what is the destination register?
        const auto Dn = GetRegIndex<2>(opcode);
        addrMode = AddressingMode::MakeDn(Dn);
    }

    shiftCount %= 64;

    /*
     * Read the original data (from memory or data register) and then perform the required
     * prefetch cycle to get the next address.
     */
    const auto resolved = ResolveEffectiveAddr(proc, addrMode, size);
    const auto input = ReadEffectiveAddr(proc, resolved, size);

    if(kLogLogicalShift) Logger::Trace("LSL: Shift {}, dest {}, {}", shiftCount, resolved, size);

    PrefetchCycle(proc);

    /*
     * Now perform the operation. For every bit we shift, we insert an additional no-op microcycle
     * to emulate the correct timings; except in memory modes, where this latency is hidden.
     *
     * Also update the flags; N and Z are normal, while C/X will both hold the last bit that was
     * shifted left.
     */
    uint32_t result = input;
    result = result << static_cast<uint32_t>(shiftCount);

    if(!toMemory) {
        for(size_t i = 0; i < shiftCount; i++) {
            proc.microcycleNop();
        }
    }

    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Overflow, false);

    if(shiftCount) {
        if(shiftCount > SizeToBits(size)) {
            proc.regs.setCcr(Ccr::Carry | Ccr::Extend, false);
        } else {
            const auto bitIndex = SizeToBits(size) - shiftCount;
            proc.regs.setCcr(Ccr::Carry | Ccr::Extend, !!(input & (1 << bitIndex)));
        }
    } else {
        proc.regs.setCcr(Ccr::Carry, false);
    }

    /*
     * Write the result back. Add one or two nop cycles for register modes, depending on size.
     */
    WriteEffectiveAddr(proc, resolved, size, result);

    if(kLogLogicalShift) Logger::Trace("LSL: Read data ${:08x}, write data ${:08x}", input,result);

    if(!toMemory) {
        proc.microcycleNop();
        if(size == Size::Longword) proc.microcycleNop();
    }
}

/**
 * @brief LSR
 *
 * Shift the destination right `n` bits. It is virtually identical to LSL.
 */
void Decoder::LogicalShiftRight(const uint16_t opcode, Processor &proc) {
    uint8_t shiftCount{0};
    Size size;

    // get sources
    constexpr static const uint16_t kMemoryMask{0b1111111111000000},
              kMemoryMaskValue{0b1110001011000000};
    const bool toMemory = (opcode & kMemoryMask) == kMemoryMaskValue;
    AddressingMode addrMode;

    if(toMemory) {
        shiftCount = 1;
        size = Size::Word;

        addrMode = GetAddrMode<5,2>(opcode);
    } else {
        const bool ir = !!GetBits<5,1>(opcode);
        size = GetSize(opcode);

        // shift count is in a data register
        if(ir) {
            const auto shiftCountReg = GetRegIndex<11>(opcode);
            shiftCount = proc.regs.getDn(shiftCountReg);
        }
        // shift count is in bits 11-9
        else {
            shiftCount = GetBits<11,3>(opcode);
            if(!shiftCount) shiftCount = 8;
        }

        // what is the destination register?
        const auto Dn = GetRegIndex<2>(opcode);
        addrMode = AddressingMode::MakeDn(Dn);
    }

    shiftCount %= 64;

    // read data
    const auto resolved = ResolveEffectiveAddr(proc, addrMode, size);
    const auto input = ReadEffectiveAddr(proc, resolved, size);

    if(kLogLogicalShift) Logger::Trace("LSR: Shift {}, dest {}, {}", shiftCount, resolved, size);

    PrefetchCycle(proc);

    // do operation
    uint32_t result = input;
    result = result >> static_cast<uint32_t>(shiftCount);

    if(!toMemory) {
        for(size_t i = 0; i < shiftCount; i++) {
            proc.microcycleNop();
        }
    }

    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Overflow, false);

    if(shiftCount) {
        if(shiftCount > SizeToBits(size)) {
            proc.regs.setCcr(Ccr::Carry | Ccr::Extend, false);
        } else {
            const auto bitIndex = shiftCount - 1;
            proc.regs.setCcr(Ccr::Carry | Ccr::Extend, !!(input & (1 << bitIndex)));
        }
    } else {
        proc.regs.setCcr(Ccr::Carry, false);
    }

    // writeback
    WriteEffectiveAddr(proc, resolved, size, result);

    if(kLogLogicalShift) Logger::Trace("LSR: Read data ${:08x}, write data ${:08x}", input,result);

    if(!toMemory) {
        proc.microcycleNop();
        if(size == Size::Longword) proc.microcycleNop();
    }
}


/**
 * @brief ASL
 *
 * This works basically the same as LSL, except that V is updated to indicate whether the sign bit
 * changed at any point during the operation.
 *
 * @todo Look at the behavior of shifts ≥ 32 bits on x86 platform; the shift count is modulus 32
 *       here and might break this. It's worth looking into this for other platforms too.
 */
void Decoder::ArithmeticShiftLeft(const uint16_t opcode, Processor &proc) {
    uint8_t shiftCount{0};
    Size size;

    constexpr static const uint16_t kMemoryMask{0b1111111111000000},
              kMemoryMaskValue{0b1110000111000000};
    const bool toMemory = (opcode & kMemoryMask) == kMemoryMaskValue;
    AddressingMode addrMode;

    if(toMemory) {
        shiftCount = 1;
        size = Size::Word;

        addrMode = GetAddrMode<5,2>(opcode);
    } else {
        const bool ir = !!GetBits<5,1>(opcode);
        size = GetSize(opcode);

        // shift count is in a data register
        if(ir) {
            const auto shiftCountReg = GetRegIndex<11>(opcode);
            shiftCount = proc.regs.getDn(shiftCountReg);
        }
        // shift count is in bits 11-9
        else {
            shiftCount = GetBits<11,3>(opcode);
            if(!shiftCount) shiftCount = 8;
        }

        // what is the destination register?
        const auto Dn = GetRegIndex<2>(opcode);
        addrMode = AddressingMode::MakeDn(Dn);
    }

    shiftCount %= 64;

    // read data
    const auto resolved = ResolveEffectiveAddr(proc, addrMode, size);
    const auto input = ReadEffectiveAddr(proc, resolved, size);

    if(kLogArithmeticShift) Logger::Trace("ASL: Shift {}, dest {}, {}", shiftCount, resolved,size);

    PrefetchCycle(proc);

    /*
     * Perform update. The carry flag indicates if the sign bit changed at all: to test this we
     * look at the part of the destination that was shifted through the sign bit, and if it isn't
     * all 1 or 0 bits, we know the sign changed.
     *
     * Also update the flags; N and Z are normal, while C/X will both hold the last bit that was
     * shifted left.
     */
    uint32_t result = input;
    result = result << static_cast<uint32_t>(shiftCount);

    if(!toMemory) {
        for(size_t i = 0; i < shiftCount; i++) {
            proc.microcycleNop();
        }
    }

    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));

    if(shiftCount) {
        if(shiftCount > SizeToBits(size)) {
            proc.regs.setCcr(Ccr::Carry | Ccr::Extend, false);
        } else {
            const auto bitIndex = SizeToBits(size) - shiftCount;
            proc.regs.setCcr(Ccr::Carry | Ccr::Extend, !!(input & (1 << bitIndex)));
        }

        // get the high n bits that were yeeted out
        const auto shiftedOut = GetBits(input, (SizeToBits(size) - 1), shiftCount);
        std::bitset<32> bits(shiftedOut);
        const auto overflow = !((!bits.count()) || (bits.count() == shiftCount));

        if(kLogArithmeticShift) Logger::Trace("ASL: Shifted out ${:08x}, count {}, V = {}",
                shiftedOut, bits.count(), overflow);

        proc.regs.setCcr(Ccr::Overflow, overflow);
    } else {
        proc.regs.setCcr(Ccr::Carry | Ccr::Overflow, false);
    }

    // writeback
    WriteEffectiveAddr(proc, resolved, size, result);

    if(kLogArithmeticShift) Logger::Trace("ASL: Read data ${:08x}, write data ${:08x}", input,result);

    if(!toMemory) {
        proc.microcycleNop();
        if(size == Size::Longword) proc.microcycleNop();
    }
}

/*
 * @brief ASR
 *
 * This is essentially the same as ASL but with the reverse direction.
 *
 * @todo The behavior of the sign extension should be validated particularly with shifts greater
 *       than the width of the access
 */
void Decoder::ArithmeticShiftRight(const uint16_t opcode, Processor &proc) {
    uint8_t shiftCount{0};
    Size size;

    // get sources
    constexpr static const uint16_t kMemoryMask{0b1111111111000000},
              kMemoryMaskValue{0b1110000011000000};
    const bool toMemory = (opcode & kMemoryMask) == kMemoryMaskValue;
    AddressingMode addrMode;

    if(toMemory) {
        shiftCount = 1;
        size = Size::Word;

        addrMode = GetAddrMode<5,2>(opcode);
    } else {
        const bool ir = !!GetBits<5,1>(opcode);
        size = GetSize(opcode);

        // shift count is in a data register
        if(ir) {
            const auto shiftCountReg = GetRegIndex<11>(opcode);
            shiftCount = proc.regs.getDn(shiftCountReg);
        }
        // shift count is in bits 11-9
        else {
            shiftCount = GetBits<11,3>(opcode);
            if(!shiftCount) shiftCount = 8;
        }

        // what is the destination register?
        const auto Dn = GetRegIndex<2>(opcode);
        addrMode = AddressingMode::MakeDn(Dn);
    }

    shiftCount %= 64;

    // read data
    const auto resolved = ResolveEffectiveAddr(proc, addrMode, size);
    const auto input = ReadEffectiveAddr(proc, resolved, size);

    if(kLogArithmeticShift) Logger::Trace("ASR: Shift {}, dest {}, {}", shiftCount, resolved, size);

    PrefetchCycle(proc);

    /*
     * Shift right, and fill the bits on the left in with the sign bit of the input. Then, update
     * the flags and write it back. By definition the sign bit can never change so the overflow
     * bit is always cleared.
     */
    uint32_t result = input;
    result = result >> static_cast<uint32_t>(shiftCount);

    uint8_t lastDataBit{0};
    if(shiftCount < SizeToBits(size)) {
        lastDataBit = SizeToBits(size) - shiftCount;
    }

    std::bitset<32> bits(result);
    const auto signBit = !!(input & SignBitFor(size));

    for(size_t i = 0; i < (SizeToBits(size) - lastDataBit); i++) {
        const auto bitOff = SizeToBits(size) - i - 1;
        bits[bitOff] = signBit;
    }
    result = bits.to_ulong();

    if(!toMemory) {
        for(size_t i = 0; i < shiftCount; i++) {
            proc.microcycleNop();
        }
    }

    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Overflow, false);

    if(shiftCount) {
        if(shiftCount > SizeToBits(size)) {
            // TODO: verify this is correct
            proc.regs.setCcr(Ccr::Carry | Ccr::Extend, input & SignBitFor(size));
        } else {
            const auto bitIndex = shiftCount - 1;
            proc.regs.setCcr(Ccr::Carry | Ccr::Extend, !!(input & (1 << bitIndex)));
        }
    } else {
        proc.regs.setCcr(Ccr::Carry, false);
    }

    WriteEffectiveAddr(proc, resolved, size, result);
    if(kLogArithmeticShift) Logger::Trace("ASR: Read data ${:08x}, write data ${:08x}", input,result);

    if(!toMemory) {
        proc.microcycleNop();
        if(size == Size::Longword) proc.microcycleNop();
    }
}
