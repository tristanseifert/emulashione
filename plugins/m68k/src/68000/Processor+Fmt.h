/*
 * Pretty print formatting for various processor structures
 */
#ifndef M68000_PROCESSOR_FMT_H
#define M68000_PROCESSOR_FMT_H

#include "Processor.h"

#include <fmt/core.h>
#include <magic_enum.hpp>

/// Formatter for 68000 registers
template <> struct fmt::formatter<emulashione::m68k::m68000::Processor::Registers> {
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const emulashione::m68k::m68000::Processor::Registers &r,
            FormatContext& ctx) -> decltype(ctx.out()) {
        auto it = ctx.out();

        for(size_t i = 0; i < 8; i++) {
            it = format_to(it, "  D{:1}: ${:08x}{}", i, r.d[i], (i == 3 || i == 7) ? "\n" : "");
        }
        for(size_t i = 0; i < 8; i++) {
            it = format_to(it, "  A{:1}: ${:08x}{}", i, r.a[i], (i == 3 || i == 7) ? "\n" : "");
        }
        it = format_to(it, " SSP: ${:08x}  PC: ${:08x}  SR: ${:04x}\n", r.ssp, r.pc-2,
                r.getStatus());
        it = format_to(it, "Priv: {} Lvl: ${:1x} Trace: {}",
                magic_enum::enum_name(r.getPrivilegeLevel()), r.getInterruptMask(),
                r.isTraceEnabled());

        return it;
    }
};

/// Formatter for 68000 prefetch queue
template <> struct fmt::formatter<emulashione::m68k::m68000::Processor::PrefetchQueue> {
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const emulashione::m68k::m68000::Processor::PrefetchQueue &q,
            FormatContext& ctx) -> decltype(ctx.out()) {
        return format_to(ctx.out(), "IRC ${:04x}  IR ${:04x} IRD ${:04x}", q.irc(), q.ir(),
                q.ird());
    }
};

#endif
