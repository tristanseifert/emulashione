#include "Decoder.h"
#include "Processor.h"

#include <Logger.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k::m68000;

/**
 * @brief Table of instruction handler methods
 *
 * When we want to execute an instruction, it's used as an index into this array, in which all
 * instruction handlers are registered.
 */
emulashione::m68k::OpcodeTable<Decoder::InstructionHandler2> Decoder::gOpcodes;

/**
 * Populates the instruction decoder's handler map. This should be invoked when the processor is
 * first registered as a device.
 */
void Decoder::InitMap() {
    /*
     * Reset all handlers explicitly; while the array is zeroed the first time the plugin is
     * loaded, on subsequent times the global constructors may not rerun.
     */
    gOpcodes.reset();

    RegisterAddress();
    RegisterBranch();
    RegisterConditionalBranch();
    RegisterConditionalSet();
    RegisterMove();
    RegisterCompare();
    RegisterBitwise();
    RegisterArithmetic();
    RegisterGeneral();

    // any remaining instructions are mapped to the illegal handler
    const auto illegals = gOpcodes.addUnmappedTrap(Illegal);
    Logger::Info("Registered {} instructions", 0x10000 - illegals);
}


/**
 * @brief ILLEGAL
 *
 * Handles an illegal instruction; this handler is invoked both for the specifically reserved
 * illegal instruction opcode, and any unassigned opcodes.
 */
void Decoder::Illegal(const uint16_t opcode, Processor &proc) {
    // log a message about opcodes that _aren't_ the assigned illegal opcode
    if(opcode != 0b0100101011111100) {
        Logger::Warning("Invalid, non-ILLEGAL opcode: ${:04x}", opcode);
        proc.abort(Processor::AbortReason::UnknownOpcode);
    }
    // otherwise, the illegal instruction still does a prefetch (TODO: verify on hw)
    else {
        PrefetchCycle(proc);
    }

    // and begin exception processing
    proc.setEvent(Processor::EventFlags::IllegalInstruction);
}

/**
 * @brief NOP
 *
 * Handles a noop.
 */
void Decoder::NoOp(const uint16_t, Processor &proc) {
    PrefetchCycle(proc);
}

/**
 * @brief RESET
 *
 * Handles a reset instruction; this 'asserts' the reset line for 124 clock cycles.
 */
void Decoder::Reset(const uint16_t, Processor &proc) {
    proc.microcycleNop();
    proc.microcycleNop();

    // assert reset and chill for 124 cycles
    Logger::Info("RESET executed");

    for(size_t i = 0; i < (124/2); i++) {
        proc.microcycleNop();
    }

    proc.abort(Processor::AbortReason::Unimplemented);

    // deassert reset and do prefetch
    PrefetchCycle(proc);
}

