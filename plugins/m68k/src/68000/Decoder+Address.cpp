/**
 * Common address calculation logic, and address loading (LEA/PEA) instructions
 */
#include "Decoder.h"
#include "Decoder+Fmt.h"

#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

#include <magic_enum.hpp>

using namespace emulashione::plugin;
using namespace emulashione::m68k::m68000;

/**
 * @brief LEA
 *
 * Calculates the effective address, based on the provided addressing mode, and stores it in an
 * address register.
 *
 * The only supported addressing modes are M = [0b010, 0b101, 0b110] and when M = 0b111, valid
 * Xn = [0b000, 0b001, 0b010, 0b011].
 */
void Decoder::LoadEffectiveAddr(const uint16_t opcode, Processor &proc) {
    uint32_t result;

    // perform addressing calculation
    result = CalculateEffectiveAddr(proc, GetAddrMode<5,2>(opcode), Size::Longword,
            kAddrModeControl);

    // store result
    const size_t An = GetRegIndex<11>(opcode);
    proc.regs.getAn(An) = result;

    // prefetch
    PrefetchCycle(proc);
}

/**
 * @brief PEA
 *
 * Calculates the effective address, based on the provided addressing mode, and pushes it as a
 * longword to the stack. It is identical to LoadEffectiveAddr in function besides this change.
 *
 * The only supported addressing modes are M = [0b010, 0b101, 0b110] and when M = 0b111, valid
 * Xn = [0b000, 0b001, 0b010, 0b011].
 *
 * @todo Fix the ordering of the stack pushes and prefetches; for absolute short and long, the last
 *       of the two prefetches happens _after_ stacking is complete.
 */
void Decoder::PushEffectiveAddr(const uint16_t opcode, Processor &proc) {
    uint32_t result;
    result = CalculateEffectiveAddr(proc, GetAddrMode<5,2>(opcode), Size::Longword,
            kAddrModeControl);
    proc.stackPush(result);

    PrefetchCycle(proc);
}

/**
 * Gets the increment/decrement value for the given addressing mode and size. If the access is to
 * the stack, and byte sized, it will be yeeted up to word size.
 */
constexpr inline static size_t IncrementFor(const Decoder::AddressingMode mode, const Decoder::Size size) {
    if(mode.isStackPointerIndirect() && size == Decoder::Size::Byte) {
        return 2;
    }

    return Decoder::SizeToBytes(size);
}

/**
 * @brief Calculate an effective address
 *
 * Given a standard addressing mode -- defined by two fields, M and Xn -- this function will
 * calculate the resulting address based on the processor's current state.
 *
 * In some cases, not all available addressing modes are desired by the calling instruction; in
 * these cases, the valid bitmasks are available. If bit `n` is clear in the corresponding M/Xn
 * bitmask, the addressing mode is _not_ allowed and an exception is raised. (XXX: what type?)
 *
 * @param proc Processor whose state to calculate the address with
 * @param mode Pair containing the major addressing type flag (0...7) and register index or
 *        addressing sub-type (0...7)
 * @param size Access size, used to handle autoincrement/decrement
 * @param validModes Pair containing bitmasks indicating which values of M and Xnare valid for this
 *        invocation
 * @param wantPrefetch When set, any addressing modes that read extension words will perform the
 *        required number of prefetches to refill the prefetch queue. This should be the case for
 *        most cases, with the notable exception being jumps.
 *
 * @return The calculated effective address
 */
uint32_t Decoder::CalculateEffectiveAddr(Processor &proc, const AddressingMode mode,
        const Size size, const AllowedAddressingModeMask validModes, const bool wantPrefetch) {
    const auto [M, Xn] = mode;

    // validate
    if(!mode.validate(validModes)) {
        return HandleInvalidAddressingMode(proc, mode, size, wantPrefetch);
    }

    // invoke first level handler
    constexpr static const std::array<AddressCalculator, 8> kAddrCalculators{
        // 0b000 Dn             Data register
        HandleRegisterAddressingMode,
        // 0b001 An             Address register
        HandleRegisterAddressingMode,
        // 0b010 (An)           Address indirect
        [](auto proc, auto mode, auto size, auto prefetch) -> uint32_t {
            return proc.regs.getAn(mode.Xn);
        },
        // 0b011 (An)+          Address postincrement
        [](auto proc, auto mode, auto size, auto prefetch) -> uint32_t {
            auto &An = proc.regs.getAn(mode.Xn);
            const uint32_t value{An};
            An += IncrementFor(mode, size);
            return value;
        },
        // 0b100 -(An)          Address predecrement
        [](auto proc, auto mode, auto size, auto prefetch) -> uint32_t {
            /*
             * TODO: Figure out what to do with this extra no-op cycle. It's shown in the effective
             * address calculation time, but a lot of instructions "hide" this latency.
             */
            // proc.microcycleNop();
            auto &An = proc.regs.getAn(mode.Xn);
            An -= IncrementFor(mode, size);
            return An;
        },
        // 0b101 (d16, An)      Address with displacement
        [](auto proc, auto mode, auto size, auto prefetch) -> uint32_t {
            // fetch an extension word
            const auto val16 = proc.getExtensionWord();
            if(prefetch) {
                PrefetchCycle(proc);
            }

            // add it to the contents of the specified address register
            uint32_t An = proc.regs.getAn(mode.Xn);
            return An + SignExtend<int32_t,16>(val16);
        },
        // 0b110 (d8, An, Xn)   Address with index
        [](auto proc, auto mode, auto size, auto prefetch) -> uint32_t {
            // there's an extra no-op cycle here (for the "calculation") time
            proc.microcycleNop();

            // fetch the brief extension word
            const auto val16 = proc.getExtensionWord();
            if(prefetch) {
                PrefetchCycle(proc);
            }

            // calculate the resulting address
            auto &An = proc.regs.getAn(mode.Xn);
            return CalculateRegisterIndirectIndex(proc, mode, size, val16, An);
        },
        // 0b111                Bonus modes without register references (Xn is a second level)
        HandleBonusAddressingModes,
    };

    const auto result = kAddrCalculators[M & 0b111](proc, mode, size, wantPrefetch);
    return result;
}

/**
 * Calculates the result of a register indirect with index (8 bit displacement) effective
 * address.
 *
 * This works by reading the value of the specified register, adding the (sign-extended) 8 bit
 * displacement, then reading either a word or long from the source register and adding it, sign
 * extending this as well if necessary.
 *
 * @param proc Processor whose state to reference
 * @param mode Addressing mode in use
 * @param size Size of the underlying access
 * @param extWord Brief extension word
 * @param base Register to calculate the base address relative to
 *
 * @return The resulting address
 */
uint32_t Decoder::CalculateRegisterIndirectIndex(Processor &proc, const AddressingMode mode,
        const Size size, const uint16_t extWord, const uint32_t &base) {
    // add the displacement
    uint32_t addr = base;
    addr += SignExtend<int32_t,8>(GetBits<7,8>(extWord));

    // determine what index register and whether to use it as a word/long
    const bool isLong = GetBits<11,1>(extWord);

    const auto Xn = GetBits<14,3>(extWord);
    auto indexReg = (!GetBits<15,1>(extWord)) ? proc.regs.getDn(Xn) : proc.regs.getAn(Xn);

    // check scale field (if enabled)
    if(kCheckBriefExtScale) {
        if(GetBits<10,3>(extWord)) {
            Logger::Error("Brief extension word ${:04x} with invalid scale", extWord);
            proc.abort(Processor::AbortReason::UnknownOpcode);
        }
    }

    // apply the index
    if(isLong) {
        addr += indexReg;
    } else {
        addr += SignExtend<int32_t,16>(static_cast<uint16_t>(indexReg));
    }

    // done
    return addr;
}

/**
 * @brief PC relative, absolute, immediate address calculation
 *
 * Calculate the effective address for various PC relative, absolute and immediate modes where
 * M = 0b111.
 */
uint32_t Decoder::HandleBonusAddressingModes(Processor &proc, const AddressingMode mode,
        const Size size, const bool wantPrefetch) {
    // invoke second level handler
    constexpr static const std::array<AddressCalculator, 8> kAddrCalculators{
        // 0b000 (xxx).W        Absolute short
        [](auto proc, auto mode, auto size, auto prefetch) -> uint32_t {
            const auto val16 = proc.getExtensionWord();

            if(prefetch) {
                PrefetchCycle(proc);
            }

            return static_cast<uint32_t>(SignExtend<int32_t,16>(val16));
        },
        // 0b001 (xxx).L        Absolute long
        [](auto proc, auto mode, auto size, auto prefetch) -> uint32_t {
            const auto high = proc.getExtensionWord();

            /*
             * TODO: Figure out what to do with this extra no-op cycle. It's shown in the effective
             * address calculation time, but a lot of instructions "hide" this latency.
             */
            //proc.microcycleNop();
            const auto low = proc.getExtensionWordBonus();

            // we already did one prefetch cycle for the bonus word
            if(prefetch) {
                PrefetchCycle(proc);
            }

            return (static_cast<uint32_t>(high) << 16) | low;
        },
        // 0b010 (d16, PC)      Program counter with displacement
        [](auto proc, auto mode, auto size, auto prefetch) -> uint32_t {
            auto dest{proc.regs.pc - 2};

            const auto val16 = proc.getExtensionWord();

            if(prefetch) {
                PrefetchCycle(proc);
            }

            dest += SignExtend<int32_t,16>(val16);
            return dest;
        },
        // 0b011 (d8, PC, Xn)   Program counter with index
        [](auto proc, auto mode, auto size, auto prefetch) -> uint32_t {
            // read PC _before_ reading the extension word
            const auto pc = proc.regs.pc - 2;

            // there's an extra no-op cycle here (for the "calculation") time
            proc.microcycleNop();

            // fetch the brief extension word
            const auto val16 = proc.getExtensionWord();
            if(prefetch) {
                PrefetchCycle(proc);
            }

            // calculate the resulting address
            return CalculateRegisterIndirectIndex(proc, mode, size, val16, pc);
        },
        // 0b100 #imm           Immediate
        HandleInvalidAddressingMode,

        // 0b101 - 0b111: unsupported on 68000
        HandleInvalidAddressingMode,
        HandleInvalidAddressingMode,
        HandleInvalidAddressingMode
    };

    return kAddrCalculators[mode.Xn & 0b111](proc, mode, size, wantPrefetch);
}

/**
 * Handles an invalid addressing mode; whether this is because it's straight up not supported by
 * the processor, or because the particular instruction doesn't support it.
 *
 * @param proc Processor that executed the invalid addressing mode
 * @param mode Pair of `[M, Xn]` data defining this addressing mode
 * @param size Access size
 * @param wantPrefetch Whether prefetching (due to consumed extension words) was requested
 *
 * @return A bogus value; this is so it can be used in a placeholder of address calculation table
 *
 * @todo Figure out what actually happens in this case. For now, just abort emulation…
 */
uint32_t Decoder::HandleInvalidAddressingMode(Processor &proc, const AddressingMode mode,
        const Size size, const bool wantPrefetch) {
    Logger::Warning("Invalid addressing mode {} (prefetch {}, size {})", mode, wantPrefetch,
            magic_enum::enum_name(size));
    proc.abort(Processor::AbortReason::InvalidAddressingMode);

    return 0;
}

/**
 * Called when attempting to calculate the effective address for a register address. This is not
 * valid; callers should ensure the addressing mode refers to memory before attempting to calculate
 * the corresponding address.
 */
uint32_t Decoder::HandleRegisterAddressingMode(Processor &proc, const AddressingMode mode,
        const Size size, const bool wantPrefetch) {
    Logger::Critical("Can't calculate ea for mode {}!", mode);
    proc.abort(Processor::AbortReason::PreconditionViolated);

    return 0;
}



/**
 * @brief Resolve an effective address into a physical address
 *
 * This calculates the physical memory address for this effective address and applies any side
 * effects, such as reading extension words or pre/post decrements of registers.
 *
 * @param proc Processor executing this instruction
 * @param mode Addressing mode to use
 * @param size Width of this access
 * @param validModes Bitfield indicating which major and minor addressing modes are allowed
 *
 * @return A structure representing the resolved effective address
 */
Decoder::ResolvedAddressingMode Decoder::ResolveEffectiveAddr(Processor &proc,
        const AddressingMode mode, const Size size, const AllowedAddressingModeMask validModes) {
    // TODO: validate modes

    // data and address register based do not need any work
    if(mode.isRegister()) {
        return {mode, 0};
    }
    // same for immediates
    else if(mode.isImmediate()) {
        return {mode, 0};
    }

    // all other modes access memory
    const auto addr = CalculateEffectiveAddr(proc, mode, size);
    if(kLogEACalculation) Logger::Trace("EA source addr: ${:08x}", addr);

    return {mode, addr};
}



/**
 * @brief Reads the contents of a previously resolved effective address.
 *
 * All addressing modes supported by the 68000 are supported by this call for reading data from
 * registers, an immediate value, or memory.
 *
 * @param mode Resolved effective address
 * @param size Width of this access
 */
uint32_t Decoder::ReadEffectiveAddr(Processor &proc, const ResolvedAddressingMode mode,
        const Size size) {
    // read register contents
    if(mode.isDataRegister()) {
        return proc.regs.getDn(mode.getXn()) & MaskFor(size);
    } else if(mode.isAddressRegister()) {
        return proc.regs.getAn(mode.getXn()) & MaskFor(size);
    }
    /*
     * An immediate value follows the instruction. When a byte size is specified, we still read an
     * entire word, but only look at the low byte. We do an extra prefetch for each word read.
     */
    else if(mode.isImmediate()) {
        return ReadImmediate(proc, size);
        uint32_t temp{0};

        // read first word
        temp = proc.getExtensionWord();

        // read the second extension word (for longword)
        if(size == Size::Longword) {
            temp = ((temp & 0xFFFF) << 16) | proc.getExtensionWordBonus();
        }

        // perform one prefetch (for the first extension word that was already in the queue)
        PrefetchCycle(proc);

        // return this value
        return temp & MaskFor(size);
    }
    /*
     * Most other addressing modes reference memory. Calculate the required address and read from
     * it to get the value.
     */
    else if(mode.isMemory()) {
        switch(size) {
            case Size::Byte:
                proc.microcycleNop();
                return proc.readByte(proc.getDataFc(), mode.address);
            case Size::Word:
                proc.microcycleNop();
                return proc.readWord(proc.getDataFc(), mode.address);
            case Size::Longword:
                return proc.readLongword(proc.getDataFc(), mode.address);
            default:
                goto beach;
        }
    }

    // unsupported addressing mode
beach:;
    Logger::Critical("Invalid ea read addressing mode {} ({})", mode,
            magic_enum::enum_name(size));
    proc.abort(Processor::AbortReason::InvalidAddressingMode);

    return -1;
}

/**
 * @brief Writes data to the location specified by a previously resolved effective address.
 *
 * Only modes in the data alterable category are supported by this call: notably, writing to an
 * address register directly, immediates, and PC relative addressing are not allowed.
 *
 * @param mode Resolved effective address to use
 * @param size Width of this access
 * @param value Value to be written
 * @param dummyRead When set, and the destination is memory, read from the location before the
 *        write is performed.
 */
void Decoder::WriteEffectiveAddr(Processor &proc, const ResolvedAddressingMode mode,
        const Size size, const uint32_t value, const bool dummyRead) {
    // store direct to a data register
    if(mode.isDataRegister()) {
        auto &Dn = proc.regs.getDn(mode.getXn());

        switch(size) {
            case Size::Byte:
                Dn = (Dn & ~0xFF) | (value & 0xFF); return;
            case Size::Word:
                Dn = (Dn & ~0xFFFF) | (value & 0xFFFF); return;
            case Size::Longword:
                Dn = value; return;
            default:
                goto beach;
        }
    }
    /*
     * Writing to an address register will always sign extend 16 bit values. Byte writes are not
     * explicitly supported.
     */
    else if(mode.isAddressRegister()) {
        auto &An = proc.regs.getAn(mode.getXn());

        switch(size) {
            case Size::Word:
                An = SignExtend<int32_t,16>(value & 0xFFFF); return;
            case Size::Longword:
                An = value; return;
            default:
                goto beach;
        }
    }
    // it is in memory, so calculate the address and write to it
    else if(mode.isMemory()) {
        const auto fc = proc.getDataFc();

        switch(size) {
            case Size::Byte:
                if(dummyRead) {
                    proc.microcycleNop();
                    proc.readByte(fc, mode.address);
                }

                proc.microcycleNop();
                return proc.writeByte(fc, mode.address, value);
            case Size::Word:
                if(dummyRead) {
                    proc.microcycleNop();
                    proc.readWord(fc, mode.address);
                }

                proc.microcycleNop();
                return proc.writeWord(fc, mode.address, value);
            case Size::Longword:
                if(dummyRead) proc.readLongword(fc, mode.address);
                return proc.writeLongword(fc, mode.address, value);
            default:
                goto beach;
        }
    }

    // unsupported mode
beach:;
    Logger::Critical("Invalid EA dest addressing mode {} ({}, value ${:08x})", mode,
            magic_enum::enum_name(size), value);
    proc.abort(Processor::AbortReason::InvalidAddressingMode);
}

