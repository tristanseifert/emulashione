/*
 * Conditional branches: Bcc, DBcc
 */
#include "Decoder.h"
#include "Decoder+Fmt.h"
#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k::m68000;

/**
 * @brief Bcc
 *
 * Branch iff the condition is true.
 *
 * @todo Validate timings
 */
void Decoder::BranchConditional(const uint16_t opcode, Processor &proc) {
    const auto disp8 = static_cast<uint8_t>(GetBits<7,8>(opcode));

    // we start with a no-op cycle
    proc.microcycleNop();

    // if the condition is false, bail out of the loop
    const auto condition = GetCondition<11>(opcode);
    if(!proc.regs.test(condition)) {
        proc.microcycleNop();

        // consume extension word if there is one
        if(!disp8) {
            PrefetchCycle(proc);
        }

        // finish with a normal prefetch cycle
        return PrefetchCycle(proc);
    }

    // condition is true, so branch
    const auto pc = proc.regs.pc;
    int32_t disp;

    // if the 8-bit displacement is nonzero, use it directly
    if(disp8) {
        disp = SignExtend<int32_t,8>(disp8);
    }
    // fetch the extension word; note this does not require any extra bus cycles
    else {
        const auto disp16 = proc.getExtensionWord();
        disp = SignExtend<int32_t,16>(disp16);
    }

    // perform the PC update and prefetch
    proc.regs.pc = (pc - 2) + disp;

    for(size_t i = 0; i < 2; i++) {
        PrefetchCycle(proc);
    }
}

/**
 * @brief DBcc
 *
 * Test condition, decrement, then branch.
 *
 * This will first test a condition, which, if true, will cause the loop to terminate immediately;
 * otherwise, the loop counter (specified in a register) is decremented. If the loop counter is not
 * equal to -1, the loop continues.
 *
 * For the loop counter, only the low 16 bits of the register are considered.
 *
 * @todo Validate timings
 */
void Decoder::BranchConditionalWithCount(const uint16_t opcode, Processor &proc) {
    const auto pc = proc.regs.pc;
    auto &countReg = proc.regs.getDn(GetRegIndex<2>(opcode));

    // we start with a no-op cycle
    proc.microcycleNop();

    // if the condition is true, bail out of the loop
    const auto condition = GetCondition<11>(opcode);
    if(proc.regs.test(condition)) {
        // consume extension word and return
        proc.microcycleNop();

        PrefetchCycle(proc);
        return PrefetchCycle(proc);
    }

    // decrement count by one
    uint16_t count = countReg;
    count--;
    if(kLogConditionalBranch) {
        Logger::Trace("DBcc count ${:04x} ${:04x}", count, countReg);
    }
    countReg = (countReg & ~0xFFFF) | count;

    // if not -1, repeat loop
    if(count != 0xFFFF) {
        const auto disp16 = proc.getExtensionWord();
        int32_t disp = SignExtend<int32_t,16>(disp16);

        proc.regs.pc = (pc - 2) + disp;

        for(size_t i = 0; i < 2; i++) {
            PrefetchCycle(proc);
        }
    }
    // counter expired
    else {
        // TODO: this is really a third prefetch cycle, but from what address?
        proc.microcycleNop();
        proc.microcycleNop();

        for(size_t i = 0; i < 2; i++) {
            PrefetchCycle(proc);
        }
    }
}
