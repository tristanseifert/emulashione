/*
 * Arithmetic - Addition: ADD, ADDA, ADDI, ADDQ, ADDX
 */
#include "Decoder.h"
#include "Decoder+Fmt.h"
#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k;
using namespace emulashione::m68k::m68000;

using namespace magic_enum::bitwise_operators;

/**
 * @brief ADD
 *
 * Adds the source and destination, then stores the result in the destination.
 */
void Decoder::Add(const uint16_t opcode, Processor &proc) {
    const auto storeToEa = !!GetBits<8,1>(opcode);
    const auto regNum = GetRegIndex<11>(opcode);
    const auto size = GetSize(opcode);
    const auto addrMode = GetAddrMode<5,2>(opcode);

    // resolve effective address and read from it
    auto resolved = ResolveEffectiveAddr(proc, addrMode, size);
    const auto readData = ReadEffectiveAddr(proc, resolved, size);

    // perform operation, update flags, prefetch
    const uint32_t Dn = (proc.regs.getDn(regNum) & MaskFor(size));
    const uint32_t result = readData + Dn;

    const bool srcMsb = !!(Dn & SignBitFor(size)), dstMsb = !!(readData & SignBitFor(size)),
          resMsb = !!(result & SignBitFor(size));
    bool carry = (srcMsb && dstMsb) || (!resMsb && (srcMsb || dstMsb));
    bool overflow = (srcMsb == dstMsb) && (resMsb != (srcMsb && dstMsb));

    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Overflow, overflow);
    proc.regs.setCcr(Ccr::Carry, carry);
    proc.regs.setCcr(Ccr::Extend, carry);

    PrefetchCycle(proc);

    // write back
    if(storeToEa) {
        WriteEffectiveAddr(proc, resolved, size, result);
    } else {
        switch(size) {
            case Size::Byte:
                proc.regs.storeData(regNum, static_cast<uint8_t>(result));
                break;
            case Size::Word:
                proc.regs.storeData(regNum, static_cast<uint16_t>(result));
                break;
            case Size::Longword:
                proc.regs.storeData(regNum, static_cast<uint32_t>(result));
                break;
            default: proc.abort(Processor::AbortReason::InternalError);
        }
    }

    /*
     * When storing into a register, _and_ it is a longword, add an extra no-op cycle. If the
     * source was _also_ a data register (or immediate), add another cycle.
     */
    if(!storeToEa && size == Size::Longword) {
        proc.microcycleNop();
        if(addrMode.isDataRegister() || addrMode.isImmediate()) {
            proc.microcycleNop();
        }
    }
}
/**
 * @brief ADDA
 *
 * Reads either a word or longword from the effective address specified, sign extends it if less
 * than 32 bits, then adds it to the specified address register.
 *
 * No condition codes are modified.
 */
void Decoder::AddAddress(const uint16_t opcode, Processor &proc) {
    // get size and destination register number
    const auto An = GetRegIndex<11>(opcode);
    const auto size = GetBits<8,1>(opcode) ? Size::Longword : Size::Word;
    const auto addrMode = GetAddrMode<5,2>(opcode);

    // resolve addressing mode and read data source; perform the operation
    auto resolved = ResolveEffectiveAddr(proc, addrMode, size);
    auto readData = ReadEffectiveAddr(proc, resolved, size);

    if(size == Size::Word) {
        readData = SignExtend<int32_t,16>(readData);
    }

    proc.regs.getAn(An) += readData;

    /*
     * Perform the prefetch cycle, and then one or two no-op microcycles; we'll do two for all word
     * operations, and one for all longword operations with sources other than Dn, An, and #imm.
     */
    PrefetchCycle(proc);

    proc.microcycleNop();

    if(size == Size::Longword && (addrMode.isDataRegister() || addrMode.isAddressRegister() ||
                addrMode.isImmediate())) {
        proc.microcycleNop();
    } else if(size == Size::Word) {
        proc.microcycleNop();
    }
}

/**
 * @brief ADDI
 *
 * Read an immediate value (one or two words, of which either 8, 16 or 32 bits are used) then adds
 * it to the value specified by the effective address, and writes it back.
 *
 * @todo Validate when flags get updated during execution
 */
void Decoder::AddImmediate(const uint16_t opcode, Processor &proc) {
    // get opcode parameters and immediate word
    const auto size = GetSize(opcode);
    const auto addrMode = GetAddrMode<5,2>(opcode);

    const auto imm = ReadImmediate(proc, size);

    // resolve addressing mode and read data source; perform the operation
    auto resolved = ResolveEffectiveAddr(proc, addrMode, size, kAddrModeDataAlterable);
    const auto readData = ReadEffectiveAddr(proc, resolved, size);
    const uint32_t result = readData + imm;

    // update flags and perform prefetch
    const bool srcMsb = !!(imm & SignBitFor(size)), dstMsb = !!(readData & SignBitFor(size)),
          resMsb = !!(result & SignBitFor(size));
    bool carry = (srcMsb && dstMsb) || (!resMsb && (srcMsb || dstMsb));
    bool overflow = (srcMsb == dstMsb) && (resMsb != (srcMsb && dstMsb));

    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Overflow, overflow);
    proc.regs.setCcr(Ccr::Carry, carry);
    proc.regs.setCcr(Ccr::Extend, carry);

    PrefetchCycle(proc);

    // write back to destination
    WriteEffectiveAddr(proc, resolved, size, result);

    /*
     * If this is a longword op with a data reg destination, add two extra no-op cycles at the end
     *
     * This behavior is common with EORI, ORI, ANDI, SUBI, ADDI
     */
    if(size == Size::Longword && addrMode.isDataRegister()) {
        proc.microcycleNop();
        proc.microcycleNop();
    }
}

/**
 * @brief ADDQ
 *
 * Adds a value 0-7, embedded in the opcode, to the specified effective address.
 *
 * When the destination is an address register, the operation behaves as if it's a longword, and
 * condition codes will not be updated.
 */
void Decoder::AddQuick(const uint16_t opcode, Processor &proc) {
    // get opcode parameters and immediate word
    auto size = GetSize(opcode);
    const auto addrMode = GetAddrMode<5,2>(opcode);

    if(addrMode.isAddressRegister()) {
        size = Size::Longword;
    }

    const auto imm = GetBits<11,3>(opcode);

    // resolve addressing mode and read data source; perform the operation
    auto resolved = ResolveEffectiveAddr(proc, addrMode, size, kAddrModeAlterable);
    const auto readData = ReadEffectiveAddr(proc, resolved, size);
    const uint32_t result = readData + imm;

    // update flags and perform prefetch
    if(!addrMode.isAddressRegister()) {
        const bool srcMsb = !!(imm & SignBitFor(size)), dstMsb = !!(readData & SignBitFor(size)),
              resMsb = !!(result & SignBitFor(size));
        bool carry = (srcMsb && dstMsb) || (!resMsb && (srcMsb || dstMsb));
        bool overflow = (srcMsb == dstMsb) && (resMsb != (srcMsb && dstMsb));

        proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));
        proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
        proc.regs.setCcr(Ccr::Overflow, overflow);
        proc.regs.setCcr(Ccr::Carry, carry);
        proc.regs.setCcr(Ccr::Extend, carry);
    }

    PrefetchCycle(proc);

    // write back to destination
    WriteEffectiveAddr(proc, resolved, size, result);

    /*
     * If this is a longword op with a data reg destination, add two extra no-op cycles at the end
     *
     * This behavior is common with EORI, ORI, ANDI, SUBI, ADDI
     */
    if(size == Size::Longword && addrMode.isDataRegister()) {
        proc.microcycleNop();
        proc.microcycleNop();
    }
}

