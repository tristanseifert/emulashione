#ifndef M68000_DECODER_H
#define M68000_DECODER_H

#include "../Helpers.h"
#include "Processor.h"
#include "OpcodeTable.h"

#include <array>
#include <cstdint>
#include <cstddef>

namespace emulashione::m68k::m68000 {
/**
 * @brief Instruction decoder for all 68000 and 68010 instructions.
 *
 * This decoder is implemented by a large jump table; all 16 bit instructions simply index into
 * that table to invoke the appropriate handler. For memory efficiency reasons, this table is
 * shared between all processors.
 *
 * Instructions are implemented as static functions on the decoder, which receive a processor
 * instance as an argument.
 */
struct Decoder {
    /// Method to handle an instruction
    using InstructionHandler = void(*)(Processor &proc, const uint16_t opcode);
    using InstructionHandler2 = void(*)(const uint16_t opcode, Processor &proc);

    /**
     * A pair of bitmasks, where each bit corresponds to a value of M or Xn, respectively. If the
     * bit is set, the given addressing mode is permitted.
     */
    using AllowedAddressingModeMask = std::pair<uint8_t, uint8_t>;

    /**
     * @brief Effective addressing mode
     *
     * Holds a pair of the `M` and `Xn` fields that define how an effective address is calculated;
     * the following modes are encoded in instructions as two 3-bit values:
     *
     * |                              Mode |    Format    |   M   |   Xn  |
     * |----------------------------------:|:------------:|:-----:|:-----:|
     * |                     Data Register |      Dn      | `000` |  reg  |
     * |                  Address register |      An      | `001` |  reg  |
     * |                           Address |     (An)     | `010` |  reg  |
     * |        Address with Postincrement |     (An)+    | `011` |  reg  |
     * |         Address with Predecrement |     -(An)    | `100` |  reg  |
     * |         Address with Displacement |   (d16, An)  | `101` |  reg  |
     * |                Address with Index | (d8, An, Xn) | `110` |  reg  |
     * | Program Counter with Displacement |   (d16, PC)  | `111` | `010` |
     * |        Program Counter with Index | (d8, PC, Xn) | `111` | `011` |
     * |                    Absolute Short |    (xxx).w   | `111` | `000` |
     * |                     Absolute Long |    (xxx).l   | `111` | `001` |
     * |                         Immediate |     #imm     | `111` | `100` |
     *
     * When the `Xn` column specifies a register value, it is the literal 0-7 index of the
     * register. A7 = SP, which refers to either SSP or USP depending on the current privilege
     * level.
     */
    struct AddressingMode {
        /// Major addressing mode field
        uint8_t M;
        /// Register index or minor addressing mode if M = 0b111
        uint8_t Xn;

        /// Check whether the addressing mode refers to adata register's contents
        constexpr inline bool isDataRegister() const {
            return (M == 0b000);
        }
        /// Check whether the addressing mode refers to an address register's contents
        constexpr inline bool isAddressRegister() const {
            return (M == 0b001);
        }
        /// Check whether the addressing mode refers to an address register indirection
        constexpr inline bool isAddressIndirect() const {
            return (M == 0b010);
        }
        /// Is the mode "address register indirect with postincrement"?
        constexpr inline bool isAddressPostincrement() const {
            return (M == 0b011);
        }
        /// Is the mode "address register indirect with predecrement"?
        constexpr inline bool isAddressPredecrement() const {
            return (M == 0b100);
        }
        /// Check whether the addressing mode refers to the contents of a data or address register
        constexpr inline bool isRegister() const {
            return (M == 0b000 || M == 0b001);
        }
        /// Check whether the addressing mode refers to an absolute value
        constexpr inline bool isAbsolute() const {
            return (M == 0b111) && (Xn == 0b000 || Xn == 0b001);
        }
        /// Check whether the addressing mode is PC relative
        constexpr inline bool isPcRelative() const {
            return (M == 0b111) && (Xn == 0b010 || Xn == 0b011);
        }
        /// Check whether the addressing mode refers to an immediate value
        constexpr inline bool isImmediate() const {
            return (M == 0b111) && (Xn == 0b100);
        }
        /// Check whether the mode refers to an address in memory, which can be calculated
        constexpr inline bool isMemory() const {
            return !isRegister() && !isImmediate();
        }

        /**
         * Check whether the addressing mode refers to an address register indirect mode that
         * targets the stack pointer (A7) as the base register.
         */
        constexpr inline bool isStackPointerIndirect() const {
            if(M >= 0b010 && M <= 0b110 && Xn == 7) return true;
            return false;
        }

        /**
         * Does this addressing mode have any extension words?
         *
         * This applies to all of the "extended" (M = `0b111`) addressing modes, as well as the
         * address with displacement/index modes.
         */
        constexpr inline bool hasExtensionWords() const {
            return (M == 0b101) || (M == 0b110) || (M == 0b111);
        }

        /**
         * Test whether this addressing mode is compatible with the specified allowed mask.
         *
         * @param mask A pair where each bit indicates a supported major and minor addressing mode,
         *        respectively
         *
         * @return Whether this addressing mode is compatible with the provided mask.
         */
        constexpr inline bool validate(const AllowedAddressingModeMask mask) const {
            const auto [validM, validXn] = mask;
            if(!(validM & (1 << M))) {
                return false;
            } else if(M == 0b111) {
                if(!(validXn & (1 << Xn))) {
                    return false;
                }
            }
            return true;
        }

        /**
         * Return an addressing mode that corresponds to the contents of the given data register.
         *
         * @param Dn Data register index
         */
        constexpr inline static AddressingMode MakeDn(const uint8_t Dn) {
            return {0b000, static_cast<uint8_t>(Dn & 0b111)};
        }
    };

    /**
     * @brief Effective address resolved to a physical address
     *
     * For read/modify/write instructions, it's important that the side effects of effective
     * address calculation (be it the actual cycles taken by the calculation, reads of any
     * extension words, etc.) take place only once.
     *
     * In these instances, the effective address can be resolved, which performs any side effects
     * once, and can then be reused to access memory as many times as desired.
     */
    struct ResolvedAddressingMode {
        /**
         * @brief Original addressing mode
         *
         * This is the addressing mode that was extracted from the instruction.
         */
        AddressingMode mode;

        /**
         * @brief Memory address the effective address was resolved to.
         *
         * If the addressing mode specifies memory -- whether that's indirectly through an address
         * register, or by absolute specifications, or even immediate values -- this variable will
         * hold it.
         *
         * For register modes, no data is stored here.
         */
        uint32_t address{0};

        /// Is the underlying addressing mode specify a data register?
        inline constexpr auto isDataRegister() const {
            return mode.isDataRegister();
        }
        /// Is the underlying addressing mode specify an address register?
        inline constexpr auto isAddressRegister() const {
            return mode.isAddressRegister();
        }
        /// Is the underlying addressing mode specify memory?
        inline constexpr auto isMemory() const {
            return mode.isMemory();
        }
        /// Is the underlying effective address an immediate value?
        inline constexpr auto isImmediate() const {
            return mode.isImmediate();
        }

        /// Returns the register index/minor type value
        inline constexpr auto getXn() const {
            return mode.Xn;
        }
    };

    /// Size associated with an opcode
    enum class Size {
        Byte, Word, Longword,
        /// The instruction does not have an associated size field
        None,
    };
    /// Gets the number of bits required for the given size
    static inline constexpr size_t SizeToBits(const Size size) {
        switch(size) {
            case Size::Byte: return 8;
            case Size::Word: return 16;
            case Size::Longword: return 32;
            default: return 0;
        }
    }
    /// Gets the number of bytes required for the given size
    static inline constexpr size_t SizeToBytes(const Size size) {
        switch(size) {
            case Size::Byte: return 1;
            case Size::Word: return 2;
            case Size::Longword: return 4;
            default: return 0;
        }
    }
    /// Gets the sign bit for an integer value of the given size
    static inline constexpr uint32_t SignBitFor(const Size size) {
        switch(size) {
            case Size::Byte: return (1 << 7);
            case Size::Word: return (1 << 15);
            case Size::Longword: return (1 << 31);
            default: return 0;
        }
    }
    /// Gets a bit mask for an integer value of the given size
    static inline constexpr uint32_t MaskFor(const Size size) {
        switch(size) {
            case Size::Byte: return 0xFF;
            case Size::Word: return 0xFFFF;
            case Size::Longword: return 0xFFFFFFFF;
            default: return 0;
        }
    }

    static void InitMap();

    /**
     * @brief Execute an instruction given its opcode
     *
     * Invokes the appropriate handler for a particular processor instruction; the first word of
     * the opcode is enough to fully define the remainder of the opcode and whether any extension
     * words need to be fetched.
     *
     * @param proc Processor instance whose state this instruction operates on
     * @param opcode The opcode to be executed
     */
    static inline void Execute(Processor &proc, const uint16_t opcode) {
        gOpcodes(opcode, proc);
    }

    /**
     * @brief Extract a conditional test type from an opcode.
     *
     * @tparam M Most significant bit of the 4-bit condition test field
     * @tparam T Integer type to use
     *
     * @param val Value to extract the conditional test type from (such as an opcode)
     *
     * @return Extracted conditional test type
     */
    template<size_t M, typename T>
    static inline constexpr auto GetCondition(const T val) {
        return static_cast<Condition>(GetBits<M,4>(val));
    }

    /**
     * @brief Extracts a 3-bit register index from an opcode.
     *
     * @tparam M Most significant bit of the 3-bit value
     * @tparam T Integer type to use
     *
     * @param val Value to extract the register index from (such as an opcode)
     *
     * @return Extracted byte
     */
    template<size_t M, typename T>
    static inline constexpr uint8_t GetRegIndex(const T val) {
        return GetBits<M,3>(val);
    }

    /**
     * @brief Extracts a size flag from an opcode.
     *
     * The encoding is as follows: byte '00', word '01', long '10'. It is used for all but the MOVE
     * instructions, which have their own encoding.
     *
     * @tparam Msb Most significant bit of the size field; this defaults to bit 7, where all of the
     *         supported instructions that use this format have the size field.
     * @tparam T Type of input variable
     *
     * @param opcode Opcode to extract the operation size from
     *
     * @return Size enum corresponding to this size field
     */
    template<size_t Msb = 7, typename T>
    constexpr static inline auto GetSize(const T opcode) {
        switch(GetBits<Msb,2>(opcode)) {
            case 0b00:
                return Size::Byte;
            case 0b01:
                return Size::Word;
            case 0b10:
                return Size::Longword;
            default:
                return Size::None;
        }
    }

    /// @{ @name Address calculation
    /**
     * Register addressing mode instructions
     */
    static void RegisterAddress() {
        gOpcodes.add(PushEffectiveAddr, "0100100001AAAAAA",
                "AAAAAA=010000-010111,101000-110111,111000,111001,111010,111011");
        gOpcodes.add(LoadEffectiveAddr, "0100***111AAAAAA",
                "AAAAAA=010000-010111,101000-110111,111000,111001,111010,111011");
    }

    static void LoadEffectiveAddr(const uint16_t, Processor &);
    static void PushEffectiveAddr(const uint16_t, Processor &);

    static uint32_t CalculateEffectiveAddr(Processor &, const AddressingMode mode,
            const Size size, const AllowedAddressingModeMask validMode = kAddrModeAll,
            const bool wantPrefetch = true);

    static ResolvedAddressingMode ResolveEffectiveAddr(Processor &proc, const AddressingMode mode,
            const Size size, const AllowedAddressingModeMask validModes = kAddrModeAll);

    /**
     * Reads the contents of an effective address.
     *
     * All addressing modes supported by the 68000 are supported by this call for reading data from
     * registers, an immediate value, or memory. The effective address is resolved before calling
     * the read method.
     *
     * @param mode Addressing mode to use
     * @param size Width of this access
     * @param validModes Bitfield indicating which major and minor addressing modes are allowed
     *
     * @return Value read from the address; for some sources, such as registers, the whole 32 bits
     *         are returned regardless of the access size. Accessing memory will always respect the
     *         size field however.
     */
    static inline uint32_t ReadEffectiveAddr(Processor &proc, const AddressingMode mode,
            const Size size, const AllowedAddressingModeMask validModes = kAddrModeAll) {
        auto resolved = ResolveEffectiveAddr(proc, mode, size, validModes);
        return ReadEffectiveAddr(proc, resolved, size);
    }

    /**
     * Writes data to the location specified by an effective address.
     *
     * Only modes in the data alterable category are supported by this call: notably, writing to an
     * address register directly, immediates, and PC relative addressing are not allowed.
     *
     * @param mode Addressing mode to use
     * @param size Width of this access
     * @param value Value to be written
     * @param dummyRead When set, and the destination is memory, read from the location before the
     *        write is performed.
     * @param validModes Bitfield indicating which major and minor addressing modes are allowed
     */
    static void WriteEffectiveAddr(Processor &proc, const AddressingMode mode, const Size size,
            const uint32_t value, const bool dummyRead = false,
            const AllowedAddressingModeMask validModes = kAddrModeDataAlterable) {
        auto resolved = ResolveEffectiveAddr(proc, mode, size, validModes);
        return WriteEffectiveAddr(proc, resolved, size, value, dummyRead);
    }

    static uint32_t ReadEffectiveAddr(Processor &proc, const ResolvedAddressingMode mode,
            const Size size);
    static void WriteEffectiveAddr(Processor &proc, const ResolvedAddressingMode mode,
            const Size size, const uint32_t value, const bool dummyRead = false);

    /**
     * @brief Extracts an effective addressing mode from an integer value.
     *
     * @tparam M Most significant bit of the 3-bit mode value
     * @tparam Xn Most significant bit of the 3-bit register value
     * @tparam T Integer type to use
     *
     * @param val Value to extract the addressing mode from (such as an opcode)
     *
     * @return Extracted addressing mode
     */
    template<size_t M, size_t Xn, typename T>
    static inline constexpr AddressingMode GetAddrMode(const T val) {
        return {static_cast<uint8_t>(GetBits<M,3>(val)),
            static_cast<uint8_t>(GetBits<Xn,3>(val))};
    }


    /// @}

    /// @{ @name Branches and jumps
    /**
     * Register branch and jump instructions
     */
    static void RegisterBranch() {
        gOpcodes.add(ReturnException, 0b0100'1110'0111'0011);
        gOpcodes.add(ReturnSub, 0b0100'1110'0111'0101);

        gOpcodes.add(Jump, "0100111011AAAAAA",
                "AAAAAA=010000-010111,101000-110111,111000,111001,111010,111011");
        gOpcodes.add(JumpSub, "0100111010AAAAAA",
                "AAAAAA=010000-010111,101000-110111,111000,111001,111010,111011");

        gOpcodes.add(Branch, "01100000********");
        gOpcodes.add(BranchSub, "01100001********");

        gOpcodes.add(Trap, "010011100100****");
        gOpcodes.add(TrapV, 0b0100'1110'0111'0110);
    }

    static void Branch(const uint16_t, Processor &);
    static void BranchSub(const uint16_t, Processor &);
    static void BranchCommon(const uint16_t, Processor &);
    static void Jump(const uint16_t, Processor &);
    static void JumpSub(const uint16_t, Processor &);
    static void ReturnSub(const uint16_t, Processor &);
    static void ReturnException(const uint16_t, Processor &);
    static void Trap(const uint16_t, Processor &);
    static void TrapV(const uint16_t, Processor &);
    /// @}

    /// @{ @name Conditional - Branches
    /**
     * Register conditional branch instructions
     */
    static void RegisterConditionalBranch() {
        gOpcodes.add(BranchConditionalWithCount, "0101****11001***");
        gOpcodes.add(BranchConditional, "0110CCCC********", "CCCC=0010-1111");
    }

    static void BranchConditionalWithCount(const uint16_t, Processor &);
    static void BranchConditional(const uint16_t, Processor &);

    /// @}

    /// @{ @name Conditional - Set
    /**
     * Registers the Exclusive OR (EOR/XOR) type arithmetic instructions.
     */
    static void RegisterConditionalSet() {
        gOpcodes.add(SetConditional, "0101****11AAAAAA",
                "AAAAAA=000000-000111,010000-110111,111000,111001");
    }

    static void SetConditional(const uint16_t, Processor &);
    /// @}

    /// @{ @name Moves
    /**
     * Register MOVE type instructions
     */
    static void RegisterMove() {
        gOpcodes.add(MoveAddress, "00SS***001AAAAAA",
                "SS=11,10 AAAAAA=000000-110111,111000,111001,111010,111011,111100");

        gOpcodes.add(Move, "00SS***DDDAAAAAA",
                "SS=01 DDD=000,010,011,100,101,110 "
                "AAAAAA=000000-000111,010000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(Move, "00SSAAAAAAEEEEEE",
                "SS=01 AAAAAA=000111,001111 "
                "EEEEEE=000000-000111,010000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(Move, "00SS***DDDEEEEEE",
                "SS=11,10 DDD=000,010,011,100,101,110"
                "EEEEEE=000000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(Move, "00SSAAAAAAEEEEEE",
                "SS=11,10 AAAAAA=000111,001111 "
                "EEEEEE=000000-110111,111000,111001,111010,111011,111100");

        gOpcodes.add(MoveFromSr, "0100000011AAAAAA",
                "AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(MoveToCcr, "0100010011AAAAAA",
                "AAAAAA=000000-000111,010000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(MoveToSr, "0100011011AAAAAA",
                "AAAAAA=000000-000111,010000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(MoveUsp, "010011100110****");

        gOpcodes.add(MoveMultiple, "01001B001*AAAAAA",
                "B=0 AAAAAA=010000-010111,100000-110111,111000,111001");
        gOpcodes.add(MoveMultiple, "01001B001*AAAAAA",
                "B=1 AAAAAA=010000-011111,101000-110111,111000,111001,111010,111011");

        gOpcodes.add(MoveQuick, "0111***0********");

        gOpcodes.add(Clear, "01000010SSAAAAAA",
                "SS=00-10 AAAAAA=000000-000111,010000-110111,111000,111001");

        gOpcodes.add(Swap, "0100100001000***");
        gOpcodes.add(ExchangeRegs, "1100***1MMMMM***", "MMMMM=01000,01001,10001");
    }

    static void MoveMultiple(const uint16_t, Processor &);
    static void MoveQuick(const uint16_t, Processor &);
    static void MoveAddress(const uint16_t, Processor &);
    static void Move(const uint16_t, Processor &);

    static void MoveToCcr(const uint16_t, Processor &);
    static void MoveFromSr(const uint16_t, Processor &);

    static void MoveToSr(const uint16_t, Processor &);
    static void MoveUsp(const uint16_t, Processor &);

    static void MoveUpdateFlags(Processor &, const Size, const uint32_t);
    static void MoveMultipleImplPredecrement(Processor &, const uint16_t, const bool,
            const AddressingMode, const Size, uint32_t &);
    static void MoveMultipleImpl(Processor &, const uint16_t, const bool,
            const AddressingMode, const Size, uint32_t &);

    static void Clear(const uint16_t, Processor &);
    static void Swap(const uint16_t, Processor &);
    static void ExchangeRegs(const uint16_t, Processor &);

    /// @}

    /// @{ @name Comparisons
    /**
     * Register CMP type instructions
     */
    static void RegisterCompare() {
        gOpcodes.add(Compare, "1011***0SSAAAAAA", // byte
                "SS=00 AAAAAA=000000-000111,010000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(Compare, "1011***0SSAAAAAA", // word and longword
                "SS=01-10 AAAAAA=000000-110111,111000,111001,111010,111011,111100");

        gOpcodes.add(CompareAddress, "1011***SS1AAAAAA",
                "SS=01,11 AAAAAA=000000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(CompareMemory, "1011***1SS001***", "SS=00-10");
        gOpcodes.add(CompareImmediate, "00001100SSAAAAAA",
                "SS=00-10 AAAAAA=000000-000111,010000-110111,111000,111001");

        gOpcodes.add(Test, "01001010SSAAAAAA",
                "SS=00-10 AAAAAA=000000-000111,010000-110111,111000,111001");
    }

    static void CompareAddress(const uint16_t, Processor &);
    static void CompareImmediate(const uint16_t, Processor &);
    static void CompareMemory(const uint16_t, Processor &);
    static void Compare(const uint16_t, Processor &);
    static void Test(const uint16_t, Processor &);

    /// @}

    /// Types of bit change operations that can be performed
    enum class BitChangeOp {
        /// Set a bit in the destination
        BSET,
        /// Clear a bit in the destination
        BCLR,
        /// Change (or toggle) a bit in the destination
        BCHG,
    };

    /// @{ @name Bitwise
    /**
     * Register bitwise operation type opcodes
     */
    static void RegisterBitwise() {
        gOpcodes.add(BitChange, "0000***B01AAAAAA",
                "B=1 AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(BitChange, "0000100B01AAAAAA",
                "B=0 AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(BitClear, "0000***B10AAAAAA",
                "B=1 AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(BitClear, "0000100B10AAAAAA",
                "B=0 AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(BitSet, "0000***B11AAAAAA",
                "B=1 AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(BitSet, "0000100B11AAAAAA",
                "B=0 AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(BitTest, "0000***B00AAAAAA",
                "B=1 AAAAAA=000000-000111,010000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(BitTest, "0000100B00AAAAAA",
                "B=0 AAAAAA=000000-000111,010000-110111,111000,111001,111010,111011");
    }

    /**
     * @brief BSET
     *
     * Sets a bit in the destination operand; either a data register (long) or memory location
     * (byte.)
     *
     * The zero flag is loaded with the inverse of the original value of the bit.
     *
     * @todo Validate timings
     */
    static void BitSet(const uint16_t opcode, Processor &proc) {
        BitChangeCommon(opcode, proc, BitChangeOp::BSET);
    }

    /**
     * @brief BCLR
     *
     * Clears a bit in the destination operand.
     *
     * The zero flag is loaded with the inverse of the original value of the bit.
     *
     * @todo Validate timings
     */
    static void BitClear(const uint16_t opcode, Processor &proc) {
        BitChangeCommon(opcode, proc, BitChangeOp::BCLR);
    }

    /**
     * @brief BCHG
     *
     * Toggle a bit in the destination operand.
     *
     * The zero flag is loaded with the inverse of the original value of the bit.
     *
     * @todo Validate timings
     */
    static void BitChange(const uint16_t opcode, Processor &proc) {
        BitChangeCommon(opcode, proc, BitChangeOp::BCHG);
    }


    static void BitTest(const uint16_t, Processor &);

    static void BitChangeCommon(const uint16_t, Processor &, const BitChangeOp);

    /// @}
    /// @{ @name Arithmetic
    /**
     * Registers all arithmetic instructions.
     */
    static void RegisterArithmetic() {
        // two's complement stuff
        RegisterAdd();
        RegisterSub();
        // bitwise arithmetic
        RegisterAnd();
        RegisterOr();
        RegisterExclusiveOr();
        RegisterBitShift();
        RegisterBitRotation();
        // others
        RegisterNegation();
    }
    /// @}

    /// @{ @name Arithmetic - Subtraction
    /**
     * Registers all subtraction instructions
     */
    static void RegisterSub() {
        gOpcodes.add(Subtract, "1001***DSSAAAAAA",
                "D=0 SS=00 AAAAAA=000000-000111,010000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(Subtract, "1001***DSSAAAAAA",
                "D=0 SS=01-10 AAAAAA=000000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(Subtract, "1001***DSSAAAAAA",
                "D=1 SS=00-10 AAAAAA=010000-110111,111000,111001");

        gOpcodes.add(SubtractAddress, "1001***SS1AAAAAA",
                "SS=01,11 AAAAAA=000000-110111,111000,111001,111010,111011,111100");

        gOpcodes.add(SubtractImmediate, "00000100SSAAAAAA",
                "SS=00-10 AAAAAA=000000-000111,010000-110111,111000,111001");

        gOpcodes.add(SubtractQuick, "0101***1SSAAAAAA",
                "SS=00 AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(SubtractQuick, "0101***1SSAAAAAA",
                "SS=01-10 AAAAAA=000000-110111,111000,111001");
    }

    static void SubtractQuick(const uint16_t, Processor &);
    static void SubtractImmediate(const uint16_t, Processor &);
    static void SubtractAddress(const uint16_t, Processor &);
    static void Subtract(const uint16_t, Processor &);
    /// @}

    /// @{ @name Arithmetic - Addition
    /**
     * Registers all addition instructions
     */
    static void RegisterAdd() {
        gOpcodes.add(Add, "1101***DSSAAAAAA", // <ea> + Dn -> Dn, byte
                "D=0 SS=00 AAAAAA=000000-000111,010000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(Add, "1101***DSSAAAAAA", // <ea> + Dn -> Dn, word/long
                "D=0 SS=01-10 AAAAAA=000000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(Add, "1101***DSSAAAAAA", // Dn + <ea> -> <ea>
                "D=1 SS=00-10 AAAAAA=010000-110111,111000,111001");

        gOpcodes.add(AddAddress, "1101***SS1AAAAAA",
                "SS=01,11 AAAAAA=000000-110111,111000,111001,111010,111011,111100");

        gOpcodes.add(AddImmediate, "00000110SSAAAAAA",
                "SS=00-10 AAAAAA=000000-000111,010000-110111,111000,111001");

        gOpcodes.add(AddQuick, "0101***0SSAAAAAA", // byte modes
                "SS=00 AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(AddQuick, "0101***0SSAAAAAA", // word, longword modes
                "SS=01-10 AAAAAA=000000-110111,111000,111001");
    }

    static void AddQuick(const uint16_t, Processor &);
    static void AddImmediate(const uint16_t, Processor &);
    static void AddAddress(const uint16_t, Processor &);
    static void Add(const uint16_t, Processor &);
    /// @}

    /// @{ @name Arithmetic - AND
    /**
     * Registers the AND type arithmetic instructions.
     */
    static void RegisterAnd() {
        gOpcodes.add(And, "1100***DSSAAAAAA",
                "D=0 SS=00-10 "
                "AAAAAA=000000-000111,010000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(And, "1100***DSSAAAAAA",
                "D=1 SS=00-10 AAAAAA=010000-110111,111000,111001");

        gOpcodes.add(AndImmediate, "00000010SSAAAAAA",
                "SS=00-10 AAAAAA=000000-000111,010000-110111,111000,111001");

        gOpcodes.add(AndImmediateToStatus, 0b0000'0010'0111'1100); // ANDI to SR
        gOpcodes.add(AndImmediateToStatus, 0b0000'0010'0011'1100); // ANDI to CCR
    }

    static void AndImmediateToStatus(const uint16_t, Processor &);
    static void AndImmediate(const uint16_t, Processor &);
    static void And(const uint16_t, Processor &);
    /// @}

    /// @{ @name Arithmetic - OR
    /**
     * Registers the OR type arithmetic instructions.
     */
    static void RegisterOr() {
        gOpcodes.add(Or, "1000***DSSAAAAAA", // store to register
                "D=0 SS=00-10 "
                "AAAAAA=000000-000111,010000-110111,111000,111001,111010,111011,111100");
        gOpcodes.add(Or, "1000***DSSAAAAAA", // store to memory
                "D=1 SS=00-10 AAAAAA=010000-110111,111000,111001");

        gOpcodes.add(OrImmediate, "00000000SSAAAAAA",
                "SS=00-10 AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(OrImmediateToStatus, 0b0000'0000'0111'1100); // ORI to SR
        gOpcodes.add(OrImmediateToStatus, 0b0000'0000'0011'1100); // ORI to CCR
    }

    static void OrImmediateToStatus(const uint16_t, Processor &);
    static void OrImmediate(const uint16_t, Processor &);
    static void Or(const uint16_t, Processor &);
    /// @}

    /// @{ @name Arithmetic - Exclusive OR
    /**
     * Registers the Exclusive OR (EOR/XOR) type arithmetic instructions.
     */
    static void RegisterExclusiveOr() {
        gOpcodes.add(ExclusiveOr, "1011***1SSAAAAAA",
                "SS=00-10 AAAAAA=000000-000111,010000-110111,111000,111001");

        gOpcodes.add(ExclusiveOrImmediate, "00001010SSAAAAAA",
                "SS=00-10 AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(ExclusiveOrImmediateToStatus, 0b0000'1010'0111'1100); // EORI to SR
        gOpcodes.add(ExclusiveOrImmediateToStatus, 0b0000'1010'0011'1100); // EORI to SSR
    }

    static void ExclusiveOrImmediateToStatus(const uint16_t, Processor &);
    static void ExclusiveOrImmediate(const uint16_t, Processor &);
    static void ExclusiveOr(const uint16_t, Processor &);
    /// @}

    /// @{ @name Arithmetic - Exclusive OR
    /**
     * Registers all negation arithmetic instructions.
     */
    static void RegisterNegation() {
        gOpcodes.add(LogicalComplement, "01000110SSAAAAAA",
                "SS=00-10 AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(Negate, "01000100SSAAAAAA",
                "SS=00-10 AAAAAA=000000-000111,010000-110111,111000,111001");
        gOpcodes.add(ExtendSign, "010010001*000***");
    }

    static void LogicalComplement(const uint16_t, Processor &);
    static void Negate(const uint16_t, Processor &);
    static void ExtendSign(const uint16_t, Processor &);

    /// @}

    /// @{ @name Arithmetic - Bit shifts
    /**
     * Registers all bit shift instructions.
     */
    static void RegisterBitShift() {
        gOpcodes.add(LogicalShiftLeft, "1110***1SS*01***", "SS=00-10");
        gOpcodes.add(LogicalShiftLeft, "1110001111AAAAAA", "AAAAAA=010000-110111,111000,111001");
        gOpcodes.add(LogicalShiftRight, "1110***0SS*01***", "SS=00-10");
        gOpcodes.add(LogicalShiftRight, "1110001011AAAAAA", "AAAAAA=010000-110111,111000,111001");

        gOpcodes.add(ArithmeticShiftLeft, "1110***1SS*00***", "SS=00-10");
        gOpcodes.add(ArithmeticShiftLeft, "1110000111AAAAAA", "AAAAAA=010000-110111,111000,111001");
        gOpcodes.add(ArithmeticShiftRight, "1110***0SS*00***", "SS=00-10");
        gOpcodes.add(ArithmeticShiftRight, "1110000011AAAAAA", "AAAAAA=010000-110111,111000,111001");
    }

    static void LogicalShiftLeft(const uint16_t, Processor &);
    static void LogicalShiftRight(const uint16_t, Processor &);

    static void ArithmeticShiftLeft(const uint16_t, Processor &);
    static void ArithmeticShiftRight(const uint16_t, Processor &);

    /// @}

    /// @{ @name Arithmetic - Bit rotation
    /**
     * Registers all bit rotation instructions.
     */
    static void RegisterBitRotation() {
        gOpcodes.add(RotateLeft, "1110***1SS*11***", "SS=00-10");
        gOpcodes.add(RotateLeft, "1110011111AAAAAA", "AAAAAA=010000-110111,111000,111001");

        gOpcodes.add(RotateRight, "1110***0SS*11***", "SS=00-10");
        gOpcodes.add(RotateRight, "1110011011AAAAAA", "AAAAAA=010000-110111,111000,111001");
    }

    static void RotateLeft(const uint16_t, Processor &);
    static void RotateRight(const uint16_t, Processor &);

    /// @}


    /// @{ @name General
    /**
     * Registers general opcodes.
     */
    static void RegisterGeneral() {
        gOpcodes.add(Illegal, 0b0100'1010'1111'1100);
        gOpcodes.add(Reset, 0b0100'1110'0111'0000);
        gOpcodes.add(NoOp, 0b0100'1110'0111'0001);
    }

    static void NoOp(const uint16_t, Processor &);
    static void Reset(const uint16_t, Processor &);
    static void Illegal(const uint16_t, Processor &);
    /// @}

    /// @{ @name Helpers
    /**
     * Performs a prefetch cycle; all instructions should do this at least once, more times if
     * they have extension words or redirect program execution.
     */
    inline static void PrefetchCycle(Processor &proc) {
        proc.microcycleNop();
        proc.microcyclePrefetch();
    }

    /**
     * Reads an immediate word. This should only be used for the _first_ immediate word in an
     * instruction. Either 16 or 32 bits are read, but the data is masked appropriately.
     */
    inline static uint32_t ReadImmediate(Processor &proc, const Size size) {
        uint32_t temp{0};
        // read first word (common for all)
        temp = proc.getExtensionWord();
        // for longword, that was the high word so now read the low word
        if(size == Size::Longword) {
            const auto low = proc.getExtensionWordBonus();
            temp = (temp << 16) | (low & 0xFFFF);
        }
        // the bonus word will have already done one prefetch, if needed
        PrefetchCycle(proc);
        return temp & MaskFor(size);
    }
    /// @}

    protected:
        /// Method that can resolve an address calculation request
        using AddressCalculator = uint32_t(*)(Processor &, const AddressingMode, const Size,
                const bool);

    /// @{ @name Address calculation helpers
        static uint32_t CalculateRegisterIndirectIndex(Processor &, const AddressingMode,
                const Size, const uint16_t, const uint32_t &);
        static uint32_t HandleBonusAddressingModes(Processor &, const AddressingMode, const Size,
                const bool);
        static uint32_t HandleRegisterAddressingMode(Processor &, const AddressingMode, const Size,
                const bool);
        static uint32_t HandleInvalidAddressingMode(Processor &, const AddressingMode, const Size,
                const bool);

        /// Valid mode field for control addressing modes
        constexpr static const AllowedAddressingModeMask kAddrModeControl{0b1110'0100, 0b0000'1111};
        /// Valid mode field for data addressing modes
        constexpr static const AllowedAddressingModeMask kAddrModeData{0b1111'1101, 0b0001'1111};
        /// Valid mode field for data alterable (e.g. usable as a MOVE destination) modes
        constexpr static const AllowedAddressingModeMask kAddrModeDataAlterable{0b1111'1101, 0b0000'0011};
        /// Valid mode field for alterable modes (same as data alterable, but includes An)
        constexpr static const AllowedAddressingModeMask kAddrModeAlterable{0b1111'1111, 0b0000'0011};
        /// Valid mode field for all supported addressing modes
        constexpr static const AllowedAddressingModeMask kAddrModeAll{0b1111'1111, 0b0001'1111};

    /// @}

    /// @{ @name Configuration

    /**
     * Enables detection and fatal handling of brief extension words that have an invalid "scale"
     * field. This is useful to detect 68020+ code.
     *
     * By default, the 68000 and 68010 do not cause any exceptions (or even detect) when this field
     * is nonzero. The PRM (section 2.4) indicates this.
     */
    static constexpr const bool kCheckBriefExtScale{true};

    /// Output effective address calculation info
    static constexpr const bool kLogEACalculation{false};

    /// Output all branches (BRA/BSR, JMP/JSR, RTS/RTE)
    static constexpr const bool kLogBranch{false};
    /// Output all exception instructions (TRAP/TRAPV)
    static constexpr const bool kLogTraps{false};
    /// Output all conditional branches (Bcc, DBcc)
    static constexpr const bool kLogConditionalBranch{false};

    /// Output all comparisons (CMP)
    static constexpr const bool kLogCompare{false};

    /// Output all bit set/clear/change instructions
    static constexpr const bool kLogBitwiseChange{false};
    /// Output all logical shift instructions
    static constexpr const bool kLogLogicalShift{false};
    /// Output all arithmetic shift instructions
    static constexpr const bool kLogArithmeticShift{false};
    /// Output all bit rotation instructions
    static constexpr const bool kLogRotate{false};

    /// Output every move instruction
    static constexpr const bool kLogMove{false};
    /// Output logging for move instruction address calculation
    static constexpr const bool kLogMoveAddresses{false};
    /// Output logging for MOVEM
    static constexpr const bool kLogMoveMultiple{false};
    /// Output logging for each register saved/restored by MOVEM
    static constexpr const bool kLogMoveMultipleAddr{false};

    /// @}

    protected:
        static OpcodeTable<InstructionHandler2> gOpcodes;

    private:
        using Condition = Processor::Registers::Condition;
        using Ccr = Processor::Registers::ConditionCode;
};
}

#endif
