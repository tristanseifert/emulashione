/*
 * Conditional set: Scc - Set according to condition
 */
#include "Decoder.h"
#include "Decoder+Fmt.h"
#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

using namespace emulashione::plugin;
using namespace emulashione::m68k;
using namespace emulashione::m68k::m68000;

using namespace magic_enum::bitwise_operators;

/**
 * @brief Scc
 *
 * Set the resulting effective address according to the given condition.
 *
 * TODO: Verify the timings of this instruction, they're likely wrong
 */
void Decoder::SetConditional(const uint16_t opcode, Processor &proc) {
    // extract opcode fields
    const auto condition = GetCondition<11>(opcode);
    const auto addrMode = GetAddrMode<5,2>(opcode);

    // resolve address and perform a dummy read
    constexpr static const auto size{Size::Byte};
    auto resolved = ResolveEffectiveAddr(proc, addrMode, size, kAddrModeDataAlterable);

    //const auto readData = ReadEffectiveAddr(proc, resolved, size);
    //(void) readData;

    // evaluate condition and set true (0xFF) or false (0x00)
    const uint8_t res = proc.regs.test(condition) ? 0xFF : 0x00;
    Logger::Warning("Scc {} = {} ({:02x})", condition, resolved, res);

    // prefetch and write result back
    PrefetchCycle(proc);

    WriteEffectiveAddr(proc, resolved, size, res);

    // apply a dummy no-op if condition is true and result is Dn
    if(res && resolved.isDataRegister()) {
        proc.microcycleNop();
    }
}

