/*
 * Implementations of move instructions: MOVE, MOVEA, MOVEQ, and various special MOVE to/from
 * SR, CCR, and USP.
 *
 * @todo Add MOVEM, MOVEP
 */
#include "Decoder.h"
#include "Decoder+Fmt.h"
#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

#include <algorithm>
#include <bitset>

using namespace emulashione::plugin;
using namespace emulashione::m68k;
using namespace emulashione::m68k::m68000;
using namespace magic_enum::bitwise_operators;

/**
 * The MOVE instructions use an interesting size encoding; `01` is byte, `11` is word and `10` is
 * longword. It is only used for the move instructions (and CHK, but that's fixed at word)
 */

template<size_t Msb = 13, typename T>
constexpr static inline auto GetMoveSize(const T opcode) {
    switch(GetBits<Msb,2>(opcode)) {
        case 0b01:
            return Decoder::Size::Byte;
        case 0b11:
            return Decoder::Size::Word;
        case 0b10:
            return Decoder::Size::Longword;
        default:
            return Decoder::Size::None;
    }
}

/**
 * @brief MOVE
 *
 * Implements the basic MOVE instruction. This handles all source and destination modes; that is,
 * all source addressing modes are supported, and the destination must be the data alterable
 * modes.
 *
 * Condition codes are updated according to the standard MOVE semantics.
 */
void Decoder::Move(const uint16_t opcode, Processor &proc) {
    // get opcode size
    Size size = GetMoveSize(opcode);

    // get source and read it
    const auto srcMode = GetAddrMode<5,2>(opcode);
    const auto value = ReadEffectiveAddr(proc, srcMode, size);

    /*
     * Decode destination address and write to it; if this is a stack access, and the access is
     * byte wide, it's upgraded to a word write with the byte in the upper slice.
     */
    const auto dstMode = GetAddrMode<8,11>(opcode);

    if(dstMode.isStackPointerIndirect() && size == Size::Byte) {
        WriteEffectiveAddr(proc, dstMode, Size::Word, static_cast<uint16_t>(value & 0xFF) << 8);
    } else {
        WriteEffectiveAddr(proc, dstMode, size, value);
    }

    if(kLogMove) {
        Logger::Trace("MOVE {}: Src {} ${:08x}, dst {} PC ${:08x}", magic_enum::enum_name(size),
                srcMode, value, dstMode, proc.regs.pc);
    }

    // Update flags and prefetch
    MoveUpdateFlags(proc, size, value);
    PrefetchCycle(proc);
}

/**
 * @brief MOVEA
 *
 * Implements MOVE with a destination being an address register. This works identically to the MOVE
 * instruction, except that only longword and word sizes are allowed; and that words are size
 * extended to longwords before being loaded to the address register. Logically, the only
 * destination mode permitted is address register.
 *
 * Condition codes are not modified.
 */
void Decoder::MoveAddress(const uint16_t opcode, Processor &proc) {
    uint32_t value{0};

    // get opcode size
    Size size = GetMoveSize(opcode);
    if(size == Size::Byte) {
        // TODO: this is probably just na illegal instruction exception
        Logger::Error("MOVEA with invalid size: {}", magic_enum::enum_name(size));
        return proc.abort(Processor::AbortReason::InvalidSize);
    }

    // read the source value
    const auto srcMode = GetAddrMode<5,2>(opcode);
    value = ReadEffectiveAddr(proc, srcMode, size);

    if(size == Size::Word) {
        value = SignExtend<int32_t,16>(value);
    }

    // store in destination register
    const size_t An = GetRegIndex<11>(opcode);
    proc.regs.getAn(An) = value;

    if(kLogMove) Logger::Trace("MOVEA: Src {}, dst A{} = ${:08x}", srcMode, An, value);

    // prefetch
    PrefetchCycle(proc);
}

/**
 * @brief MOVEQ
 *
 * Reads an immediate 8-bit value from the opcode, sign extends it to 32 bits and writes it to one
 * of the data registers.
 *
 * Condition codes are updated according to the standard MOVE semantics.
 */
void Decoder::MoveQuick(const uint16_t opcode, Processor &proc) {
    const auto Dn = GetRegIndex<11>(opcode);
    const auto value = static_cast<uint32_t>(SignExtend<int32_t, 8>(GetBits<7,8>(opcode)));

    proc.regs.d[Dn] = value;

    if(kLogMove) Logger::Trace("MOVEQ: Value ${:08x} D{:1}", value, Dn);

    // update flags and do prefetch
    MoveUpdateFlags(proc, Size::Longword, value);
    PrefetchCycle(proc);
}



/**
 * @brief MOVE to CCR
 *
 * Reads a word from the provided effective address, copying the low byte to the condition code
 * register. The high byte is ignored, and the status register is not modified.
 *
 * All addressing modes _except_ address registers are allowed. Size is fixed at word.
 *
 * Condition codes are set according to the source operand.
 *
 * @todo Check timing of this instruction against real hw
 */
void Decoder::MoveToCcr(const uint16_t opcode, Processor &proc) {
    // read source (and ensure it's not an address register)
    const auto srcMode = GetAddrMode<5,2>(opcode);

    if(srcMode.isAddressRegister()) {
        // TODO: is this the correct behavior?
        proc.setEvent(Processor::EventFlags::IllegalInstruction);
        return PrefetchCycle(proc);
    }

    const auto value = ReadEffectiveAddr(proc, srcMode, Size::Word);

    // update flags and do prefetch
    proc.regs.setCcr(static_cast<Ccr>(value & static_cast<uint8_t>(Ccr::Mask)));
    PrefetchCycle(proc);

    /*
     * XXX: this is "faking" the timing; normally there's two extra prefetch cycles here,
     * ostensibly because the prefetch queue is flushed
     */
    proc.microcycleNopWait();
    proc.microcycleNopWait();
}

/**
 * @brief MOVE from SR
 *
 * Reads the contents of the status register into the effective address specified. This is always
 * word wide.
 *
 * Condition codes are not modified.
 */
void Decoder::MoveFromSr(const uint16_t opcode, Processor &proc) {
    // read SR and mask out unimplemented bits
    auto sr = proc.regs.getStatus();
    sr &= Processor::Registers::kStatusMask;

    // write to destination
    const auto dstMode = GetAddrMode<5,2>(opcode);
    WriteEffectiveAddr(proc, dstMode, Size::Word, sr, true);

    // do prefetch
    PrefetchCycle(proc);

    // destination of data reg has a no-op cycle at end
    if(dstMode.isDataRegister()) {
        proc.microcycleNop();
    }
}

/**
 * @brief MOVE to SR
 *
 * Writes the word from the provided effective address, copying it into the status register. All
 * addressing modes _except_ address registers are allowed. Size is fixed at word.
 *
 * Condition codes are set according to the source word operand.
 *
 * @todo Check timing of this instruction against real hw
 */
void Decoder::MoveToSr(const uint16_t opcode, Processor &proc) {
    // ensure supervisor mode
    if(proc.regs.getPrivilegeLevel() != Processor::PrivilegeLevel::Supervisor) {
        PrefetchCycle(proc);
        return proc.setEvent(Processor::EventFlags::PrivilegeViolation);
    }

    // read source (and ensure it's not an address register)
    const auto srcMode = GetAddrMode<5,2>(opcode);

    if(srcMode.isAddressRegister()) {
        // TODO: is this the correct behavior?
        proc.setEvent(Processor::EventFlags::IllegalInstruction);
        return PrefetchCycle(proc);
    }

    const auto value = ReadEffectiveAddr(proc, srcMode, Size::Word);

    // update and do prefetch
    proc.regs.setStatus(value & Processor::Registers::kStatusMask);
    PrefetchCycle(proc);

    /*
     * XXX: this is "faking" the timing; normally there's two extra prefetch cycles here,
     * ostensibly because the prefetch queue is flushed
     */
    proc.microcycleNopWait();
    proc.microcycleNopWait();
}

/**
 * @brief MOVE USP
 *
 * Moves the contents of the user stack pointer to/from an address register. The user stack
 * pointer is always stored as A7; the getAn method handles aliasing it to SSP when running in
 * supervisor mode.
 *
 * Condition codes are not affected.
 *
 * @todo Verify behavior when destination register is A7
 */
void Decoder::MoveUsp(const uint16_t opcode, Processor &proc) {
    // ensure supervisor mode
    if(proc.regs.getPrivilegeLevel() != Processor::PrivilegeLevel::Supervisor) {
        PrefetchCycle(proc);
        return proc.setEvent(Processor::EventFlags::PrivilegeViolation);
    }

    // perform the operation
    auto &An = proc.regs.getAn(GetRegIndex<2>(opcode));

    if(GetBits<3,1>(opcode)) { // dr = 1: USP -> An
        An = proc.regs.a[7];
    } else { // dr = 0; An -> USP
        proc.regs.a[7] = An;
    }

    // prefetch
    PrefetchCycle(proc);
}



/**
 * @brief MOVEM
 *
 * Moves multiple registers to or from memory. This instruction operators on either word (which are
 * sign extended to longword when loaded) or longword data, and allows only certain addressing
 * modes depending on the direction:
 *
 * - Register-to-memory: (An), -(An), (d16,An), (d8,An,Xn), (xxx).w, (xxx).l
 * - Memory-to-register: (An), (An)+, (d16,An), (d8,An,Xn), (xxx).w, (xxx).l, (d16,PC), (d8,PC,Xn)
 *
 * Following the opcode is a word, which is a bitmask indicating what registers should be saved or
 * restored, where the low order bit is the first register to be transfered. For control and
 * postincrement mode addresses, the word is laid out as follows, with the left side being bit 15,
 * and bit 0 on the right:
 *
 * A7...A0, D7...D0
 *
 * Whereas for the predecrement addressing mode, the word is laid out like so:
 *
 * D0...D7, A0...A7
 *
 * Condition codes are not affected.
 *
 * @todo This needs a _lot_ of testing, particularly around the (An)+ and -(An) modes.
 */
void Decoder::MoveMultiple(const uint16_t opcode, Processor &proc) {
    uint32_t currentAddr{0};

    // read the size, direction, and register mask
    const auto size = GetBits<6,1>(opcode) ? Size::Longword : Size::Word;
    const auto regToMem = !GetBits<10,1>(opcode);

    auto regMask = proc.getExtensionWord();
    PrefetchCycle(proc);

    /*
     * Read out and validate the addressing mode; the allowable modes vary depending on whether
     * the transfer is from memory or from registers.
     */
    constexpr static const AllowedAddressingModeMask kAddrRegToMemModes{0b1111'0100, 0b0000'0011};
    constexpr static const AllowedAddressingModeMask kAddrMemToRegModes{0b1110'1100, 0b0000'1111};

    const auto addrMode = GetAddrMode<5,2>(opcode);

    if(!addrMode.validate(regToMem ? kAddrRegToMemModes : kAddrMemToRegModes)) {
        Logger::Warning("Unsupported MOVEM reg-to-mem addressing mode: {}", addrMode);

        proc.setEvent(Processor::EventFlags::IllegalInstruction);
        return PrefetchCycle(proc);
    }

    if(kLogMoveMultiple) {
        Logger::Trace("MOVEM: {}, {}, regs ${:04x} pc ${:08x}",
                (regToMem ? "register-to-memory" : "memory-to-register"),
                magic_enum::enum_name(size), regMask, proc.regs.pc-2);
    }

    /*
     * Get the starting address; if the mode is (An)+ or -(An), read out the address register
     * manually, so we avoid performing the side effects that come with those; namely, the change
     * of the value in the register.
     *
     * For all other addressing modes, we have the effective addressing code resolve it for us.
     */
    if(addrMode.isAddressPostincrement() || addrMode.isAddressPredecrement()) {
        currentAddr = proc.regs.getAn(addrMode.Xn);
    } else {
        currentAddr = CalculateEffectiveAddr(proc, addrMode, size);
    }

    /*
     * For longword memory to register transfers, there is an extra read that takes place right
     * before all of the registers are read; it is the most significant word of an address (XXX:
     * which address?)
     */
    if(size == Size::Longword && !regToMem) {
        // TODO: determine the actual address this uses
        proc.microcycleNop();
        (void) proc.readWord(proc.getDataFc(), currentAddr);
    }

    /*
     * Select a different method to handle the actual moving of registers based on the addressing
     * mode; the predecrement mode interprets the mask differently (because the registers are
     * stored in the reverse order.)
     *
     * The mask can't be flipped since the order of storing is still different.
     */
    if(kLogMoveMultiple) Logger::Trace("MOVEM: Final mask: ${:04x}, starting ${:08x}", regMask,
            currentAddr);

    if(addrMode.isAddressPredecrement()) {
        MoveMultipleImplPredecrement(proc, regMask, regToMem, addrMode, size, currentAddr);
    } else {
        MoveMultipleImpl(proc, regMask, regToMem, addrMode, size, currentAddr);
    }

    /*
     * For word memory to register transfers, there is an extra read that takes place after all
     * registers have been read. Do it here if needed.
     */
    if(size == Size::Word && !regToMem) {
        // TODO: determine the actual address this uses
        proc.microcycleNop();
        (void) proc.readWord(proc.getDataFc(), currentAddr);
    }

    /*
     * If the addressing mode is either (An)+ or -(An), write back the address of the last element,
     * which varies based on the addressing mode. For predecrement, it is the address of the last
     * operand; for postincrement, it is the address immediately following the last operand.
     */
    if(addrMode.isAddressPredecrement() || addrMode.isAddressPostincrement()) {
        proc.regs.getAn(addrMode.Xn) = currentAddr;
    }

    PrefetchCycle(proc);
}

/**
 * Performs the register moving part of the MOVEM instruction, where the registers are stored in
 * the order of D0..D7, A0..A7.
 */
void Decoder::MoveMultipleImpl(Processor &proc, const uint16_t mask, const bool regToMem,
        const AddressingMode mode, const Size size, uint32_t &currentAddr) {
    const auto fc = proc.getDataFc();

    // handle data registers
    for(size_t i = 0; i < 8; i++) {
        // skip this register if its bit isn't set in the mask
        const uint16_t kRegisterBit = 1 << i;
        if(!(mask & kRegisterBit)) continue;

        auto &Dn = proc.regs.getDn(i);

        if(size == Size::Longword) {
            if(regToMem) proc.writeLongword(fc, currentAddr, Dn);
            else Dn = proc.readLongword(fc, currentAddr);
        } else {
            proc.microcycleNop();
            if(regToMem) proc.writeWord(fc, currentAddr, static_cast<uint16_t>(Dn));
            else Dn = SignExtend<int32_t,16>(proc.readWord(fc, currentAddr));
        }
        // increment address
        if(kLogMoveMultipleAddr) Logger::Trace("Restore D{} = ${:08x}", i, currentAddr);
        currentAddr += SizeToBytes(size);
    }

    // handle address registers
    for(size_t i = 0; i < 8; i++) {
        // skip this register if its bit isn't set in the mask
        const uint16_t kRegisterBit = 0x100 << i;
        if(!(mask & kRegisterBit)) continue;

        auto &An = proc.regs.getAn(i);

        if(size == Size::Longword) {
            if(regToMem) proc.writeLongword(fc, currentAddr, An);
            else An = proc.readLongword(fc, currentAddr);
        } else {
            proc.microcycleNop();
            if(regToMem) proc.writeWord(fc, currentAddr, static_cast<uint16_t>(An));
            else An = SignExtend<int32_t,16>(proc.readWord(fc, currentAddr));
        }

        // increment address
        if(kLogMoveMultipleAddr) Logger::Trace("Restore A{} = ${:08x}", i, currentAddr);
        currentAddr += SizeToBytes(size);
    }
}

/**
 * Performs the register moving part of the MOVEM instruction, where the registers are stored in
 * the order of A7..A0, D7..D0, and the address is decremented before each transfer.
 */
void Decoder::MoveMultipleImplPredecrement(Processor &proc, const uint16_t mask,
        const bool regToMem, const AddressingMode mode, const Size size, uint32_t &currentAddr) {
    const auto fc = proc.getDataFc();

    // handle address registers
    for(size_t i = 0; i < 8; i++) {
        // skip this register if its bit isn't set in the mask
        const uint16_t kRegisterBit = 1 << i;
        if(!(mask & kRegisterBit)) continue;

        // decrement address
        currentAddr -= SizeToBytes(size);
        if(kLogMoveMultipleAddr) Logger::Trace("Restore A{} = ${:08x}", 7 - i, currentAddr);

        auto &An = proc.regs.getAn(7 - i);

        if(size == Size::Longword) {
            if(regToMem) proc.writeLongword(fc, currentAddr, An);
            else An = proc.readLongword(fc, currentAddr);
        } else {
            proc.microcycleNop();
            if(regToMem) proc.writeWord(fc, currentAddr, static_cast<uint16_t>(An));
            else An = SignExtend<int32_t,16>(proc.readWord(fc, currentAddr));
        }
    }

    // handle data registers
    for(size_t i = 0; i < 8; i++) {
        // skip this register if its bit isn't set in the mask
        const uint16_t kRegisterBit = 0x100 << i;
        if(!(mask & kRegisterBit)) continue;

        // decrement address
        currentAddr -= SizeToBytes(size);
        if(kLogMoveMultipleAddr) Logger::Trace("Restore D{} = ${:08x}", 7 - i, currentAddr);

        auto &Dn = proc.regs.getDn(7 - i);

        if(size == Size::Longword) {
            if(regToMem) proc.writeLongword(fc, currentAddr, Dn);
            else Dn = proc.readLongword(fc, currentAddr);
        } else {
            proc.microcycleNop();
            if(regToMem) proc.writeWord(fc, currentAddr, static_cast<uint16_t>(Dn));
            else Dn = SignExtend<int32_t,16>(proc.readWord(fc, currentAddr));
        }
    }
}



/**
 * Updates the flags as a result of a MOVE instruction.
 *
 * The carry and overflow flags are always cleared; zero is set if the value is zero, and the
 * negative flag is set if the value is negative.
 */
void Decoder::MoveUpdateFlags(Processor &proc, const Size size, const uint32_t value) {
    Ccr set{Ccr::None}, clear{Ccr::Carry | Ccr::Overflow};

    // zero flag
    if(!value) {
        set |= Ccr::Zero;
    } else {
        clear |= Ccr::Zero;
    }
    // negative flag
    if(value & SignBitFor(size)) {
        set |= Ccr::Negative;
    } else {
        clear |= Ccr::Negative;
    }

    proc.regs.setCcr(set, clear);
}



/**
 * @brief CLR
 *
 * Clears the destination operand to zero.
 */
void Decoder::Clear(const uint16_t opcode, Processor &proc) {
    // resolve the effective address
    const auto size = GetSize(opcode);
    const auto addrMode = GetAddrMode<5,2>(opcode);

    const auto resolved = ResolveEffectiveAddr(proc, addrMode, size, kAddrModeDataAlterable);

    // do a dummy read from this address, and do the prefetch
    (void) ReadEffectiveAddr(proc, resolved, size);

    PrefetchCycle(proc);

    // then write zero to it and update the flags
    WriteEffectiveAddr(proc, resolved, size, 0);

    proc.regs.setCcr(Ccr::Zero, Ccr::Negative | Ccr::Overflow | Ccr::Carry);

    // longword clears of data registers have an extra noop
    if(size == Size::Longword && addrMode.isDataRegister()) {
        proc.microcycleNop();
    }
}

/**
 * @brief SWAP
 *
 * Exchange the 16 bit halves of a data register.
 *
 * V, C are always cleared, N and Z updated.
 */
void Decoder::Swap(const uint16_t opcode, Processor &proc) {
    // get register and exchange halves
    const auto regIndex = GetRegIndex<2>(opcode);
    auto &Dn = proc.regs.getDn(regIndex);

    const auto top = (Dn >> 16), bottom = (Dn & 0xFFFF);
    Dn = top | (bottom << 16);

    // update flags and perform prefetch
    proc.regs.setCcr(Ccr::Overflow | Ccr::Carry, false);
    proc.regs.setCcr(Ccr::Zero, !Dn);
    proc.regs.setCcr(Ccr::Negative, !!(Dn & SignBitFor(Size::Longword)));

    PrefetchCycle(proc);
}

/**
 * @brief EXG
 *
 * Exchange the contents of two 32-bit registers. This can be two data registers, two address
 * registers, or a data register and an address register.
 */
void Decoder::ExchangeRegs(const uint16_t opcode, Processor &proc) {
    const auto Rx = GetRegIndex<11>(opcode), Ry = GetRegIndex<2>(opcode);
    const auto opmode = GetBits<7,5>(opcode);

    // exchange two data registers
    if(opmode == 0b01000) {
        const auto Dx = proc.regs.getDn(Rx), Dy = proc.regs.getDn(Ry);
        proc.regs.storeData(Ry, Dx);
        proc.regs.storeData(Rx, Dy);
    }
    // exchange two address registers
    else if(opmode == 0b01001) {
        const auto Ax = proc.regs.getAn(Rx), Ay = proc.regs.getAn(Ry);
        proc.regs.getAn(Ry) = Ax;
        proc.regs.getAn(Rx) = Ay;
    }
    // exchange an address and data register
    else if(opmode == 0b10001) {
        const auto Dx = proc.regs.getDn(Rx), Ay = proc.regs.getAn(Ry);
        proc.regs.getAn(Ry) = Dx;
        proc.regs.storeData(Rx, Ay);
    }

    // perform the prefetch and nop cycle at the end
    PrefetchCycle(proc);

    proc.microcycleNop();
}
