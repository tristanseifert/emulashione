/*
 * Arithmetic - RotationsL ROL/ROR, ROXL/ROXR
 */
#include "Decoder.h"
#include "Decoder+Fmt.h"
#include "Processor.h"
#include "../Helpers.h"

#include <Logger.h>

#include <bit>

using namespace emulashione::plugin;
using namespace emulashione::m68k;
using namespace emulashione::m68k::m68000;

using namespace magic_enum::bitwise_operators;

/**
 * @brief ROL
 *
 * Rotate the value in the destination (either a register or memory) left by the specified number
 * of bits, then writes the result back. Memory rotations always rotate by one bit, and are fixed
 * at word size.
 */
void Decoder::RotateLeft(const uint16_t opcode, Processor &proc) {
    uint8_t shiftCount{0};
    Size size;

    // decode the opcode
    constexpr static const uint16_t kMemoryMask{0b1111111111000000},
              kMemoryMaskValue{0b1110011111000000};
    const bool toMemory = (opcode & kMemoryMask) == kMemoryMaskValue;
    AddressingMode addrMode;

    if(toMemory) {
        shiftCount = 1;
        size = Size::Word;

        addrMode = GetAddrMode<5,2>(opcode);
    } else {
        const bool ir = !!GetBits<5,1>(opcode);
        size = GetSize(opcode);

        // shift count is in a data register
        if(ir) {
            const auto shiftCountReg = GetRegIndex<11>(opcode);
            shiftCount = proc.regs.getDn(shiftCountReg);
        }
        // shift count is in bits 11-9
        else {
            shiftCount = GetBits<11,3>(opcode);
            if(!shiftCount) shiftCount = 8;
        }

        // what is the destination register?
        const auto Dn = GetRegIndex<2>(opcode);
        addrMode = AddressingMode::MakeDn(Dn);
    }

    shiftCount %= 64;

    // read original data and do prefetch
    const auto resolved = ResolveEffectiveAddr(proc, addrMode, size);
    const auto input = ReadEffectiveAddr(proc, resolved, size);

    if(kLogRotate) Logger::Trace("ROL: Shift {}, dest {}, {}", shiftCount, resolved, size);

    PrefetchCycle(proc);

    /*
     * Perform the operation; we have to do this per size so we can use the correct type and the
     * template for rotation is inferred correctly.
     *
     * Also update the flags; N and Z are normal, V is always cleared, and C has the last bit that
     * was rotated out.
     */
    uint32_t result;

    if(size == Size::Byte) {
        result = std::rotl<uint8_t>(input, shiftCount);
    } else if(size == Size::Word) {
        result = std::rotl<uint16_t>(input, shiftCount);
    } else {
        result = std::rotl<uint32_t>(input, shiftCount);
    }

    if(!toMemory) {
        for(size_t i = 0; i < shiftCount; i++) {
            proc.microcycleNop();
        }
    }

    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Overflow, false);

    if(shiftCount) {
        const auto bitIndex = SizeToBits(size) - (shiftCount % SizeToBits(size));
        proc.regs.setCcr(Ccr::Carry, !!(input & (1 << bitIndex)));
    } else {
        proc.regs.setCcr(Ccr::Carry, false);
    }

    // write result back
    WriteEffectiveAddr(proc, resolved, size, result);
    if(kLogLogicalShift) Logger::Trace("ROL: Read data ${:08x}, write data ${:08x}", input,result);

    if(!toMemory) {
        proc.microcycleNop();
        if(size == Size::Longword) proc.microcycleNop();
    }
}

/**
 * @brief ROR
 *
 * Same as ROL, but rotates the other way around.
 */
void Decoder::RotateRight(const uint16_t opcode, Processor &proc) {
    uint8_t shiftCount{0};
    Size size;

    // decode the opcode
    constexpr static const uint16_t kMemoryMask{0b1111111111000000},
              kMemoryMaskValue{0b1110011011000000};
    const bool toMemory = (opcode & kMemoryMask) == kMemoryMaskValue;
    AddressingMode addrMode;

    if(toMemory) {
        shiftCount = 1;
        size = Size::Word;

        addrMode = GetAddrMode<5,2>(opcode);
    } else {
        const bool ir = !!GetBits<5,1>(opcode);
        size = GetSize(opcode);

        if(ir) {
            const auto shiftCountReg = GetRegIndex<11>(opcode);
            shiftCount = proc.regs.getDn(shiftCountReg);
        }
        else {
            shiftCount = GetBits<11,3>(opcode);
            if(!shiftCount) shiftCount = 8;
        }

        const auto Dn = GetRegIndex<2>(opcode);
        addrMode = AddressingMode::MakeDn(Dn);
    }

    shiftCount %= 64;

    // read original data and do prefetch
    const auto resolved = ResolveEffectiveAddr(proc, addrMode, size);
    const auto input = ReadEffectiveAddr(proc, resolved, size);

    if(kLogRotate) Logger::Trace("ROR: Shift {}, dest {}, {}", shiftCount, resolved, size);

    PrefetchCycle(proc);

    // perform the operation; same disclaimer as for ROL applies
    uint32_t result;

    if(size == Size::Byte) {
        result = std::rotr<uint8_t>(input, shiftCount);
    } else if(size == Size::Word) {
        result = std::rotr<uint16_t>(input, shiftCount);
    } else {
        result = std::rotr<uint32_t>(input, shiftCount);
    }

    if(!toMemory) {
        for(size_t i = 0; i < shiftCount; i++) {
            proc.microcycleNop();
        }
    }

    proc.regs.setCcr(Ccr::Negative, !!(result & SignBitFor(size)));
    proc.regs.setCcr(Ccr::Zero, !(result & MaskFor(size)));
    proc.regs.setCcr(Ccr::Overflow, false);

    if(shiftCount) {
        // TODO: is this correct?
        const auto bitIndex = ((shiftCount - 1) % SizeToBits(size));
        proc.regs.setCcr(Ccr::Carry, !!(input & (1 << bitIndex)));
    } else {
        proc.regs.setCcr(Ccr::Carry, false);
    }

    // write result back
    WriteEffectiveAddr(proc, resolved, size, result);
    if(kLogLogicalShift) Logger::Trace("ROR: Read data ${:08x}, write data ${:08x}", input,result);

    if(!toMemory) {
        proc.microcycleNop();
        if(size == Size::Longword) proc.microcycleNop();
    }
}
