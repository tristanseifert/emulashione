/*
 * Definitions of trace messages used by the m68k plugin.
 */
#ifndef M68K_TRACETYPES_H
#define M68K_TRACETYPES_H

#include <algorithm>
#include <cstdint>

#include <fmt/core.h>

namespace emulashione::m68k {
/**
 * @brief Trace message ids
 */
enum TraceMessageId: uint64_t {
    /**
     * @brief The processor has been reset
     *
     * Indicates the processor has been reset. No additional payload is provided
     */
    kTraceMessageReset                  = 0x000000000001,

    /**
     * @brief An instruction has been executed
     *
     * Indicates that the processor has executed an instruction; the payload consists of the
     * register state of the processor as defined below.
     */
    kTraceMessageInstruction            = 0x000000000002,
};

/**
 * @brief Register state included with instruction traces
 */
struct TraceRegisterState {
    uint32_t d[8];
    // a7 = usp
    uint32_t a[8];
    uint32_t ssp;
    // PC shall point to the start of the instruction that was executed
    uint32_t pc;
    uint16_t status;

    // set if the processor aborted execution after this instruction
    uint16_t aborted : 1;

    /**
     * Compare the register states; this considers only registers, not flags (last bitfields) and
     * will only compare the active stack pointer.
     */
    auto operator==(const TraceRegisterState &in) const {
        // common stuff
        if(!std::equal(in.d, in.d + 8, this->d) || !std::equal(in.a, in.a + 7, this->a) ||
                (in.pc != this->pc) || (in.status != this->status)) {
            return false;
        }
        // supervisor mode
        if(this->status & 0x2000) {
            return (in.ssp == this->ssp);
        }
        // user mode
        else {
            return (in.a[7] == this->a[7]);
        }
    }
};
}

/// Formatter for trace register state
template <> struct fmt::formatter<emulashione::m68k::TraceRegisterState> {
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const emulashione::m68k::TraceRegisterState &r,
            FormatContext& ctx) -> decltype(ctx.out()) {
        auto it = ctx.out();

        for(size_t i = 0; i < 8; i++) {
            it = format_to(it, "  D{:1}: ${:08x}{}", i, r.d[i], (i == 3 || i == 7) ? "\n" : "");
        }
        for(size_t i = 0; i < 8; i++) {
            it = format_to(it, "  A{:1}: ${:08x}{}", i, r.a[i], (i == 3 || i == 7) ? "\n" : "");
        }
        it = format_to(it, " SSP: ${:08x}  PC: ${:08x}  SR: ${:04x}\n", r.ssp, r.pc,
                r.status);

        return it;
    }
};

#endif
