#ifndef M68KPLUGIN_H
#define M68KPLUGIN_H

#include <Plugin.h>

#include <string_view>

/**
 * This namespace contains the implementations of Motorola 68000 family devices, exposed by the
 * `m68k` plugin.
 *
 * \section sec_content Content
 *
 * Currently, only the 68000 processor is supported. Implementations exist in both the from-scratch
 * emulation core (M68000) or Musashi (Musashi68000) core.
 *
 * @brief Implementations of Motorola 68000 family processors
 */
namespace emulashione::m68k {
/**
 * @brief Plugin class for the 68k CPU plugin.
 */
class M68kPlugin: public emulashione::plugin::Plugin {
    public:
        /**
         * @name Lifecycle
         *
         * Initialization of the plugin and registration of devices exported by it
         *
         * @{
         */
        M68kPlugin(struct emulashione_plugin_context *ctx) : Plugin(ctx) {}

        void init() override;
        /// @}

        /**
         * @name Required plugin fields
         *
         * These fields are mandatory to identify the plugin and to build the plugin descriptor
         * that is exported and read by the core library.
         *
         * @{
         */
        constexpr static const std::string_view kDisplayName{"Motorola 68000 Processor Family"};
        constexpr static const std::string_view kShortName{"m68k"};
        constexpr static const std::string_view kAuthors{"Tristan Seifert"};
        constexpr static const std::string_view kDescription{
            "Provides cycle accurate emulation of the Motorola 68000 family of processors through "
            "various emulation backends."
        };
        constexpr static const std::string_view kInfoUrl{"https://gitlab.trist.network/mega-drive-stuff/emulashione/-/tree/main/plugins/m68k"};
        constexpr static const std::string_view kLicense{"ISC"};
        static const std::string_view kVersion;
        /// @}
};
};

#endif
