#ifndef CONFIG_CONFIG_H
#define CONFIG_CONFIG_H

#include <string_view>

#include <boost/property_tree/ptree.hpp>

#include <emulashione.h>

namespace emulashione::core {
/**
 * In-memory representation of emulator configuration; this is a simple key-value store, where each
 * key can correspond to a scalar, string, blob, or a list of any of the previous types; or, a map
 * of any of the previous types.
 *
 * @brief Global access to emulator wide configuration
 */
class Config {
    friend class ConfigReader;

    public:
        /// Returns the global config instance
        static Config *The();
        /// Deallocates the shared config instance
        static void DeallocShared();

        /// Check if the given key exists
        static bool HasKey(const std::string &key) {
            try {
                The()->storage.get<int>(key);
            } catch(const boost::property_tree::ptree_bad_path &) {
                return false;
            } catch(const boost::property_tree::ptree_error &) {}
            return true;
        }

        /// Return the config value for the given key.
        template<typename T>
        static constexpr inline T Get(const std::string &key) {
            return The()->storage.get<T>(key);
        }

        /// Return the config value for the given key.
        template<typename T>
        constexpr T value(const std::string &key) {
            return this->storage.get<T>(key);
        }

        /**
         * Store the given value under the provided property key, replacing it (and any children)
         * if it already exists.
         */
        template<typename T>
        inline void set(const std::string &key, const T &value) {
            this->storage.put<T>(key, value);
        }

        /**
         * Adds the given value under the provided property key, adding additional values as
         * children of the parent key if needed.
         */
        template<typename T>
        inline void add(const std::string &key, const T &value) {
            this->storage.add<T>(key, value);
        }

        /// Reads the configuration from disk
        void load(const std::string_view &path);
        /// Writes configuration back to disk
        void save(const std::string_view &path);

        void applyDefaults();

    private:
        /// Shared config instance
        static Config *gShared;

        /// Storage for configuration data
        boost::property_tree::ptree storage;
};
};

#endif
