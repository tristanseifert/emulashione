#include "ConfigReader.h"
#include "Config.h"

#include <iostream>

#include <stdexcept>
#include <toml++/toml.h>

#include <boost/property_tree/ptree.hpp>

using namespace emulashione::core;

/**
 * Reads the TOML file at the given path and loads all known config keys into the in memory config
 * representation.
 */
void ConfigReader::Read(Config &c, const std::string_view &path) {
    // open the file
    auto tbl = toml::parse_file(path);

    // process all keys at the root, possibly recursing as needed
    for(const auto &[key, value] : tbl) {
        ProcessKey(key, value, c.storage);
    }
}

/**
 * Processes a key that was read from the TOML file.
 *
 * For scalar keys, it is written directly into the config. If it's an array, prepare an array in
 * the config tree and then process each entry to push it into the array. If it's a map, we just
 * descend one level deeper into the hierarchy.
 *
 * @param c Configuration object to store data in
 * @param key String name of this key/value pair
 * @param value Content of this key/value pair
 * @param parent Property tree node under which this key should be added.
 * @param isArray Whether the key should be overwritten or arrayified.
 */
void ConfigReader::ProcessKey(const std::string_view &key, const toml::node &value,
        boost::property_tree::ptree &node, const bool isArray) {
    boost::property_tree::ptree child;

    // handle scalar types
    if(value.is_boolean() || value.is_number() || value.is_string()) {
            if(value.is_boolean()) {
                child.put_value(*value.value<bool>());
            }
            else if(value.is_floating_point()) {
                child.put_value(*value.value<double>());
            }
            else if(value.is_integer()) {
                child.put_value(*value.value<int64_t>());
            }
            else if(value.is_string()) {
                child.put_value(*value.value<std::string>());
            }

            // if not in an array, write it with the key
            if(!isArray) {
                node.push_back(std::make_pair(std::string(key), child));
            }
            // in arrays, do not write a key
            else {
                node.push_back(std::make_pair("", child));
            }
    }
    // if we've found an array, create a node and populate it
    else if(value.is_array()) {
        const auto arr = *value.as_array();
        for(const auto &value : arr) {
            ProcessKey("", value, child, true);
        }

        node.push_back(std::make_pair(std::string(key), child));
    }
    // if we've found a table, create a node and populate it
    else if(value.is_table()) {
        const auto tbl = *value.as_table();
        for(const auto &[key, value] : tbl) {
            ProcessKey(key, value, child);
        }

        node.push_back(std::make_pair(std::string(key), child));
    }
    // we should NEVER get here
    else {
        throw std::runtime_error("Unsupported value type");
    }
}

