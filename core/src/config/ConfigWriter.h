#ifndef CONFIG_CONFIGWRITER_H
#define CONFIG_CONFIGWRITER_H

#include <string_view>

namespace emulashione::core {
class Config;

/**
 * Handles writing the in-memory configuration to a TOML file on disk.
 *
 * @brief Support for writing configuration files back to disk
 */
class ConfigWriter {
    public:
        /**
         * Serializes the TOML configuration to the specified file.
         *
         * @note This will likely destroy any comments or other specific ordering the user has done
         * to the configuration file.
         */
        static void Write(Config &cfg, const std::string_view &path);
};
}

#endif
