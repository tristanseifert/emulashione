#include "Config.h"
#include "ConfigReader.h"
#include "ConfigWriter.h"

using namespace emulashione::core;

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
Config *Config::gShared{nullptr};

/**
 * Allocates the shared config instance, if required, then returns it.
 */
Config *Config::The() {
    // allocate if needed
    if(!gShared) {
        // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
        gShared = new Config;
    }

    return gShared;
}

/**
 * Deallocates the shared config instance.
 */
void Config::DeallocShared() {
    // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    delete gShared;
    gShared = nullptr;
}



/**
 * Applies the default configuration for any keys that have not been specified.
 */
void Config::applyDefaults() {
    if(!HasKey("log.workerThreads")) this->storage.put("log.workerThreads", 1U);
    if(!HasKey("log.queueSize")) this->storage.put("log.queueSize", 10000U);
    if(!HasKey("log.drop")) this->storage.put("log.drop", false);

    if(!HasKey("log.file.enabled")) this->storage.put("log.file.enabled", false);

    if(!HasKey("log.stdout.enabled")) {
        this->storage.put("log.stdout.enabled", true);
        if(!HasKey("log.stdout.level")) this->storage.put("log.stdout.level", 2);
        if(!HasKey("log.stdout.colorize")) this->storage.put("log.stdout.colorize", true);
    }
}



/**
 * Reads the configuration from the given file. Any existing keys are overwritten, but keys not
 * specified in the config file are not replaced.
 */
void Config::load(const std::string_view &path) {
    ConfigReader::Read(*this, path);
}

/**
 * Saves the configuration back to disk. It's serialized to TOML via the `ConfigWriter` class.
 */
void Config::save(const std::string_view &path) {
    ConfigWriter::Write(*this, path);
}
