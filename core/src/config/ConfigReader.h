#ifndef CONFIG_CONFIGREADER_H
#define CONFIG_CONFIGREADER_H

#include <string>
#include <string_view>

#include <toml++/toml.h>
#include <boost/property_tree/ptree_fwd.hpp>

namespace emulashione::core {
class Config;

namespace system {
class Parser;
}

/**
 * Handles reading out the configuration from an on-disk TOML file and loading it into the current
 * in memory configuration.
 *
 * This only supports values that are either strings, integers, floats, or booleans. Additionally,
 * these values may be nested arbitrarily in arrays or maps. (That is to say, the time, date, and
 * datetime types TOML specifies aren't supported.)
 *
 * @brief Generic TOML to boost::property_tree loader
 */
class ConfigReader {
    friend class system::Parser;

    public:
        /**
         * Read configuration file at the specified path, and load it into the in-memory
         * configuration store.
         *
         * @throws If the file can't be opened, or the TOML is malformed.
         */
        static void Read(Config &cfg, const std::string_view &path);

    private:
        static void ProcessKey(const std::string_view &, const toml::node &,
                boost::property_tree::ptree &, const bool = false);
};
}

#endif
