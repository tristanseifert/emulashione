#include "log/Logger.h"
#include "plugin/Manager.h"

#include <cstddef>
#include <span>

#include <uuid.h>

#include <emulashione.h>

using namespace emulashione::core;
using namespace emulashione::core::plugin;

emulashione_status_t libemulashione_interface_get(const void *id, const size_t idBytes,
        const struct emulashione_interface_header **outInterface) {
    if(!id || idBytes < 16 || !outInterface) {
        return kEmulashioneApiMisuse;
    }

    // i really have no excuse for this. lord pls forgive me
    const std::span<uint8_t, 16> uuidSpan{reinterpret_cast<uint8_t *>(const_cast<void *>(id)),
        std::min(16UL, idBytes)};
    const uuids::uuid uuid(uuidSpan);

    try {
        auto intf = Manager::The()->getInterface(uuid);
        if(!intf) {
            *outInterface = nullptr;
            return kEmulashioneNoSuchInterface;
        }

        *outInterface = intf->getInterfacePtr();
        return kEmulashioneSuccess;
    } catch(const std::exception &e) {
        Logger::Error("Failed to query interface: {}", e.what());
        return kEmulashioneUnknownError;
    }
}

