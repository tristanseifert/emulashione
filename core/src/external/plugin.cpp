#include <filesystem>

#include <emulashione.h>

#include "log/Logger.h"
#include "plugin/Loader.h"
#include "plugin/Manager.h"
#include "plugin/Plugin.h"

using namespace emulashione::core;
using namespace emulashione::core::plugin;

/**
 * Check that the directory exists, then invoke the plugin loader for it.
 */
emulashione_status_t libemulashione_plugin_load_dir(const char *directoryPath) {
    // check file exists
    std::filesystem::path path{directoryPath};
    if(!std::filesystem::exists(path)) {
        return kEmulashioneNonexistentFile;
    } else if(!std::filesystem::is_directory(path)) {
        return kEmulashioneUnknownError;
    }

    // invoke loader
    try {
        Loader::LoadFromDirectory(path);
    } catch(const Plugin::LoadError &e) {
        Logger::Warn("Plugin failed to load (from {}): {}", path.string(), e.what());
        return kEmulashioneInvalidPlugin;
    } catch(const std::exception &e) {
        Logger::Warn("Unhandled error while loading plugin (from {}): {}", path.string(),
                e.what());
        return kEmulashioneUnknownError;
    }

    return kEmulashioneSuccess;
}

/**
 * Check whether the path exists, then invoke the plugin loader for it.
 */
emulashione_status_t libemulashione_plugin_load_file(const char *_path) {
    // check file exists
    std::filesystem::path path{_path};
    if(!std::filesystem::exists(path)) {
        return kEmulashioneNonexistentFile;
    }

    // invoke the loader
    try {
        Loader::Load(path);
    } catch(const Manager::AlreadyLoaded &) {
        Logger::Info("Ignoring load request for '{}' -- already loaded", _path);
        return kEmulashioneSuccess;
    } catch(const Plugin::LoadError &e) {
        Logger::Warn("Plugin '{}' failed to load: {}", _path, e.what());
        return kEmulashioneInvalidPlugin;
    } catch(const std::exception &e) {
        Logger::Warn("Unhandled error while loading plugin '{}': {}", _path, e.what());
        return kEmulashioneUnknownError;
    }

    return kEmulashioneSuccess;
}

/**
 * Try to register an in-memory plugin.
 */
emulashione_status_t libemulashione_plugin_load(const struct emulashione_plugin_info *info) {
    if(!info) return kEmulashioneApiMisuse;

    try {
        Loader::Load(info);
    } catch(const Plugin::LoadError &e) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        Logger::Warn("Plugin '{}' failed to load: {}", reinterpret_cast<const void *>(info), e.what());
        return kEmulashioneInvalidPlugin;
    } catch(const std::exception &e) {
        Logger::Warn("Unhandled error while loading plugin '{}': {}",
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
                reinterpret_cast<const void *>(info), e.what());
        return kEmulashioneUnknownError;
    }

    return kEmulashioneSuccess;
}


