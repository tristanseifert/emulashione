#include <memory>

#include "device/Device.h"
#include "log/Logger.h"
#include "system/System.h"

#include <emulashione.h>

using namespace emulashione::core;
using namespace emulashione::core::system;

#ifndef DOXYGEN_SHOULD_SKIP_THIS

/**
 * A small wrapper struct that's passed to calling C code; it contains a pointer to the system
 * instance, and serves as a convenient place to add any extra metadata we might need later. Its
 * contents are totally opaque to the clients of the library.
 *
 * @brief Wrapper struct provided to plugins to identify a system
 */
struct emulashione_system {
    /// System instance we're emulating
    std::shared_ptr<System> instance;
};

#endif

/**
 * Allocates a new emulation system, and creates a wrapper for it.
 */
struct emulashione_system *libemulashione_system_alloc() {
    try {
        auto sys = std::make_shared<System>();

        // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
        auto wrapper = new emulashione_system;
        wrapper->instance = std::move(sys);

        return wrapper;
    } catch(const std::exception &e) {
        Logger::Error("{} failed: {}", __FUNCTION__, e.what());
        return nullptr;
    }
}

/**
 * Deallocates an emulation system. This deletes the wrapper, which in turn will drop the
 * reference to the system object.
 */
emulashione_status_t libemulashione_system_free(struct emulashione_system *wrapper) {
    try {
        // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
        delete wrapper;
    } catch(const std::exception &e) {
        Logger::Error("{} failed: {}", __FUNCTION__, e.what());
        return kEmulashioneUnknownError;
    }

    return kEmulashioneSuccess;
}

/**
 * Loads a system definition.
 */
emulashione_status_t libemulashione_system_load_definition(struct emulashione_system *system,
        const char *pathStr) {
    // ensure the path exists
    std::filesystem::path path(pathStr);
    if(!std::filesystem::exists(path)) {
        return kEmulashioneNonexistentFile;
    }

    // perform load
    try {
        system->instance->loadDefinition(path);
    } catch(const std::exception &e) {
        Logger::Error("Failed to load definition ({}): {}", path.string(), e.what());
        return kEmulashioneUnknownError;
    }

    return kEmulashioneSuccess;
}

/**
 * Parse an already loaded system definition.
 */
emulashione_status_t libemulashione_system_parse_definition(struct emulashione_system *system,
        const char *definition) {
    // perform load
    try {
        std::string_view sv{definition};
        system->instance->loadDefinition(sv);
    } catch(const std::exception &e) {
        Logger::Error("Failed to load definition: {}", e.what());
        return kEmulashioneUnknownError;
    }

    return kEmulashioneSuccess;
}

/**
 * Pause a system
 */
emulashione_status_t libemulashione_system_pause(struct emulashione_system *system) {
    try {
        system->instance->pause();
    } catch(const System::InvalidState &) {
        return kEmulashioneInvalidSystemState;
    } catch(const std::exception &e) {
        Logger::Error("{} failed: {}", __FUNCTION__, e.what());
        return kEmulashioneUnknownError;
    }
    return kEmulashioneSuccess;
}

/**
 * Resume a system
 */
emulashione_status_t libemulashione_system_resume(struct emulashione_system *system) {
    try {
        system->instance->resume();
    } catch(const System::InvalidState &) {
        return kEmulashioneInvalidSystemState;
    } catch(const std::exception &e) {
        Logger::Error("{} failed: {}", __FUNCTION__, e.what());
        return kEmulashioneUnknownError;
    }
    return kEmulashioneSuccess;
}

/**
 * Reset a system
 */
emulashione_status_t libemulashione_system_reset(struct emulashione_system *system,
        const bool hard) {
    try {
        system->instance->reset(hard);
    } catch(const System::InvalidState &) {
        return kEmulashioneInvalidSystemState;
    } catch(const std::exception &e) {
        Logger::Error("{} failed: {}", __FUNCTION__, e.what());
        return kEmulashioneUnknownError;
    }
    return kEmulashioneSuccess;
}

/**
 * Enters the main execution loop for the system.
 *
 * This will perform final initialization and begin emulation if this is the first time this method
 * is called on a system; it will then continue until some sort of stop event, which means either
 * the system was paused, shut down, or an error occurred.
 */
int libemulashione_system_run(struct emulashione_system *system) {
    try {
        return system->instance->run();
    } catch(const System::InvalidState &) {
        return kEmulashioneInvalidSystemState;
    } catch(const std::exception &e) {
        Logger::Error("{} failed: {}", __FUNCTION__, e.what());
        return kEmulashioneUnknownError;
    }
}

emulashione_status_t libemulashione_system_abort(struct emulashione_system *system,
        const int status) {
    try {
        // TODO: ensure the system is running
        system->instance->abortEmulation(status);
        return kEmulashioneSuccess;
    } catch(const System::InvalidState &) {
        return kEmulashioneInvalidSystemState;
    } catch(const std::exception &e) {
        Logger::Error("{} failed: {}", __FUNCTION__, e.what());
        return kEmulashioneUnknownError;
    }
}

emulashione_status_t libemulashione_system_get_device(struct emulashione_system *system,
        const char *name, struct emulashione_device_instance_header **outDevice) {
    if(!system || !name || !outDevice) return kEmulashioneApiMisuse;

    try {
        auto devPtr = system->instance->getDevice(name);
        if(!devPtr) {
            *outDevice = nullptr;
            return kEmulashioneNoSuchDevice;
        }

        *outDevice = devPtr->getInstance();
        return kEmulashioneSuccess;
    } catch(const std::exception &e) {
        Logger::Error("{} failed: {}", __FUNCTION__, e.what());
        return kEmulashioneUnknownError;
    }
}

