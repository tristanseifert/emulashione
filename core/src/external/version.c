#include <emulashione.h>

#include "misc/version.h"

/**
 * Returns the short version string, which is just the current git tag. For release builds, this
 * will be a pure SemVer string; otherwise it'll have an offset from such a version.
 */
 const char *libemulashione_version() {
    return gVERSION_SHORT;
}

/**
 * Returns a reference to the long version string.
 */
const char *libemulashione_version_long() {
    return gVERSION_LONG;
}

