#ifndef SYSTEM_SYNC_SCHEDULER_H
#define SYSTEM_SYNC_SCHEDULER_H

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <mutex>
#include <optional>
#include <string>
#include <shared_mutex>
#include <thread>
#include <unordered_map>
#include <vector>

#include <fmt/core.h>
#include <libcommunism/Cothread.h>

struct emulashione_scheduler_instance;

namespace emulashione::core::device {
class ClockSource;
class Device;
}

namespace emulashione::core::system {
class System;

/**
 * Most devices are emulated using a cooperative method, where devices execute in lock step on the
 * same physical kernel thread, and voluntarily give up control of the processor after executing an
 * operation. This class is responsible for coordinating all of these devices, to ensure that they
 * stay in sync.
 *
 * Schedulers are created as a system definition is loaded, and read their constraints (regarding
 * dependencies between devices, permissible desync, etc.) at that time.
 *
 * \section sec_times Timing
 *
 * The scheduler does not use clock ticks, nor real integral seconds as a timebase; instead, it
 * uses a scheme where 2^63 is equal to one second.
 *
 * \section sec_threads Thread Safety
 *
 * Unless specified otherwise, all methods on this class are not thread safe, and may only be
 * called from the context of the worker thread (in practice: from device implementations) once
 * emulation has been started. There are no locks or checks to enforce this, so violating this
 * assumption will result in Problems™.
 *
 * @remark Any mutating operations (adding devices, etc.) should only be performed when there are
 *         no devices executing.
 *
 * @brief Cooperative device emulation scheduler
 */
class Scheduler: public std::enable_shared_from_this<Scheduler> {
    public:
        /**
         * @brief An error indicating the device configuration of the scheduler is invalid.
         */
        class InvalidConfiguration: public std::exception {
            friend class Scheduler;

            private:
                /// Create an error with no detail string.
                InvalidConfiguration() = default;

                /// Create an error with the given format string.
                template<typename... Args>
                InvalidConfiguration(fmt::format_string<Args...> fmt, Args &&...args) {
                    this->message = fmt::format(fmt, std::forward<Args>(args)...);
                }

            public:
                const char *what() const noexcept override {
                    return this->message.c_str();
                }

            private:
                std::string message;
        };

    protected:
        /**
         * Encompasses information for a single device, required to emulate it.
         */
        struct DeviceInfo {
            /**
             * @brief Device
             */
            std::shared_ptr<device::Device> device;

            /**
             * @brief Device reference clock
             *
             * Devices interact with the scheduler in terms of ticks of some clock. This clock is
             * called the reference clock, and must be manually specified by the device during
             * initialization time.
             */
            std::shared_ptr<device::ClockSource> refclk;

            /**
             * @brief Scheduler ticks per ref clock tick
             *
             * Whenever the refclk is changed, this is updated with the scheduler ticks that
             * correspond to one tick of the clock.
             */
            uint64_t refclkTicks{0};

            /**
             * @brief Current timepoint of this device, in scheduler ticks
             *
             * Whenever this device gives up processor control, we'll update this field with the
             * corresponding number of ticks.
             */
            uint64_t time{0};

            /**
             * Cooperative thread which houses the current context of the device.
             *
             * The scheduler handles switching between these threads. At its initialization, the
             * thread will enter the coMain method of the device.
             */
            std::unique_ptr<libcommunism::Cothread> thread;
        };

        /// Worker thread states
        enum class WorkerState {
            /// Worker thread has initialized and is idle.
            Idle,
            /**
             * @brief Emulate devices by cooperatively sharing the kernel thread between them.
             *
             * When in this state, the scheduler executes all of its devices. It will only
             * periodically check whether the requested "new" state has changed.
             */
            Run,
            /**
             * @brief Emulation is paused
             *
             * All devices are being paused and will not receive further emulation until the run
             * is resumed.
             */
            Paused,
            /**
             * @brief System is shutting down
             *
             * Worker thread should clean up and exit as soon as possible.
             */
            Shutdown,
        };

    public:
        Scheduler(System *, const std::string &name);
        ~Scheduler() = default;

        void willShutDown();

        void run();
        void stop(const bool wait = false);
        void pause();
        void resume();

        struct emulashione_scheduler_instance *getInstancePtr();

        void associate(const std::shared_ptr<device::Device> &device);
        /**
         * Get the number of devices associated with this scheduler.
         */
        constexpr auto inline numDevices() const {
            return this->infoMap.size();
        }

        void setDeviceRefClock(const std::shared_ptr<device::Device> &device,
                const std::shared_ptr<device::ClockSource> &clock);
        void yield(const size_t ticks, double *outTime = nullptr);

        /**
         * Get the short name of this scheduler. This is unique only within a particular system
         * instance.
         *
         * @return Scheduler short name
         */
        constexpr auto &getName() {
            return this->name;
        }

    public:
        /**
         * Converts the given number of nanoseconds to scheduler ticks.
         *
         * @param nanos Input time value, in nanoseconds. A little under 2 seconds is the maximum
         *        value that can be represented.
         *
         * @return Number of scheduler ticks corresponding to the nanosecond value
         *
         * @remark The smallest representable value is roughly 0.10842 attoseconds; one tick.
         */
        static inline uint64_t ConvertToTicks(const double nanos) {
            return nanos * static_cast<double>(kTicksPerNs);
        }

        /**
         * Converts the given scheduler ticks to nanoseconds.
         *
         * @param ticks Input time value, in scheduler ticks.
         *
         * @return Nanoseconds corresponding to this scheduler ticks value.
         */
        static inline double ConvertToNanos(const uint64_t ticks) {
            return ticks / static_cast<double>(kTicksPerNs);
        }


    private:
        void workerMain();
        void workerStateTransition(const WorkerState);

        void scheduleNextDevice(const std::shared_ptr<device::Device> & = nullptr);

        void validateDevices();

        void deviceEntry(const size_t);

        void updateTimebaseIfNeeded();

        /**
         * Updates the worker state machine state, notifying the thread in the process. It may
         * not pick up this change immediately.
         *
         * @param newState New state of the worker state machine
         */
        void setWorkerState(const WorkerState newState) {
            {
                std::lock_guard lg(this->workerStateLock);
                this->workerState = newState;
            }
            this->workerStateCv.notify_all();
        }

    private:
        /**
         * This is the conversion ratio used to convert between nanoseconds and scheduler ticks.
         *
         * @brief Number of scheduler ticks per nanosecond
         */
        constexpr static const uint64_t kTicksPerNs{0x8000'0000'0000'0000ULL/1'000'000'000ULL};

        /**
         * @brief Threshold for timebase updates.
         *
         * When the time counter of a device advances beyond this value, the timebase counter is
         * updated to ensure an overflow doesn't occur. See updateTimebaseIfNeeded().
         */
        constexpr static const uint64_t kTickNormalizationThreshold{kTicksPerNs * 100'000'000ULL};

    private:
        /// Instance wrapper passed to plugins
        struct emulashione_scheduler_instance *instance{nullptr};
        /// Short name of this scheduler
        std::string name;

        /// The system to which the scheduler belongs
        System *system{nullptr};

        /**
         * Information records for all devices associated with this scheduler. These records hold
         * all data needed to schedule the device; see DeviceInfo.
         *
         * @remark This is in a flat array so we can simply pass an index to the device cothread.
         */
        std::vector<DeviceInfo> info;

        /**
         * @brief Index of the info slot of the currently executing device
         */
        std::optional<size_t> currentInfo;

        /**
         * Mapping of device pointer to an index in the info array.
         */
        std::unordered_map<std::shared_ptr<device::Device>, size_t> infoMap;

        /**
         * Kernel thread used to execute devices
         */
        std::unique_ptr<std::thread> worker;
        /// Cothread corresponding to the worker thread entry
        libcommunism::Cothread *workerCothread{nullptr};

        /**
         * Lock to protect the current worker state; this is so we can safely update it from other
         * threads.
         */
        std::mutex workerStateLock;
        /**
         * Condition variable to allow signaling a worker state change. We signal this whenever
         * this variable is updated.
         */
        std::condition_variable workerStateCv;
        /// Current worker thread state
        WorkerState workerState{WorkerState::Idle};
        /// Flag set while the worker thread should run
        std::atomic_bool workerRun{true};
        /// System is shutting down
        bool isShuttingDown{false};

        /// Signalled to indicate the worker has changed states
        std::condition_variable stateChangeCv;
        /// mutex for the worker state change cv
        std::mutex stateChangeLock;
        /// the state change index; this counter increments every time the state changes
        size_t currentStateIndex{0};

        /**
         * Timebase offset, in nanoseconds.
         *
         * This is applied on top of each device's fine grained (with 1/2^63 sec resolution) tick
         * counter to get its actual real execution time point.
         *
         * Periodically, when these tick counters overflow, we'll subtract from them and add it to
         * this counter.
         */
        uint64_t timebase{0};

        /**
         * Last time the timebase was updated. This is used to keep a running history of the time
         * the emulation takes compared to real time.
         */
        std::chrono::high_resolution_clock::time_point lastTimebaseUpdate;
};
}

#endif
