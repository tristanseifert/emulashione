#include "Scheduler.h"
#include "../Wrappers.h"
#include "system/System.h"

#include "device/ClockSource.h"
#include "device/Device.h"
#include "log/Logger.h"
#include "platform/platform.h"

#include <magic_enum.hpp>
#include <libcommunism/Cothread.h>

#include <algorithm>
#include <mutex>
#include <stdexcept>

using namespace emulashione::core;
using namespace emulashione::core::system;

/**
 * Initializes a new scheduler.
 *
 * @param system The system that the scheduler belongs to
 * @param name Short name for the scheduler; used to refer to it within the system
 */
Scheduler::Scheduler(System *system, const std::string &name) : name(name), system(system) {
}

/**
 * Notifies the scheduler that the system its contained in is about to shut down. At this point,
 * all devices will have already been shut down.
 *
 * We release the wrapper here to avoid a retain cycle when the reference to the scheduler is
 * dropped by the system.
 */
void Scheduler::willShutDown() {
    if(this->instance) {
        delete this->instance;
    }

    // wait for worker to shut down completely
    this->workerRun = false;
    this->setWorkerState(WorkerState::Shutdown);
    this->worker->join();

    this->infoMap.clear();
    this->info.clear();
}

/**
 * Begins an emulation loop for devices in this scheduler.
 *
 * @remark The scheduler's worker thread is lazily allocated here, if necessary.
 */
void Scheduler::run() {
    if(!this->worker) {
        this->worker = std::make_unique<std::thread>(&Scheduler::workerMain, this);
    }

    this->setWorkerState(WorkerState::Run);
}

/**
 * Indicates an emulation loop is finished.
 *
 * @param wait When set, wait for the scheduler to process this state change
 */
void Scheduler::stop(const bool wait) {
    size_t nextIdx = this->currentStateIndex + 1;

    this->setWorkerState(WorkerState::Idle);

    if(wait) {
        std::unique_lock lg(this->workerStateLock);
        this->stateChangeCv.wait(lg, [&]{
            return (nextIdx > this->currentStateIndex);
        });
    }
}

/**
 * Pauses emulation of devices associated with this scheduler.
 */
void Scheduler::pause() {
    this->setWorkerState(WorkerState::Paused);
}

/**
 * Resumes emulation of devices associated with this scheduler.
 */
void Scheduler::resume() {
    this->setWorkerState(WorkerState::Run);
}

/**
 * Allocates the scheduler instance wrapper, if needed, and returns it.
 *
 * @return Pointer to the scheduler instance, to be passed to plugins.
 */
struct emulashione_scheduler_instance *Scheduler::getInstancePtr() {
    if(!this->instance) {
        this->instance = new emulashione_scheduler_instance{this->shared_from_this()};
    }
    return this->instance;
}



/**
 * Associates the provided device with this scheduler. This means we will emulate this device, and
 * allocate the required resources (cooperative threads) for it.
 *
 * Additionally, this invokes the device's preparation methods with a handle to this scheduler.
 *
 * @param device Device instance to associate
 *
 * @throw std::runtime_error This device is already associated with a scheduler.
 */
void Scheduler::associate(const std::shared_ptr<device::Device> &device) {
    // ensure device isn't already associated (TODO: check beyond just this scheduler)
    if(this->infoMap.contains(device)) {
        throw std::runtime_error("Device already associated with scheduler");
    }

    // create the info object and allocate cothread
    DeviceInfo info{};
    info.device = device;

    const auto infoIdx = this->info.size();

    info.thread = std::make_unique<libcommunism::Cothread>(std::bind(&Scheduler::deviceEntry,
                this, infoIdx));
    info.thread->setLabel(fmt::format("scheduler '{}', device '{}'", this->name,
                device->getName()));

    // insert the info object
    this->info.emplace_back(std::move(info));
    this->infoMap.emplace(device, infoIdx);

    // invoke its handler
    device->coPrepare(this->shared_from_this());
}

/**
 * Registers a reference clock for the given device. This clock is used to convert all ticks
 * specified by the device to a concrete time value.
 *
 * @param device Device to register the reference clock for
 * @param clock Reference clock to register for the given device, or `nullptr` to remove a
 *        previously registered reference clock.
 *
 * @throw std::out_of_range Invalid device specified
 */

void Scheduler::setDeviceRefClock(const std::shared_ptr<device::Device> &device,
        const std::shared_ptr<device::ClockSource> &clock) {
    auto &info = this->info.at(this->infoMap.at(device));

    info.refclk = clock;
    if(clock) {
        info.refclkTicks = ConvertToTicks(clock->getPeriod());
    } else {
        info.refclkTicks = 0;
    }

    Logger::Trace("New refclk for device {}: {} Hz, {} ticks", device->getName(), clock ?
            clock->getFrequency() : 0, info.refclkTicks);
}


/**
 * Notifies the scheduler that the specified device has executed a specified number of ticks,. We
 * update the internal state, and switch to emulating another device, if needed, or continue
 * emulation of this device.
 *
 * The exact behavior of which device executes when, and how out of sync devices can be relative to
 * one another is defined in the system definition.
 *
 * @remark This must be called from the worker thread context while the state is run; it assumes
 *         the current device is the one indicated by currentInfo.
 *
 * @param ticks Number of ticks executed by the device (in terms of its refclk)
 * @param outTime If specified, a variable to receive the current system time (in ns) on resume
 *
 * @throw std::out_of_range Invalid device specified
 */
void Scheduler::yield(const size_t ticks, double *outTime) {
    auto &info = this->info[*this->currentInfo];

    // update its counter
    info.time += (static_cast<uint64_t>(ticks) * info.refclkTicks);

    if(info.time > kTickNormalizationThreshold) {
        this->updateTimebaseIfNeeded();
    }

    // check for exit condition
    if(this->workerState != WorkerState::Run) {
        this->workerCothread->switchTo();
    }

    // TODO: do runahead accounting

    // TODO: find next device

    // output current time
    if(outTime) {
        *outTime = this->timebase + ConvertToNanos(info.time);
    }
}



/**
 * Entry point for the worker thread's main loop.
 *
 * This forms the main loop, which waits on a condition variable that signals that the worker
 * should enter a new state; in effect, other threads are control of the state transitions of this
 * worker. When emulation begins, the cooperative threading switching begins.
 *
 * Once running devices, the scheduler only periodically checks whether a state change has been
 * requested.
 */
void Scheduler::workerMain() {
    WorkerState state{WorkerState::Idle}, newState;

    // do some thread setup
    platform::SetThreadName(fmt::format("Scheduler '{}'", this->name));

    // store current cothread (so we can jump back to it if needing to exit)
    this->workerCothread = libcommunism::Cothread::Current();

    // main processing loop
    while(this->workerRun) {
        // wait for state change
        {
            std::unique_lock lg(this->workerStateLock);
            this->workerStateCv.wait(lg, [&]{
                return (state != this->workerState);
            });
            newState = this->workerState;
        }

        // signal that we have reached a new state
        {
            std::unique_lock lg(this->stateChangeLock);
            this->currentStateIndex++;
            this->stateChangeCv.notify_all();
        }

        // perform the state transition
        Logger::Trace("Scheduler state change: {} -> {} ({})", magic_enum::enum_name(state),
                magic_enum::enum_name(newState), (uintptr_t) newState);

        try {
            this->workerStateTransition(newState);
        } catch(const std::exception &e) {
            Logger::Error("Scheduler state change to {} failed: {}",
                    magic_enum::enum_name(newState), e.what());

            this->system->abortEmulation(-2); // TODO: create an error code enum
            this->setWorkerState(WorkerState::Idle);
        }

        state = newState;
    }
}

/**
 * Performs the state transitions of the worker state machine.
 *
 * @param state State that the worker thread shall transition to
 *
 * @throw std::exception An error occurred during the state transition; we will then transition
 *        back to the idle state.
 */
void Scheduler::workerStateTransition(const WorkerState state) {
    switch(state) {
        /**
         * Select the next runnable device, and switch to it.
         */
        case WorkerState::Run:
            this->validateDevices();
            this->scheduleNextDevice();

            /*
             * When we return here, device execution has returned for some reason; so we want to
             * reset the "current device" index.
             */
            this->currentInfo.reset();
            break;

        /**
         * Clean up all resources allocated by the worker thread and exit.
         */
        case WorkerState::Shutdown:
            this->isShuttingDown = true;
            this->workerRun = false;
            break;

        /**
         * For all other state changes, there are no explicit actions to perform.
         */
        default:
            break;
    }
}

/**
 * Ensures that all devices have an assigned reference clock.
 */
void Scheduler::validateDevices() {
    for(const auto &info : this->info) {
        if(!info.refclk) {
            throw InvalidConfiguration("Missing refclk for '{}'", info.device->getName());
        }
    }
}

/**
 * Selects the next device to emulate, and switches to its context.
 *
 * This is based on which device is the furthest behind, i.e. which has the lowest tick count.
 *
 * @remark This will perform a context switch to the device that's the most behind, regardless of
 *         any allowed runahead or other configurations. However, if the device that's the most
 *         behind is the currently active device, it will not perform a switch.
 */
void Scheduler::scheduleNextDevice(const std::shared_ptr<device::Device> &current) {
    /// index of the device info struct to switch to
    size_t to{0};

    // TODO: a proper implementation lol

    // perform switch
    auto &info = this->info[to];
    if(info.device == current) return;

    this->currentInfo = to;

    info.thread->switchTo();
}

/**
 * Iterates over the tick counters for all devices, and subtracts the largest common whole
 * nanosecond value from all of them, then adds it to the timebase value.
 */
void Scheduler::updateTimebaseIfNeeded() {
    // find minimum value
    auto min = std::min_element(this->info.begin(), this->info.end(),
            [](const auto &a, const auto &b) -> bool {
        return a.time < b.time;
    });

    // subtract it from them all
    const auto nanos = min->time / kTicksPerNs;
    const auto ticks = nanos * kTicksPerNs;

    // Logger::Trace("Yeeting timestamps by {} nsec ({} ticks, timebase {} ns)", nanos, ticks, this->timebase);

    for(auto &info : this->info) {
        info.time -= ticks;
    }

    this->timebase += nanos;

    // record the last time we did this
    const auto now = std::chrono::high_resolution_clock::now();
    const auto diff = now - this->lastTimebaseUpdate;
    const auto diffNanos = std::chrono::duration_cast<std::chrono::nanoseconds>(diff).count();

    Logger::Trace("Emulated {} ns (wall time {} ns, {:3f}x)", nanos, diffNanos,
            static_cast<double>(nanos) / static_cast<double>(diffNanos));

    this->lastTimebaseUpdate = now;
}


/**
 * Entry point for a device cothread.
 *
 * @param idx Device index in the `info` array
 */
void Scheduler::deviceEntry(const size_t idx) {
    int error{0};

    {
        auto &info = this->info.at(idx);

        // invoke the device's main method
        try {
            info.device->coMain();
        } catch(const device::Device::Error &e) {
            Logger::Error("Device '{}' cothread main failed: {} ({})", info.device->getName(), e.what(),
                    e.getErrorCode());
            error = e.getErrorCode();
            goto beach;
        } catch(const std::exception &e) {
            Logger::Error("Device '{}' cothread main unhandled exception: {}",
                    info.device->getName(), e.what());
            error = -1; // no better error code available :(
            goto beach;
        }


        // if we're not shutting down, this is considered an error
        if(!this->isShuttingDown) {
            Logger::Error("Device '{}' returned from main!", info.device->getName());
            error = -1; // TODO: common error code handling
        }
    }

beach:;
    /*
     * If we get here, the device returned from its main method. This can be either because of a
     * success or failure, which really only changes whether we send an abort notify.
     *
     * Last, we have to switch back to the cothread associated with the worker kernel thread; this
     * is where the context was saved when we first invoked scheduleNextDevice() and will get us
     * back in the main state machine.
     */
    // if we get here, it was either an error condition or it returned from main
    if(error) this->system->abortEmulation(error);
    this->workerCothread->switchTo();
}
