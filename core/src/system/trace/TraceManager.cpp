#include "TraceManager.h"
#include "Output.h"
#include "../System.h"
#include "../Wrappers.h"

#include "log/Logger.h"

#include <condition_variable>
#include <type_traits>

#include <magic_enum.hpp>
#include <fmt/format.h>
#include <platform/platform.h>

using namespace emulashione::core::device;
using namespace emulashione::core::system;

/**
 * @brief Initializes a new trace manager.
 *
 * @param name Short name to identify this trace
 * @param maxPayloadSize Maximum size of trace message payloads, in bytes, or 0 to indicate that
 *        no trace messages will carry a payload.
 */
TraceManager::TraceManager(const uint16_t ns, const std::string &name,
        const size_t maxPayloadSize) : name(name), messageNamespace(ns), maxMsgLen(maxPayloadSize),
    worker(std::make_unique<std::thread>(&TraceManager::workerMain, this)) {
    // preallocate payload buffers if needed
    if(maxPayloadSize) {
        for(size_t i = 0; i < kPayloadBufPreallocate; i++) {
            auto buf = std::make_unique<PayloadBuffer>(maxPayloadSize);
            const auto success = this->payloadBuffers.enqueue(std::move(buf));
            XASSERT(success, "failed to enqueue payload buffer ({} of {})", i+1,
                    kPayloadBufPreallocate);
        }
    }
}

/**
 * Ensure the worker thread is kill.
 */
TraceManager::~TraceManager() {
    this->workerRun = false;
    this->workQueue.enqueue(Command{Command::Type::NoOp});

    this->worker->join();

    Logger::Warn("The crystals have been ignited !!");
}

/**
 * Cleans up the instance pointer, which releases the strong reference to this manager and will
 * ensure it gets shut down/deallocated.
 */
void TraceManager::willShutDown() {
    if(this->instance) {
        delete this->instance;
    }
}

/**
 * @brief Allocates the trace manager instance wrapper, if needed, and returns it.
 *
 * @return Pointer to the trace manager instance wrapper, to be passed to plugins.
 */
struct emulashione_trace_manager_instance *TraceManager::getInstancePtr() {
    if(!this->instance) {
        this->instance = new emulashione_trace_manager_instance{this->shared_from_this()};
    }
    return this->instance;
}

/**
 * Adds an output method to this trace manager.
 *
 * @remark This method should never be called once the trace manager is actively processing
 *         incoming messages.
 *
 * @param out Output to add
 */
void TraceManager::addOutput(const std::shared_ptr<trace::Output> &out) {
    this->outputs.emplace_back(out);
}

/**
 * Sends the given trace data to the trace worker.
 */
void TraceManager::putMessage(const double time, const uint64_t id,
        const std::span<std::byte> &payload) {
    TraceItem item{time, this->messageNamespace, id};

    if(!payload.empty()) {
        std::unique_ptr<PayloadBuffer> buf;
        if(!this->payloadBuffers.try_dequeue(buf)) {
            buf = std::make_unique<PayloadBuffer>(this->maxMsgLen);
        }

        buf->used = payload.size();
        std::copy(payload.begin(), payload.end(), buf->storage.begin());

        item.payload = std::move(buf);
    }

    this->workQueue.enqueue(std::move(item));
}

/**
 * Serializes execution with the worker thread; the caller will block until the worker has caught
 * up with all trace messages sent so far.
 */
void TraceManager::waitForWorker() {
    const auto expected = (this->workerSerializationCount + 1);

    this->workQueue.enqueue(Command{Command::Type::WaitFor});

    std::unique_lock lk(this->workerSerializationLock);
    this->workerSerialization.wait(lk, [&, expected]{
        return (expected <= this->workerSerializationCount);
    });
}



/**
 * @brief Entry point for the worker thread of the trace manager.
 *
 * This will loop waiting for trace data to be available, then distributes this data to all of the
 * outputs associated with the manager.
 */
void TraceManager::workerMain() {
    // set up
    platform::SetThreadName(fmt::format("TraceManager {}", this->name));

    /*
     * Continuously dequeue work items from the work queue.
     */
    moodycamel::ConsumerToken ctok(this->workQueue);
    moodycamel::ProducerToken bufTok(this->payloadBuffers);

    WorkItem item;

    while(this->workerRun) {
        this->workQueue.wait_dequeue(ctok, item);

        std::visit([&](auto&& arg) {
            using T = std::decay_t<decltype(arg)>;
            if constexpr (std::is_same_v<T, Command>) {
                this->processCommand(arg);
            } else if constexpr (std::is_same_v<T, TraceItem>) {
                this->processTraceItem(bufTok, arg);
            } else if constexpr (std::is_same_v<T, std::monostate>) {
                // nothing to be done here
            }
        }, item);
    }

    // clean up
    for(auto &out : this->outputs) {
        out->willShutDown();
    }

    std::unique_ptr<PayloadBuffer> buf;
    while(this->payloadBuffers.try_dequeue(buf)) {
        buf.reset();
    }
}

/**
 * Process a worker command.
 */
void TraceManager::processCommand(const Command &cmd) {
    switch(cmd.type) {
        case Command::Type::NoOp:
            break;

        /*
         * Signal the worker serialization variable and increment the serialization counter.
         */
        case Command::Type::WaitFor:
            {
                std::lock_guard lk(this->workerSerializationLock);
                this->workerSerializationCount++;
            }
            this->workerSerialization.notify_all();
            break;

        default:
            Logger::Warn("Unhandled command type: {}", magic_enum::enum_name(cmd.type));
            break;
    }
}

/**
 * Forwards the trace item to the output handlers.
 *
 * If the trace item has a payload buffer, at the end of the operation it's returned back to the
 * cache to be reused.
 *
 * @param token Producer token for the payload buffer queue
 * @param item Trace item to process
 */
void TraceManager::processTraceItem(moodycamel::ProducerToken &token, TraceItem &item) {
    std::span<std::byte> payload{};
    if(item.payload) {
        const auto &pbuf = item.payload;
        payload = {pbuf->storage.data(), pbuf->used};
    }

    for(auto &output : this->outputs) {
        output->handleTraceItem(item.timestamp, item.typeNamespace, item.type, payload);
    }

    // return payload buffer to queue
    if(item.payload) {
        this->payloadBuffers.enqueue(token, std::move(item.payload));
    }
}
