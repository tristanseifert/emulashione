#include "MemoryOutput.h"
#include "../Wrappers.h"

#include "log/Logger.h"

#include <algorithm>
#include <stdexcept>

using namespace emulashione::core::system::trace;

/**
 * Initialize a memory-based trace item storage.
 *
 * @throw std::invalid_argument The provided parameters are invalid
 *
 * @remark The behavior of the chunk allocation changes depending on the upper limit on the number
 *         of chunks. If the limit is specified, that number of chunks are immediately allocated
 *         during construction. Otherwise, only the first chunk is allocated, with additional
 *         chunks allocated as more storage space is needed.
 */
MemoryOutput::MemoryOutput(const size_t maxChunks, const size_t chunkSize) : maxChunks(maxChunks),
    chunkSize(chunkSize) {
    if(chunkSize < kMinimumChunkSize) {
        throw std::invalid_argument("invalid chunk size");
    }
}

/**
 * @brief Allocates the memory output instance wrapper, if needed, and returns it.
 *
 * @return Pointer to the memory outputinstance wrapper, to be passed to plugins.
 */
struct emulashione_trace_manager_mem_output *MemoryOutput::getInstancePtr() {
    if(!this->instance) {
        this->instance = new emulashione_trace_manager_mem_output{this->shared_from_this()};
    }
    return this->instance;
}

/**
 * On shutdown, release the instance pointer to avoid a retain cycle.
 */
void MemoryOutput::willShutDown() {
    if(!this->instance) {
        delete this->instance;
    }
}

/**
 * Store the received trace item in the first available storage chunk.
 *
 * If there is no chunk, or the current chunk is full, we'll allocate one to store the data into
 * if the configuration allows it.
 *
 *
 * @param timestamp System time (in nanoseconds) this event occurred
 * @param ns Namespace for type codes
 * @param id Identifier of the trace event
 * @param payload An optional payload to store with the message; it is copied directly into the
 *        chunk buffer.
 */
bool MemoryOutput::handleTraceItem(const double timestamp, const uint16_t ns, const uint64_t id,
        const std::span<std::byte> payload) {
    // bytes required to hold this record
    const size_t bytesNeeded = sizeof(Record) + payload.size();

    // find a chunk with sufficient space
    if(this->chunks.empty()) {
        this->allocateChunk();
    }

    auto &last = this->chunks.back();
    const auto bytesRemaining = this->chunkSize - last.storageUsed;

    if(bytesRemaining < bytesNeeded) {
        // no upper chunk limit or it hasn't been reached yet
        if(!this->maxChunks || this->chunks.size() < this->maxChunks) {
            this->allocateChunk();
            return this->handleTraceItem(timestamp, ns, id, payload);
        }
        // we can't expand the chunk storage so discard message
        else {
            this->numDiscarded++;
            return false;
        }
    }

    // initialize the chunk if needed
    if(!last.used) {
        last.firstTimestamp = timestamp;
        last.used = true;
    }

    // reserve the memory and write record out
    auto writePtr = last.getWritePtr();
    last.storageUsed += bytesNeeded;
    last.numRecords++;
    this->numMessages++;

    auto record = reinterpret_cast<Record *>(writePtr);
    record->time = timestamp;
    record->ns = ns;
    record->type = id;

    if(!payload.empty()) {
        record->length = sizeof(Record) + payload.size();
        std::copy(payload.begin(), payload.end(), record->payload);
    } else {
        record->length = sizeof(Record);
    }

    return true;
}

/**
 * Allocates a new chunk.
 *
 * @remark You are responsible for ensuring the allocated chunk would not bring the chunk count
 *         above the limit before making this call.
 */
void MemoryOutput::allocateChunk() {
    Chunk ch(this->chunkSize);
    this->chunks.emplace_back(std::move(ch));
}
