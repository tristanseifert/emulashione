#ifndef SYSTEM_TRACEMANAGER_H
#define SYSTEM_TRACEMANAGER_H

#include <atomic>
#include <condition_variable>
#include <cstddef>
#include <memory>
#include <mutex>
#include <span>
#include <thread>
#include <variant>
#include <vector>

#include <blockingconcurrentqueue.h>

struct emulashione_trace_manager_instance;

namespace emulashione::core::system {
namespace trace {
    class Output;
}

/**
 * @brief Mechanism to allow outputs of traces of the internal operation of various parts of the
 *        system including in plugins
 *
 * Trace managers can be instantiated by devices (busses, plugin devices, etc.) based on their
 * configuration, and will output state information for that device at regular intervals. The
 * format of the trace data is defined by each of these devices; but the persistence aspect of
 * tracing is handled by the trace manager.
 *
 * At any time, trace messages can be generated; they consist of a namespace (which is fixed at
 * initialization time,) a type code to identify the message, and a timestamp, at a minimum. The
 * message may also carry a payload, which is binary data whose interpretation is specific to that
 * namespace and type code value.
 *
 * Payload sizes are fixed at a maximum also specified during initialization. Note that while there
 * is no enforced maximum on this, there may be many thousands of buffers of this size allocated
 * for temporary storage of payloads; and output methods themselves may have upper limits on the
 * size of trace buffers.
 *
 * Resulting traces can be sent to one or more destinations, defined by trace outputs. See the
 * FileOutput and MemoryOutput classes for some existing implementations.
 *
 * @todo We should keep some sort of information on what object (bus/device/clock) created this
 *       trace manager.
 */
class TraceManager: public std::enable_shared_from_this<TraceManager> {
    public:
        TraceManager(const uint16_t ns, const std::string &name, const size_t maxPayloadSize = 0);
        ~TraceManager();

        void willShutDown();

        struct emulashione_trace_manager_instance *getInstancePtr();

        void addOutput(const std::shared_ptr<trace::Output> &out);

        void putMessage(const double time, const uint64_t id, const std::span<std::byte> &payload);
        void waitForWorker();

    protected:
        /**
         * @brief Worker thread command
         */
        struct Command {
            enum class Type {
                NoOp, WaitFor,
            };

            Type type{Type::NoOp};
        };

        /**
         * @brief Buffer for a trace message payload
         */
        struct PayloadBuffer {
            /**
             * Initializes a payload buffer and allocates the given number of bytes of initial
             * storage.
             *
             * @param size Initial size of payload buffer, in bytes
             */
            PayloadBuffer(const size_t size) {
                this->storage.resize(size);
            }

            /// used storage, in bytes
            size_t used{0};
            /// actual storage; should be `maxPayloadSize` in size
            std::vector<std::byte> storage;
        };

        /**
         * @brief Trace item to be logged
         *
         * If the item has a payload associated with it, we'll try to use an existing buffer from
         * the cache to copy it into and carry with this item, to be returned to the pool later
         * once we're done with it.
         */
        struct TraceItem {
            /// System time (in nanoseconds) of this event
            double timestamp {0};
            /// Namespace for the type code; this is typically per plugin
            uint64_t typeNamespace : 16 {0};
            /// Type code for this trace item
            uint64_t type : 48 {0};

            /// if there's a payload, the associated buffer
            std::unique_ptr<PayloadBuffer> payload{nullptr};
        };

        /**
         * @brief Represents a trace message or command to be processed
         *
         * Whenever a new trace item is received, or we want to signal the worker thread to do an
         * action, push one of these items into the work queue.
         */
        using WorkItem = std::variant<std::monostate, Command, TraceItem>;

        void workerMain();
        void processCommand(const Command &);
        void processTraceItem(moodycamel::ProducerToken &, TraceItem &);

    private:
        /// Number of payload buffers to preallocate
        constexpr static const size_t kPayloadBufPreallocate{1000};

        /// Instance wrapper passed to plugins
        struct emulashione_trace_manager_instance *instance{nullptr};
        /// Short name of this trace manager (uniquely identifies it)
        std::string name;
        /// Namespace id for trace messages
        uint16_t messageNamespace;

        /// Maximum payload size, in bytes, for trace messages
        size_t maxMsgLen;

        /**
         * @brief Payload buffers to copy payload into
         *
         * When a trace message with payload is received, the contents are copied into one of these
         * buffers so the worker thread can handle them at its leisure.
         */
        moodycamel::ConcurrentQueue<std::unique_ptr<PayloadBuffer>> payloadBuffers;

        /// Worker thread (to process requests)
        std::unique_ptr<std::thread> worker;
        /// Flag indicating the worker shall continue to process requests
        std::atomic_bool workerRun{true};

        /// lock used to serialize with worker
        std::mutex workerSerializationLock;
        /// used to synchronize with worker
        std::condition_variable workerSerialization;
        /// counter incremented by worker upon serialization
        size_t workerSerializationCount{0};

        /// Queue containing work items (trace it ems, commands, etc.)
        moodycamel::BlockingConcurrentQueue<WorkItem> workQueue;

        /// Output methods to receive trace messages
        std::vector<std::shared_ptr<trace::Output>> outputs;
};
}

#endif
