#ifndef SYSTEM_TRACE_FILEOUTPUT_H
#define SYSTEM_TRACE_FILEOUTPUT_H

#include <filesystem>

#include "Output.h"

namespace emulashione::core::system::trace {
/**
 * @brief Serializes trace data to a file
 *
 * Handles serializing trace messages and writing them to a file. The file has a common format,
 * which supports arbitrary user-defined metadata (provided during initialization; this can be
 * used to further define how data is interpreted) and then stores all of the trace messages as
 * individual records that can be accessed sequentially.
 */
class FileOutput: public Output {
    public:
        virtual ~FileOutput() = default;
};
}

#endif
