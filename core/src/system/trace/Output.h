#ifndef SYSTEM_TRACE_OUTPUT_H
#define SYSTEM_TRACE_OUTPUT_H

#include <cstddef>
#include <cstdint>
#include <memory>
#include <span>

/**
 * @brief Support for TraceManager instances
 */
namespace emulashione::core::system::trace {
/**
 * @brief Interface defining trace manager output paths
 *
 * Concrete output methods must implement this interface, which is used by the trace manager to
 * send trace data.
 */
class Output {
    public:
        virtual ~Output() = default;

        /**
         * Notifies the output that a new trace item has been received.
         *
         * @param timestamp System time (in nanoseconds) this event occurred
         * @param ns Namespace for type codes
         * @param id Identifier of the trace event
         * @param payload An optional payload to store with the message; the provided memory is not
         *        valid beyond the duration of this call.
         *
         * @return Whether the trace item was successfully handeled
         */
        virtual bool handleTraceItem(const double timestamp, const uint16_t ns, const uint64_t id,
                const std::span<std::byte> payload) = 0;

        /**
         * If the output method may drop messages (due to insufficient memory, user imposed limits
         * or other factors) it should keep a counter of these and return it here.
         *
         * This default implementation assumes messages are never dropped.
         *
         * @return Number of dropped trace items/messages
         */
        virtual size_t getDiscardedMessages() const {
            return 0;
        }

        /**
         * Notifies the output method that the trace manager is shutting down.
         */
        virtual void willShutDown() {};
};
}

#endif
