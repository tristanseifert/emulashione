#ifndef SYSTEM_TRACE_MEMORYOUTPUT_H
#define SYSTEM_TRACE_MEMORYOUTPUT_H

#include <cmath>
#include <cstddef>
#include <deque>
#include <memory>
#include <vector>

#include "Output.h"

struct emulashione_trace_manager_mem_output;

namespace emulashione::core::system::trace {
/**
 * @brief Allocates chunks of memory to store trace data in
 *
 * Trace messages are collected in memory, which is allocated as large chunks that are chained
 * together to form the full trace buffer. An upper limit on the number of these chunks can be
 * specified to limit the amount of memory used.
 *
 * Access to trace messages is later possible by iterating over the buffers. Messages prior to a
 * particular timestamp can also be discarded to reclaim memory, either returning the memory to the
 * system (if above the maximum number of chunks) or reuse queue.
 *
 * @remark If a trace message is received, and no chunks are available (because we've filled all
 *         allocated chunks, and reached the allocation limit) the message is discarded. There are
 *         counters to track the number of discarded messages.
 *
 * @remark There is no synchronization between producers (trace managers) and consumers (any
 *         clients that iterate over the message buffer) so the contents of the buffers may not be
 *         correctly read unless the device is not actively generating new trace messages. In
 *         practice, this means you should only iterate over the contents when the system is
 *         suspended.
 *
 * @remark The maximum supported payload size is a bit under 64K.
 */
class MemoryOutput: public Output, public std::enable_shared_from_this<MemoryOutput> {
    public:
        /**
         * @brief A single data record in a chunk
         *
         * This encapsulates a trace item. It is a variable length structure to accomodate the
         * inline payload data.
         */
        struct Record {
            /// Length of this entire record, in bytes, including payload
            uint16_t length{sizeof(Record)};
            double time{0};
            uint64_t ns         : 16{0};
            uint64_t type       : 48{0};

            /// If there's a payload, this is where it would be stored
            std::byte payload[];
        } __attribute__((packed));

        /**
         * @brief Fixed size container for storing records
         *
         * Chunks hold a globule of memory, which is used to store back-to-back records. Each
         * chunk also carries some metadata about the records within.
         */
        struct Chunk {
            /// Const iterator over records
            struct ConstIterator {
                friend struct Chunk;

                private:
                    /**
                     * Set up a new iterator with the given chunk. It is initialized to point at
                     * the record `n` bytes into the storage.
                     *
                     * @param chunk The chunk to iterate over
                     * @param offset Byte offset into chunk storage for the first record, in bytes
                     *
                     * @remark There's no checking to ensure that the offset is valid; the only
                     *         offset guaranteed to be valid is 0.
                     */
                    ConstIterator(const Chunk *chunk, const size_t offset = 0) : chunk(chunk),
                    i(offset) {}

                public:
                    ~ConstIterator() = default;

                    /**
                     * Increment the iterator to go the next record.
                     */
                    ConstIterator &operator++() {
                        auto rec = reinterpret_cast<const Record *>(this->chunk->storage.data() +
                                this->i);
                        this->i += rec->length;
                        return *this;
                    }

                    /**
                     * Dereference the record at the current position.
                     */
                    const Record &operator*() const {
                        return *reinterpret_cast<const Record *>(this->chunk->storage.data() +
                                this->i);
                    }

                    /**
                     * Check if this iterator equals another one.
                     */
                    bool operator !=(const ConstIterator &it) {
                        return (it.i != this->i) || (it.chunk != this->chunk);
                    }

                protected:
                    /// Storage chunk to reference
                    const Chunk *chunk{nullptr};
                    /// Current byte offset into the chunk's storage
                    size_t i{0};
            };

            /**
             * @brief Iterator for records in a chunk
             */
            struct Iterator: public ConstIterator {
                friend struct Chunk;

                private:
                    /**
                     * Set up a new iterator with the given chunk. It is initialized to point at
                     * the record `n` bytes into the storage.
                     *
                     * @param chunk The chunk to iterate over
                     * @param offset Byte offset into chunk storage for the first record, in bytes
                     *
                     * @remark There's no checking to ensure that the offset is valid; the only
                     *         offset guaranteed to be valid is 0.
                     */
                    Iterator(Chunk *chunk, const size_t offset = 0) : ConstIterator(chunk, offset),
                        nonConstChunk(chunk) {}

                public:
                    ~Iterator() = default;

                    /**
                     * Increment the iterator to go the next record.
                     */
                    Iterator &operator++() {
                        auto rec = reinterpret_cast<const Record *>(this->chunk->storage.data() +
                                this->i);
                        this->i += rec->length;
                        return *this;
                    }

                    /**
                     * Dereference the record at the current position.
                     */
                    Record &operator*() {
                        return *reinterpret_cast<Record *>(this->nonConstChunk->storage.data() +
                                this->i);
                    }

                    /**
                     * Check if this iterator equals another one.
                     */
                    bool operator !=(const Iterator &it) {
                        return (it.i != this->i) || (it.chunk != this->chunk);
                    }

                private:
                    /// If created with a non-const instance ptr, this is it
                    Chunk *nonConstChunk{nullptr};
            };

            /**
             * Allocates a chunk with the given storage size.
             *
             * @param size Size of storage region, in bytes.
             */
            Chunk(const size_t size) {
                this->storage.resize(size);
            }

            /**
             * Get the current write pointer.
             *
             * @return Byte at which the next record may be written
             *
             * @remark You must ensure that there is actually space available in the chunk before
             *         trying to write to this location.
             */
            inline std::byte *getWritePtr() {
                return (this->storage.data() + this->storageUsed);
            }

            /**
             * Get an iterator to the first record in the chunk's storage.
             */
            inline Iterator begin() {
                return Iterator(this, 0);
            }
            inline ConstIterator begin() const {
                return ConstIterator(this, 0);
            }

            /**
             * Get an iterator past the end of the storage actively used to hold records.
             */
            inline Iterator end() {
                return Iterator(this, this->storageUsed);
            }
            inline ConstIterator end() const {
                return ConstIterator(this, this->storageUsed);
            }

            /**
             * Are there no records?
             */
            inline bool empty() const {
                return !this->numRecords;
            }

            /// Indicates whether this chunk is freshly allocated or contains data
            bool used{false};

            /// Number of records in this chunk
            size_t numRecords{0};
            /// Timestamp of the first record in this chunk
            double firstTimestamp{NAN};

            /// Number of bytes of storage used
            size_t storageUsed{0};
            /// Storage for the contents of this chunk
            std::vector<std::byte> storage;
        };

    public:
        /// Default size of storage chunks (bytes)
        constexpr static const size_t kDefaultChunkSize{8388608UL};
        /// Minimum size of chunk storage
        constexpr static const size_t kMinimumChunkSize{65536UL};

        MemoryOutput(const size_t maxChunks, const size_t chunkSize = kDefaultChunkSize);
        virtual ~MemoryOutput() = default;

        bool handleTraceItem(const double timestamp, const uint16_t ns, const uint64_t id,
                const std::span<std::byte> payload) override;

        struct emulashione_trace_manager_mem_output *getInstancePtr();

        /// Return the total number of messages processed
        constexpr size_t getNumMessages() const {
            return this->numMessages;
        }

        size_t getDiscardedMessages() const override {
            return this->numDiscarded;
        }

        void willShutDown() override;

        /// Return an iterator to the start of the chunk storage
        auto inline begin() const {
            return this->chunks.begin();
        }
        /// Return an iterator to the end of the chunk storage
        auto inline end() const {
            return this->chunks.end();
        }

    private:
        void allocateChunk();

    private:
        /// instance struct corresponding to this output
        struct emulashione_trace_manager_mem_output *instance{nullptr};

        /// maximum number of chunks
        size_t maxChunks;
        /// size of a chunk, in bytes
        size_t chunkSize;
        /// number of messages discarded due to insufficient space
        size_t numDiscarded{0};
        /// number of total messages written
        size_t numMessages{0};

        /**
         * @brief Storage for chunks
         *
         * Holds a sequential list of chunks (that is, a chunk before another one is guaranteed to
         * contain records with timestamps that are all less than or equal to the timestamp of the
         * first record in the subsequent chunk) containing recorded trace messages.
         *
         * New chunks are always inserted at the end.
         */
        std::deque<Chunk> chunks;
};
}

#endif
