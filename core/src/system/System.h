#ifndef SYSTEM_SYSTEM_H
#define SYSTEM_SYSTEM_H

#include <condition_variable>
#include <filesystem>
#include <memory>
#include <mutex>
#include <stdexcept>
#include <string>
#include <string_view>
#include <unordered_map>

namespace emulashione::core {
namespace device {
class Bus;
class ClockSource;
class Device;
}


namespace system {
class Scheduler;

/**
 * Systems are the basic client accessible building blocks of the library; as the name implies,
 * each instance encapsulates all devices, and their interconnections, required to emulate a
 * particular machine.
 *
 * These devices and their connections can be read from a human-readable system description, or the
 * client library can manually build up a system.
 *
 * All systems consume some input (controllers, keyboards, etc.) and produce output (video, audio,
 * other interfaces) that the client can provide and consume via callbacks. Additionally, the
 * behavior of the emulation can be modified by changing certain properties on the system or its
 * devices.
 *
 * @remark Each type of object (bus, clock, device, scheduler) has its own namespace. Within that
 *         namespace, names must be unique. This means you can have both a bus and a clock named
 *         `main` in the same system, but not two busses or clocks with the same name. For
 *         readability of system definitions, it's suggested to not reuse the same name even in
 *         different namespaces.
 *
 * @brief Instance of a single emulated system and container for its devices
 */
class System: public std::enable_shared_from_this<System> {
    public:
        enum class State {
            Unknown,
            Initializing,
            Running,
            Suspended,
            Stopped,
        };

    public:
        /// The call cannot be completed due to the system's current state
        struct InvalidState: public std::runtime_error {
            InvalidState() : std::runtime_error("System is in an incorrect state for this call") {};
        };
        /// A name specified was invalid
        struct InvalidName: public std::runtime_error {
            InvalidName(const std::string_view &) : std::runtime_error("Invalid name") {};
        };


    public:
        ~System();

        void loadDefinition(const std::filesystem::path &definitionPath);
        void loadDefinition(const std::string_view &definition);

        void addBus(const std::string &name, const std::shared_ptr<device::Bus> &bus);
        std::shared_ptr<device::Bus> getBus(const std::string &name);

        void addClock(const std::string &name, const std::shared_ptr<device::ClockSource> &clock);
        std::shared_ptr<device::ClockSource> getClock(const std::string &name);

        void addDevice(const std::string &name, const std::shared_ptr<device::Device> &device);
        std::shared_ptr<device::Device> getDevice(const std::string &name);

        void addScheduler(const std::string &name, const std::shared_ptr<Scheduler> &scheduler);
        std::shared_ptr<Scheduler> getScheduler(const std::string &name);

        /**
         * Returns the total number of schedulers associated with this system.
         */
        constexpr auto inline numSchedulers() {
            return this->schedulers.size();
        }

        int run();
        void pause();
        void resume();
        void reset(const bool hard);

        void abortEmulation(int why);

    private:
        void invokeWillStartCallbacks();

    private:
        /// Lock to guard the busses list
        std::mutex bussesLock;
        /// Busses on the system
        std::unordered_map<std::string, std::shared_ptr<device::Bus>> busses;

        /// Lock to guard against accesses of clock sources list
        std::mutex clocksLock;
        /// All system clock sources
        std::unordered_map<std::string, std::shared_ptr<device::ClockSource>> clocks;

        /// Lock to guard against mutation of devices list
        std::mutex devicesLock;
        /// all loaded devices
        std::unordered_map<std::string, std::shared_ptr<device::Device>> devices;

        /// Lock to guard against mutation of schedulers list
        std::mutex schedulersLock;
        /// scheduler instances in the system
        std::unordered_map<std::string, std::shared_ptr<Scheduler>> schedulers;

        /// lock over state
        std::mutex stateLock;
        /// current emulation state
        State state{State::Unknown};
        /// whether the `willStartEmulation()` callbacks have executed
        std::once_flag didInitDevices;

        /// Signalled to indicate that the current run loop may be over
        std::condition_variable runEventCv;
        /// Lock protecting access to the run event state
        std::mutex runEventLock;
        /// System run state (indicates why emulation exited)
        int runEvent{0};
        /// Should emulation continue to run?
        bool runEmulation{false};
};
}
}

#endif
