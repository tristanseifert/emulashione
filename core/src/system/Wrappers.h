#ifndef SYSTEM_WRAPPERS_H
#define SYSTEM_WRAPPERS_H

#include "sync/Scheduler.h"
#include "trace/TraceManager.h"
#include "trace/MemoryOutput.h"

#include <memory>

/**
 * @brief Wrapper passed to plugins to represent a scheduler.
 */
struct emulashione_scheduler_instance {
    std::shared_ptr<emulashione::core::system::Scheduler> p;
};

/**
 * @brief Wrapper passed to plugins to represent a trace manager.
 */
struct emulashione_trace_manager_instance {
    std::shared_ptr<emulashione::core::system::TraceManager> p;
};

/**
 * @brief Wrapper passed to plugins to represent a memory based trace output.
 */
struct emulashione_trace_manager_mem_output {
    std::shared_ptr<emulashione::core::system::trace::MemoryOutput> p;
};

#endif
