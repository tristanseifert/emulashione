#include "Parser.h"
#include "../System.h"
#include "system/sync/Scheduler.h"

#include "config/ConfigReader.h"
#include "device/Bus.h"
#include "device/Device.h"
#include "device/Info.h"
#include "device/Registry.h"
#include "device/DerivedClock.h"
#include "device/PrimaryClock.h"
#include "log/Logger.h"
#include "misc/StringHelpers.h"

#include <algorithm>
#include <cstdint>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <utility>
#include <unordered_set>
#include <vector>

#include <boost/property_tree/ptree.hpp>
#include <ctre.hpp>
#include <toml++/toml.h>

using namespace emulashione::core::system;

/**
 * Read the TOML formatted system description from the provided path and apply it against the
 * system.
 *
 * @param filePath Location on the filesystem of a TOML system definition to read
 * @param destination System to apply the configuration against
 */
void Parser::Parse(const std::filesystem::path &filePath, System &destination) {
    auto tbl = toml::parse_file(filePath.string());
    Load(tbl, destination);
}

/**
 * Parse the in memory TOML document and apply the defined system configuration.
 *
 * @param definition String containing a TOML system definition
 * @param destination System to apply the configuration against
 */
void Parser::Parse(const std::string_view &definition, System &destination) {
    auto tbl = toml::parse(definition);
    Load(tbl, destination);
}

/**
 * Sets up all devices, clocks, and connections specified in the system definition loaded into
 * the given TOML table.
 */
void Parser::Load(const toml::table &table, System &destination) {
    // initialize clocks
    auto clocks = table["clocks"];
    if(!clocks || !clocks.is_array()) {
        throw LoadError("Missing or invalid clock definition");
    }

    LoadClocks(*clocks.as_array(), destination);

    // set up busses (allocating them and setting some basic configs)
    auto busses = table["busses"];
    if(!busses || !busses.is_array()) {
        throw LoadError("Missing or invalid bus definition");
    }

    InitBusses(*busses.as_array(), destination);

    // set up devices (keeping track of if any require schedulers)
    bool needsScheduler{false};

    auto devices = table["devices"];
    if(!devices || !devices.is_array()) {
        throw LoadError("Missing or invalid device definition");
    }

    InitDevices(*devices.as_array(), destination, needsScheduler);

    // establish schedulers if defined
    auto schedulers = table["schedulers"];
    if(schedulers) {
        if(!schedulers.is_array()) {
            throw LoadError("Invalid schedulers key (expected array)");
        }
        InitSchedulers(*schedulers.as_array(), destination);
    } else {
        throw LoadError("One or more devices require definition of a scheduler object, but no "
                "`schedulers` array is present.");
    }

    // TODO: validate all devices with the cothread mode have an associated scheduler

    // set up bus mappings
    for(const auto &node : *busses.as_array()) {
        const auto &info = *node.as_table();
        const auto busName = *info["name"].value<std::string>();
        auto bus = destination.getBus(busName);
        if(!bus) {
            throw LoadError("Failed to get bus '{}', what the fuck?", busName);
        }

        ParseBusMap(info, bus, destination);
        Logger::Debug("Address mapping for bus '{}': {}", busName, bus->dumpAddressMap());
    }

    // make device connections
    for(const auto &node : *devices.as_array()) {
        const auto &info = *node.as_table();
        const auto deviceName = *info["name"].value<std::string>();
        auto device = destination.getDevice(deviceName);
        if(!device) {
            throw LoadError("Failed to get device '{}', what the fuck?", deviceName);
        }

        EstablishDeviceConnections(info, device, destination);
    }
}



/**
 * Initializes clocks, based on the clocks array provided.
 *
 * Two general types of clocks can be defined: Primary clocks, which specify one or more
 * frequencies directly; or secondary clocks, which are derived from existing primary clocks by
 * means of a divisor.
 */
void Parser::LoadClocks(const toml::array &clocks, System &destination) {
    for(const auto &node : clocks) {
        if(!node.is_table()) {
            throw LoadError("Invalid clock entry (expected table)");
        }
        auto info = *node.as_table();

        // determine its type
        if(info.contains("parent")) {
            AddDerivedClock(info, destination);
        } else {
            AddPrimaryClock(info, destination);
        }
    }

}

/**
 * Instantiates a primary clock with one or more frequency settings and adds it to the system.
 */
void Parser::AddPrimaryClock(const toml::table &info, System &destination) {
    if(!info.contains("name") || !info["name"].is_string()) {
        throw LoadError("Invalid primary clock (missing or invalid `name` key)");
    } else if(!info.contains("freqs") || !info["freqs"].is_array()) {
        throw LoadError("Invalid primary clock (missing or invalid `freqs` key)");
    }

    // get its name
    auto name = *(info["name"].value<std::string>());

    // get its frequency options
    std::vector<std::pair<std::string, double>> freqs;
    for(const auto &entry : *(info["freqs"].as_array())) {
        // if it's just a number, insert it as is
        if(entry.is_floating_point()) {
            freqs.emplace_back("", *entry.value<double>());
        } else if(entry.is_integer()) {
            freqs.emplace_back("", *entry.value<int64_t>());
        }
        // in the case of a table, parse each key
        else if(entry.is_table()) {
            auto table = *entry.as_table();
            if(!table.contains("freq") || !table["freq"].is_number()) {
                throw LoadError("Invalid primary clock frequency variant (missing `freq` key)");
            }

            // the name is optional
            std::string name{""};
            if(table.contains("name")) {
                name = *(table["name"].value<std::string>());
            }

            auto freq = table["freq"];
            if(freq.is_floating_point()) {
                freqs.emplace_back(name, *freq.value<double>());
            } else if(freq.is_integer()) {
                freqs.emplace_back(name, *freq.value<int64_t>());
            } else {
                throw LoadError("Invalid primary clock frequency variant (invalid `freq` key)");
            }
        }
        // invalid type
        else {
            throw LoadError("Invalid primary clock frequency variant (expected number or table)");
        }
    }

    // create the clock
    {
        std::stringstream str;
        // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
        str << std::setprecision(10);

        for(auto it = freqs.begin(); it != freqs.end(); it++) {
            const auto &freq = *it;
            if(!freq.first.empty()) {
                str << freq.second << "Hz (" << freq.first << ")";
            } else {
                str << freq.second << "Hz";
            }

            if(it != freqs.end()-1) {
                str << ", ";
            }
        }

        Logger::Trace("Creating primary clock {}: [{}]", name, str.str());
    }

    auto clock = std::make_shared<device::PrimaryClock>(name, freqs.begin(), freqs.end());
    destination.addClock(name, std::move(clock));
}

/**
 * Instantiates a derived clock and adds it to the system.
 */
void Parser::AddDerivedClock(const toml::table &info, System &destination) {
    if(!info.contains("name") || !info["name"].is_string()) {
        throw LoadError("Invalid derived clock (missing or invalid `name` key)");
    } else if(!info.contains("parent") || !info["parent"].is_string()) {
        throw LoadError("Invalid derived clock (missing or invalid `parent` key)");
    } else if(!info.contains("divisor") || !info["divisor"].is_number()) {
        throw LoadError("Invalid derived clock (missing or invalid `divisor` key)");
    }

    // get a reference to the parent clock
    auto name = *(info["name"].value<std::string>());
    auto parentName = *(info["parent"].value<std::string>());

    auto parent = destination.getClock(parentName);
    if(!parent) {
        throw LoadError("Invalid derived clock (couldn't find parent '{}')", parentName);
    }

    // create the clock
    double divisor{0};
    if(info["divisor"].is_floating_point()) {
        divisor = *(info["divisor"]).value<double>();
    } else if(info["divisor"].is_integer()) {
        divisor = *(info["divisor"]).value<int64_t>();
    }

    auto clock = std::make_shared<device::DerivedClock>(name, parent, divisor);
    destination.addClock(name, clock);

    Logger::Trace("Creating derived clock {}, parent {}, divisor {} = {} Hz", name, parentName,
            divisor, clock->getFrequency());
}



/**
 * Allocates the named busses and loads some initial configuration. The address mappings to devices
 * are not yet performed; we wait until all devices have been allocated to do that.
 */
void Parser::InitBusses(const toml::array &busses, System &destination) {
    Logger::Debug("Initializing {} busses", busses.size());

    for(const auto &node : busses) {
        if(!node.is_table()) {
            throw LoadError("Invalid bus entry (expected table)");
        }
        auto info = *node.as_table();

        AddBus(info, destination);
    }

    (void) destination;
}

/**
 * Allocates a new bus with the provided information table.
 */
void Parser::AddBus(const toml::table &info, System &destination) {
    if(!info.contains("name") || !info["name"].is_string()) {
        throw LoadError("Invalid bus entry (missing or invalid `name` key)");
    } else if(!info.contains("dataWidth") || !info["dataWidth"].is_integer()) {
        throw LoadError("Invalid bus entry (missing or invalid `dataWidth` key)");
    } else if(!info.contains("addrWidth") || !info["addrWidth"].is_integer()) {
        throw LoadError("Invalid bus entry (missing or invalid `addrWidth` key)");
    }

    // create the bus
    const auto name = *info["name"].value<std::string>();
    const auto dataWidth = *info["dataWidth"].value<size_t>(),
          addrWidth = *info["addrWidth"].value<size_t>();

    bool abortOnUnhandledAccess{false};
    if(info.contains("abortOnUnhandledAccess") && info["abortOnUnhandledAccess"].is_boolean()) {
        abortOnUnhandledAccess = *info["abortOnUnhandledAccess"].value<bool>();
    }

    auto bus = std::make_shared<device::Bus>(name, dataWidth, addrWidth, abortOnUnhandledAccess);

    // apply configuration
    uint64_t pullUp{0}, pullDown{0};

    if(info.contains("pullup")) {
        pullUp = *info["pullup"].value<uint64_t>();
    }
    if(info.contains("pulldown")) {
        pullDown = *info["pulldown"].value<uint64_t>();
    }
    if(pullUp || pullDown) {
        bus->setPulls(pullUp, pullDown);
    }

    destination.addBus(name, bus);
}

/**
 * Parses the address mapping for this bus.
 */
void Parser::ParseBusMap(const toml::table &info, const std::shared_ptr<device::Bus> &bus,
        System &sys) {
    // bail if there's no mappings
    if(!info.contains("map")) {
        return Logger::Warn("No mappings for bus '{}'; this is highly unusual, yet valid…",
                bus->getName());
    } else if(!info["map"].is_array_of_tables()) {
        throw LoadError("Mappings for bus '{}' are invalid (expected array)", bus->getName());
    }

    // handle each entry in the map
    for(const auto &node : *info["map"].as_array()) {
        const auto &entry = *node.as_table();

        // dereference the target of the mapping
        if(!entry.contains("target") || !entry["target"].is_string()) {
            throw LoadError("Invalid bus map entry target string (expected string)");
        }
        const auto targetName = *entry["target"].value<std::string>();
        auto device = sys.getDevice(targetName);
        if(!device) {
            throw LoadError("No such device '{}' (for memory map of bus '{}')", targetName,
                    bus->getName());
        }

        // parse the range
        if(!entry.contains("range") || !entry["range"].is_array()) {
            throw LoadError("Invalid range (for memory map of bus '{}'; expected array)",
                    bus->getName());
        }
        const auto range = *entry["range"].as_array();
        if(range.size() < 2) {
            throw LoadError("Invalid range (for memory map of bus '{}'; size {}, expected {})",
                    bus->getName(), range.size(), 2);
        }

        const auto start = *range[0].value<uint64_t>(), end = *range[1].value<uint64_t>();
        bus->mapDevice(start, end, device);
    }
}



/**
 * Allocates the devices described in the system description and applies their initial
 * configuration.
 *
 * @param needsScheduler Set to `true` if any device requires the use of cooperative scheduling.
 */
void Parser::InitDevices(const toml::array &devices, System &destination, bool &needsScheduler) {
    Logger::Debug("Initializing {} devices", devices.size());

    for(const auto &node : devices) {
        if(!node.is_table()) {
            throw LoadError("Invalid device entry (expected table)");
        }
        auto info = *node.as_table();

        AddDevice(info, destination, needsScheduler);
    }
}

/**
 * Allocates the device specified in the information table. Devices are identified by the `type`
 * key, which is one or more comma separated identifiers that are looked up in the device registry.
 *
 * @param needsScheduler Set if this device uses cooperative scheduling, unchanged otherwise.
 */
void Parser::AddDevice(const toml::table &info, System &destination, bool &needsScheduler) {
    if(!info.contains("name") || !info["name"].is_string()) {
        throw LoadError("Invalid device entry (missing or invalid `name` key)");
    } else if(!info.contains("type") || !info["type"].is_string()) {
        throw LoadError("Invalid device entry (missing or invalid `width` key)");
    }

    // parse type string to determine device instance
    const auto name = *info["name"].value<std::string>();
    const auto typeStr = *info["type"].value<std::string>();

    for(auto match: ctre::range<"(.+?)(?:,\\s*|$)">(typeStr)) {
        // get the type of device
        const std::string deviceType(std::string_view{match.get<1>()});
        Logger::Trace("Trying to allocate device of type '{}' for '{}'", deviceType, name);

        // read any device options
        static const std::unordered_set<std::string> kSkippedOptionKeys{{
            "type", "name", "connections"
        }};

        boost::property_tree::ptree options;
        for(const auto &[key, value] : info) {
            // XXX: this is probably suboptimal
            if(kSkippedOptionKeys.contains(std::string(key))) continue;
            ConfigReader::ProcessKey(key, value, options);
        }

        // find device info for this device type and create it
        auto info = device::Registry::The()->getDevice(deviceType);
        if(!info) continue;

        auto instance = info->makeInstance(name, options);
        if(!instance) throw LoadError("Failed to instantiate device type '{}'", deviceType);

        if(instance->getEmulationMode() == device::Device::EmulationMode::Cothread) {
            needsScheduler = true;
        }

        destination.addDevice(name, std::move(instance));
        return;
    }

    // if we get here, the device type was not known!
    throw LoadError("No device found for type '{}'", typeStr);
}

/**
 * Connects to the device any busses, clocks, or other devices.
 *
 * This is defined by the `connections` array on the device; this is a table with the following
 * keys specified:
 * - `name`: Name of the connection slot on the device
 * - `target`: Name under which the device that is to be connected is registered under
 * - `type`: Type of connection; if not specified, defaults to `device`.
 */
void Parser::EstablishDeviceConnections(const toml::table &info,
        const std::shared_ptr<device::Device> &device, System &destination) {
    const auto deviceName = *info["name"].value<std::string>();

    // are there any connections?
    if(!info.contains("connections")) return;
    else if(!info["connections"].is_array()) {
        throw LoadError("Invalid connections list for device '{}' (expected array)", deviceName);
    }

    auto &connections = *info["connections"].as_array();
    for(const auto &node : connections) {
        // ensure we've got the right type and validate it
        if(!node.is_table()) {
            throw LoadError("Invalid connections list entry for device '{}' (expected table)",
                    deviceName);
        }
        const auto &connection = *node.as_table();

        if(!connection.contains("name") || !connection["name"].is_string()) {
            throw LoadError("Invalid or missing connections list entry key `name`");
        } else if(!connection.contains("target") || !connection["target"].is_string()) {
            throw LoadError("Invalid or missing connections list entry key `target`");
        } else if(connection.contains("type") && !connection["type"].is_string()) {
            throw LoadError("Invalid connections list entry key `type`");
        }

        // get the connection target name and type
        ConnectionType type{ConnectionType::Device};
        const auto targetName = *connection["target"].value<std::string>();

        if(connection.contains("type")) {
            auto typeStr = helpers::trimCopy(*connection["type"].value<std::string>());
            std::transform(typeStr.begin(), typeStr.end(), typeStr.begin(), ::tolower);

            if(typeStr == "bus") {
                type = ConnectionType::Bus;
            } else if(typeStr == "clock") {
                type = ConnectionType::Clock;
            } else if(typeStr == "device") {
                type = ConnectionType::Device;
            } else {
                throw LoadError("Invalid connections list entry type '{}'", typeStr);
            }
        }

        // and make the connection
        const auto slotName = *connection["name"].value<std::string>();

        switch(type) {
            case ConnectionType::Bus: {
                const auto target = destination.getBus(targetName);
                if(!target) {
                    throw LoadError("Failed to find bus '{}' to connect to slot '{}' on device '{}'",
                            targetName, slotName, deviceName);
                }
                device->addConnection(slotName, target);
                break;
            }
            case ConnectionType::Clock: {
                const auto target = destination.getClock(targetName);
                if(!target) {
                    throw LoadError("Failed to find clock '{}' to connect to slot '{}' on device '{}'",
                            targetName, slotName, deviceName);
                }
                device->addConnection(slotName, target);
                break;
            }
            case ConnectionType::Device: {
                const auto target = destination.getDevice(targetName);
                if(!target) {
                    throw LoadError("Failed to find device '{}' to connect to slot '{}' on device '{}'",
                            targetName, slotName, deviceName);
                }

                device->addConnection(slotName, target);
                break;
            }
        }
    }
}



/**
 * Iterate over all defined schedulers in the system definition and allocate Scheduler instances
 * for each of them.
 *
 * @param schedulers Array of tables of scheduler info
 * @param destination The System that these schedulers belong to.
 *
 * @throw LoadError A required key was invalid or missing
 */
void Parser::InitSchedulers(const toml::array &schedulers, System &destination) {
    for(const auto &node : schedulers) {
        if(!node.is_table()) {
            throw LoadError("Invalid scheduler entry (expected table)");
        }
        auto info = *node.as_table();

        AddScheduler(info, destination);
    }
}

/**
 * Initializes a new Scheduler instance with the given information.
 *
 * @param info Config keys for this scheduler
 * @param sys System to contain the scheduler
 *
 * @throw LoadError A required system description key is missing or invalid
 */
void Parser::AddScheduler(const toml::table &info, System &sys) {
    // get mandatory information
    if(!info.contains("name") || !info["name"].is_string()) {
        throw LoadError("Invalid scheduler entry (missing or invalid `name` key)");
    } else if(!info.contains("devices") || !info["devices"].is_array_of_tables()) {
        throw LoadError("Invalid scheduler entry (missing or invalid `devices` key)");
    }

    const auto name = *info["name"].value<std::string>();
    const auto devices = *info["devices"].as_array();

    // create the scheduler
    auto sched = std::make_shared<Scheduler>(&sys, name);

    // associate devices
    for(const auto &node : devices) {
        auto info = *node.as_table();

        const auto deviceName = *info["target"].value<std::string>();
        if(deviceName.empty()) continue;

        auto device = sys.getDevice(deviceName);
        if(!device) {
            throw LoadError("Failed to associate device to scheduler {}: no such device '{}'", name, deviceName);
        }

        AssociateDevice(info, sys, sched, device);
    }

    if(!sched->numDevices()) {
        throw LoadError("No devices specified for scheduler {}", name);
    }

    // add to system
    sys.addScheduler(name, std::move(sched));
}

/**
 * Associates a particular device with a scheduler instance.
 *
 * @remark This relationship between devices and schedulers is central to actually emulating the
 *         devices and system correctly. It's considered an error to have one device associated to
 *         multiple schedulers.
 *
 * @param info All config keys for this device association
 * @param system System that contains the device and scheduler instances
 * @param scheduler Scheduler instance the device shall be associated with
 * @param dev Device to associate with the scheduler
 */
void Parser::AssociateDevice(const toml::table &info, System &system,
        const std::shared_ptr<Scheduler> &scheduler, const std::shared_ptr<device::Device> &dev) {
    // TODO: ensure device isn't already associated
    (void) system;

    // TODO: read emulation settings
    (void) info;

    // and make the association
    scheduler->associate(dev);
}
