#ifndef SYSTEM_DEFINITIONS_PARSER
#define SYSTEM_DEFINITIONS_PARSER

#include <filesystem>
#include <memory>
#include <string>
#include <string_view>

#include <fmt/core.h>
#include <toml++/toml.h>


namespace emulashione::core::device {
class Bus;
class Device;
}

/**
 * @brief Classes related to managing entire systems and interconnections between their devices
 */
namespace emulashione::core::system {
class Scheduler;
class System;

/**
 * Reads system definitions (in TOML format; see the `data/systemdefs` directory for examples) and
 * instantiates the therein defined devices and establishes connections.
 *
 * @remark A more thorough description of the format of system definitions is [avialable here.]
 *         (https://bookstack.trist.network/books/emulashione/page/system-definitions)
 *
 * @brief System definition parser
 */
class Parser {
    private:
        enum class ConnectionType {
            Bus,
            Clock,
            Device,
        };

    public:
        /// Some sort of error during loading of system definition
        class LoadError: public std::exception {
            friend class Parser;

            private:
                /// Create a load error with no detail string.
                LoadError() = default;

                /// Create a load error with the given format string.
                template<typename... Args>
                LoadError(fmt::format_string<Args...> fmt, Args &&...args) {
                    this->message = fmt::format(fmt, std::forward<Args>(args)...);
                }

            public:
                const char *what() const noexcept override {
                    return this->message.c_str();
                }

            private:
                std::string message;
        };

    public:
        static void Parse(const std::string_view &definition, System &destination);
        static void Parse(const std::filesystem::path &filePath, System &destination);

    private:
        static void Load(const toml::table &, System &);

        static void LoadClocks(const toml::array &, System &);
        static void AddPrimaryClock(const toml::table &, System &);
        static void AddDerivedClock(const toml::table &, System &);

        static void InitBusses(const toml::array &, System &);
        static void AddBus(const toml::table &, System &);
        static void ParseBusMap(const toml::table &, const std::shared_ptr<device::Bus> &, System &);

        static void InitDevices(const toml::array &, System &, bool &);
        static void AddDevice(const toml::table &, System &, bool &);
        static void EstablishDeviceConnections(const toml::table &, const std::shared_ptr<device::Device> &,
                System &);

        static void InitSchedulers(const toml::array &, System &);
        static void AddScheduler(const toml::table &, System &);
        static void AssociateDevice(const toml::table &, System &,
                const std::shared_ptr<Scheduler> &, const std::shared_ptr<device::Device> &);
};
}

#endif
