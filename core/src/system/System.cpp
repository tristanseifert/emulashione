#include "System.h"
#include "definitions/Parser.h"
#include "sync/Scheduler.h"

#include "device/Bus.h"
#include "device/Cache.h"
#include "device/Device.h"
#include "device/DerivedClock.h"
#include "device/PrimaryClock.h"

#include "platform/platform.h"

#include "log/Logger.h"
#include "plugin/Plugin.h"
#include <memory>
#include <mutex>

using namespace emulashione::core::device;
using namespace emulashione::core::system;

/**
 * Cleans up all devices and interconnections.
 */
System::~System() {
    // invoke device shutdown methods, and then remove them from the caches
    std::unique_lock<std::mutex> l(this->devicesLock);
    for(const auto &[name, device] : this->devices) {
        Logger::Trace("Shutting down device {}", name);
        device->willShutDown();
    }
    for(const auto &[_, device] : this->devices) {
        Cache::The()->remove(device);
    }

    this->devices.clear();
    l.unlock();
    // XXX: devices used by scheduler would _still_ be alive now...

    // invoke bus shutdown methods
    std::unique_lock<std::mutex> l2(this->bussesLock);
    for(const auto &[name, bus] : this->busses) {
        bus->willShutDown();
    }
    this->busses.clear();
    l2.unlock();

    // invoke clock source shutdown methods
    std::unique_lock<std::mutex> l3(this->clocksLock);
    for(const auto &[name, clock] : this->clocks) {
        clock->willShutDown();
    }
    this->clocks.clear();
    l3.unlock();

    // kill schedulers
    std::unique_lock<std::mutex> l4(this->schedulersLock);
    for(const auto &[name, sched] : this->schedulers) {
        sched->willShutDown();
    }
    this->schedulers.clear();
    l4.unlock();
}



/**
 * Invoke the system definition parser and set up the appropriate devices.
 *
 * @param path Location of a TOML formatted system definition
 *
 * @throw toml::parse_error TOML file was malformed and could not be parsed
 * @throw system::Parser::LoadError An error occurred while loading the system description
 * @throw std::runtime_error Unspecified error setting up a device
 */
void System::loadDefinition(const std::filesystem::path &path) {
    Logger::Debug("Loading system definition from '{}'", path.string());
    system::Parser::Parse(path, *this);
}

/**
 * Invoke the system definition parser and set up the appropriate devices.
 *
 * @param definiton A string containing a fully formed TOML system definition
 *
 * @throw toml::parse_error TOML file was malformed and could not be parsed
 * @throw system::Parser::LoadError An error occurred while loading the system description
 * @throw std::runtime_error Unspecified error setting up a device
 */
void System::loadDefinition(const std::string_view &definition) {
    system::Parser::Parse(definition, *this);
}

/**
 * Adds a bus, assuming it's not a duplicate.
 *
 * @param name Identifier for this bus; must be unique
 * @param bus Bus to associate with this identifier
 *
 * @throw InvalidName If a bus with the given name has already been registered
 */
void System::addBus(const std::string &name, const std::shared_ptr<device::Bus> &bus) {
    std::lock_guard<std::mutex> lg(this->bussesLock);
    if(this->busses.contains(name)) {
        throw InvalidName(name);
    }

    bus->setSystem(this->shared_from_this());
    this->busses.emplace(name, std::move(bus));
}

/**
 * Returns an existing bus, or `nullptr` if not found.
 *
 * @param name Identifier for this bus, as passed to addBus()
 *
 * @return Pointer to the bus instance, or `nullptr` if no bus with the given identifier
 */
std::shared_ptr<Bus> System::getBus(const std::string &name) {
    std::lock_guard<std::mutex> lg(this->bussesLock);
    if(!this->busses.contains(name)) return nullptr;
    return this->busses.at(name);
}

/**
 * Adds a clock, if there's not already a clock with this identifier.
 *
 * @param name Identifier for the clock; must be unique
 * @param clock Clock to associate with this identifier
 *
 * @throw InvalidName If a clock with the given name has already been registered
 */
void System::addClock(const std::string &name,
        const std::shared_ptr<device::ClockSource> &clock) {
    std::lock_guard<std::mutex> lg(this->clocksLock);
    if(this->clocks.contains(name)) {
        throw InvalidName(name);
    }
    this->clocks.emplace(name, std::move(clock));
}

/**
 * Returns an existing clock, or `nullptr` if not found.
 *
 * @param name Identifier for this clock, as passed to addClock()
 *
 * @return Pointer to the clock instance, or `nullptr` if no clock with the given identifier
 */
std::shared_ptr<ClockSource> System::getClock(const std::string &name) {
    std::lock_guard<std::mutex> lg(this->clocksLock);
    if(!this->clocks.contains(name)) return nullptr;
    return this->clocks.at(name);
}

/**
 * Adds a device, if there's not already a device with this identifier.
 *
 * @param name Identifier for this device; must be unique
 * @param device Device to associate with this identifier
 *
 * @throw InvalidName If a device with the given name has already been registered
 */
void System::addDevice(const std::string &name,
        const std::shared_ptr<device::Device> &device) {
    std::lock_guard<std::mutex> lg(this->devicesLock);
    if(this->devices.contains(name)) {
        throw InvalidName(name);
    }

    device->setSystem(this->shared_from_this());
    this->devices.emplace(name, std::move(device));
}

/**
 * Returns an existing device based on its identifier, or `nullptr`
 *
 * @param name Identifier for the device, as passed to addDevice()
 *
 * @return Pointer to the device instance, or `nullptr` if no device with the given identifier
 */
std::shared_ptr<Device> System::getDevice(const std::string &name) {
    std::lock_guard<std::mutex> lg(this->devicesLock);
    if(!this->devices.contains(name)) return nullptr;
    return this->devices.at(name);
}

/**
 * Adds a new scheduler to the system.
 *
 * @param name Identifier for this scheduler; must be unique
 * @param sched Scheduler to associate with this identifier
 *
 * @throw InvalidName If a scheduler with the given name has already been registered
 */
void System::addScheduler(const std::string &name, const std::shared_ptr<Scheduler> &sched) {
    std::lock_guard<std::mutex> lg(this->schedulersLock);
    if(this->schedulers.contains(name)) {
        throw InvalidName(name);
    }
    this->schedulers.emplace(name, std::move(sched));
}

/**
 * Returns an existing scheduler based on its identifier, or `nullptr`
 *
 * @param name Identifier for the scheduler, as passed to addScheduler()
 *
 * @return Pointer to the scheduler instance, or `nullptr` if no scheduler with the given identifier
 */
std::shared_ptr<Scheduler> System::getScheduler(const std::string &name) {
    std::lock_guard<std::mutex> lg(this->schedulersLock);
    if(!this->schedulers.contains(name)) return nullptr;
    return this->schedulers.at(name);
}

/**
 * Enters the run loop for this system. It will begin emulation for the system, and the call will
 * not return until a stop event is reached.
 *
 * @return 0 if the emulation was paused or otherwise suspended, 1 for power off
 */
int System::run() {
    // perform one-time initialization
    std::call_once(this->didInitDevices, &System::invokeWillStartCallbacks,  this);

    // reset some state
    this->runEvent = 0;
    this->runEmulation = true;

    // update state
    {
        std::lock_guard lg(this->stateLock);
        this->state = State::Running;
    }

    // start all schedulers
    {
        std::lock_guard lg(this->schedulersLock);
        for(const auto &[id, scheduler] : this->schedulers) {
            scheduler->run();
        }
    }

    // wait for event
    std::unique_lock lg(this->runEventLock);
    this->runEventCv.wait(lg, [&]{
        return !this->runEmulation;
    });

    // stop all schedulers
    {
        std::lock_guard lg(this->schedulersLock);
        for(const auto &[id, scheduler] : this->schedulers) {
            scheduler->stop();
        }
    }

    // update state
    {
        std::lock_guard lg(this->stateLock);
        this->state = State::Stopped;
    }

    // return the state
    return this->runEvent;
}

/**
 * Stores information specified and aborts the current emulation loop.
 *
 * @param why Status code indicating why the emulation terminated
 */
void System::abortEmulation(int why) {
    platform::StackTrace t;

    Logger::Error("Aborting emulation ({}); backtrace:\n{}", why, t);

    {
        std::unique_lock lg(this->runEventLock);
        this->runEvent = why;
        this->runEmulation = false;

        // ensure all schedulers are stopped
        std::unique_lock lg2(this->schedulersLock);
        for(const auto &[name, sched] : this->schedulers) {
            sched->stop(true);
        }
    }
    this->runEventCv.notify_all();
}

/**
 * Invokes the `willStartEmulation()` callbacks.
 */
void System::invokeWillStartCallbacks() {
    std::lock_guard<std::mutex> lg(this->devicesLock);
    for(const auto &[id, device] : this->devices) {
        device->willStartEmulation();
    }
}

/**
 * Pauses system emulation.
 */
void System::pause() {
    std::lock_guard lg(this->stateLock);

    if(this->state != State::Running) throw InvalidState();
    Logger::Info("Pausing system");

    {
        std::lock_guard lg(this->schedulersLock);
        for(const auto &[id, scheduler] : this->schedulers) {
            scheduler->pause();
        }
    }
    {
        std::lock_guard lg(this->devicesLock);
        for(const auto &[id, device] : this->devices) {
            device->willPauseEmulation();
        }
    }

    this->state = State::Suspended;
}

/**
 * Resumes a previously paused system.
 */
void System::resume() {
    std::lock_guard lg(this->stateLock);

    if(this->state != State::Suspended) throw InvalidState();
    Logger::Info("Resuming system");

    {
        std::lock_guard lg(this->devicesLock);
        for(const auto &[id, device] : this->devices) {
            device->willResumeEmulation();
        }
    }
    {
        std::lock_guard lg(this->schedulersLock);
        for(const auto &[id, scheduler] : this->schedulers) {
            scheduler->resume();
        }
    }

    this->state = State::Running;
}

/**
 * Resets the system.
 *
 * @param hard Whether the reset is soft (`false`) or hard (`true`)
 */
void System::reset(const bool hard) {
    std::lock_guard lg(this->stateLock);

    if(this->state != State::Running) throw InvalidState();
    Logger::Info("Resetting system ({})", hard ? "hard" : "soft");

    std::lock_guard lg2(this->devicesLock);
    for(const auto &[id, device] : this->devices) {
        device->reset(hard);
    }
}
