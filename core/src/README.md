# Source Organization

- **config**: Configuration store, reading config from disk, and friends.
- **device**: Wrappers around plugin provided device implementations, device registry, and device matching.
- **external**: Implementations of the C wrapper functions that comprise the external API that driver applications call in to.
- **log**: Logging subsystem, based around spdlog.
- **misc**: Miscellaneous utility functions and constants, used internally.
- **platform**: Platform specific helpers, for finding paths, IO, etc. Each platform has its own directory.
- **plugin**: Plugin system, used to implement the different components that make up emulated systems. This consists of a plugin loader and registry, and some standardized interfaces exported by the core library.
    - **bridge**: Implementations of interfaces and other code to bridge between the internal library calls and the plugin APIs.
    - **interface**: Header definitions of plain C structures and callbacks that are used by plugins to communicate with the core library.
- **system**: Glue for tying together multiple devices into an emulation system.
    - **definitions**: Parser for system definition files
    - **interconnect**: Logic for connecting devices together by means of one or more busses
    - **sync**: Clock distribution and device synchronization logic
    - **trace**: Facilities to allow components to dump internal state at regular intervals as a debugging aide
