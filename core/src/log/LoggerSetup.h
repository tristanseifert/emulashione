#ifndef LOG_LOGGERSETUP_H
#define LOG_LOGGERSETUP_H

#include <spdlog/fwd.h>
#include <spdlog/common.h>

#include <memory>
#include <string>
#include <vector>

namespace emulashione::core {
/**
 * Configures the logging system based on the information read from configuration files and
 * environment variables.
 *
 * @brief Initialization for core logger and initialization of plugin loggers
 */
class LoggerSetup {
    public:
        /// Start the logging system
        static void Start();
        /// Stop the logging system
        static void Stop();

        static std::shared_ptr<spdlog::logger> MakeLogger(const std::string &identifier,
                const std::vector<spdlog::sink_ptr> &sinks);

        static spdlog::level::level_enum ConvertLevel(const int);

    private:
        static std::shared_ptr<spdlog::logger> MakeLogger(const std::string &,
                const std::vector<spdlog::sink_ptr> &, const bool, const bool);
};
}

#endif
