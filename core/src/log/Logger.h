#ifndef LOG_LOGGER_H
#define LOG_LOGGER_H

#include <memory>
#include <mutex>
#include <thread>
#include <sstream>
#include <utility>

#include <spdlog/fwd.h>
#include <spdlog/logger.h>

namespace spdlog {
void shutdown();
}

/**
 * @brief Emulashione core library
 */
namespace emulashione::core {
/**
 * Handles logging for the library. Log messages have an associated severity level, which
 * determines which "sinks" receive the message for processing.
 *
 * @brief Core library logging wrapper
 */
class Logger {
    friend class LoggerSetup;

    public:
        /// return the shared logger instance
        static Logger *The() {
            return gShared;
        }

        ~Logger();

        /// Trace level logging
        template<typename... Args>
        static inline void Trace(fmt::format_string<Args...> fmt, Args &&...args) {
            gShared->l->trace(fmt, std::forward<Args>(args)...);
        }

        /// Debug level logging
        template<typename... Args>
        static inline void Debug(fmt::format_string<Args...> fmt, Args &&...args) {
            gShared->l->debug(fmt, std::forward<Args>(args)...);
        }

        /// Info level logging
        template<typename... Args>
        static inline void Info(fmt::format_string<Args...> fmt, Args &&...args) {
            gShared->l->info(fmt, std::forward<Args>(args)...);
        }

        /// Warning level logging
        template<typename... Args>
        static inline void Warn(fmt::format_string<Args...> fmt, Args &&...args) {
            gShared->l->warn(fmt, std::forward<Args>(args)...);
        }

        /// Error level logging
        template<typename... Args>
        static inline void Error(fmt::format_string<Args...> fmt, Args &&...args) {
            gShared->l->error(fmt, std::forward<Args>(args)...);
        }

        /// Critical level logging
        template<typename... Args>
        static inline void Crit(fmt::format_string<Args...> fmt, Args &&...args) {
            gShared->l->critical(fmt, std::forward<Args>(args)...);
        }

        /**
         * Handles a failed assertion. This will log the message out, but not terminate; the
         * XASSERT() macro has a call to std::abort().
         */
        template<typename... Args>
        static bool AssertFailed(const char *expr, const char *file,
                int line, fmt::format_string<Args...> fmt, Args &&...args) {
            auto fmtMsg = fmt::format(fmt, std::forward<Args>(args)...);
            Crit("ASSERTION FAILURE ({}:{}) {} {}", file, line, expr,
                    fmtMsg);
            spdlog::shutdown();
            return true;
        }

        /// Dispense a new logger backed by the same sinks as the core logger.
        std::shared_ptr<spdlog::logger> makeLogger(const std::string &identifier);

    private:
        Logger(const std::shared_ptr<spdlog::logger> &);

    private:
        static Logger *gShared;

        // logger instance for the core module
        std::shared_ptr<spdlog::logger> l;
        // mutex protecting access to the sinks of the logger (for creating child loggers)
        std::mutex sinkLock;
};
}

#define ASSERT_HALT() (std::abort())

#define ASSERT_HANDLER(x, y, z, t, ...) (emulashione::core::Logger::AssertFailed)(x, y, z, t, ##__VA_ARGS__)
#define XASSERT(x, m, ...) (!(x) && ASSERT_HANDLER(#x, __FILE__, __LINE__, m, ##__VA_ARGS__) && (ASSERT_HALT(), 1))

/**
 * If we're building the core library, define custom formatters for a few types.
 */
#ifdef BUILDING_EMULASHIONE_CORE
#include <fmt/core.h>

#include <uuid.h>

/// Formatter for UUID type
template <> struct fmt::formatter<uuids::uuid> {
    constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
        auto it = ctx.begin(), end = ctx.end();
        if (it != end && *it != '}')
        throw format_error("invalid format");
        return it;
    }

    template <typename FormatContext>
    auto format(const uuids::uuid &u, FormatContext &ctx) -> decltype(ctx.out()) {
        return format_to(ctx.out(), "{}", uuids::to_string(u));
    }
};

/// Formatter for thread id type
template <> struct fmt::formatter<std::thread::id> {
    constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
        auto it = ctx.begin(), end = ctx.end();
        if (it != end && *it != '}')
        throw format_error("invalid format");
        return it;
    }

    template <typename FormatContext>
    auto format(const std::thread::id &id, FormatContext &ctx) -> decltype(ctx.out()) {
        std::stringstream str;
        str << id;
        return format_to(ctx.out(), "{}", str.str());
    }
};

#endif
#endif
