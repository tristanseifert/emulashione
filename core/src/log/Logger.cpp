#include "Logger.h"
#include "LoggerSetup.h"

#include <spdlog/spdlog.h>

using namespace emulashione::core;

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
Logger *Logger::gShared{nullptr};

/**
 * Initializes the logger.
 */
Logger::Logger(const std::shared_ptr<spdlog::logger> &_l) : l(_l) {
    // register the logger
    spdlog::register_logger(this->l);

    // print some info
    this->l->info("Logger has been initialized");
}

/**
 * Shut down logging resources.
 */
Logger::~Logger() {
    this->l.reset();
    spdlog::shutdown();
}

/**
 * Creates a new logger that's backed by the same sinks as the core logger, but has a different
 * identifier.
 */
std::shared_ptr<spdlog::logger> Logger::makeLogger(const std::string &identifier) {
    std::shared_ptr<spdlog::logger> logger;
    {
        std::lock_guard<std::mutex> lg(this->sinkLock);
        auto sinks = this->l->sinks();
        logger = LoggerSetup::MakeLogger(identifier, sinks);
    }
    return logger;
}

