#include "LoggerSetup.h"
#include "Logger.h"

#include "config/Config.h"

#include <mutex>
#include <spdlog/spdlog.h>
#include <spdlog/async.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include <atomic>
#include <stdexcept>
#include <string>
#include <iostream>
#include <memory>
#include <vector>

using namespace emulashione::core;

/**
 * Initializes the logger.
 */
void LoggerSetup::Start() {
    std::vector<spdlog::sink_ptr> sinks;

    // initialize the logging thread pool
    const size_t logQueueSz = Config::Get<size_t>("log.queueSize");
    const size_t logThreads = Config::Get<size_t>("log.workerThreads");

    // set up stdout logger?
    if(Config::Get<bool>("log.stdout.enabled")) {
        std::shared_ptr<spdlog::sinks::sink> p;

        if(Config::Get<bool>("log.stdout.colorize")) {
            p = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        } else {
            p = std::make_shared<spdlog::sinks::stdout_sink_mt>();
        }

        p->set_level(ConvertLevel(Config::Get<int>("log.stdout.level")));

        sinks.emplace_back(std::move(p));
    }

    // set up file logger?
    if(Config::Get<bool>("log.file.enabled")) {
        std::shared_ptr<spdlog::sinks::sink> p;

        const std::string path = Config::Get<std::string>("log.file.path");
        const size_t rotCount = Config::Get<size_t>("log.file.rotation");

        if(rotCount) {
            const size_t maxSz = Config::Get<size_t>("log.file.maxFileSize");
            const bool rotateOnOpen = Config::Get<bool>("log.file.rotateOnOpen");

            p = make_shared<spdlog::sinks::rotating_file_sink_mt>(path, maxSz, rotCount,
                    rotateOnOpen);
        } else {
            p = make_shared<spdlog::sinks::basic_file_sink_mt>(path);
        }

        p->set_level(ConvertLevel(Config::Get<int>("log.file.level")));
        sinks.emplace_back(std::move(p));
    }

    // set up thread pool (if required for async logging) create the logger
    if(logThreads) {
        if(logQueueSz <= 0) {
            throw std::invalid_argument("log.queueSize is invalid");
        }
        spdlog::init_thread_pool(logQueueSz, logThreads);
    }

    auto logger = MakeLogger("core", sinks, !!logThreads, !Config::Get<bool>("log.drop"));
    logger->set_level(spdlog::level::trace);

    // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    Logger::gShared = new Logger(std::move(logger));
}

/**
 * Shuts down the logger and releases all associated resources.
 */
void LoggerSetup::Stop() {
    // get the old logger ptr and zero it out
    auto old = Logger::gShared;
    Logger::gShared = nullptr;
    std::atomic_thread_fence(std::memory_order_release);

    // then we can actually shut it down
    // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    delete old;
}

/**
 * Creates a new logger object according to the logger configuration.
 */
std::shared_ptr<spdlog::logger> LoggerSetup::MakeLogger(const std::string &identifier,
        const std::vector<spdlog::sink_ptr> &sinks) {
    const size_t logThreads = Config::Get<size_t>("log.workerThreads");
    return MakeLogger(identifier, sinks, !!logThreads, !Config::Get<bool>("log.drop"));
}


/**
 * Converts a numerical log level value to the corresponding enum value.
 */
spdlog::level::level_enum LoggerSetup::ConvertLevel(const int in) {
    switch(in) {
        case 0:
            return spdlog::level::trace;
        case 1:
            return spdlog::level::debug;
        case 2:
            return spdlog::level::info;
        case 3:
            return spdlog::level::warn;
        case 4:
            return spdlog::level::err;
        // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
        case 5:
            return spdlog::level::critical;

        default:
            throw std::invalid_argument("Invalid log level");
    }
}

/**
 * Creates a logger object with the given identifier.
 *
 * If the logger is asynchronous, the shared spdlog thread pool will be used.
 *
 * @param identifier Unique identifier for the logger (should be module id)
 * @param sinks A list of sinks to output messages to
 * @param async Whether the logger uses a background queue for messages or logs synchronously
 * @param blocking Whether an async logger blocks if the log queue is full
 */
std::shared_ptr<spdlog::logger> LoggerSetup::MakeLogger(const std::string &identifier,
        const std::vector<spdlog::sink_ptr> &sinks, const bool async, const bool blocking) {
    std::shared_ptr<spdlog::logger> logger;

    if(async) {
        const auto policy = !blocking ? 
            spdlog::async_overflow_policy::overrun_oldest : spdlog::async_overflow_policy::block;

        logger = std::make_shared<spdlog::async_logger>(identifier, begin(sinks), end(sinks),
                spdlog::thread_pool(), policy);
    } else {
        logger = std::make_shared<spdlog::logger>(identifier, begin(sinks), end(sinks));
    }

    logger->set_level(spdlog::level::trace);
    return logger;
}

