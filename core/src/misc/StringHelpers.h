#ifndef MISC_STRINGHELPERS_H
#define MISC_STRINGHELPERS_H

#include <locale>
#include <string>

/**
 * @brief Miscellaneous helper methods
 */
namespace emulashione::core::helpers {
/**
 * Trim whitespace from the left side of the string.
 */
inline std::string &ltrim(std::string &str) {
    auto it2 = std::find_if(str.begin(), str.end(), [](char ch){
        return !std::isspace<char>(ch, std::locale::classic());
    });
    str.erase(str.begin(), it2);
    return str;
}

/**
 * Trim whitespace from the right side of the string.
 */
inline std::string &rtrim(std::string &str) {
    auto it1 = std::find_if(str.rbegin(), str.rend(), [](char ch){
        return !std::isspace<char>(ch, std::locale::classic());
    });
    str.erase(it1.base(), str.end());
    return str;
}

/**
 * Trims whitespace from both sides of a string.
 */
inline std::string & trim(std::string &str){
    return ltrim(rtrim(str));
}

/**
 * Copies the string and trims whitespace from both sides.
 */
inline std::string trimCopy(const std::string &str) {
    auto s = str;
    return ltrim(rtrim(s));
}

};

#endif
