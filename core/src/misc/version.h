#ifndef VERSION_H
#define VERSION_H

#ifdef __cplusplus
extern "C" {
#endif

// Global version strings
extern const char *const gVERSION;
extern const char *const gVERSION_SHORT;
extern const char *const gVERSION_TAG;
extern const char *const gVERSION_HASH;
extern const char *const gVERSION_BRANCH;
extern const char *const gVERSION_LONG;

#ifdef __cplusplus
}
#endif

#endif // VERSION_H
