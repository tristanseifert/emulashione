#include "Paths.h"

#include <Foundation/Foundation.h>

#include <string>

using namespace emulashione::core;

/// Fake "bundle identifier"
static NSString * const kBundleIdentifier = @"me.tseifert.emulashione";

/**
 * Returns the path to the default configuration file. This is inside the user's preferences
 * directory, inside a directory with our bundle identifier. (Normally, this is read from an
 * Info.plist, but since this is distributed as a dylib rather than a framework, we don't
 * have that luxury.)
 */
const std::string platform::GetConfigFilePath() {
    @autoreleasepool {
        // get the library directory
        NSArray<NSString *> *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,
            NSUserDomainMask, YES);
        NSCAssert(!!paths.count, @"Failed to get user library directory! (%@)", paths);

        // then build the full path
        auto path = paths[0];
        path = [path stringByAppendingPathComponent:@"Preferences"];
        path = [path stringByAppendingPathComponent:kBundleIdentifier];
        path = [path stringByAppendingPathComponent:@"config_core.toml"];

        // create a C++ string from it
        return std::string([path UTF8String],
            [path lengthOfBytesUsingEncoding:NSUTF8StringEncoding]);
    }
}
