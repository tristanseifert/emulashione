#include "Dylib.h"
#include "log/Logger.h"

#include <dlfcn.h>

using namespace emulashione::core::platform;

const std::string_view Dylib::gFileExtension{".dylib"};

/**
 * Attempts to open a shared library from the given path. Use lazy binding to speed up the process
 * of loading the object.
 */
Dylib::Dylib(const std::filesystem::path &path) {
    const auto pathStr = path.string();
    this->handle = dlopen(pathStr.c_str(), RTLD_LAZY | RTLD_LOCAL | RTLD_FIRST);

    if(!this->handle) {
        throw LoadError("dlopen({}) failed: {}", pathStr, dlerror());
    }
}

/**
 * Unloads a dynamically loaded module.
 */
Dylib::~Dylib() {
    int err = dlclose(this->handle);
    if(err) {
        Logger::Error("Failed to unload dylib ({}, {}): {}", this->handle, err, dlerror());
    }
}

/**
 * Looks up the address of the given symbol in the library. This will search _only_ the exported
 * symbols on this object, since we specified `RTLD_FIRST` during loading.
 *
 * @return Address of symbol, or `nullptr` if not found.
 */
void *Dylib::getSymbol(const std::string_view &name) {
    return dlsym(this->handle, name.data());
}

/**
 * Returns the last dynamic linker error string.
 */
const char *Dylib::getError() const {
    return dlerror();
}

