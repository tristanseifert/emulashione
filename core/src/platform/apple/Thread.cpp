#include "Thread.h"

#include <pthread.h>

using namespace emulashione::core;


void platform::SetThreadName(const std::string_view &name) {
    pthread_setname_np(name.data());
}

