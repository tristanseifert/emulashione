/*
 * This is an implementation that manually walks the stack, since `backtrace()` on macOS does some
 * bounds checks on the stack; while we could manually update these stack bounds every time that's
 * a huge pain in the ass; this is instead aware of cothreads and checks their stack bounds instead
 * of the pthread bounds if needed.
 *
 * This scheme relies on the compiler not omitting the base pointer (%rbp) when it builds function
 * call frames on the stack.
 *
 * Otherwise, this code is identical to that from Libc-1439.40.11. It _probably_ requires clang
 * to compile -- why bother with GCC on macOS, anyhow?
 */
#include "StackTrace.h"

#include "log/Logger.h"

#include <cxxabi.h>
#include <execinfo.h>
#include <pthread/stack_np.h>

#include <libcommunism/Cothread.h>

#include <array>
#include <cstdlib>
#include <regex>
#include <span>
#include <stdexcept>
#include <string_view>
#include <vector>

using namespace emulashione::core;
using namespace emulashione::core::platform;

static size_t DoBacktrace(std::span<uintptr_t>, const size_t = 1);
static std::string DemangleName(const std::string &);

void _thread_stack_pcs(uintptr_t *, unsigned, size_t *, unsigned, void *);

StackTrace::StackTrace() {
    int err;
    size_t numFrames{0};

    // capture a stack trace, up to 256 frames deep
    constexpr static const size_t kMaxFrames{256};
    std::array<uintptr_t, kMaxFrames> frames;

    err = DoBacktrace(frames, 2);

    if(!err) {
        return;
    }

    numFrames = err;
    this->frames.reserve(numFrames);

    // symbolicate the symbols
    auto strs = backtrace_symbols(reinterpret_cast<void * const *>(frames.data()), numFrames);
    if(!strs) {
        // TODO: is this really a fatal error?
        throw std::runtime_error("failed to symbolicate stack trace");
    }

    // and store the frames
    for(size_t i = 0; i < numFrames; i++) {
        Frame f{reinterpret_cast<uintptr_t>(frames[i])};

        /*
         * Split the strings that are produced by the macOS library; these follow the format of
         * something like the following: `%-4d%-35s 0x%016lx %s + %lu`, where each of the fields is
         * the frame number, image name, address, symbol name and symbol offset.
         *
         * This regex will work, assuming that symbol names never contain plus signs, and that
         * image names likewise are only numbers/characters and periods.
         *
         * Match groups are as follows:
         *
         * 1. Frame number
         * 2. Image name
         * 3. Return address
         * 4. Symbol name
         * 5. Symbol offset
         */
        static const std::regex kRegex{"^(\\d{1,4})(?:\\s*)(\\S+)(?:\\s*)([\\dA-Fa-fx]+)(?:\\s*)([^+\\s]+)(?:\\s*\\+\\s*)(\\d+)"};

        if(strs[i]) {
            std::string str{strs[i]};
            auto beg = std::sregex_iterator(str.begin(), str.end(), kRegex);
            auto end = std::sregex_iterator();

            // if no matches, bail
            if(!std::distance(beg, end)) {
                continue;
            }
            auto match = *beg;

            const auto symbolName = match[4].str();
            if(!symbolName.empty()) {
                try {
                    f.symbolName = DemangleName(symbolName);
                } catch(const std::exception &e) {
                    Logger::Debug("Failed to demangle '{}': {}", symbolName, e.what());
                    f.symbolName = symbolName;
                }
            }
            const auto symbolOffset = match[5].str();
            if(!symbolOffset.empty()) {
                f.symbolOffset = strtol(symbolOffset.c_str(), nullptr, 10);
            }

            const auto imageName = match[2].str();
            if(!imageName.empty()) {
                f.imageName = imageName;
            }

        }

        this->frames.emplace_back(std::move(f));
    }

    // clean up
    free(strs);

    this->valid = true;
}

/**
 * Attempts to demangle the specified symbol name.
 *
 * @param name Input symbol name
 * 
 * @return Demangled symbol name
 *
 * @throw std::runtime_error If an error occurred during demangling; this is likely because the
 *        provided symbol name is not a valid C++ symbol.
 */
static std::string DemangleName(const std::string &name) {
    int err{0};
    size_t size{0};

    auto actualName = abi::__cxa_demangle(name.c_str(), nullptr, &size, &err);
    if(!actualName) {
        // this means it's not a valid C++ name. likely it's a C function so return it as is
        if(err == -2) {
            return name;
        }

        throw std::runtime_error(fmt::format("__cxa_demangle failed: {}", err));
    }

    std::string str{actualName};
    free(actualName);

    return str;
}

/**
 * This is our hacked together backtrace method.
 *
 * @param buffer Storage for the resulting return addresses
 * @param skip Number of stack frames to skip
 */
static size_t DoBacktrace(std::span<uintptr_t> buffer, const size_t skip) {
    size_t numFrames{0};
    _thread_stack_pcs(buffer.data(), buffer.size(), &numFrames, skip, nullptr);
    while (numFrames >= 1 && !buffer[numFrames-1]) numFrames -= 1;
    return numFrames;
}

/*
 * Code below was shamelessly "borrowed" from Apple Libc-1439.40.11. I had a similar code in
 * kush-os but that didn't have all the same bounds checking Apple's implementation does so copying
 * that seems okay.
 *
 * This is written with the expectation of an upwards growing stack… which is the case for all
 * platforms macOS runs on. And probably also any sane machine out there.
 */
#define	INSTACK(a)	((a) >= stackbot && (a) <= stacktop)
#if defined(__x86_64__)
#define	ISALIGNED(a)	((((uintptr_t)(a)) & 0xf) == 0)
#elif defined(__i386__)
#define	ISALIGNED(a)	((((uintptr_t)(a)) & 0xf) == 8)
#elif defined(__arm__) || defined(__arm64__)
#define	ISALIGNED(a)	((((uintptr_t)(a)) & 0x1) == 0)
#endif

/// Cothread stacks larger than this are assumed to be for a real cothread
constexpr static const size_t kDummyCothreadStackThreshold{4096};

/**
 * Returns the stack size of the current thread, corresponding either to the current cothread or
 * the kernel thread.
 *
 * @note This is kind of a hack since we assume any cothread with a stack smaller than 4K is a
 *       dummy that corresponds to the main thread.
 */
static size_t GetStackSize() {
    auto coCurrent = libcommunism::Cothread::Current();
    if(coCurrent->getStackSize() > kDummyCothreadStackThreshold) {
        return coCurrent->getStackSize();
    }

    // not in a cothread, return kernel thread size
    return pthread_get_stacksize_np(pthread_self());
}

/**
 * Returns the top of the current thread's stack; this is either the currently executing cothread,
 * or the kernel thread's user stack. (Yeah, confusing…)
 *
 * The naming of this function is kind of misleading since this is actually really the highest
 * (that is, the end of the region) address of the stack.
 *
 * @note The same remark as GetStackSize() applies; this is a gigantic hack :D
 */
static const std::byte *GetStackTop() {
    auto coCurrent = libcommunism::Cothread::Current();

    if(coCurrent->getStackSize() > kDummyCothreadStackThreshold) {
        return reinterpret_cast<const std::byte *>(coCurrent->getStack()) + coCurrent->getStackSize();
    }


    // not in a cothread, return kernel thread's stack top
    return reinterpret_cast<const std::byte *>(pthread_get_stackaddr_np(pthread_self()));
}

/**
 * This actually resolves the stack frames. Most of the work is done by the (poorly undocumented)
 * pthread_stack_frame_decode_np call; THAT doesn't do any bounds checks so we can use it to do the
 * annoying platform specific stack frame walking.
 */
__attribute__((noinline))
static void __thread_stack_pcs(uintptr_t *buffer, unsigned max, size_t *nb, unsigned skip,
        void *startfp) {
    const std::byte *frame, *next;
    auto stacktop = GetStackTop();
    auto stackbot = stacktop - GetStackSize();

    *nb = 0;

    // Rely on the fact that our caller has an empty stackframe (no local vars)
    // to determine the minimum size of a stackframe (frame ptr & return addr)
    frame = reinterpret_cast<const std::byte *>(__builtin_frame_address(0));
    next = reinterpret_cast<const std::byte *>(pthread_stack_frame_decode_np(
                reinterpret_cast<uintptr_t>(frame), nullptr));

    // make sure return address is never out of bounds
    stacktop -= (next - frame);

    if(!INSTACK(frame) || !ISALIGNED(frame)) {
        return;
    }

    while (startfp || skip--) {
        if (startfp && startfp < next) break;
        if(!INSTACK(next) || !ISALIGNED(next) || next <= frame) {
            return;
        }

        frame = next;
        next = reinterpret_cast<const std::byte *>(pthread_stack_frame_decode_np(
                    reinterpret_cast<uintptr_t>(frame), nullptr));
    }

    while (max--) {
        uintptr_t retaddr;
        next = reinterpret_cast<const std::byte *>(pthread_stack_frame_decode_np(
                    reinterpret_cast<uintptr_t>(frame), &retaddr));

        buffer[*nb] = retaddr;
        (*nb)++;

        if(!INSTACK(next) || !ISALIGNED(next) || next <= frame) {
            return;
        }

        frame = next;
    }
}

// Note that callee relies on this function having a minimal stackframe
// to introspect (i.e. no tailcall and no local variables)
__attribute__((disable_tail_calls))
void _thread_stack_pcs(uintptr_t *buffer, unsigned max, size_t *nb, unsigned skip,
        void *startfp) {
    // skip this frame
    __thread_stack_pcs(buffer, max, nb, skip + 1, startfp);
}

// Prevent thread_stack_pcs() from getting tail-call-optimized into
// __thread_stack_pcs() on 64-bit environments, thus making the "number of hot
// frames to skip" be more predictable, giving more consistent backtraces.
//
// See <rdar://problem/5364825> "stack logging: frames keep getting truncated"
// for why this is necessary.
//
// Note that callee relies on this function having a minimal stackframe
// to introspect (i.e. no tailcall and no local variables)
__attribute__((disable_tail_calls))
void thread_stack_pcs(uintptr_t *buffer, unsigned max, size_t *nb) {
    __thread_stack_pcs(buffer, max, nb, 0, NULL);
}

