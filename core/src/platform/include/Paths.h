#ifndef PLATFORM_PATHS_H
#define PLATFORM_PATHS_H

#include <string_view>

namespace emulashione::core::platform {
/**
 * Returns the default configuration file path.
 */
const std::string GetConfigFilePath();
};

#endif
