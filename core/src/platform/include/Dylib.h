#ifndef PLATFORM_DYLIB_H
#define PLATFORM_DYLIB_H

#include <filesystem>
#include <string>
#include <string_view>

#include <fmt/core.h>

/**
 * @brief Platform specific code
 */
namespace emulashione::core::platform {
/**
 * @brief Wrapper around platform specific dynamic module loading
 */
class Dylib {
    public:
        /// Indicates an error occurred during object loading.
        class LoadError: public std::exception {
            friend class Dylib;

            private:
                /// Create a load error with no detail string.
                LoadError() = default;

                /// Create a load error with the given format string.
                template<typename... Args>
                LoadError(fmt::format_string<Args...> fmt, Args &&...args) {
                    this->message = fmt::format(fmt, std::forward<Args>(args)...);
                }

            public:
                const char *what() const noexcept override {
                    return this->message.c_str();
                }

            private:
                std::string message;
        };

    public:
        /// Attempt to load a library from the file at the given path
        Dylib(const std::filesystem::path &path);
        ~Dylib();

        /// Resolve the address of the given symbol
        void *getSymbol(const std::string_view &name);
        /// Gets an error string if the last `getSymbol()` call failed.
        const char *getError() const;

        /// Platform specific file extension for dylibs
        static const std::string_view gFileExtension;

    protected:
        /// For macOS/Linux: handle returned by a call to `dlopen()`
        void *handle{nullptr};
};
};

#endif

