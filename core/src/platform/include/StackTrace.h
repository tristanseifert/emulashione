#ifndef PLATFORM_STACKTRACE_H
#define PLATFORM_STACKTRACE_H

#include <cstddef>
#include <cstdint>
#include <optional>
#include <string>
#include <vector>

#include <fmt/core.h>

namespace emulashione::core::platform {
/**
 * This class implements a way for code to capture a stack trace (at the time the object is
 * constructed) and revisit it later. Stack traces are broken down into individual frames, which
 * provide an address, and optional symbol information.
 *
 * The level of detail (symbol offsets and names, as well as image information) may not be
 * available on all platforms.
 *
 * @brief Captures a stack trace and stores it for later use
 */
class StackTrace {
    public:
        /**
         * Represents a single stack frame.
         */
        struct Frame {
            uintptr_t address{0};

            /**
             * @brief String name of this symbol, if available.
             *
             * This name may or may not be unmangled, depending on the capabilities of the
             * underlying platform.
             */
            std::optional<std::string> symbolName{std::nullopt};

            /**
             * @brief Offset from the definition of the symbol specified above
             *
             * This value indicates the number of bytes the address is offset from the symbol name
             * specified above.
             *
             * @remark This information may not be available on all platforms.
             */
            std::optional<size_t> symbolOffset{std::nullopt};

            /**
             * @brief Name of the binary image that contains this symbol
             *
             * @remark This information may not be available on all platforms, and even for all
             *         stack frames. It also may not directly correspond to the filename of the
             *         image.
             */
            std::optional<std::string> imageName{std::nullopt};
        };

    public:
        /**
         * Captures a stack trace at the time of construction and stores it for later.
         */
        StackTrace();

        /// Returns the total number of stack frames
        auto numFrames() const {
            return this->frames.size();
        }
        /// Returns an interator to the start of the trace's stack frames
        auto begin() const {
            return this->frames.begin();
        }
        /// Returns an interator to the end of the trace's stack frames
        auto end() const {
            return this->frames.end();
        }

        /// Is the stack trace valid?
        constexpr auto isValid() const {
            return this->valid;
        }

    private:
        /// All captured stack frames
        std::vector<Frame> frames;
        /// Whether a stack trace was successfully captured
        bool valid{false};
};
};

/**
 * @brief Formatter for stack traces, allowing them to be printed directly.
 */
template <> struct fmt::formatter<emulashione::core::platform::StackTrace> {
    constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const emulashione::core::platform::StackTrace &s, FormatContext &ctx) -> decltype(ctx.out()) {
        if(!s.isValid()) {
            return format_to(ctx.out(), "<invalid stack trace>");
        }
        auto it = ctx.out();
        for(const auto &frame : s) {
            if(frame.symbolName && frame.symbolOffset) {
                it = format_to(it, "{:>28} ${:016x}: {} + {}\n",
                        frame.imageName.value_or("(unknown)"),
                        frame.address, *frame.symbolName, *frame.symbolOffset);
            } else {
                it = format_to(it, "{:>28} ${:016x}: {}\n", frame.imageName.value_or("(unknown)"),
                        frame.address, frame.symbolName.value_or("(unknown)"));
            }
        }
        return it;
    }
};

#endif

