#ifndef PLATFORM_THREAD_H
#define PLATFORM_THREAD_H

#include <string_view>

namespace emulashione::core::platform {
/**
 * Sets the name of the calling thread. This name is not normally user visible, and is intended to
 * be primarily a debugging aide.
 *
 * @param name New thread name
 */
void SetThreadName(const std::string_view &name);
};

#endif
