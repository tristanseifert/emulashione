/*
 * Use the standard glibc backtrace APIs to resolve a backtrace.
 */
#include "StackTrace.h"

#include "log/Logger.h"

#include <cxxabi.h>
#include <execinfo.h>

#include <array>
#include <cstddef>
#include <cstdlib>
#include <stdexcept>
#include <string>

using namespace emulashione::core;
using namespace emulashione::core::platform;

static std::string DemangleName(const std::string &);

StackTrace::StackTrace() {
    int err;
    size_t numFrames{0};

    // capture a stack trace, up to 256 frames deep
    constexpr static const size_t kMaxFrames{256};
    std::array<uintptr_t, kMaxFrames> frames;

    err = backtrace(reinterpret_cast<void **>(frames.data()), kMaxFrames);

    if(!err) {
        return;
    }

    numFrames = err;
    this->frames.reserve(numFrames);

    // symbolicate the symbols; this _may_ fail
    auto strs = backtrace_symbols(reinterpret_cast<void * const *>(frames.data()), numFrames);

    // and store the frames
    for(size_t i = 0; i < numFrames; i++) {
        Frame f{reinterpret_cast<uintptr_t>(frames[i])};

        if(strs && strs[i]) {
            // XXX: can this provide more information?
            auto symbolName = std::string(strs[i]);

            try {
                f.symbolName = DemangleName(symbolName);
            } catch(const std::exception &e) {
                Logger::Debug("Failed to demangle '{}': {}", symbolName, e.what());
                f.symbolName = symbolName;
            }
        }

        this->frames.emplace_back(std::move(f));
    }

    // clean up
    free(strs);

    this->valid = true;
}

/**
 * Attempts to demangle the specified symbol name.
 *
 * @param name Input symbol name
 * 
 * @return Demangled symbol name
 *
 * @throw std::runtime_error If an error occurred during demangling; this is likely because the
 *        provided symbol name is not a valid C++ symbol.
 */
static std::string DemangleName(const std::string &name) {
    int err{0};
    size_t size{0};

    auto actualName = abi::__cxa_demangle(name.c_str(), nullptr, &size, &err);
    if(!actualName) {
        // this means it's not a valid C++ name. likely it's a C function so return it as is
        if(err == -2) {
            return name;
        }

        throw std::runtime_error(fmt::format("__cxa_demangle failed: {}", err));
    }

    std::string str{actualName};
    free(actualName);

    return str;
}
