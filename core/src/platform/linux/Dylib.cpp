#include "Dylib.h"
#include "log/Logger.h"

#include <dlfcn.h>

using namespace emulashione::core::platform;

const std::string_view Dylib::gFileExtension{".so"};

/**
 * On some Linuxes, we don't have the `RTLD_DEEPBIND` mode. In that case, define it as zero (so
 * that code compiles, but doesn't do anything) and _hope_ it doesn't make a difference.
 */
#ifndef RTLD_DEEPBIND
#define RTLD_DEEPBIND 0
#endif

/**
 * Opens a dynamic library for a plugin.
 *
 * The `RTLD_DEEPBIND` flag specifies that any subsequent `dlsym()` calls will search first this
 * object file, rather than any other global objects.
 *
 * @param path Filesystem path to the dynamic library
 */
Dylib::Dylib(const std::filesystem::path &path) {
    const auto pathStr = path.string();
    this->handle = dlopen(pathStr.c_str(), RTLD_LAZY | RTLD_LOCAL | RTLD_DEEPBIND);

    if(!this->handle) {
        throw LoadError("dlopen({}) failed: {}", pathStr, dlerror());
    }
}

Dylib::~Dylib() {
    int err = dlclose(this->handle);
    if(err) {
        Logger::Error("Failed to unload dylib ({}, {}): {}", this->handle, err, dlerror());
    }
}

/**
 * Looks up the address of the given symbol in the library. This will search _only_ the exported
 * symbols on this object, since we specified `RTLD_FIRST` during loading.
 *
 * @return Address of symbol, or `nullptr` if not found.
 */
void *Dylib::getSymbol(const std::string_view &name) {
    return dlsym(this->handle, name.data());
}

const char *Dylib::getError() const {
    return dlerror();
}

