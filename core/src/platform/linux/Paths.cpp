#include "Paths.h"
#include "log/Logger.h"

#include <wordexp.h>
#include <string>
#include <string_view>

using namespace emulashione::core;

/**
 * Returns the path to the default configuration file. This is inside the `.config` directory in
 * the user's home directory.
 */
const std::string platform::GetConfigFilePath() {
    int err;

    // resolve the config directory
    constexpr static const std::string_view kConfigDirPath{"~/.config/emulashione/"};

    wordexp_t exp{};
    err = wordexp(kConfigDirPath.data(), &exp, 0);
    if(err) {
        Logger::Crit("wordexp() failed: {}", err);
    }

    std::string base(exp.we_wordv[0]);
    wordfree(&exp);

    // and yeet it
    return base + "/config_core.toml";
}
