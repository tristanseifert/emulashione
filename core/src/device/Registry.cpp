#include "Registry.h"
#include "Cache.h"
#include "Info.h"

#include "log/Logger.h"
#include "plugin/Plugin.h"
#include "plugin/interface/Device.h"

#include <algorithm>
#include <atomic>
#include <mutex>
#include <span>

using namespace emulashione::core::device;

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
Registry *Registry::gShared{nullptr};

/**
 * Initialize the shared device registry and the device cache.
 */
void Registry::Start() {
    XASSERT(!gShared, "cannot reinitialize device registry");
    // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    gShared = new Registry;

    Cache::Start();
}

/**
 * Deallocate the shared device registry and device cache.
 */
void Registry::Stop() {
    Cache::Stop();

    XASSERT(!!gShared, "cannot double deallocate device registry");
    auto temp = gShared;
    gShared = nullptr;
    std::atomic_thread_fence(std::memory_order_release);
    // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    delete temp;
}



/**
 * Returns a device info with the given uuid.
 *
 * @param uuid UUID of the device info to look up
 *
 * @return Info object, or `nullptr` if no such device
 */
std::shared_ptr<Info> Registry::getDevice(const uuids::uuid &uuid) {
    std::lock_guard<std::mutex> lg(this->devicesLock);

    // get the pointer pls
    if(!this->devices.contains(uuid)) return nullptr;
    auto intf = this->devices.at(uuid).lock();
    if(!intf) return nullptr;

    // TODO: ensure the plugin is still loaded (can this be done in a non-race-y way?)
    return intf;
}

/**
 * Finds the first device with a matching short name. This has to iterate the list of all devices.
 *
 * @param shortName Short name of the device
 *
 * @return Info object for the short name, or `nullptr` if no such device info
 */
std::shared_ptr<Info> Registry::getDevice(const std::string &shortName) {
    std::lock_guard<std::mutex> lg(this->devicesLock);

    for(const auto &[uuid, infoPtr] : this->devices) {
        auto device = infoPtr.lock();
        if(!device) continue;
        if(device->getShortName() == shortName) {
            return device;
        }
    }

    return nullptr;
}

/**
 * Registers a device.
 *
 * @param plug Plugin that this device resides in
 * @param device Device descriptor structure for the device
 *
 * @throw DuplicateDevice A device with the same UUID has been registered already
 * @throw std::exception Unknown error during device registration or initialization
 */
void Registry::registerDevice(const std::shared_ptr<plugin::Plugin> &plug,
        const struct emulashione_device_descriptor *device) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
    const std::span<uint8_t, 16> uuidBytes{const_cast<uint8_t *>(device->uuid), 16};
    uuids::uuid uuid{uuidBytes};

    // ensure we don't have this device already
    std::lock_guard<std::mutex> lg(this->devicesLock);
    if(this->devices.contains(uuid)) {
        Logger::Error("Duplicate device with uuid {}!", uuid);
        throw DuplicateDevice();
    }

    // create device info and register it
    auto info = std::make_shared<Info>(plug, device);
    this->devices.emplace(uuid, info);
    plug->exportedDevices.emplace(uuid, info);

    try {
        info->invokeInitializers();
    } catch(const std::exception &e) {
        Logger::Warn("Failed to initialize device {} ({}): {}",
                device->strings.shortName ? device->strings.shortName : "(invalid device)", uuid,
                e.what());
        this->devices.erase(uuid);
        plug->exportedDevices.erase(uuid);
        throw;
    }
}

/**
 * Unregisters the given plugin.
 *
 * @param uuid Identifier of the device
 * @param plug Plugin that registered the device
 */
void Registry::unregisterDevice(const uuids::uuid &uuid,
        const std::shared_ptr<plugin::Plugin> &plug) {
    // validate args
    if(uuid.is_nil() || !plug) {
        throw std::invalid_argument("Invalid uuid or plugin pointer");
    }

    // ensure it exists, then validate owner
    std::lock_guard<std::mutex> lg(this->devicesLock);
    if(!this->devices.contains(uuid)) {
        throw NoSuchDevice();
    }

    auto info = this->devices.at(uuid).lock();
    XASSERT(!!info, "device {} has already been deallocated, what the fuck?", uuid);

    auto owner = info->getPlugin();
    if(owner && owner != plug) {
        throw NotOwner();
    }

    // we can get rid of it
    this->devices.erase(uuid);
    plug->exportedDevices.erase(uuid);
}

/**
 * Unregister all devices exposed by the given plugin.
 *
 * @param plug The plugin that is about to be unloaded
 */
void Registry::pluginWillUnload(const std::shared_ptr<plugin::Plugin> &plug) {
    std::lock_guard<std::mutex> lg(this->devicesLock);

    std::erase_if(this->devices, [&](const auto &item) {
        const auto &[uuid, deviceWeak] = item;

        auto device = deviceWeak.lock();
        // XXX: should we take this opportunity to get rid of stale device registrations?
        if(!device) return false;

        if(device->isBuiltIn()) return false;
        else if(device->getPlugin() == plug) {
            Logger::Debug("Deregistering device '{}' ({}; loaded by '{}') at plugin unload time",
                    device->getShortName(), uuid, plug->getShortName());
            return true;
        }
        return false;
    });
}
