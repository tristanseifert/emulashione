#include "Device.h"
#include "Info.h"
#include "Bus.h"
#include "ClockSource.h"

#include "plugin/Manager.h"
#include "plugin/bridge/SchedulerInterface.h"
#include "plugin/interface/Device.h"

#include "system/sync/Scheduler.h"

#include "log/Logger.h"

#include <algorithm>
#include <array>
#include <cstddef>
#include <stdexcept>

using namespace emulashione::core::device;

/**
 * Allocates a new device object, with an existing instance pointer.
 *
 * @param _info Device information object (containing descriptor)
 * @param _instance An active instance; we take ownership of it and will destroy it when the
 *        destructor is invoked.
 */
Device::Device(const std::shared_ptr<Info> &_info,
        struct emulashione_device_instance_header *_instance, const std::string &_name) :
    name(_name), info(_info), descriptor(_info->getDescriptor()), devicePtr(_instance) {
    // copy some support flags
    this->supportsStrobe = this->descriptor->supported & kSupportsStrobe;
    this->supportsMemory = this->descriptor->supported & kSupportsMemory;

    switch(this->descriptor->threadingMode & kEmulationModeMask) {
        case kEmulationModePassive:
            this->mode = EmulationMode::Passive;
            break;
        case kEmulationModeTimeSlice:
            this->mode = EmulationMode::Timeslice;
            break;
        case kEmulationModeStep:
            this->mode = EmulationMode::Step;
            break;
        case kEmulationModeCoThread:
            this->mode = EmulationMode::Cothread;
            break;
        default:
            XASSERT(false, "Invalid emulation mode (threading mode ${:x})",
                    this->descriptor->threadingMode & kEmulationModeMask);
    }
}

/**
 * Invoke the device's deallocation method.
 */
Device::~Device() {
    // deallocate device
    descriptor->lifecycle.free(this->devicePtr);
}

/**
 * Query the device to see whether it supports the given interface.
 *
 * @param interfaceId UUID of the interface to query
 *
 * @return Whether the interface is supported
 */
bool Device::supportsInterface(const uuids::uuid &interfaceId) {
    const auto &uuidBytes = interfaceId.as_bytes();

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    return this->descriptor->info.supportsInterface(this->devicePtr,
            reinterpret_cast<const uint8_t *>(uuidBytes.data()), uuidBytes.size());
}

/**
 * Emulation is about to begin.
 */
void Device::willStartEmulation() {
    int err{0};
    auto cb = this->descriptor->emulation.willStartEmulation;
    if(cb && (err = cb(this->devicePtr))) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}
/**
 * Emulation is about to be halted.
 */
void Device::willStopEmulation() {
    int err{0};
    auto cb = this->descriptor->emulation.willStopEmulation;
    if(cb && (err = cb(this->devicePtr))) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * Emulation is about to be paused.
 */
void Device::willPauseEmulation() {
    int err{0};
    auto cb = this->descriptor->emulation.willPauseEmulation;
    if(cb && (err = cb(this->devicePtr))) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * Previously paused system is about to be resumed.
 */
void Device::willResumeEmulation() {
    int err{0};
    auto cb = this->descriptor->emulation.willResumeEmulation;
    if(cb && (err = cb(this->devicePtr))) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * The system is shutting down.
 */
void Device::willShutDown() {
    int err{0};
    auto cb = this->descriptor->emulation.willShutDown;
    if(cb && (err = cb(this->devicePtr))) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}
/**
 * Resets the device's internal state.
 */
void Device::reset(const bool hard) {
    int err{0};
    auto cb = this->descriptor->emulation.reset;
    if(cb && (err = cb(this->devicePtr, hard))) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * Commits the device state up to the given time point.
 */
void Device::commitTo(const double time) {
    int err{0};
    auto cb = this->descriptor->emulation.commitTo;
    if(cb && (err = cb(this->devicePtr, time))) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}



/**
 * A new time slice is about to be executed.
 *
 * @param currentTime System time at the beginning of the time slice, in nanoseconds
 * @param duration Duration of the time slice, in nanoseconds
 */
void Device::timeSliceWillBegin(const double currentTime, const double duration) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(this->mode != EmulationMode::Timeslice) throw MethodNotSupported(__PRETTY_FUNCTION__);

    int err{0};
    auto cb = this->descriptor->slice.timeSliceWillBegin;
    if(cb && (err = cb(this->devicePtr, currentTime, duration))) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * Executes a time slice.
 *
 * @param currentTime System time at the beginning of the time slice, in nanoseconds
 * @param duration Duration of the time slice, in nanoseconds
 */
void Device::executeTimeSlice(const double currentTime, const double duration) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(this->mode != EmulationMode::Timeslice) throw MethodNotSupported(__PRETTY_FUNCTION__);

    int err{0};
    auto cb = this->descriptor->slice.executeTimeSlice;
    if((err = cb(this->devicePtr, currentTime, duration))) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * All devices have finished executing this time slice.
 *
 * @param currentTime Current system time at the END of the time slice.
 */
void Device::timeSliceDidEnd(const double currentTime) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(this->mode != EmulationMode::Timeslice) throw MethodNotSupported(__PRETTY_FUNCTION__);

    int err{0};
    auto cb = this->descriptor->slice.timeSliceDidEnd;
    if(cb && (err = cb(this->devicePtr, currentTime))) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * Returns the device's current progress into the time slice, in nanoseconds.
 */
double Device::getTimeSliceProgress() const {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(this->mode != EmulationMode::Timeslice) throw MethodNotSupported(__PRETTY_FUNCTION__);

    return this->descriptor->slice.getTimeSliceProgress(this->devicePtr);
}


/**
 * If the device uses single stepping mode of execution, executes a single step.
 *
 * @return Number of nanoseconds of time consumed by this step.
 */
double Device::executeStep(const double currentTime) {
    double timeTaken{0};
    int err{0};

    if((err = this->descriptor->step.step(this->devicePtr, currentTime, &timeTaken))) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }

    return timeTaken;
}


/**
 * Prepares the cothread based execution model. This associates the device and a scheduler
 * instance for its lifetime.
 *
 * @remark This will be called early during device initialization, before any connections are
 *         made; required to ensure that we can call into the scheduler when the device's refclk is
 *         connected.
 *
 * @param scheduler Scheduler instance to associate with this device
 */
void Device::coPrepare(const std::shared_ptr<system::Scheduler> &scheduler) {
    this->scheduler = scheduler;

    // get scheduler interface
    auto intf = plugin::Manager::The()->getInterface(plugin::SchedulerInterface::kInterfaceUuid);
    XASSERT(!!intf, "failed to retrieve scheduler interface ({})", plugin::SchedulerInterface::kInterfaceUuid);

    // allocate scheduler context
    this->schedCtx = new emulashione_device_scheduler_context;
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    this->schedCtx->interface = reinterpret_cast<const struct emulashione_scheduler_interface *>(intf->getInterfacePtr());
    this->schedCtx->scheduler = scheduler->getInstancePtr();

    int err = this->descriptor->cothread.prepare(this->devicePtr, this->schedCtx);
    if(err < 0) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * Executes the main method of the device, in the context of the device's emulation thread.
 */
void Device::coMain() {
    int err = this->descriptor->cothread.main(this->devicePtr, this->schedCtx);
    if(err < 0) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}



/**
 * Fetches all supported address space IDs.
 */
void Device::getSupportedAddressSpaces(std::vector<uint16_t> &outAddrSpaces) {
    int err{0};
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(!this->supportsMemory) throw MethodNotSupported(__PRETTY_FUNCTION__);

    constexpr static const size_t kMaxAddressSpaces{512};
    std::array<uint16_t, kMaxAddressSpaces> addressSpaces{0};

    err = this->descriptor->memory.getSupportedAddressSpaces(this->devicePtr, addressSpaces.data(),
            kMaxAddressSpaces);
    if(err < 0) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }

    // copy out `err` number of records
    if(err) {
        outAddrSpaces.resize(err);
        std::copy(addressSpaces.begin(), addressSpaces.begin() + err, outAddrSpaces.begin());
    } 
    // no address spaces supported
    else {
        outAddrSpaces.clear();
    }
}

/**
 * Performs a read from the device.
 */
void Device::memoryRead(struct emulashione_device_instance_header *caller, const uint64_t address,
        const double accessTime, struct emulashione_device_memory_access &accessInfo) {
    int err{0};
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(!this->supportsMemory) throw MethodNotSupported(__PRETTY_FUNCTION__);

    err = this->descriptor->memory.read(this->devicePtr, caller, address, accessTime, &accessInfo);
    if(err) {
        throw Error(err, "Device read failed: {}", err);
    }
}

/**
 * Performs a write to the device.
 */
void Device::memoryWrite(struct emulashione_device_instance_header *caller, const uint64_t address,
        const double accessTime, struct emulashione_device_memory_access &accessInfo) {
    int err{0};
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(!this->supportsMemory) throw MethodNotSupported(__PRETTY_FUNCTION__);

    err = this->descriptor->memory.write(this->devicePtr, caller, address, accessTime,
            &accessInfo);
    if(err) {
        throw Error(err, "Device write failed: {}", err);
    }
}


/**
 * Gets the highest supported strobe id
 */
unsigned int Device::getMaxStrobeId() {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(!this->supportsStrobe) throw MethodNotSupported(__PRETTY_FUNCTION__);
    return this->descriptor->strobe.maxId(this->devicePtr);
}
/**
 * Returns a name for the strobe with the given id.
 *
 * @param strobeId ID of the strobe to look up
 *
 * @return The name for the id, if applicable
 */
std::optional<std::string> Device::getStrobeName(const unsigned int strobeId) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(!this->supportsStrobe) throw MethodNotSupported(__PRETTY_FUNCTION__);
    auto name = this->descriptor->strobe.nameFor(this->devicePtr, strobeId);
    if(name) {
        return std::string(name);
    }
    return nullptr;
}

/**
 * Gets the strobe id for the given name. The device must perform a case insensitive search.
 *
 * @param name Strobe name to look up
 *
 * @return ID of the strobe, or 0 if not found
 */
unsigned int Device::getStrobeIdForName(const std::string_view &name) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(!this->supportsStrobe) throw MethodNotSupported(__PRETTY_FUNCTION__);
    int err = this->descriptor->strobe.idFor(this->devicePtr, name.data());
    if(err < 0) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
    return err;
}

/**
 * Gets the width, in bits, of a particular strobe.
 *
 * @param strobeId ID of the strobe to look up
 *
 * @return Number of bits in the strobe, or 0 if strobe is not found.
 */
unsigned int Device::getStrobeWidthFor(const unsigned int strobeId) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(!this->supportsStrobe) throw MethodNotSupported(__PRETTY_FUNCTION__);
    int err = this->descriptor->strobe.widthFor(this->devicePtr, strobeId);
    if(err < 0) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
    return err;
}

/**
 * Sets the state of a strobe at the given system time point.
 *
 * @param caller Pointer to the device making the strobe set request, if any
 * @param strobeId ID of the strobe to set
 * @param value Value to set on the strobe; only the least significant `width` bits are kept
 * @param accessTime System time, in nanoseconds, of this access
 */
void Device::setStrobeState(const std::shared_ptr<Device> &caller, const unsigned int strobeId,
        const uint64_t value, const double accessTime) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(!this->supportsStrobe) throw MethodNotSupported(__PRETTY_FUNCTION__);
    int err = this->descriptor->strobe.setState(this->devicePtr,
            caller ? caller->devicePtr : nullptr, strobeId, value, accessTime);
    if(err < 0) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * Indicates the given strobe is no longer being driven.
 *
 * @param caller Pointer to the device making the request, if any
 * @param strobeId ID of the strobe to stop driving
 * @param accessTime System time, in nanoseconds, when the strobe is no longer being driven
 */
void Device::stopDrivingStrobes(const std::shared_ptr<Device> &caller,
        const unsigned int strobeId, const double accessTime) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(!this->supportsStrobe) throw MethodNotSupported(__PRETTY_FUNCTION__);
    int err = this->descriptor->strobe.stopDriving(this->devicePtr,
            caller ? caller->devicePtr : nullptr, strobeId, 0, accessTime);
    if(err < 0) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * Blocks the caller until the specified strobe reaches the specified value.
 *
 * @param caller Device instance that wants to be notified when the strobe changes
 * @param strobeID ID of the strobe to observe
 * @param value Value to wait for the strobe to have
 * @param mask Mask to apply against strobe value; 0 indicates no mask
 * @param accessTime System time, in nanoseconds, at which this access is made
 *
 * @return `true` if we successfully advanced until the strobe changed, `false` otherwise.
 */
bool Device::waitForStrobeChange(const std::shared_ptr<Device> &caller,
        const unsigned int strobeId, const uint64_t value, const uint64_t mask,
        const double accessTime) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
    if(!this->supportsStrobe) throw MethodNotSupported(__PRETTY_FUNCTION__);
    int err = this->descriptor->strobe.waitForChange(this->devicePtr,
            caller ? caller->devicePtr : nullptr, strobeId, value, mask, accessTime);
    if(err < 0) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }

    return (err == 1);
}


/**
 * Adds the given bus connection.
 *
 * @param slot Name of the slot to connect
 * @param target Bus to connect to this slot
 */
void Device::addConnection(const std::string_view &slot, const std::shared_ptr<Bus> &target) {
    if(slot.empty() || !target) throw std::invalid_argument("invalid slot or target ptr");
    int err = this->descriptor->connections.addBus(this->devicePtr, slot.data(),
            target->getInstance());
    if(err < 0) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * Adds the given clock source connection.
 *
 * @param slot Name of the slot to connect
 * @param target Clock source to connect to this slot
 */
void Device::addConnection(const std::string_view &slot,
        const std::shared_ptr<ClockSource> &target) {
    if(slot.empty() || !target) throw std::invalid_argument("invalid slot or target ptr");
    int err = this->descriptor->connections.addClockSource(this->devicePtr, slot.data(),
            target->getInstance());
    if(err < 0) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * Adds the given device connection.
 *
 * @param slot Name of the slot to connect
 * @param target Device to connect to this slot
 */
void Device::addConnection(const std::string_view &slot, const std::shared_ptr<Device> &target) {
    if(slot.empty() || !target) throw std::invalid_argument("invalid slot or target ptr");
    int err = this->descriptor->connections.addDevice(this->devicePtr, slot.data(),
            target->devicePtr);
    if(err < 0) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

/**
 * Removes the device connected to the given slot.
 *
 * @param slot Name of the slot to empty.
 */
void Device::removeConnection(const std::string_view &slot, const ConnectionType type) {
    int err{0};

    if(slot.empty()) throw std::invalid_argument("Slot cannot be empty");

    switch(type) {
        case ConnectionType::Bus:
            err = this->descriptor->connections.removeBus(this->devicePtr, slot.data());
            break;
        case ConnectionType::ClockSource:
            err = this->descriptor->connections.removeClockSource(this->devicePtr, slot.data());
            break;
        case ConnectionType::Device:
            err = this->descriptor->connections.removeDevice(this->devicePtr, slot.data());
            break;

        default:
            throw Error("Can't remove connection for type {}: unimplemented", type);
    }

    // handle errors
    if(err < 0) {
        throw Error(err, "Device method {} failed: {}", __PRETTY_FUNCTION__, err);
    }
}

