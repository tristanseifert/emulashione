#ifndef DEVICE_PRIMARYCLOCK_H
#define DEVICE_PRIMARYCLOCK_H

#include "ClockSource.h"

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <memory>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

/**
 * @brief Built in device implementations
 */
namespace emulashione::core::device {
/**
 * Primary clocks are clocks whose frequency is directly specified; most systems may have a few
 * master oscillators, which would be modeled as primary clocks. More than one frequency option may
 * be available, which can be selected from.
 *
 * They can be used directly as timing references for emulated device, but also as the source
 * timebase for other clocks. See the `DerivedClock` class for more.
 *
 * @brief Clock source with a directly specified frequency
 */
class PrimaryClock: public ClockSource {
    public:
        /// name, frequency pair to identify a frequency variant
        using Variant = std::pair<std::string, double>;

    public:
        /**
         * Create a primary clock with a single fixed frequency variant
         *
         * @param name Identifier of the clock in the system definition
         * @param frequency Frequency of the clock, in Hz
         */
        PrimaryClock(const std::string_view &name, const double frequency) : ClockSource(name) {
            this->variants.emplace_back("default", frequency);
        }

        /**
         * Create a primary clock by copying the given frequency variants.
         *
         * @param name Identifier of the clock in the system definition
         * @param start Start of a range of frequency Variant pairs
         * @param end End of a range of frequency Variant pairs
         */
        template <class Iter>
        PrimaryClock(const std::string_view &name, Iter start, Iter end) : ClockSource(name) {
            std::copy(start, end, std::back_inserter(this->variants));
        }

        /// Return the clock's frequency
        double getFrequency() const override {
            if(this->currentVariant == SIZE_MAX) {
                return this->overrideFreq;
            }
            return this->variants[this->currentVariant].second;
        }

        /// Returns the total number of variants.
        size_t numVariants() const {
            return this->variants.size();
        }
        /// Returns the nth variant
        const Variant &operator[](size_t i) {
            return this->variants[i];
        }

        /**
         * Sets the currently active frequency variant.
         *
         * @param i Index of the variant to activate
         *
         * @throw std::invalid_argument Invalid variant index was specified
         */
        void setActiveVariant(const size_t i) {
            if(i >= this->variants.size()) {
                throw std::invalid_argument("index out of bounds");
            }
            this->currentVariant = i;
        }
        /**
         * Sets a frequency that overrides the currently active variant.
         *
         * @remark If a variant with the same _exact_ frequency (floating point comparisons are
         *         inexact by nature) it will be activated instead.
         *
         * @param freq Frequency to set, in Hz
         */
        void setFrequency(const double freq) {
            // first, check if there's a suitable variant
            for(size_t i = 0; i < this->numVariants(); i++) {
                const auto &variant = this->variants[i];
                if(variant.second == freq) {
                    this->currentVariant = i;
                    return;
                }
            }

            this->overrideFreq = freq;
            std::atomic_thread_fence(std::memory_order_release);
            this->currentVariant = SIZE_MAX;
        }

    private:
        /// currently selected frequency option
        size_t currentVariant{0};
        /// if variant is SIZE_MAX, this frequency is used directly
        double overrideFreq{NAN};

        /**
         * Frequency variants for this clock
         *
         * Variants are named frequency settings that the clock can be set to; this is useful to
         * for example implement different per region clocks.
         */
        std::vector<Variant> variants;
};
};

#endif
