#ifndef DEVICE_WRAPPERS_H
#define DEVICE_WRAPPERS_H
/*
 * Various kinds of simple wrapper structures for device related data
 */
#include <memory>

#include "Bus.h"
#include "ClockSource.h"

using namespace emulashione::core::device;


/**
 * @brief Wrapper for a bus object, provided to plugins
 */
struct emulashione_bus_instance {
    std::shared_ptr<Bus> ptr;
};

/**
 * @brief Wrapper for a clock source object, provided to plugins
 */
struct emulashione_clock_source_instance {
    std::shared_ptr<ClockSource> ptr;
};

#endif
