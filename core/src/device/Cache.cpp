#include "Cache.h"
#include "Device.h"

#include "log/Logger.h"

using namespace emulashione::core::device;

Cache *Cache::gShared{nullptr};

/**
 * Allocates the global device cache instance.
 */
void Cache::Start() {
    XASSERT(!gShared, "cannot reinitialize device cache");

    gShared = new Cache;
}

/**
 * Releases the device cache instance.
 */
void Cache::Stop() {
    XASSERT(!!gShared, "cannot stop a null device cache");

    auto ptr = gShared;
    gShared = nullptr;
    delete ptr;
}


/**
 * Retrieves a device instance from the cache.
 *
 * @param devicePtr Device instance pointer to search for
 *
 * @return The corresponding Device object, or `nullptr` if not found
 */
std::shared_ptr<Device> Cache::get(struct emulashione_device_instance_header *devicePtr) {
    std::shared_lock lg(this->lock);
    if(this->cache.contains(devicePtr)) {
        return this->cache[devicePtr];
    }

    return nullptr;
}

/**
 * Inserts a device into the cache.
 *
 * @param device Device to insert
 *
 * @throw DuplicateKey If the device is already in the cache
 */
void Cache::insert(const std::shared_ptr<Device> &device) {
    std::lock_guard lg(this->lock);
    if(this->cache.contains(device->getInstance())) {
        throw DuplicateKey();
    }

    this->cache.emplace(device->getInstance(), device);
}

/**
 * Removes a device from the cache.
 *
 * @param device Device object to remove from the cache
 *
 * @throw NotFound If there is no such device in the cache.
 */
void Cache::remove(const std::shared_ptr<Device> &device) {
    remove(device->getInstance());
}

/**
 * Removes a device from the cache based on its instance pointer.
 *
 * @param devicePtr Device instance pointer (key) to remove from the cache
 *
 * @throw NotFound If there is no such device in the cache.
 */
void Cache::remove(struct emulashione_device_instance_header *devicePtr) {
    std::lock_guard lg(this->lock);
    if(!this->cache.erase(devicePtr)) {
        throw NotFound();
    }
}
