#ifndef DEVICE_CLOCKSOURCE_H
#define DEVICE_CLOCKSOURCE_H

#include <memory>
#include <string>
#include <string_view>

struct emulashione_clock_source_instance;

namespace emulashione::core::device {
/**
 * @brief Abstract interface for a clock source, used by an emulated device to keep track of time.
 */
class ClockSource: public std::enable_shared_from_this<ClockSource> {
    public:
        /**
         * Initializes a new clock source.
         *
         * @param _name Identifier of the clock source in the system description
         */
        ClockSource(const std::string_view &_name) : name(_name) {}
        virtual ~ClockSource() = default;

        /// Returns the clock source's name.
        constexpr const std::string &getName() const {
            return this->name;
        }

        /// Returns the frequency of the clock, in Hz.
        virtual double getFrequency() const = 0;
        /// Returns the period of the clock, in nanoseconds.
        virtual double getPeriod() const {
            constexpr static const double kNsPerHz{1000000000.0};
            return kNsPerHz / this->getFrequency();
        }

        struct emulashione_clock_source_instance *getInstance();
        virtual void willShutDown();

    protected:
        /// Name of this clock; used to refer to it in the system
        std::string name;
        /// Structure passed to plugins/devices to represent this clock
        struct emulashione_clock_source_instance *clockSourcePtr{nullptr};
};
};

#endif
