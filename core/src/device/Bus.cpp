#include "Bus.h"
#include "Device.h"
#include "Wrappers.h"

#include "plugin/interface/Bus.h"
#include "plugin/interface/Device.h"
#include "log/Logger.h"
#include "system/System.h"

#include <algorithm>
#include <mutex>
#include <sstream>
#include <stdexcept>

#include <fmt/core.h>
#include <tabulate/table.hpp>

using namespace emulashione::core::device;

/**
 * Initializes a new bus.
 *
 * @param _name Identifier of the bus in the system description
 * @param _dataWidth Width of the data component of the bus, in bits
 * @param _addrWidth Width of the address component of the bus, in bits
 * @param abortOnUnhandledAccess When set, the emulation is aborted when an unhandled access is
 *        encountered, rather than rising a bus error or similar system specific behavior.
 *
 * @throw std::invalid_argument Invalid name or width values specified
 */
Bus::Bus(const std::string &_name, const uint8_t _dataWidth, const uint8_t _addrWidth,
        const bool abortOnUnhandledAccess) : abortOnUnhandledAccess(abortOnUnhandledAccess),
    dataWidth(_dataWidth), addrWidth(_addrWidth), name(_name) {
    if(!_dataWidth || !_addrWidth || _name.empty()) {
        throw std::invalid_argument("Invalid name or width specified");
    }

    // calculate valid bitmask
    this->dataMask = static_cast<uint64_t>(std::pow(2, _dataWidth)) - 1;
    this->addrMask = static_cast<uint64_t>(std::pow(2, _addrWidth)) - 1;
}

/**
 * Gets the bus instance wrapper, allocating it if needed.
 *
 * @return Bus instance wrapper
 */
struct emulashione_bus_instance *Bus::getInstance() {
    if(!this->busPtr) {
        // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
        this->busPtr = new emulashione_bus_instance;
        this->busPtr->ptr = this->shared_from_this();
        XASSERT(!!this->busPtr->ptr, "failed to get bus shared ptr");
    }

    return this->busPtr;
}

/**
 * Deallocates the instance object, if we have one. This works around otherwise creating a retain
 * cycle with the bus object.
 */
void Bus::willShutDown() {
    if(this->busPtr) {
        delete this->busPtr;
        this->busPtr = nullptr;
    }
}


/**
 * Sets the pullup and pulldown resistors in one go.
 *
 * @note This is done like this since in the majority of the cases, a bus will only have one type
 * of pull resistor, and this way we can easily validate whether a state is valid before the bus
 * is modified.
 *
 * @param up Bitmask indicating which bits are pulled up (to 1)
 * @param down Bitmask indicating which bits are pulled down (to 0)
 */
void Bus::setPulls(const uint64_t up, const uint64_t down) {
    if((up & down) != 0) {
        const auto bits = up & down;
        auto msg = fmt::format("Cannot specify both pullup and pull down for bits: {:016x}", bits);

        throw std::invalid_argument(msg);
    }

    this->pullUps = up & this->dataMask;
    this->pullDowns = down & this->dataMask;
}

/**
 * Connects a device to the bus. We keep a weak reference to the device, so that it's free to go
 * away on its own terms without involving us, preventing retain cycles.
 *
 * XXX: What is this used for?
 */
void Bus::connectDevice(const std::shared_ptr<Device> &device) {
    std::lock_guard<std::mutex> lg(this->devicesLock);
    this->devices.emplace_back(device);
}

/**
 * Maps the given device into the address space of the bus.
 *
 * @note Should only be called during system initialization; there is no lock on the mappings
 * container.
 *
 * @param start Starting address (inclusive) for the device
 * @param end Ending address (not inclusive) of the device
 * @param device Device to register
 *
 * @throw std::invalid_argument If the range is invalid
 * @throw std::runtime_error If a conflict arises with an existing mapping or it is out of bounds
 */
void Bus::mapDevice(const uint64_t start, const uint64_t end,
        const std::shared_ptr<Device> &device) {
    // validate args
    if(start >= end) {
        throw std::invalid_argument("starting address must be less than ending address");
    } else if(start > (start & this->addrMask)) {
        throw std::runtime_error(fmt::format("starting address out of bounds (max ${:x})",
                    this->addrMask));
    } else if(end > (end & this->addrMask)) {
        throw std::runtime_error(fmt::format("ending address out of bounds (max ${:x})",
                    this->addrMask));
    }

    // validate the mapping (should not overlap any existing ones)
    if(std::any_of(this->mappings.begin(), this->mappings.end(), [&](const auto &entry) -> bool {
        return entry.contains({start, end});
    })) {
        throw std::runtime_error("Conflict with existing map entry ()");
    }

    // store the mapping
    this->mappings.emplace_back(Mapping{{start, end}, device});
}

/**
 * Pretty prints a table form of the bus address mapping.
 */
std::string Bus::dumpAddressMap() const {
    using namespace tabulate;
    Table tbl;

    tbl.add_row({"Start", "End", "Device"});

    for(const auto &map : this->mappings) {
        tbl.add_row({fmt::format("${:08x}", map.range.first),
                fmt::format("${:08x}", map.range.second), map.device->getName()});
    }

    for(size_t i = 0; i < 3; i++) {
        tbl[0][i].format().font_style({FontStyle::bold}).font_align(FontAlign::center);
    }

    std::stringstream str;
    str << tbl;
    return str.str();
}



/**
 * Determines what device shall handle this read access and forwards the request to it.
 *
 * @param device Device performing this read
 * @param address Bus address to read from
 * @param info Memory access descriptor that carries the required information
 */
void Bus::accessRead(struct emulashione_device_instance_header *device, uint64_t address,
        struct emulashione_device_memory_access &info) {
    address &= this->addrMask;

    // find a device to handle this
    for(const auto &mapping : this->mappings) {
        if(!mapping.contains(address)) continue;

        // XXX: is there something useful to put in the "access time" field?
        return mapping.device->memoryRead(device, address, 0, info);
    }

    // read to unmapped area
    this->handleUnmappedAccess(device, address, info, true);
}

/**
 * Determines what device shall handle this write access and forwards the request to it.
 *
 * @param device Device performing this write
 * @param address Bus address to write to
 * @param info Memory access descriptor that carries the required information
 */
void Bus::accessWrite(struct emulashione_device_instance_header *device, uint64_t address,
        struct emulashione_device_memory_access &info) {
    address &= this->addrMask;

    // find a device to handle this
    for(const auto &mapping : this->mappings) {
        if(!mapping.contains(address)) continue;

        // XXX: is there something useful to put in the "access time" field?
        return mapping.device->memoryWrite(device, address, 0, info);
    }

    // read to unmapped area
    this->handleUnmappedAccess(device, address, info, false);
}

/**
 * Handles an access to a region of the bus that has no devices associated with it.
 *
 * @param device Device performing this read
 * @param address Bus address to read from
 * @param info Memory access descriptor that carries the required information
 * @param read Whether the access is a read (`true`) or write (`false`)
 *
 * @throw UnmappedAccess If unmapped accesses are fatal, this exception is thrown
 */
void Bus::handleUnmappedAccess(struct emulashione_device_instance_header *device,
        const uint64_t address, struct emulashione_device_memory_access &info, const bool read) {
    (void) device, (void) address;

    if(this->abortOnUnhandledAccess) {
        // TODO: use a centralized error code
        this->system.lock()->abortEmulation(-10);
    }

    if(this->unmappedAccessIsFatal) {
        throw UnmappedAccess();
    } else {
        /*
         * If the access is not treated as fatal, it should read open bus; currently, that's just
         * simulated by whatever the pull ups and downs set.
         *
         * TODO: Handle bits not pulled up or down
         */
        if(read) {
            switch(info.flags & kAccessWidthMask) {
                case kAccessWidthByte:
                    info.d8 = static_cast<uint8_t>(this->pullUps & 0xFF);
                    break;
                case kAccessWidthWord:
                    info.d16 = static_cast<uint16_t>(this->pullUps & 0xFFFF);
                    break;
                case kAccessWidthLongWord:
                    info.d32 = static_cast<uint32_t>(this->pullUps & 0xFFFFFFFF);
                    break;
                case kAccessWidthQuadWord:
                    info.d64 = this->pullUps;
                    break;
                default:
                    XASSERT(false, "invalid access width, flags={:x}", info.flags);
            }
        }
        /*
         * Writes are simply silently ignored.
         */
        else {

        }

        /*
         * XXX: This default access time is a hack. For now, this is just set something really
         * small so devices will round it up to their smallest access timings. This may not be
         * correct for many situations though.
         */
        info.time = 10.;
    }
}

