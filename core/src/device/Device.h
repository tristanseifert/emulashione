#ifndef DEVICE_DEVICE_H
#define DEVICE_DEVICE_H

#include <cstdint>
#include <exception>
#include <memory>
#include <optional>
#include <string>
#include <string_view>
#include <vector>

#include <fmt/core.h>
#include <uuid.h>

struct emulashione_device_descriptor;
struct emulashione_device_instance_header;
struct emulashione_device_memory_access;
struct emulashione_device_scheduler_context;

namespace emulashione::core {
namespace system {
class Scheduler;
class System;
};

namespace device {
class Bus;
class ClockSource;
class Info;

/**
 * Encapsulates an instance of a device (as exported by a plugin) used in the core emulation
 * library. In effect, this is a thin shim around the function pointers in the device descriptor.
 *
 * @brief Instance of a device class
 */
class Device: public std::enable_shared_from_this<Device> {
    friend class Info;

    public:
        /// How is the device emulated?
        enum class EmulationMode {
            /// No regular updates are required
            Passive,
            /// Submit an entire time slice to the device directly
            Timeslice,
            /// Core steps through a time slice for the device
            Step,
            /// Cooperatively relinquish CPU to other devices
            Cothread,
        };

        /// Connection type
        enum class ConnectionType {
            Bus, ClockSource, Device
        };

        /// Indicates a device method failed
        class Error: public std::exception {
            friend class Device;

            private:
                /// Create an error with no detail string.
                Error() = default;

                /// Create an error with the given format string.
                template<typename... Args>
                Error(fmt::format_string<Args...> fmt, Args &&...args) {
                    this->message = fmt::format(fmt, std::forward<Args>(args)...);
                }

                /// Create an error with the given format string and error code.
                template<typename... Args>
                Error(int code, fmt::format_string<Args...> fmt, Args &&...args) : code(code) {
                    this->message = fmt::format(fmt, std::forward<Args>(args)...);
                }

            public:
                const char *what() const noexcept override {
                    return this->message.c_str();
                }

                /// Returns the error code, if available.
                constexpr auto getErrorCode() const noexcept {
                    return this->code;
                }

            private:
                std::string message;
                int code{0};
        };
        /// The called method is not supported by the device
        class MethodNotSupported: public Error {
            friend class Device;

            private:
                MethodNotSupported(const char *meth) : Error("Unsupported method: {}", meth) {}
        };

    public:
        Device(const std::shared_ptr<Info> &info,
                struct emulashione_device_instance_header *instance, const std::string &name);
        virtual ~Device();

        /// Returns the device info object corresponding to this device, if it's still active.
        auto getDeviceInfo() const {
            return this->info.lock();
        }
        /// Returns the descriptor of this device, which also indicates its type.
        constexpr const struct emulashione_device_descriptor *getDescriptor() const {
            return this->descriptor;
        }
        /// Returns the raw device instance pointer.
        constexpr struct emulashione_device_instance_header *getInstance() {
            return this->devicePtr;
        }
        /// How is the device to be emulated?
        constexpr auto getEmulationMode() const {
            return this->mode;
        }
        /// Get the instance name of the device
        const std::string &getName() const {
            return this->name;
        }

        /**
         * Set the system that this device is connected to. It's automatically set when added to a
         * new system.
         *
         * @param system New system pointer
         */
        void setSystem(const std::shared_ptr<system::System> &system) {
            this->system = system;
        }
        /**
         * Get the system this device is associated with.
         */
        inline auto getSystem() {
            return this->system.lock();
        }

        virtual bool supportsInterface(const uuids::uuid &interfaceId);

        virtual void willStartEmulation();
        virtual void willStopEmulation();
        virtual void willPauseEmulation();
        virtual void willResumeEmulation();
        virtual void willShutDown();
        virtual void reset(const bool isHardReset);
        virtual void commitTo(const double time);

        virtual void timeSliceWillBegin(const double currentTime, const double duration);
        virtual void executeTimeSlice(const double currentTime, const double duration);
        virtual void timeSliceDidEnd(const double currentTime);
        virtual double getTimeSliceProgress() const;

        virtual double executeStep(const double currentTime);

        virtual void coPrepare(const std::shared_ptr<system::Scheduler> &scheduler);
        virtual void coMain();

        virtual void getSupportedAddressSpaces(std::vector<uint16_t> &outAddrSpaces);
        virtual void memoryRead(struct emulashione_device_instance_header *caller,
                const uint64_t address, const double accessTime,
                struct emulashione_device_memory_access &accessInfo);
        virtual void memoryWrite(struct emulashione_device_instance_header *caller,
                const uint64_t address, const double accessTime,
                struct emulashione_device_memory_access &accessInfo);

        virtual unsigned int getMaxStrobeId();
        virtual std::optional<std::string> getStrobeName(const unsigned int strobeId);
        virtual unsigned int getStrobeIdForName(const std::string_view &name);
        virtual unsigned int getStrobeWidthFor(const unsigned int strobeId);
        virtual void setStrobeState(const std::shared_ptr<Device> &caller,
                const unsigned int strobeId, const uint64_t value, const double accessTime);
        virtual void stopDrivingStrobes(const std::shared_ptr<Device> &caller,
                const unsigned int strobeId, const double accessTime);
        virtual bool waitForStrobeChange(const std::shared_ptr<Device> &caller,
                const unsigned int strobeId, const uint64_t value, const uint64_t mask,
                const double accessTime);

        virtual void addConnection(const std::string_view &slot,
                const std::shared_ptr<Bus> &target);
        virtual void addConnection(const std::string_view &slot,
                const std::shared_ptr<ClockSource> &target);
        virtual void addConnection(const std::string_view &slot,
                const std::shared_ptr<Device> &target);

        virtual void removeConnection(const std::string_view &slot,
                const ConnectionType type = ConnectionType::Device);

    private:
        /// Is the strobe interface supported?
        bool supportsStrobe{false}, supportsMemory{false};
        /// How shall the device be updated?
        EmulationMode mode;

        /// Instance name, if any
        std::string name;

        /// Info object from which the device was created
        std::weak_ptr<Info> info;

        /// descriptor with the function pointers we thunk through to
        const struct emulashione_device_descriptor *descriptor{nullptr};
        /// instance object
        struct emulashione_device_instance_header *devicePtr{nullptr};

        /// If this device is emulated using cothread mode, the scheduler it's attached to
        std::shared_ptr<system::Scheduler> scheduler;
        /// cothread context, allocated in coPrepare()
        struct emulashione_device_scheduler_context *schedCtx{nullptr};

        /// System in which this device is executing
        std::weak_ptr<system::System> system;
};
}
}

#endif
