#ifndef DEVICE_DERIVEDCLOCK_H
#define DEVICE_DERIVEDCLOCK_H

#include "ClockSource.h"
#include "PrimaryClock.h"

#include <memory>

namespace emulashione::core::device {
/**
 * Derived clocks are set by dividing the frequency of a primary clock by some divisor.
 *
 * @brief Clock derived from another via a fixed divisor
 */
class DerivedClock: public ClockSource {
    public:
        /**
         * Initializes a new derived clock, whose frequency is the frequency of its parent divided
         * by the specified divisor.
         */
        DerivedClock(const std::string_view &name, const std::shared_ptr<ClockSource> &_parent,
                const double _divisor) : ClockSource(name), parent(_parent),
                divisor(1. / _divisor) {};

        /// Returns the frequency taking the divisor into account
        double getFrequency() const override {
            const double freq = this->parent->getFrequency();
            // divisor is stored as its reciporical so this works
            return freq * this->divisor;
        }

    private:
        /// Parent clock from which this one's frequency is derived
        std::shared_ptr<ClockSource> parent;
        /// Clock divisor; stored as its reciporical to avoid an extra division in the hot path
        double divisor;
};
}

#endif
