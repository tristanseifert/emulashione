#include "ClockSource.h"
#include "Wrappers.h"

#include "log/Logger.h"

using namespace emulashione::core::device;


/**
 * Gets the clock instance wrapper, allocating it if needed.
 *
 * @return Clock instance wrapper
 */
struct emulashione_clock_source_instance *ClockSource::getInstance() {
    if(!this->clockSourcePtr) {
        // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
        this->clockSourcePtr = new emulashione_clock_source_instance;
        this->clockSourcePtr->ptr = this->shared_from_this();
        XASSERT(!!this->clockSourcePtr->ptr, "failed to get clock source shared ptr");
    }

    return this->clockSourcePtr;
}

/**
 * Cleans up the clock source info structure during shutdown time to avoid a retain cycle.
 */
void ClockSource::willShutDown() {
    if(this->clockSourcePtr) {
        delete this->clockSourcePtr;
        this->clockSourcePtr = nullptr;
    }
}
