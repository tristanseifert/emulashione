#ifndef DEVICE_BUS_H
#define DEVICE_BUS_H

#include <cstddef>
#include <cstdint>
#include <memory>
#include <mutex>
#include <string>
#include <utility>
#include <vector>

struct emulashione_bus_instance;
struct emulashione_device_instance_header;
struct emulashione_device_memory_access;

namespace emulashione::core {
namespace system {
class System;
}

namespace device {
class Device;

/**
 * Busses encapsulate the bidirectional flow of data between various components in a system.
 *
 * They can emulate the behavior of partially driven busses by specifying which bits are pulled up
 * to a `1` state, and which are pulled down to a `0` state.
 *
 * @brief Wrapper around an address and data bus
 */
class Bus: public std::enable_shared_from_this<Bus> {
    public:
        /// Indicates an access to an unmapped memory range was attempted
        struct UnmappedAccess: public std::exception {};

    public:
        Bus(const std::string &name, const uint8_t dataWidth, const uint8_t addrWidth,
                const bool abortOnUnhandledAccess = false);
        virtual ~Bus() = default;

        /// Get the name of this bus
        constexpr const std::string &getName() const {
            return this->name;
        }
        /// Get width, in bits, of the data part of the bus
        constexpr auto getDataWidth() const {
            return this->dataWidth;
        }
        /// Get the width, in bits, of the address part of the bus
        constexpr auto getAddressWidth() const {
            return this->addrWidth;
        }

        struct emulashione_bus_instance *getInstance();
        virtual void willShutDown();

        void setPulls(const uint64_t up, const uint64_t down);

        void connectDevice(const std::shared_ptr<Device> &device);
        void mapDevice(const uint64_t start, const uint64_t end,
                const std::shared_ptr<Device> &device);

        void accessRead(struct emulashione_device_instance_header *device, uint64_t address,
                struct emulashione_device_memory_access &info);
        void accessWrite(struct emulashione_device_instance_header *device, uint64_t address,
                struct emulashione_device_memory_access &info);

        virtual std::string dumpAddressMap() const;

        /**
         * Set the system that this bus is a part of. It's automatically set when added to a new
         * system.
         *
         * @param system New system pointer
         */
        inline void setSystem(const std::shared_ptr<system::System> &system) {
            this->system = system;
        }
        /**
         * Get the system this bus is associated with.
         */
        inline auto getSystem() {
            return this->system.lock();
        }

    private:
        void handleUnmappedAccess(struct emulashione_device_instance_header *, const uint64_t,
                struct emulashione_device_memory_access &, const bool);

    private:
        struct Mapping {
            /// address range covered by the mapping
            std::pair<uint64_t, uint64_t> range;
            /// device that receives bus requests
            std::shared_ptr<Device> device;

            /**
             * Does the given address fall in this mapping?
             */
            constexpr bool contains(const uint64_t addr) const {
                return (this->range.first <= addr) && (this->range.second >= addr);
            }

            /**
             * Do the given ranges overlap?
             */
            constexpr bool contains(const std::pair<uint64_t, uint64_t> &inRange) const {
                return (inRange.first <= this->range.second) &&
                    (inRange.second >= this->range.first);
            }
        };

    private:
        /// When set, emulation is aborted on illegal accesses
        bool abortOnUnhandledAccess{false};
        /// Whether reading from an unmapped address is a fatal error, or open bus is returned
        bool unmappedAccessIsFatal{true};
        /// Width, in bits, of the bus sub components
        uint8_t dataWidth{0}, addrWidth{0};
        /// Bitmask for all active/valid bits of the bus sub components
        uint64_t dataMask{0}, addrMask{0};

        /// Short name of bus (for identification)
        std::string name;

        /// Bitmask of all pulled up bits
        uint64_t pullUps{0};
        /// Bitmask of all pulled down bits
        uint64_t pullDowns{0};

        /// Lock on the devices list
        std::mutex devicesLock;
        /// All devices connected to this bus
        std::vector<std::weak_ptr<Device>> devices;

        /// All devices that are mapped on this bus
        std::vector<Mapping> mappings;

        /// Structure passed to plugins/devices to represent this bus
        struct emulashione_bus_instance *busPtr{nullptr};

        /// System to which this bus belongs
        std::weak_ptr<system::System> system;
};
}
}

#endif
