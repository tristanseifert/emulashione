#ifndef DEVICE_INFO_H
#define DEVICE_INFO_H

#include <atomic>
#include <exception>
#include <list>
#include <memory>
#include <optional>
#include <string>

#include <boost/property_tree/ptree_fwd.hpp>
#include <fmt/core.h>
#include <uuid.h>

struct emulashione_device_descriptor;

namespace emulashione::core {
namespace plugin {
class Plugin;
}

namespace device {
class Device;

/**
 * Essentially, this is a nice wrapper around the the emulashione_device_descriptor struct the
 * plugin registers; in fact, during registration is when this object is allocated.
 *
 * @brief Information on a registered device class
 */
class Info: public std::enable_shared_from_this<Info> {
    friend class Registry;

    public:
        /// Indicates the device descriptor structure is invalid
        class DescriptorError: public std::exception {
            friend class Info;

            private:
                /// Create a load error with no detail string.
                DescriptorError() = default;

                /// Create a load error with the given format string.
                template<typename... Args>
                DescriptorError(fmt::format_string<Args...> fmt, Args &&...args) {
                    this->message = fmt::format(fmt, std::forward<Args>(args)...);
                }

            public:
                const char *what() const noexcept override {
                    return this->message.c_str();
                }

            private:
                std::string message;
        };

    public:
        ~Info();

        std::shared_ptr<Device> makeInstance(const std::string &instanceName,
                const boost::property_tree::ptree &params);

        /// Gets the plugin that loaded this device, or `nullptr` if it's been unloaded.
        std::shared_ptr<plugin::Plugin> getPlugin() const {
            return this->plugin.lock();
        }
        /// Get the original descriptor pointer.
        const struct emulashione_device_descriptor *getDescriptor() const {
            return this->descriptor;
        }

        /// Is this device built in to the library, e.g. always loaded?
        constexpr auto isBuiltIn() const {
            return this->builtIn;
        }
        /// Return the display name of the device
        constexpr const auto &getDisplayName() const {
            return this->displayName;
        }
        /// Return the short name of the device
        constexpr const auto &getShortName() const {
            return this->shortName;
        }
        /// Return the author string
        constexpr const auto &getAuthor() const {
            return this->authors;
        }
        /// Return the description string
        constexpr const auto &getDescription() const {
            return this->description;
        }
        /// Return the information URL
        const std::optional<std::string> getInfoUrl() const {
            if(this->infoUrl.empty()) return nullptr;
            return this->infoUrl;
        }
        /// Return the version string
        constexpr const auto &getVersion() const {
            return this->version;
        }
        /// Return the device UUID
        constexpr const auto &getUuid() const {
            return this->uuid;
        }

        Info(const std::shared_ptr<plugin::Plugin> &plug,
                const struct emulashione_device_descriptor *device);

    private:
        void invokeInitializers();
        void shutdown();

        static void EnsureRequiredProperties(const struct emulashione_device_descriptor *);
        static void EnsureRequiredMethods(const struct emulashione_device_descriptor *);

    private:
        bool builtIn{false};

        std::atomic_bool hasShutDown{false};

        /// Plugin that originally registered us; if null, it's been unloaded
        std::weak_ptr<plugin::Plugin> plugin;
        /// Original device descriptor. Ensure plugin is still loaded before accessing
        const struct emulashione_device_descriptor *descriptor;
        /// any instances of the device
        std::list<std::weak_ptr<Device>> instances;

        /// Display name for the device
        std::string displayName;
        /// Short "tag" type name for the device; used for matching
        std::string shortName;
        /// Author name(s)
        std::string authors;
        /// Long form description of the device
        std::string description;
        /// URL for detailed device information
        std::string infoUrl;
        /// Device version string
        std::string version;

        /// unique identifier of this device
        uuids::uuid uuid;
};
}
}

#endif
