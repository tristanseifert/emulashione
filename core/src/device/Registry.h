#ifndef DEVICE_REGISTRY_H
#define DEVICE_REGISTRY_H

#include <memory>
#include <mutex>
#include <stdexcept>
#include <unordered_map>

#include <uuid.h>

struct emulashione_device_descriptor;

namespace emulashione::core {
namespace plugin {
class Plugin;
}

namespace device {
class Info;

/**
 * Holds references to all devices that plugins have registered, and allows other code to look up
 * devices by various criteria.
 *
 * @brief Handles registration and lookup of device classes
 */
class Registry {
    public:
        /// Error indicating a device with the given UUID has already been registered
        struct DuplicateDevice: public std::runtime_error {
            DuplicateDevice() : std::runtime_error("A device with this id already exists") {};
        };
        /// Error indicating the device type UUID is unknown
        struct NoSuchDevice: public std::runtime_error {
            NoSuchDevice() : std::runtime_error("Unknown device uuid") {};
        };
        /// Error indicating the caller is not the same as the one that registered the device
        struct NotOwner: public std::runtime_error {
            NotOwner() : std::runtime_error("You did not register this device!") {};
        };

    public:
        /// Return the shared device registry instance
        inline static Registry *The() {
            return gShared;
        }

        static void Start();
        static void Stop();

        std::shared_ptr<Info> getDevice(const uuids::uuid &uuid);
        std::shared_ptr<Info> getDevice(const std::string &shortName);

        void registerDevice(const std::shared_ptr<plugin::Plugin> &plug,
                const struct emulashione_device_descriptor *device);
        void unregisterDevice(const uuids::uuid &uuid, const std::shared_ptr<plugin::Plugin> &plug);

        void pluginWillUnload(const std::shared_ptr<plugin::Plugin> &);

    private:
        static Registry *gShared;

        std::mutex devicesLock;
        std::unordered_map<uuids::uuid, std::weak_ptr<Info>> devices;
};
}
}

#endif
