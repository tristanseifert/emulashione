#ifndef DEVICE_CACHE_H
#define DEVICE_CACHE_H

#include <memory>
#include <shared_mutex>
#include <stdexcept>
#include <unordered_map>

struct emulashione_device_instance_header;

namespace emulashione::core::device {
class Device;

/**
 * Handles maintaining a mapping between the plain C pointers that are passed to the plugins, and
 * our internal Device instances.
 *
 * @remark This is cache shared between all systems instantiated in the library.
 *
 * @brief A cache mapping emulashione_device_instance_header pointers to Device objects
 */
class Cache {
    public:
        /**
         * @brief The device already exists in the cache. This indicates a fatal programming error.
         */
        class DuplicateKey: public std::runtime_error {
            friend class Cache;
            DuplicateKey() : std::runtime_error("Duplicate device") {};
        };

        /**
         * @brief No such device exists in the cache.
         */
        class NotFound: public std::runtime_error {
            friend class Cache;
            NotFound() : std::runtime_error("Device not found") {};
        };

    public:
        /**
         * Gets the global device cache instance.
         *
         * @return Pointer to the global device cache instance, initialized during library init.
         */
        static auto The() {
            return gShared;
        }

        static void Start();
        static void Stop();

        std::shared_ptr<Device> get(struct emulashione_device_instance_header *devicePtr);
        void insert(const std::shared_ptr<Device> &device);
        void remove(const std::shared_ptr<Device> &device);
        void remove(struct emulashione_device_instance_header *devicePtr);

    private:
        /// Shared instance of the device cache
        static Cache *gShared;

        /// Read/write lock for the cache entries
        mutable std::shared_mutex lock;
        /**
         * Cache contents; this maps the device struct pointers to the actual device objects.
         *
         * Because these are held as strong pointers, it's important the device unregisters itself
         * during shutdown so that its destructor can be correctly invoked.
         */
        std::unordered_map<struct emulashione_device_instance_header *, std::shared_ptr<Device>> cache;
};
}

#endif
