#include "Info.h"
#include "Cache.h"
#include "Device.h"

#include "log/Logger.h"
#include "plugin/Plugin.h"
#include "plugin/bridge/ConfigInterface.h"
#include "plugin/interface/Device.h"

#include <memory>
#include <stdexcept>
#include <string_view>

#include <emulashione.h>

#include <boost/property_tree/ptree.hpp>

using namespace emulashione::core::device;

/**
 * Instantiate the device info structure, with the given device descriptor. Some information is
 * copied out of the structure while any calls will work directly by dereferencing the pointers in
 * the original struct. Therefore, it must remain valid for as long as this object is alive.
 *
 * Additionally, once initialization is complete, we invoke the load function of the device.
 *
 * @param _plugin Plugin that the device was registered from
 * @param _descriptor Device descriptor
 *
 * @throw DescriptorErrors If the device descriptor is invalid.
 */
Info::Info(const std::shared_ptr<plugin::Plugin> &_plugin,
        const struct emulashione_device_descriptor *_descriptor) : plugin(_plugin),
    descriptor(_descriptor) {
    // validate the descriptor
    if(_descriptor->magic != kEmulashioneDeviceMagic) {
        throw DescriptorError("Invalid magic: ${:16x}", _descriptor->magic);
    } else if(_descriptor->infoVersion != 1) {
        throw DescriptorError("Invalid descriptor version: {}", _descriptor->infoVersion);
    }

    EnsureRequiredProperties(_descriptor);
    EnsureRequiredMethods(_descriptor);

    // copy info strings
    const auto &str = this->descriptor->strings;
    this->displayName = std::string(str.displayName);
    this->shortName = std::string(str.shortName);
    this->authors = std::string(str.authors);
    this->version = std::string(str.version);
    if(str.infoUrl) this->infoUrl = std::string(str.infoUrl);
    if(str.description) this->description = std::string(this->description);

    //NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
    const std::span<uint8_t, 16> uuidBytes{const_cast<uint8_t *>(this->descriptor->uuid), 16};
    const uuids::uuid uuid{uuidBytes};
    this->uuid = uuid;
}

/**
 * Invoke the initializer.
 *
 * @throw DescriptorError An unexpected error occurred during initialization
 */
void Info::invokeInitializers() {
    int err = this->descriptor->lifecycle.didLoad();
    if(err) {
        throw DescriptorError("Failed to initialize device: {}", err);
    }
}




/**
 * Invoke the shutdown handler when the device is deallocated.
 */
Info::~Info() {
    this->shutdown();
}

/**
 * Invoke the shutdown handler of the device info object.
 */
void Info::shutdown() {
    bool no = false;
    if(this->hasShutDown.compare_exchange_strong(no, true, std::memory_order_release,
                std::memory_order_relaxed)) {
        // check if there's any active instances remaining
        const auto numActive = std::count_if(this->instances.begin(), this->instances.end(),
                [](auto &instance) {
            return !!(instance.lock());
        });
        if(numActive) {
            auto plugin = this->plugin.lock();
            Logger::Error("BUG: There are still {} active instances of device '{}' (plugin '{}') "
                    "at shutdown time!", numActive, this->shortName,
                    plugin ? plugin->getShortName() : "(unknown)");
        }


        // run unload callback
        int err = this->descriptor->lifecycle.willUnload();
        if(err) {
            Logger::Warn("Device '{}' unload callback failed: {}", this->shortName, err);
        }
    }
}

/**
 * Allocates an instance of the device.
 *
 * @param instanceName Identifier for the instance in the system definition
 * @param params Parameters read from the config file, if any
 *
 * @throw std::runtime_error Unexpected error occurred converting configuration
 * @throw std::exception Unexpected error initializing the device
 *
 * @return Initialized device object
 */
std::shared_ptr<Device> Info::makeInstance(const std::string &instanceName,
        const boost::property_tree::ptree &params) {
    // allocate the options object if needed
    struct emulashione_config_object *config{nullptr};
    if(!params.empty()) {
        config = plugin::ConfigInterface::MakeConfig(params);
        if(!config) throw std::runtime_error("failed to create configuration object");
    }

    std::shared_ptr<Device> device;
    try {
        // invoke the device descriptor's alloc method
        auto hdr = this->descriptor->lifecycle.alloc(instanceName.c_str(), config);
        if(!hdr) {
            throw std::runtime_error("failed to allocate device");
        }

        // then create a device descriptor
        device = std::make_shared<Device>(this->shared_from_this(), hdr, instanceName);
    } catch(const std::exception &) {
        // ensure the config object gets cleaned up on error
        plugin::ConfigInterface::DeallocConfig(config);
        throw;
    }

    // it's fully set up :)
    this->instances.emplace_back(device);
    Cache::The()->insert(device);

    return device;
}

// NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)

/**
 * Validates that all required fields are present in the descriptor.
 */
void Info::EnsureRequiredProperties(const struct emulashione_device_descriptor *desc) {
    struct RequiredField {const std::string_view category, name; const void *ptr; };
    const std::array<RequiredField, 4> kRequiredFields = {{
        {"strings", "short name", reinterpret_cast<const void *>(desc->strings.shortName)},
        {"strings", "display name", reinterpret_cast<const void *>(desc->strings.displayName)},
        {"strings", "authors", reinterpret_cast<const void *>(desc->strings.authors)},
        {"strings", "version", reinterpret_cast<const void *>(desc->strings.version)},
    }};

    for(const auto &m : kRequiredFields) {
        if(!m.ptr) throw DescriptorError("Missing {} property: {}", m.category, m.name);
    }
}

/**
 * Validates that all required methods are defined in the device descriptor. If not, an exception
 * is thrown.
 */
void Info::EnsureRequiredMethods(const struct emulashione_device_descriptor *desc) {
    /**
     * This array contains a list of all mandatory methods for device descriptors; these are cast
     * to void pointers (we don't call them; we just need to ensure they're not `nullptr`) here
     * for that reason.
     *
     * Below are various other arrays for required methods in certain interfaces.
     */
    struct RequiredMethod {const std::string_view category, name; void *ptr; };
    const std::array<RequiredMethod, 14> kRequiredMethods = {{
        {"lifecycle", "didLoad", reinterpret_cast<void *>(desc->lifecycle.didLoad)},
        {"lifecycle", "willUnload", reinterpret_cast<void *>(desc->lifecycle.willUnload)},
        {"lifecycle", "alloc", reinterpret_cast<void *>(desc->lifecycle.alloc)},
        {"lifecycle", "free", reinterpret_cast<void *>(desc->lifecycle.free)},
        {"info", "supportsInterface", reinterpret_cast<void *>(desc->info.supportsInterface)},
        {"info", "getInfo", reinterpret_cast<void *>(desc->info.getInfo)},
        {"info", "setInfo", reinterpret_cast<void *>(desc->info.setInfo)},
        {"emulation", "commitTo", reinterpret_cast<void *>(desc->emulation.commitTo)},
        {"connection", "addBus", reinterpret_cast<void *>(desc->connections.addBus)},
        {"connection", "removeBus", reinterpret_cast<void *>(desc->connections.removeBus)},
        {"connection", "addClockSource", reinterpret_cast<void *>(desc->connections.addClockSource)},
        {"connection", "removeClockSource", reinterpret_cast<void *>(desc->connections.removeClockSource)},
        {"connection", "addDevice", reinterpret_cast<void *>(desc->connections.addDevice)},
        {"connection", "removeDevice", reinterpret_cast<void *>(desc->connections.removeDevice)},
    }};

    const std::array<RequiredMethod, 2> kSliceRequiredMethods = {{
        {"timeslice", "executeTimeSlice", reinterpret_cast<void *>(desc->slice.executeTimeSlice)},
        //{"timeslice", "didLoad", reinterpret_cast<void *>(desc->slice.timeSliceDidEnd)},
        {"timeslice", "getTimeSliceProgress",
            reinterpret_cast<void *>(desc->slice.getTimeSliceProgress)},
    }};

    const std::array<RequiredMethod, 1> kStepRequiredMethods = {{
        {"step", "step", reinterpret_cast<void *>(desc->step.step)},
    }};

    const std::array<RequiredMethod, 2> kCothreadRequiredMethods = {{
        {"cothread", "prepare", reinterpret_cast<void *>(desc->cothread.prepare)},
        {"cothread", "main", reinterpret_cast<void *>(desc->cothread.main)},
    }};

    const std::array<RequiredMethod, 3> kMemoryRequiredMethods = {{
        {"memory", "getSupportedAddressSpaces",
            reinterpret_cast<void *>(desc->memory.getSupportedAddressSpaces)},
        {"memory", "read", reinterpret_cast<void *>(desc->memory.read)},
        {"memory", "write", reinterpret_cast<void *>(desc->memory.write)},
    }};

    const std::array<RequiredMethod, 7> kStrobeRequiredMethods = {{
        {"strobe", "maxId", reinterpret_cast<void *>(desc->strobe.maxId)},
        {"strobe", "nameFor", reinterpret_cast<void *>(desc->strobe.nameFor)},
        {"strobe", "idFor", reinterpret_cast<void *>(desc->strobe.idFor)},
        {"strobe", "widthFor", reinterpret_cast<void *>(desc->strobe.widthFor)},
        {"strobe", "setState", reinterpret_cast<void *>(desc->strobe.setState)},
        {"strobe", "stopDriving", reinterpret_cast<void *>(desc->strobe.stopDriving)},
        {"strobe", "waitForChange", reinterpret_cast<void *>(desc->strobe.waitForChange)},
    }};

    // check the general mandatory methods
    for(const auto &m : kRequiredMethods) {
        if(!m.ptr) throw DescriptorError("Missing {} callback: {}", m.category, m.name);
    }

    // mandatory methods for time slice execution
    if((desc->threadingMode & kEmulationModeMask) == kEmulationModeTimeSlice) {
        for(const auto &m : kSliceRequiredMethods) {
            if(!m.ptr) throw DescriptorError("Missing {} callback: {}", m.category, m.name);
        }
    }

    // mandatory methods for step execution
    if((desc->threadingMode & kEmulationModeMask) == kEmulationModeStep) {
        for(const auto &m : kStepRequiredMethods) {
            if(!m.ptr) throw DescriptorError("Missing {} callback: {}", m.category, m.name);
        }
    }

    // mandatory methods for cothread execution
    if((desc->threadingMode & kEmulationModeMask) == kEmulationModeCoThread) {
        for(const auto &m : kCothreadRequiredMethods) {
            if(!m.ptr) throw DescriptorError("Missing {} callback: {}", m.category, m.name);
        }
    }

    // mandatory methods for memory bus interface
    if(desc->supported & kSupportsMemory) {
        for(const auto &m : kMemoryRequiredMethods) {
            if(!m.ptr) throw DescriptorError("Missing {} callback: {}", m.category, m.name);
        }
    }

    // mandatory methods for strobe interface
    if(desc->supported & kSupportsStrobe) {
        for(const auto &m : kStrobeRequiredMethods) {
            if(!m.ptr) throw DescriptorError("Missing {} callback: {}", m.category, m.name);
        }
    }
}

// NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)

