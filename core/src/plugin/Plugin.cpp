#include "Plugin.h"
#include "bridge/LogInterface.h"
#include "bridge/RegistryInterface.h"
#include "interface/Info.h"

#include "device/Registry.h"
#include "log/Logger.h"
#include "platform/platform.h"

#include <algorithm>
#include <cstring>

using namespace emulashione::core::plugin;

//NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
uint32_t Plugin::gNextId{0};

/**
 * Attempts to load a plugin from the given path.
 *
 * We'll try to load this as a shared library, try to find the plugin information structure, parse
 * it, and invoke the initializer. If any of these steps fail, this method throws.
 *
 * @param path Filesystem path to the plugin file
 *
 * @throw LoadError Something went wrong while loading plugin information
 */
Plugin::Plugin(const std::filesystem::path &path) : source(Source::File), sourcePath(path),
    identifier(++gNextId) {
    // load the plugin binary and extract info
    this->dylib = std::make_shared<platform::Dylib>(path);
    //NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    this->info = reinterpret_cast<struct emulashione_plugin_info *>(this->dylib->getSymbol(kInfoSymbolName));

    if(!this->info) {
        throw LoadError("Failed to get info symbol (error = {})", this->dylib->getError());
    }

    // parse info
    this->parseInfo();

    // perform plugin initialization
    this->initContext();
}

/**
 * Sets up a plugin whose information structure is already available.
 *
 * @param info Plugin information structure
 * @param isBuiltIn Whether the plugin is built in to the core library
 *
 * @throw LoadError Something went wrong while loading plugin information
 */
Plugin::Plugin(const struct emulashione_plugin_info *_info, const bool isBuiltIn) :
    info(_info), source(isBuiltIn ? Source::BuiltIn : Source::Other) {
    this->parseInfo();

    this->initContext();
}

/**
 * Releases all plugin resources and unloads the module.
 */
Plugin::~Plugin() {
    // ensure we've been shut down cleanly
    XASSERT(this->state != State::Ready,
            "plugin destructed in invalid state: {}", static_cast<int>(this->state));

    // clean up the plugin context
    memset(this->context, 0, sizeof(*this->context));
    delete this->context;
}



/**
 * Parses the loaded information structure; we'll validate it, extract static information, then
 * invoke the plugin's initializer which gives it a chance to export resources.
 */
void Plugin::parseInfo() {
    // validate info struct
    if(this->info->magic != kEmulashionePluginMagic) {
        throw LoadError("Invalid plugin magic ({:016x})", info->magic);
    }
    else if(this->info->infoVersion != 1) {
        throw LoadError("Invalid struct version ({:08x})", info->infoVersion);
    }
    // TODO: check plugin API version
    // TODO: check that all strings are present

    // copy some information
    this->displayName = std::string(info->strings.displayName);
    this->shortName = std::string(info->strings.shortName);
    this->authors = std::string(info->strings.authors);
    this->version = std::string(info->strings.version);
    this->license = std::string(info->strings.license);

    if(info->strings.description) this->description = std::string(info->strings.description);
    if(info->strings.infoUrl) this->infoUrl = std::string(info->strings.infoUrl);

    //NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
    const std::span<uint8_t, 16> uuidBytes{const_cast<uint8_t *>(info->uuid), 16};
    const uuids::uuid uuid{uuidBytes};
    this->uuid = uuid;

    if(uuid.is_nil()) {
        throw LoadError("Invalid plugin uuid");
    }
}

/**
 * Initializes the plugin's context.
 */
void Plugin::initContext() {
    // initialize the basic context
    //NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    auto ctx = new emulashione_plugin_context;
    ctx->contextVersion = 1;
    ctx->identifier = this->identifier;

    ctx->info = this->info;

    // allocate a logger
    this->logger = std::make_shared<plugin::LogInterface>(this);

    //NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    ctx->logger = reinterpret_cast<struct emulashione_logger_handle *>(this->logger.get());
    ctx->loggerInterface = this->logger->getInterface();

    ctx->registryInterface = plugin::RegistryInterface::GetInterface();

    this->context = ctx;
}

/**
 * Invokes the plugins' initialization callback.
 */
void Plugin::invokeInitializers() {
    this->state = State::Initializing;

    int err{0};
    if((err = this->info->initCallback(this->context))) {
        throw LoadError("Initializer for '{}' failed: {}", this->displayName, err);
    }

    this->state = State::Ready;
}

/**
 * Invoke the plugin shutdown handler.
 */
void Plugin::shutdown() noexcept {
    this->state = State::ShuttingDown;

    // notify various components we're about to be unloaded
    device::Registry::The()->pluginWillUnload(this->shared_from_this());

    // invoke shutdown callback
    int err{0};
    if((err = this->info->shutdownCallback())) {
        Logger::Warn("Shutdown callback for plugin '{}' failed: {}", this->displayName, err);
    }
}
