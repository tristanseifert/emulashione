#include "Manager.h"
#include "Plugin.h"

#include "interface/Common.h"
#include "interface/Info.h"

#include "log/Logger.h"

#include <atomic>
#include <mutex>
#include <span>
#include <stdexcept>

using namespace emulashione::core::plugin;

//NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
Manager *Manager::gShared{nullptr};

/**
 * Initializes the shared instance of the plugin manager.
 */
void Manager::Start() {
    //NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    gShared = new Manager;
}

/**
 * Shuts down the plugin manager. This disposes of all instantiated plugin resources and notifies
 * each plugin of the shutdown before releasing resources and unloading plugin binaries.
 */
void Manager::Stop() {
    // shut down all plugins
    gShared->stopPlugins();

    // deallocate plugin manager
    auto old = Manager::gShared;
    Manager::gShared = nullptr;
    std::atomic_thread_fence(std::memory_order_release);
    //NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    delete old;
}



/**
 * Initializes the plugin manager by exporting built-in interfaces.
 */
Manager::Manager() {
    this->exportBuiltinInterfaces();
}

/**
 * Tears down plugin manager resources; first, all instantiated plugin resources are released,
 * plugin shutdown callbacks are invoked, and the plugin binaries are unloaded.
 */
Manager::~Manager() {
    this->stopPlugins();

    // then release all plugin objects
    std::lock_guard<std::recursive_mutex> lg(this->pluginsLock);
    this->plugins.clear();
    this->pluginUuidMap.clear();
}

/**
 * Invokes the shutdown callbacks of all loaded plugins, so they can clean up.
 */
void Manager::stopPlugins() {
    std::call_once(this->stopFlag, [this]() {
        std::lock_guard<std::recursive_mutex> lg(this->pluginsLock);
        for(const auto &[id, plugin] : this->plugins) {
            plugin->shutdown();
        }
    });
}

/**
 * Converts the provided plugin context to a smart pointer to the plugin object that the context
 * belongs to.
 *
 * @note This has to acquire the plugin list lock; so avoid using it in hot paths.
 *
 * @param context Pointer to the plugin context value
 * @return Plugin that the context belongs to, or `nullptr` if is not for a loaded plugin
 */
std::shared_ptr<Plugin> Manager::getPlugin(const struct emulashione_plugin_context *context) {
    // validate args
    XASSERT(!!context, "context ptr may not be null");
    const auto id = context->identifier;
    XASSERT(!!id, "invalid identifier: {}", id);

    // read out from the list
    std::lock_guard<std::recursive_mutex> lg(this->pluginsLock);
    return this->plugins.at(id);
}

/**
 * Registers the given plugin. This will check all other plugins to ensure none have been loaded
 * from the same path, nor that they match UUIDs.
 *
 * @param plug Plugin to register
 *
 * @return Whether the plugin was registered; if `false`, it was a duplicate.
 */
bool Manager::registerPlugin(const std::shared_ptr<Plugin> &plug) {
    std::lock_guard<std::recursive_mutex> lg(this->pluginsLock);

    // ensure there's no plugin with this uuid already
    if(this->pluginUuidMap.contains(plug->getUuid())) {
        throw AlreadyLoaded();
    }

    this->pluginUuidMap.emplace(plug->getUuid(), plug->getIdentifier());
    this->plugins.emplace(plug->getIdentifier(), std::move(plug));

    return true;
}

/**
 * Unregisters the given plugin.
 *
 * @note This does nothing to ensure all interfaces in this plugin have been removed, nor that any
 * pointers to data in the plugin (by other code) are invalidated.
 *
 * @param plug Reference to plugin
 */
void Manager::unregisterPlugin(const std::shared_ptr<Plugin> &plug) {
    std::lock_guard<std::recursive_mutex> lg(this->pluginsLock);

    XASSERT(this->plugins.contains(plug->getIdentifier()), "failed to remove plugin: ${:x}",
            plug->getIdentifier());

    this->pluginUuidMap.erase(plug->getUuid());
    this->plugins.erase(plug->getIdentifier());
}

/**
 * Exports the interfaces built in to the library.
 * 
 * @remark The actual table accessed by this method is declared in BuiltinInterfaces.cpp.
 */
void Manager::exportBuiltinInterfaces() {
    std::lock_guard<std::recursive_mutex> lg(this->interfacesLock);
    this->builtinInterfaces.reserve(kNumBuiltinInterfaces);

    for(const auto &intf : kBuiltinInterfaces) {
        auto info = std::make_shared<Plugin::Interface>(intf.header);

        this->interfaces.emplace(intf.uuid, info);
        this->builtinInterfaces.emplace_back(std::move(info));
    }

    Logger::Debug("Registered {} builtin interface(s)", this->builtinInterfaces.size());
}



/**
 * Looks up an interface by its UUID. The interface must exist, and the plugin that it came from
 * should be able to be referenced (meaning it's still loaded).
 *
 * @param uuid Identifier of the interface
 *
 * @return Pointer to the interface descriptor or `nullptr` if not found/invalid
 */
std::shared_ptr<Plugin::Interface> Manager::getInterface(const uuids::uuid &uuid) {
    std::lock_guard<std::recursive_mutex> lg(this->interfacesLock);

    // get the pointer pls
    if(!this->interfaces.contains(uuid)) return nullptr;
    auto intf = this->interfaces.at(uuid).lock();
    if(!intf) return nullptr;

    // TODO: ensure the plugin is still loaded (can this be done in a non-race-y way?)
    return intf;
}

/**
 * Registers the given (plugin, interface) pair under the UUID specified in the interface
 * descriptor. Duplicate UUIDs are not permitted.
 *
 * @throw DuplicateInterface If the interface is a duplicate
 * @throw std::invalid_argument The provided parameters are invalid
 *
 * @param plugin Plugin that the interface is defined in
 * @param interface Pointer to the header of the interface
 */
void Manager::registerInterface(const std::shared_ptr<Plugin> &plug,
        const struct emulashione_interface_header *interface) {
    // validate args
    if(!plug || !interface) {
        throw std::invalid_argument("Plugin and interface may not be null");
    }

    //NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
    const std::span<uint8_t, 16> uuidBytes{const_cast<uint8_t *>(interface->uuid), 16};
    const uuids::uuid uuid{uuidBytes};

    // check that the interface hasn't been registered already
    std::lock_guard<std::recursive_mutex> lg(this->interfacesLock);
    if(this->interfaces.contains(uuid)) {
        throw DuplicateInterface();
    }

    // create descriptor and set. no additional per-plugin lock needed as we hold the global
    // plugin list lock
    auto intf = std::make_shared<Plugin::Interface>(interface, plug);
    XASSERT(!!intf, "failed to allocate interface descriptor");

    Logger::Trace("Registering interface {} from plugin '{}' (source: {})", uuid,
            plug->getShortName(), plug->getSource());

    plug->exportedInterfaces.emplace(uuid, intf);
    this->interfaces.emplace(uuid, std::move(intf));
}

/**
 * Unregisters the interface identified by its uuid.
 *
 * @note This does not check nor ensure that any outstanding references or instances of the
 * interface have been invalidated; only that subsequent calls to `getInterface()` with this UUID
 * will fail.
 *
 * @param uuid Identifier of the interface
 * @param plug Plugin that originally registered the interface
 *
 * @throw NoSuchInterface The interface does not exist
 * @throw NotOwner The device was registered from a different plugin
 * @throw std::invalid_argument Parameters are invalid
 */
void Manager::unregisterInterface(const uuids::uuid &uuid,
        const std::shared_ptr<Plugin> &plug) {
    // validate args
    if(uuid.is_nil() || !plug) {
        throw std::invalid_argument("Invalid uuid or plugin pointer");
    }

    // ensure it exists and validate the owner
    std::lock_guard<std::recursive_mutex> lg(this->interfacesLock);
    if(!this->interfaces.contains(uuid)) {
        throw NoSuchInterface();
    }

    auto intf = this->interfaces.at(uuid).lock();
    if(!intf) {
        // XXX: does this need different handling? (plugin went away, interface didn't get unregistered)
        throw NoSuchInterface();
    }

    auto owner = intf->owner.lock();
    if(owner && owner != plug) {
        // allow a null owner to be ignored (to allow cleanup)
        throw NotOwner();
    }

    // we can get rid of it now :D
    this->interfaces.erase(uuid);
    plug->exportedInterfaces.erase(uuid);
}

