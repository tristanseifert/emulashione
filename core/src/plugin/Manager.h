#ifndef PLUGIN_MANAGER_H
#define PLUGIN_MANAGER_H

#include "Plugin.h"

#include <array>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <mutex>
#include <stdexcept>
#include <unordered_map>

#include <uuid.h>

struct emulashione_interface_header;

namespace emulashione::core::plugin {
/**
 * Serves as the core of the plugin system; all plugins that are loaded (whether by discovery or
 * manually) are registered here, as well as all of the interfaces and resources they export.
 *
 * @brief Registry of all loaded plugins and their interfaces
 */
class Manager {
    public:
        /// A plugin with the specified UUID has already been registered
        struct AlreadyLoaded: public std::runtime_error {
            AlreadyLoaded() : std::runtime_error("Plugin with this UUID already loaded") {};
        };
        /// An interface with the specified UUID has already been registered
        struct DuplicateInterface: public std::runtime_error {
            DuplicateInterface() : std::runtime_error("An interface with this id already exists") {};
        };
        /// No interface with the given UUID exists
        struct NoSuchInterface: public std::runtime_error {
            NoSuchInterface() : std::runtime_error("Unknown interface uuid") {};
        };
        /// The caller is different from the one that registered this plugin
        struct NotOwner: public std::runtime_error {
            NotOwner() : std::runtime_error("You did not register this plugin!") {};
        };

    public:
        /// Return the shared plugin manager instance
        inline static Manager *The() {
            return gShared;
        }

        /// Initialize the plugin manager.
        static void Start();
        /// Shut down the plugin manager.
        static void Stop();

        ~Manager();

        /// Return the number of currently loaded plugins.
        size_t numPlugins() {
            std::lock_guard<std::recursive_mutex> lg(this->pluginsLock);
            return this->plugins.size();
        }

        /// Converts a plugin context pointer to a plugin instance
        std::shared_ptr<Plugin> getPlugin(const struct ::emulashione_plugin_context *context);
        /// Registers a new plugin.
        bool registerPlugin(const std::shared_ptr<Plugin> &plug);
        /// Unregisters a plugin
        void unregisterPlugin(const std::shared_ptr<Plugin> &plug);

        /// Looks up an interface by UUID
        std::shared_ptr<Plugin::Interface> getInterface(const uuids::uuid &uuid);
        /// Registers an interface exposed by a particular plugin.
        void registerInterface(const std::shared_ptr<Plugin> &plug,
                const struct ::emulashione_interface_header *interface);
        /// Unregisters an interface
        void unregisterInterface(const uuids::uuid &uuid, const std::shared_ptr<Plugin> &plug);

    private:
        /// Defines a single built in interface.
        struct BuiltinInterface {
            uuids::uuid uuid;
            const struct ::emulashione_interface_header *header{nullptr};
        };

    private:
        Manager();

        void stopPlugins();
        void exportBuiltinInterfaces();

    private:
        // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
        static Manager *gShared;

        /// total number of built in interfaces
        constexpr static const size_t kNumBuiltinInterfaces{8};
        /// list of all built in interfaces
        static const std::array<BuiltinInterface, kNumBuiltinInterfaces> kBuiltinInterfaces;

        /// lock protecting access to plugins
        std::recursive_mutex pluginsLock;
        /// All currently loaded plugins
        std::unordered_map<uint32_t, std::shared_ptr<Plugin>> plugins;
        /// Mapping of plugin uuid -> plugin id
        std::unordered_map<uuids::uuid, uint32_t> pluginUuidMap;

        /// Lock protecting the interfaces map
        std::recursive_mutex interfacesLock;
        /// All exported interfaces
        std::unordered_map<uuids::uuid, std::weak_ptr<Plugin::Interface>> interfaces;
        /// Interfaces that are built in; we must hold a strong reference to their interfaces
        std::vector<std::shared_ptr<Plugin::Interface>> builtinInterfaces;

        /// flag indicating plugins have been stopped
        std::once_flag stopFlag;
};
}

#endif
