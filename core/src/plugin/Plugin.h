#ifndef PLUGIN_PLUGIN_H
#define PLUGIN_PLUGIN_H

#include <array>
#include <cstddef>
#include <cstdint>
#include <filesystem>
#include <memory>
#include <mutex>
#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>
#include <unordered_map>

#include <uuid.h>
#include <fmt/core.h>

struct emulashione_plugin_info;
struct emulashione_plugin_context;
struct emulashione_interface_header;

namespace emulashione::core {

namespace platform {
class Dylib;
}
namespace device {
class Info;
class Registry;
}

namespace plugin {
class LogInterface;

/**
 * Represents a single plugin, including information about where it was loaded from, some metadata
 * provided by the plugin authors, and its exported interfaces, devices and resources. It can be
 * used to instantiate any of these resources, or to simply query information provided which is
 * populated during loading.
 *
 * All plugins start off with a basic plugin record that's populated from static data loaded out
 * of a structure in the plugin binary; the plugin's initializers can then export devices or modify
 * information.
 *
 * If the plugin was created by opening a file, rather than by directly providing the information
 * structure, deallocating this object will also unload the file.
 *
 * @brief Information about a single loaded plugin
 */
class Plugin: public std::enable_shared_from_this<Plugin> {
    friend class device::Registry;
    friend class Loader;
    friend class Manager;

    public:
        /// Indicates an error occurred during plugin loading.
        class LoadError: public std::exception {
            friend class Plugin;

            private:
                /// Create a load error with no detail string.
                LoadError() = default;

                /// Create a load error with the given format string.
                template<typename... Args>
                LoadError(fmt::format_string<Args...> fmt, Args &&...args) {
                    this->message = fmt::format(fmt, std::forward<Args>(args)...);
                }

            public:
                const char *what() const noexcept override {
                    return this->message.c_str();
                }

            private:
                std::string message;
        };

        /// Defines an interface exported by a plugin.
        struct Interface {
            /// Pointer to the plugin that defined this interface
            std::weak_ptr<Plugin> owner;
            /// Header of the interface (followed by the entire interface)
            const struct emulashione_interface_header *interface{nullptr};

            /// Returns pointer to the interface structure
            constexpr auto getInterfacePtr() const {
                return this->interface;
            }

            Interface() = default;
            Interface(const struct emulashione_interface_header *_interface) : 
                interface(_interface) {}
            Interface(const struct emulashione_interface_header *_interface,
                    const std::weak_ptr<Plugin> &_owner) : owner(_owner), interface(_interface) {}
        };

        /// Defines the source from which a plugin was loaded
        enum class Source {
            /// The plugin was loaded directly from a file
            File,
            /// The plugin is built in to the library
            BuiltIn,
            /// The plugin information was obtained through other means
            Other
        };

        /// State of the plugin
        enum class State {
            /// Loaded, but not yet initialized
            Pending,
            /// Initialization has begun
            Initializing,
            /// Initialization is complete
            Ready,
            /// Unloaded process has begun
            ShuttingDown,
        };

    public:
        /// Create a new plugin by loading the provided file
        Plugin(const std::filesystem::path &path);
        /// Create a new plugin from the provided information structure
        Plugin(const struct emulashione_plugin_info *info, const bool isBuiltIn = false);

        /// Deallocate and unload the plugin
        ~Plugin();

        void shutdown() noexcept;

        /// Get the plugin state; useful to see if initialization is complete
        constexpr auto getState() const {
            return this->state;
        }

        /// Returns the plugin identifier
        constexpr uint32_t getIdentifier() const {
            return this->identifier;
        }
        /// Return the plugin uuid
        constexpr auto getUuid() const {
            return this->uuid;
        }

        /// Return the display name of the plugin
        constexpr const auto &getDisplayName() const {
            return this->displayName;
        }
        /// Return the short name of the plugin
        constexpr const auto &getShortName() const {
            return this->shortName;
        }
        /// Return the author string
        constexpr const auto &getAuthor() const {
            return this->authors;
        }
        /// Return the description string
        constexpr const auto &getDescription() const {
            return this->description;
        }
        /// Return the information URL
        const std::optional<std::string> getInfoUrl() const {
            if(this->infoUrl.empty()) return nullptr;
            return this->infoUrl;
        }
        /// Return the version string
        constexpr const auto &getVersion() const {
            return this->version;
        }
        /// Return the license string
        constexpr const auto &getLicense() const {
            return this->license;
        }

        /// Returns information string on where the plugin came from
        const std::string getSource() const {
            switch(this->source) {
                case Source::File:
                    return this->sourcePath.string();
                case Source::BuiltIn:
                    return "Built In";
                default:
                    return "Unknown";
            }
        }

    private:
        void parseInfo();
        void initContext();
        void invokeInitializers();

    private:
        /// Pointer to this plugin's info structure (for callbacks)
        const struct emulashione_plugin_info *info{nullptr};
        /// Allocated plugin context structure
        struct emulashione_plugin_context *context{nullptr};

        /// Where from the plugin was loaded
        Source source;
        /// If the plugin was loaded from a file, its path
        std::filesystem::path sourcePath;
        /// If the plugin was loaded from a file, the dynamic object backing it
        std::shared_ptr<platform::Dylib> dylib;

        /// internal identifier (load order)
        uint32_t identifier{UINT32_MAX};
        /// plugin status
        State state{State::Pending};

        /// Display name for the plugin
        std::string displayName;
        /// Short "tag" type name for the plugin
        std::string shortName;
        /// Author name(s)
        std::string authors;
        /// Long form description of the plugin
        std::string description;
        /// URL for detailed plugin information
        std::string infoUrl;
        /// Plugin version string
        std::string version;
        /// Plugin license
        std::string license;

        /// unique identifier of this plugin
        uuids::uuid uuid;

        /// logger interface created during initialization for this plugin
        std::shared_ptr<plugin::LogInterface> logger;

        /// All interfaces exported by this plugin
        std::unordered_map<uuids::uuid, std::shared_ptr<Plugin::Interface>> exportedInterfaces;
        /// All devices exported by this plugin
        std::unordered_map<uuids::uuid, std::shared_ptr<device::Info>> exportedDevices;

    private:
        /// Name of the symbol that holds the plugin information
        constexpr static const std::string_view kInfoSymbolName{"_EmulashionePluginInfo"};

        // next plugin id to use
        // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
        static uint32_t gNextId;
};
}
}

#endif
