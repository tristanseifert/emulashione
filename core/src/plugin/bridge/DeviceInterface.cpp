#include "DeviceInterface.h"

#include "device/Cache.h"
#include "device/Device.h"
#include "device/Info.h"
#include "device/Registry.h"
#include "plugin/Manager.h"
#include "system/System.h"
#include "../interface/Device.h"

#include "log/Logger.h"

#include <emulashione.h>

#include <span>

using namespace emulashione::core::device;
using namespace emulashione::core::plugin;

#ifndef DOXYGEN_SHOULD_SKIP_THIS

const struct emulashione_device_interface DeviceInterface::gInterface {
    .header = {
        .uuid = {0x79, 0x4F, 0xAA, 0x0F, 0x26, 0x94, 0x47, 0xCF, 0xAB, 0x40, 0xF5, 0xB8,
            0x4E, 0x4F, 0x8B, 0x51},
        .revision = 1,
    },

    .getDevice = [](auto uuidBytes) -> const struct emulashione_device_descriptor * {
        // validate arguments
        if(!uuidBytes) return nullptr;

        //NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
        const std::span<uint8_t, 16> uuid{const_cast<uint8_t *>(uuidBytes), 16};
        return DeviceInterface::Get(uuid);
    },
    .getMatchingDevices = DeviceInterface::GetMatching,
    .registerDevice = [](auto plug, auto device, auto deviceBytes) -> int {
        if(!plug || !device || deviceBytes < sizeof(emulashione_device_descriptor)) {
            return kEmulashioneApiMisuse;
        }

        try {
            DeviceInterface::Register(plug, device);
        } catch(const Registry::DuplicateDevice &) {
            return kEmulashioneDeviceExists;
        } catch(const Info::DescriptorError &e) {
            Logger::Warn("Failed to register device {} due to invalid descriptor: {}",
                    device->strings.shortName, e.what());
            return kEmulashioneInvalidDevice;
        } catch(const std::exception &e) {
            Logger::Warn("Failed to register device {} due to unknown error: {}",
                    device->strings.shortName, e.what());
            return kEmulashioneUnknownError;
        }
        return kEmulashioneSuccess;
    },
    .unregisterDevice = [](auto plug, auto uuidBytes) -> int {
        // validate arguments
        if(!plug || !uuidBytes) return kEmulashioneApiMisuse;

        //NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
        const std::span<uint8_t, 16> uuid{const_cast<uint8_t *>(uuidBytes), 16};
        try {
            DeviceInterface::UnRegister(plug, uuid);
        } catch(const Registry::NoSuchDevice &) {
            return kEmulashioneNonexistentDevice;
        } catch(const Registry::NotOwner &) {
            return kEmulashioneDeviceOwnerMismatch;
        } catch(const std::exception &e) {
            return kEmulashioneUnknownError;
        }
        return kEmulashioneSuccess;
    },

    .getDescriptor = [](auto device) -> const struct emulashione_device_descriptor * {
        if(!device) return nullptr;
        return device->descriptor;
    },

    .abort = [](auto device, auto why) -> int {
        auto dev = device::Cache::The()->get(device);
        if(!device) {
            return kEmulashioneNoSuchDevice;
        }

        auto sys = dev->getSystem();
        if(!sys) {
            Logger::Error("Failed to get system for device {} ({})!", (void *) device,
                    dev->getName());
            return kEmulashioneUnknownError;
        }

        sys->abortEmulation(why);
        return kEmulashioneSuccess;
    }
};

#endif

// XXX: it would be nice not to have to repeat the UUID bytes here...
const uuids::uuid DeviceInterface::kInterfaceUuid{{0x79, 0x4F, 0xAA, 0x0F, 0x26, 0x94, 0x47, 0xCF,
    0xAB, 0x40, 0xF5, 0xB8, 0x4E, 0x4F, 0x8B, 0x51}};



/**
 * Retrieves a device descriptor by uuid, if it exists.
 */
//const struct emulashione_device_descriptor *DeviceInterface::Get(const uint8_t uuidBytes[16]) {
const struct emulashione_device_descriptor *DeviceInterface::Get(const UuidBytes uuidBytes) {
    uuids::uuid id{uuidBytes};

    auto info = Registry::The()->getDevice(id);
    if(info) {
        return info->getDescriptor();
    }

    return nullptr;
}

/**
 * Searches for devices matching the given predicate.
 */
// NOLINTNEXTLINE(cert-dcl50-cpp) required for C interop
int DeviceInterface::GetMatching(const struct emulashione_device_descriptor **outDevices,
        const size_t maxDevices, const char *predicate, ...) {
    // validate args
    if((outDevices && !maxDevices) || (!outDevices && maxDevices) || !predicate) {
        return kEmulashioneApiMisuse;
    }

    // TODO: Implement

    return kEmulashioneUnknownError;
}

/**
 * Registers a device.
 */
void DeviceInterface::Register(struct emulashione_plugin_context *pluginCtx,
        const struct emulashione_device_descriptor *device) {
    // resolve plugin
    auto plugin = Manager::The()->getPlugin(pluginCtx);
    if(!plugin) {
        // TODO: Convert to the appropriate exception to signal this error
        throw std::runtime_error("Invalid plugin");
    }

    Registry::The()->registerDevice(plugin, device);
 
}

/**
 * Unregisters a device.
 */
void DeviceInterface::UnRegister(struct emulashione_plugin_context *pluginCtx,
        const UuidBytes uuidBytes) {
    uuids::uuid uuid{uuidBytes};

    // resolve plugin
    auto plugin = Manager::The()->getPlugin(pluginCtx);
    if(!plugin) {
        // TODO: Convert to the appropriate exception to signal this error
        throw std::runtime_error("Invalid plugin");
    }

    Registry::The()->unregisterDevice(uuid, plugin);
}

