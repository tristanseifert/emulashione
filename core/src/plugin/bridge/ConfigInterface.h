#ifndef PLUGIN_BRIDGE_CONFIGINTERFACE_H
#define PLUGIN_BRIDGE_CONFIGINTERFACE_H

#include <boost/property_tree/ptree_fwd.hpp>
#include <uuid.h>

struct emulashione_plugin_context;
struct emulashione_config_interface;
struct emulashione_config_object;

namespace emulashione::core::plugin {
/**
 * Provides an interface to allowing plugins to read their configuration sections in the config
 * file, as well as more generally providing a general key/value store.
 *
 * @brief Exported interface for access to configuration and generic key/value data
 */
class ConfigInterface {
    friend class Manager;

    public:
        static struct emulashione_config_object *MakeConfig(const boost::property_tree::ptree &);
        static void DeallocConfig(struct emulashione_config_object *);

        /// UUID of the interface
        static const uuids::uuid kInterfaceUuid;

    private:
        /// Shared interface definition
        static const struct ::emulashione_config_interface gInterface;
};
}

#endif
