#include "LogInterface.h"
#include "plugin/Plugin.h"
#include "../interface/Log.h"

#include "log/Logger.h"

#include <emulashione.h>

#include <array>
#include <gsl/gsl_util>

using namespace emulashione::core::plugin;

#ifndef DOXYGEN_SHOULD_SKIP_THIS

/**
 * Define the interface descriptor for the log interface. This provides the thunks necessary to
 * call back into the C++ code.
 *
 * All methods that have an `emulashione_logger_handle` as the first argument are a "disguised"
 * pointer to the `LogInterface` class.
 */
const struct emulashione_logger_interface LogInterface::gInterface {
    .header = {
        .uuid = {0xfc, 0x59, 0x9f, 0xe6, 0x87, 0x39, 0x4c, 0xb9,
            0xad, 0xb7, 0x6b, 0x47, 0x57, 0x88, 0xea, 0x78},
        .revision = 1,
    },

    .setLogLevel = LogInterface::_Thunk_SetLogLevel,
    .getLogLevel = LogInterface::_Thunk_GetLogLevel,

    .logTrace = LogInterface::_Thunk_Trace,
    .logDebug = LogInterface::_Thunk_Debug,
    .logInfo = LogInterface::_Thunk_Info,
    .logWarning = LogInterface::_Thunk_Warning,
    .logError = LogInterface::_Thunk_Error,
    .logCritical = LogInterface::_Thunk_Critical,
};

#endif

// XXX: it would be nice not to have to repeat the UUID bytes here...
const uuids::uuid LogInterface::kInterfaceUuid{{0xfc, 0x59, 0x9f, 0xe6, 0x87, 0x39, 0x4c, 0xb9,
    0xad, 0xb7, 0x6b, 0x47, 0x57, 0x88, 0xea, 0x78}};

/**
 * Initializes the logging interface by allocating a logger with the specified short name; if it
 * is not specified, we'll use the plugin short name instead.
 */
LogInterface::LogInterface(Plugin *plugin, const std::string &_tag) {
    const std::string tag = _tag.empty() ? plugin->getShortName() : _tag;

    this->logger = Logger::The()->makeLogger(tag);
}


/**
 * Return the current log level.
 */
int LogInterface::_Thunk_GetLogLevel(struct ::emulashione_logger_handle *ctx) {
    XASSERT(!!ctx, "invalid logger handle");

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto l = reinterpret_cast<LogInterface *>(ctx);
    return ConvertLogLevel(l->logger->level());
}

/**
 * Updates the current log level.
 */
int LogInterface::_Thunk_SetLogLevel(struct ::emulashione_logger_handle *ctx,
        const unsigned int newLevel) {
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
    if(!ctx || newLevel > 5) {
        return kEmulashioneApiMisuse;
    }

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto l = reinterpret_cast<LogInterface *>(ctx);
    l->logger->set_level(ConvertLogLevel(newLevel));

    return kEmulashioneSuccess;
}

/**
 * Emits a trace message to the logger.
 */
void LogInterface::_Thunk_Trace(struct ::emulashione_logger_handle *ctx, const char *msg) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    reinterpret_cast<LogInterface *>(ctx)->logger->trace("{}", msg);
}

/**
 * Emits a debug message to the logger.
 */
void LogInterface::_Thunk_Debug(struct ::emulashione_logger_handle *ctx, const char *msg) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    reinterpret_cast<LogInterface *>(ctx)->logger->debug("{}", msg);
}

/**
 * Emits an info message to the logger.
 */
void LogInterface::_Thunk_Info(struct ::emulashione_logger_handle *ctx, const char *msg) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    reinterpret_cast<LogInterface *>(ctx)->logger->info("{}", msg);
}

/**
 * Emits a warning message to the logger.
 */
void LogInterface::_Thunk_Warning(struct ::emulashione_logger_handle *ctx, const char *msg) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    reinterpret_cast<LogInterface *>(ctx)->logger->warn("{}", msg);
}

/**
 * Emits an error message to the logger.
 */
void LogInterface::_Thunk_Error(struct ::emulashione_logger_handle *ctx, const char *msg) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    reinterpret_cast<LogInterface *>(ctx)->logger->error("{}", msg);
}

/**
 * Emits a critical message to the logger.
 */
void LogInterface::_Thunk_Critical(struct ::emulashione_logger_handle *ctx, const char *msg) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    reinterpret_cast<LogInterface *>(ctx)->logger->critical("{}", msg);
}



/**
 * Converts an integer log level to the appropriate enum value.
 */
spdlog::level::level_enum LogInterface::ConvertLogLevel(const unsigned int x) {
    XASSERT(x < 6, "invalid log level {}", x);

    using level = spdlog::level::level_enum;
    static const std::array<level, 6> kLevelMap{{
        level::trace, level::debug, level::info, level::warn, level::err, level::critical
    }};

    return gsl::at(kLevelMap, x);
}

/**
 * Converts a log level enum value to an integer.
 */
unsigned int LogInterface::ConvertLogLevel(const spdlog::level::level_enum x) {
    using level = spdlog::level::level_enum;

    switch(x) {
        case level::trace:
            // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
            return 0;
        case level::debug:
            // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
            return 1;
        case level::info:
            // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
            return 2;
        case level::warn:
            // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
            return 3;
        case level::err:
            // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
            return 4;
        case level::critical:
            // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
            return 5;

        default:
            return 0;
    }
}
