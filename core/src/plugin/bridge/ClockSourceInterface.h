#ifndef PLUGIN_BRIDGE_CLOCKSOURCEINTERFACE_H
#define PLUGIN_BRIDGE_CLOCKSOURCEINTERFACE_H

#include <uuid.h>

struct emulashione_clock_source_interface;

namespace emulashione::core::plugin {
/// Exported interface for interacting with clock sources
class ClockSourceInterface {
    friend class Manager;

    public:
        /// UUID of the interface
        static const uuids::uuid kInterfaceUuid;

    private:
        /// Shared interface definition
        static const struct ::emulashione_clock_source_interface gInterface;
};
}

#endif
