#include "SchedulerInterface.h"
#include "../interface/Scheduler.h"

#include "device/Bus.h"
#include "device/Cache.h"
#include "device/Device.h"
#include "device/Wrappers.h"
#include "log/Logger.h"
#include "system/Wrappers.h"
#include "system/sync/Scheduler.h"

#include <emulashione.h>

using namespace emulashione::core::plugin;

#ifndef DOXYGEN_SHOULD_SKIP_THIS

const struct emulashione_scheduler_interface SchedulerInterface::gInterface {
    .header = {
        .uuid = {
            0xF1, 0x2F, 0x67, 0xEC, 0xC1, 0xCC, 0x4B, 0xE2, 0x81, 0x55, 0x1C, 0xFE,
            0x9A, 0xC6, 0x07, 0xC3},
        .revision = 1,
    },

    .setRefClock = [](auto schedPtr, auto devPtr, auto clkPtr) -> int {
        if(!schedPtr || !devPtr || !clkPtr) return -1;

        auto device = device::Cache::The()->get(devPtr);
        if(!device) return -1;

        auto &sched = schedPtr->p;
        auto &clk = clkPtr->ptr;

        try {
            sched->setDeviceRefClock(device, clk);
        } catch(const std::exception &e) {
            Logger::Error("Scheduler method {} failed: {}", __PRETTY_FUNCTION__, e.what());
            return -1;
        }
        return 0;
    },
    .yieldTicks = [](auto schedPtr, auto devPtr, auto ticks, auto outTime) {
        if(!schedPtr || !devPtr) {
            Logger::Error("Scheduler method {} failed: {}", __PRETTY_FUNCTION__, "illegal arguments");
            return;
        }

        auto &sched = schedPtr->p;

        try {
            sched->yield(ticks, outTime);
        } catch(const std::exception &e) {
            Logger::Error("Scheduler method {} failed: {}", __PRETTY_FUNCTION__, e.what());
        }
    },
};

#endif

const uuids::uuid SchedulerInterface::kInterfaceUuid{{
    0xF1, 0x2F, 0x67, 0xEC, 0xC1, 0xCC, 0x4B, 0xE2, 0x81, 0x55, 0x1C, 0xFE, 0x9A, 0xC6, 0x07, 0xC3
}};


