#ifndef PLUGIN_BRIDGE_REGISTRYINTERFACE_H
#define PLUGIN_BRIDGE_REGISTRYINTERFACE_H

#include <cstdint>
#include <cstddef>
#include <memory>
#include <span>
#include <string>

#include <uuid.h>

struct emulashione_interface_header;
struct emulashione_plugin_context;
struct emulashione_registry_interface;

namespace emulashione::core::plugin {
/**
 * Allows plugins to look up interfaces (by their UUIDs) as well as export interfaces implemented
 * inside them.
 *
 * This is just a thin wrapper around the plugin manager's internal interface registry.
 *
 * @brief Exported interface to allow looking up other interfaces
 */
class RegistryInterface {
    friend class Manager;

    public:
        using UuidBytes = std::span<uint8_t, 16>;

        static const struct emulashione_interface_header *GetInterface(const UuidBytes &uuid);

        static void RegisterInterface(struct ::emulashione_plugin_context *plugin,
                const struct ::emulashione_interface_header *interface);
        static void UnRegisterInterface(struct ::emulashione_plugin_context *plugin,
                const UuidBytes &uuid);

        static const struct ::emulashione_registry_interface *GetInterface() {
            return &gInterface;
        }

        /// UUID of the interface
        static const uuids::uuid kInterfaceUuid;

    private:
        /// Shared interface definition
        static const struct ::emulashione_registry_interface gInterface;
};
}

#endif
