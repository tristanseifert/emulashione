#ifndef PLUGIN_BRIDGE_TRACEMANAGERINTERFACE_H
#define PLUGIN_BRIDGE_TRACEMANAGERINTERFACE_H

#include <uuid.h>

struct emulashione_trace_manager_interface;

namespace emulashione::core::plugin {
/// Exported interface for working with trace managers
class TraceManagerInterface {
    friend class Manager;

    public:
        /// UUID of the interface
        static const uuids::uuid kInterfaceUuid;

    private:
        /// Interface definition
        static const struct ::emulashione_trace_manager_interface gInterface;
};
}

#endif
