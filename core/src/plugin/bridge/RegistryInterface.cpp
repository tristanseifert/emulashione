#include "RegistryInterface.h"
#include "../Plugin.h"
#include "../Manager.h"
#include "../interface/Registry.h"

#include "log/Logger.h"

#include <emulashione.h>

using namespace emulashione::core::plugin;

#ifndef DOXYGEN_SHOULD_SKIP_THIS

const struct emulashione_registry_interface RegistryInterface::gInterface {
    .header = {
        .uuid = {0x59, 0x7F, 0x89, 0x4D, 0x6E, 0x0A, 0x40, 0xB6, 0xB0, 0x13, 0x2C, 0x8D,
            0x6B, 0x36, 0xE4, 0x4C},
        .revision = 1,
    },

    .getInterface = [](auto uuidBytes, auto) -> const struct emulashione_interface_header * {
        if(!uuidBytes) return nullptr;
        //NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
        const std::span<uint8_t, 16> uuid{const_cast<uint8_t *>(uuidBytes), 16};
        return RegistryInterface::GetInterface(uuid);
    },
    .registerInterface = [](auto plug, auto interface, auto interfaceSz) -> int {
        // validate args
        if(!plug || !interface || interfaceSz < sizeof(emulashione_interface_header)) {
            return kEmulashioneApiMisuse;
        }

        try {
            RegisterInterface(plug, interface);
        } catch(const Manager::DuplicateInterface &) {
            return kEmulashioneInterfaceExists;
        } catch(const std::exception &e) {
            Logger::Warn("Failed to register interface: {}", e.what());
            return kEmulashioneUnknownError;
        }
        return kEmulashioneSuccess;
    },
    .unregisterInterface = [](auto plug, auto uuidBytes) -> int {
        // validate args
        if(!plug || !uuidBytes) {
            return kEmulashioneApiMisuse;
        }
        //NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
        const std::span<uint8_t, 16> uuid{const_cast<uint8_t *>(uuidBytes), 16};

        try {
            UnRegisterInterface(plug, uuid);
        } catch(const Manager::NotOwner &) {
            return kEmulashioneInterfaceOwnerMismatch;
        } catch(const Manager::NoSuchInterface &) {
            return kEmulashioneNoSuchInterface;
        } catch(const std::exception &e) {
            Logger::Warn("Failed to unregister interface: {}", e.what());
            return kEmulashioneUnknownError;
        }

        return kEmulashioneSuccess;
    },
};

// XXX: it would be nice not to have to repeat the UUID bytes here...
const uuids::uuid RegistryInterface::kInterfaceUuid{{0x59, 0x7F, 0x89, 0x4D, 0x6E, 0x0A, 0x40,
    0xB6, 0xB0, 0x13, 0x2C, 0x8D, 0x6B, 0x36, 0xE4, 0x4C}};

#endif

/**
 * Looks up an interface based on its uuid.
 */
const struct emulashione_interface_header *RegistryInterface::GetInterface(const UuidBytes &uuidBytes) {
    uuids::uuid id{uuidBytes};

    auto intf = Manager::The()->getInterface(id);
    if(!intf) return nullptr;

    return intf->interface;
}

/**
 * Attempt to register the interface.
 */
void RegistryInterface::RegisterInterface(struct emulashione_plugin_context *pluginCtx,
        const struct emulashione_interface_header *interface) {
    // resolve plugin ptr
    auto plugin = Manager::The()->getPlugin(pluginCtx);
    if(!plugin) {
        // TODO: Convert to the appropriate exception to signal this error
        throw std::runtime_error("Invalid plugin");
    }

    // attempt registration
    Manager::The()->registerInterface(plugin, interface);
}

/**
 * Removes an existing registration for an interface.
 *
 * @note The plugin that registered the interface must be the one to unregister it.
 *
 * @param plugin Plugin context of the caller
 * @param uuidBytes Pointer to 16 bytes of UUID data
 *
 * @return 0 if interface was unregistered, error code otherwise.
 */
void RegistryInterface::UnRegisterInterface(struct emulashione_plugin_context *pluginCtx,
        const UuidBytes &uuidBytes) {
    uuids::uuid id{uuidBytes};

    // resolve plugin ptr
    auto plugin = Manager::The()->getPlugin(pluginCtx);
    if(!plugin) {
        // TODO: Convert to the appropriate exception to signal this error
        throw std::runtime_error("Invalid plugin");
    }

    // request unregistration
    Logger::Debug("Unregistering interface {} (registered by {})", id, plugin->getShortName());

    Manager::The()->unregisterInterface(id, plugin);
}
