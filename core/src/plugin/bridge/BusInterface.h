#ifndef PLUGIN_BRIDGE_BUSINTERFACE_H
#define PLUGIN_BRIDGE_BUSINTERFACE_H

#include <uuid.h>

struct emulashione_bus_interface;

namespace emulashione::core::plugin {
/// Exported interface for interacting with busses
class BusInterface {
    friend class Manager;

    public:
        /// UUID of the interface
        static const uuids::uuid kInterfaceUuid;

    private:
        /// Shared interface definition
        static const struct ::emulashione_bus_interface gInterface;
};
}

#endif
