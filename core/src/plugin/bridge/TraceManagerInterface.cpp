#include "TraceManagerInterface.h"
#include "../interface/TraceManager.h"

#include "device/Cache.h"
#include "log/Logger.h"
#include "system/Wrappers.h"
#include "system/trace/TraceManager.h"
#include "system/trace/MemoryOutput.h"

#include <emulashione.h>

#include <span>

using namespace emulashione::core::plugin;
#ifndef DOXYGEN_SHOULD_SKIP_THIS

const struct emulashione_trace_manager_interface TraceManagerInterface::gInterface {
    .header = {
        .uuid = {0x05, 0xC0, 0xFE, 0x3F, 0x84, 0x02, 0x40, 0xDE, 0xB4, 0xC5, 0xD1, 0x72, 0x2D,
            0xEC, 0x6C, 0x02},
        .revision = 1,
    },

    .alloc = [](auto ns, auto name, auto maxPayloadSize, auto outPtr) -> int {
        if(!name || !outPtr) return kEmulashioneApiMisuse;

        // creeate trace manager and register it
        auto man = std::make_shared<system::TraceManager>(ns, name, maxPayloadSize);

        *outPtr = man->getInstancePtr();
        return kEmulashioneSuccess;
    },
    .free = [](auto tm) {
        tm->p->willShutDown();
    },
    .addMemoryOutput = [](auto tm, auto maxChunks, auto chunkSize, auto outPtr) -> int {
        if(!tm || !outPtr) return kEmulashioneApiMisuse;

        try {
            // create the output
            const auto sz = chunkSize ? chunkSize : 0x800000;
            auto out = std::make_shared<system::trace::MemoryOutput>(maxChunks, sz);

            // associate it
            tm->p->addOutput(out);
            *outPtr = out->getInstancePtr();
        } catch(const std::exception &e) {
            Logger::Error("Failed to add memory output to trace manager: {}", e.what());
            return kEmulashioneUnknownError;
        }

        return kEmulashioneSuccess;
    },
    .putMessage = [](auto tm, auto time, auto id, auto payloadPtr, auto payloadBytes) {
        try {
            std::span<std::byte> payload;
            if(payloadPtr && payloadBytes) {
                payload = {reinterpret_cast<std::byte *>(payloadPtr), payloadBytes};
            }

            tm->p->putMessage(time, id, payload);
        } catch(const std::exception &e) {
            Logger::Crit("Failed to put trace message: {}", e.what());
        }
    },
    .waitForBacklogDrained = [](auto tm) -> int {
        try {
            tm->p->waitForWorker();
        } catch(const std::exception &e) {
            Logger::Error("Failed to wait for trace manager to be idle: {}", e.what());
            return kEmulashioneUnknownError;
        }
        return kEmulashioneSuccess;
    },

    .memGetNumRecords = [](auto out, auto outNumRecords) -> int {
        if(!out || !outNumRecords) return kEmulashioneApiMisuse;
        *outNumRecords = out->p->getNumMessages();
        return kEmulashioneSuccess;
    },
    .memIterateRecords = [](auto out, auto callback, auto callbackCtx) -> size_t {
        if(!out || !callback) return -1;
        size_t count{0};

        for(const auto &chunk : *out->p) {
            for(const auto &record : chunk) {
                ++count;

                const void *payloadPtr{nullptr};
                size_t payloadSz{0};

                if(record.length > sizeof(record)) {
                    payloadSz = record.length - sizeof(record);
                    payloadPtr = &record.payload;
                }

                // returning false aborts iteration
                if(!callback(record.time, record.ns, record.type, payloadPtr, payloadSz,
                            callbackCtx)) {
                    goto beach;
                }
            }
        }

beach:;
        return count;
    }
};

// XXX: it would be nice not to have to repeat the UUID bytes here...
const uuids::uuid TraceManagerInterface::kInterfaceUuid{{
    0x05, 0xC0, 0xFE, 0x3F, 0x84, 0x02, 0x40, 0xDE, 0xB4, 0xC5, 0xD1, 0x72, 0x2D, 0xEC, 0x6C, 0x02
}};

#endif
