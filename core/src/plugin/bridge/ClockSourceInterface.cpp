#include "ClockSourceInterface.h"
#include "../interface/ClockSource.h"

#include "device/Bus.h"
#include "device/Wrappers.h"
#include "log/Logger.h"

#include <emulashione.h>

using namespace emulashione::core::plugin;

#ifndef DOXYGEN_SHOULD_SKIP_THIS

const struct emulashione_clock_source_interface ClockSourceInterface::gInterface {
    .header = {
        .uuid = {
            0xC5, 0xF6, 0xCB, 0x2A, 0x26, 0x31, 0x45, 0xE4, 0x94, 0x5D, 0x4E, 0x02,
            0x25, 0x92, 0x92, 0x87},
        .revision = 1,
    },

    .getName = [](struct emulashione_clock_source_instance *wrapper) -> const char * {
        const auto &name = wrapper->ptr->getName();
        if(name.empty()) return nullptr;
        return name.c_str();
    },
    .getFrequency = [](struct emulashione_clock_source_instance *wrapper) -> double {
        auto clock = wrapper->ptr;
        return clock->getFrequency();
    },
    .getPeriod = [](struct emulashione_clock_source_instance *wrapper) -> double {
        auto clock = wrapper->ptr;
        return clock->getPeriod();
    },
};

// XXX: it would be nice not to have to repeat the UUID bytes here...
const uuids::uuid ClockSourceInterface::kInterfaceUuid{{
    0xC5, 0xF6, 0xCB, 0x2A, 0x26, 0x31, 0x45, 0xE4, 0x94, 0x5D, 0x4E, 0x02, 0x25, 0x92, 0x92, 0x87
}};

#endif
