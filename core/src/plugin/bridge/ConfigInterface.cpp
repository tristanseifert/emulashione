#include "ConfigInterface.h"
#include "plugin/Plugin.h"
#include "plugin/Manager.h"
#include "plugin/interface/Config.h"

#include "log/Logger.h"

#include <emulashione.h>

#include <cstring>

#include <boost/property_tree/ptree.hpp>
#include <gsl/pointers>

/**
 * A small object that's allocated to hold a property tree, against which the getters/setters
 * actually operate.
 *
 * @brief Wrapper to hold a property tree exported to plugins
 */
struct emulashione_config_object {
    /**
     * Property tree used as storage for this config
     */
    boost::property_tree::ptree tree;
};

using namespace emulashione::core::plugin;

#ifndef DOXYGEN_SHOULD_SKIP_THIS

const struct emulashione_config_interface ConfigInterface::gInterface {
    .header = {
        .uuid = {
            0xAB, 0xAB, 0x5F, 0xF9, 0xDE, 0x9A, 0x42, 0xB7, 0xBD, 0x64, 0x18, 0xA4,
            0x16, 0x59, 0x81, 0x21},
        .revision = 1,
    },

    /**
     * Retrieves a plugin's configuration.
     *
     * Currently, this is implemented by looking up a map in the emulator config at the key path
     * `plugin.${shortName}` with the plugin's information substituted in.
     */
    .configForPlugin = [](struct ::emulashione_plugin_context *pluginCtx)
        -> struct emulashione_config_object * {
        // resolve the plugin
        auto plug = Manager::The()->getPlugin(pluginCtx);
        if(!plug) return nullptr;

        // TODO: implement
        Logger::Debug("No configuration found for plugin {}", plug->getShortName());
        return nullptr;
    },

    /**
     * Allocate a new empty configuration object.
     */
    .newConfig = []() -> struct emulashione_config_object * {
        // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
        auto obj = new emulashione_config_object;
        return obj;
    },
    /**
     * Deallocates the object. This assumes it's a plain pointer to the above defined struct;
     * any dependent objects are thus automatically destructed.
     */
    .freeConfig = ConfigInterface::DeallocConfig,

    .keyExists = [](auto obj, auto key) -> bool {
        if(!obj || !key) return false;

        auto node = obj->tree.find(key);
        return (node != obj->tree.not_found());
    },
    .getString = [](auto obj, auto key, auto out, auto outSize) -> bool {
        if(!obj || !key) return false;

        auto val = obj->tree.template get_optional<std::string>(key);
        if(out && outSize && val) {
            strncpy(out, (*val).c_str(), outSize);
            return true;
        }
        return false;
    },
    .getStringLength = [](auto obj, auto key, auto outSize) -> bool {
        if(!obj || !key) return false;

        auto val = obj->tree.template get_optional<std::string>(key);
        if(outSize && val) {
            *outSize = (*val).size();
            return true;
        }
        return false;
    },
    .getSigned = [](struct emulashione_config_object *obj, const char *key, int64_t *out) {
        if(!obj || !key) return false;

        auto val = obj->tree.get_optional<int64_t>(key);
        if(out && val) {
            *out = *val;
            return true;
        }
        return false;
    },
    .getUnsigned = [](struct emulashione_config_object *obj, const char *key, uint64_t *out) {
        if(!obj || !key) return false;

        auto val = obj->tree.get_optional<uint64_t>(key);
        if(out && val) {
            *out = *val;
            return true;
        }
        return false;
    },
    .getDouble = [](struct emulashione_config_object *obj, const char *key, double *out) {
        if(!obj || !key) return false;

        auto val = obj->tree.get_optional<double>(key);
        if(out && val) {
            *out = *val;
            return true;
        }
        return false;
    },
    .getBool = [](struct emulashione_config_object *obj, auto key, auto out) {
        if(!obj || !key) return false;

        auto val = obj->tree.get_optional<bool>(key);
        if(out && val) {
            *out = *val;
            return true;
        }
        return false;
    },

    .setString = [](struct emulashione_config_object *obj, const char *key, const char *value) {
        if(!obj || !key || !value) return;
        obj->tree.put<std::string>(key, value);
    },
    .setSigned = [](struct emulashione_config_object *obj, const char *key, const int64_t value) {
        if(!obj || !key) return;
        obj->tree.put<int64_t>(key, value);
    },
    .setUnsigned = [](struct emulashione_config_object *obj, const char *key,
            const uint64_t value) {
        if(!obj || !key) return;
        obj->tree.put<uint64_t>(key, value);
    },
    .setDouble = [](struct emulashione_config_object *obj, const char *key, const double value) {
        if(!obj || !key) return;
        obj->tree.put<double>(key, value);
    },
    .setBool = [](struct emulashione_config_object *obj, auto key, auto value) {
        if(!obj || !key) return;
        obj->tree.put<bool>(key, value);
    },
};

// XXX: it would be nice not to have to repeat the UUID bytes here...
const uuids::uuid ConfigInterface::kInterfaceUuid{{0xAB, 0xAB, 0x5F, 0xF9, 0xDE, 0x9A,
    0x42, 0xB7, 0xBD, 0x64, 0x18, 0xA4, 0x16, 0x59, 0x81, 0x21
}};

#endif

/**
 * Allocates a config object with the given tree.
 */
struct emulashione_config_object *ConfigInterface::MakeConfig(const boost::property_tree::ptree &tree) {
    // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    auto obj = new emulashione_config_object;
    if(obj) {
        obj->tree = tree;
    }
    return obj;
}

/**
 * Deallocates the object.
 */
void ConfigInterface::DeallocConfig(struct emulashione_config_object *obj) {
    // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    delete obj;
}

