#ifndef PLUGIN_BRIDGE_DEVICEINTERFACE_H
#define PLUGIN_BRIDGE_DEVICEINTERFACE_H

#include <cstddef>
#include <cstdint>
#include <span>

#include <uuid.h>

struct emulashione_interface_header;
struct emulashione_plugin_context;
struct emulashione_device_interface;
struct emulashione_device_descriptor;

/**
 * @brief Namespace holding internal implementations of plugin interfaces
 */
namespace emulashione::core::plugin {
/**
 * Provides an interface allowing plugins to register devices they implement, which can then be
 * combined into entire systems by the core emulation library. All devices implement a standard
 * set of functions, which are defined in its information block -- a static structure provided
 * at device registration time, containing identifying information, callback functions, and config
 * information -- and recorded in the local device registry.
 *
 * @brief Exported interface for registering and instantiating device classes
 */
class DeviceInterface {
    friend class Manager;

    public:
        using UuidBytes = std::span<uint8_t, 16>;

        static const struct emulashione_device_descriptor *Get(const UuidBytes uuid);
        static int GetMatching(const struct emulashione_device_descriptor **outDevices,
                const size_t maxDevices, const char *predicate, ...);

        static void Register(struct emulashione_plugin_context *plug,
                const struct emulashione_device_descriptor *device);
        static void UnRegister(struct emulashione_plugin_context *plug,
                const UuidBytes uuidBytes);

        /// UUID of the interface
        static const uuids::uuid kInterfaceUuid;

    private:
        /// Shared logger interface definition
        static const struct emulashione_device_interface gInterface;
};
}

#endif
