#ifndef PLUGIN_BRIDGE_LOGINTERFACE_H
#define PLUGIN_BRIDGE_LOGINTERFACE_H

#include <memory>
#include <string>

#include <uuid.h>

#include <spdlog/fwd.h>
#include <spdlog/common.h>

struct emulashione_logger_handle;
struct emulashione_logger_interface;

namespace emulashione::core::plugin {
class Plugin;

/**
 * Implements the plugin logging interface. It vends out "logging contexts" which are pointers to
 * instances of the class, which in turn contain a logger object
 *
 * @brief Exported interface for printing messages to emulator log
 */
class LogInterface {
    friend class Manager;

    public:
        LogInterface(Plugin *plugin, const std::string &tag = "");

        /// Return a pointer to the interface structure.
        const struct ::emulashione_logger_interface *getInterface() const {
            return &gInterface;
        }

        /// UUID of the interface
        static const uuids::uuid kInterfaceUuid;

    private:
        static int _Thunk_GetLogLevel(struct ::emulashione_logger_handle *);
        static int _Thunk_SetLogLevel(struct ::emulashione_logger_handle *, unsigned int);

        static void _Thunk_Trace(struct ::emulashione_logger_handle *, const char *);
        static void _Thunk_Debug(struct ::emulashione_logger_handle *, const char *);
        static void _Thunk_Info(struct ::emulashione_logger_handle *, const char *);
        static void _Thunk_Warning(struct ::emulashione_logger_handle *, const char *);
        static void _Thunk_Error(struct ::emulashione_logger_handle *, const char *);
        static void _Thunk_Critical(struct ::emulashione_logger_handle *, const char *);

        static unsigned int ConvertLogLevel(const spdlog::level::level_enum);
        static spdlog::level::level_enum ConvertLogLevel(const unsigned int);

    private:
        /// Shared logger interface definition
        static const struct ::emulashione_logger_interface gInterface;

        /// Logging instance that all calls will thunk through to
        std::shared_ptr<spdlog::logger> logger;
};
}

#endif
