#ifndef PLUGIN_BRIDGE_SCHEDULERINTERFACE_H
#define PLUGIN_BRIDGE_SCHEDULERINTERFACE_H

#include <uuid.h>

struct emulashione_scheduler_interface;

namespace emulashione::core::plugin {
/// Exported interface for interacting with device schedulers
class SchedulerInterface {
    friend class Manager;

    public:
        /// UUID of the interface
        static const uuids::uuid kInterfaceUuid;

    private:
        /// Shared interface definition
        static const struct ::emulashione_scheduler_interface gInterface;
};

}

#endif

