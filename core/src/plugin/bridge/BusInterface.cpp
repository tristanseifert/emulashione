#include "BusInterface.h"
#include "../interface/Bus.h"

#include "device/Bus.h"
#include "device/Device.h"
#include "device/Wrappers.h"
#include "log/Logger.h"

#include <emulashione.h>

using namespace emulashione::core::plugin;

#ifndef DOXYGEN_SHOULD_SKIP_THIS

const struct emulashione_bus_interface BusInterface::gInterface {
    .header = {
        .uuid = {0xBE, 0xA4, 0x6E, 0xC1, 0xE4, 0xF4, 0x48, 0xB3, 0x84, 0xF8, 0x05, 0x5F,
            0xD9, 0x3E, 0xBA, 0xAD},
        .revision = 1,
    },

    .getName = [](auto busWrap) -> const char * {
        const auto &name = busWrap->ptr->getName();
        if(name.empty()) return nullptr;
        return name.c_str();
    },
    .getDataWidth = [](auto busWrap) -> int {
        return busWrap->ptr->getDataWidth();
    },
    .getAddressWidth = [](auto busWrap) -> int {
        return busWrap->ptr->getAddressWidth();
    },

    .read = [](auto busWrap, auto device, auto address, auto info) -> int {
        try {
            busWrap->ptr->accessRead(device, address, *info);
        }
        // catch underlying devices failing the access
        catch(const device::Device::Error &e) {
            return e.getErrorCode();
        }
        // no device available for this address
        catch(const device::Bus::UnmappedAccess &) {
            // TODO: use standard error codes
            return -1;
        }
        // access succeeded
        return 0;
    },
    .write = [](auto busWrap, auto device, auto address, auto info) -> int {
        try {
            busWrap->ptr->accessWrite(device, address, *info);
        }
        // catch underlying devices failing the access
        catch(const device::Device::Error &e) {
            return e.getErrorCode();
        }
        // no device available for this address
        catch(const device::Bus::UnmappedAccess &) {
            // TODO: use standard error codes
            return -1;
        }
        // access succeeded
        return 0;
    },
};

// XXX: it would be nice not to have to repeat the UUID bytes here...
const uuids::uuid BusInterface::kInterfaceUuid{{0xBE, 0xA4, 0x6E, 0xC1, 0xE4, 0xF4, 0x48, 0xB3,
    0x84, 0xF8, 0x05, 0x5F, 0xD9, 0x3E, 0xBA, 0xAD}};

#endif
