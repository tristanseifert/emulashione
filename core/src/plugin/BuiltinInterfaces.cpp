#include "Manager.h"
#include "Plugin.h"

#include "bridge/LogInterface.h"
#include "interface/Log.h"

#include "bridge/RegistryInterface.h"
#include "interface/Registry.h"

#include "bridge/DeviceInterface.h"
#include "interface/Device.h"

#include "bridge/ConfigInterface.h"
#include "interface/Config.h"

#include "bridge/BusInterface.h"
#include "interface/Bus.h"

#include "bridge/ClockSourceInterface.h"
#include "interface/ClockSource.h"

#include "bridge/SchedulerInterface.h"
#include "interface/Scheduler.h"

#include "bridge/TraceManagerInterface.h"
#include "interface/TraceManager.h"

using namespace emulashione::core::plugin;
using PM = Manager;

/// List of all interfaces built in to the library
const std::array<PM::BuiltinInterface, PM::kNumBuiltinInterfaces> PM::kBuiltinInterfaces{{
    {
        .uuid = LogInterface::kInterfaceUuid,
        .header = &LogInterface::gInterface.header,
    },
    {
        .uuid = RegistryInterface::kInterfaceUuid,
        .header = &RegistryInterface::gInterface.header,
    },
    {
        .uuid = DeviceInterface::kInterfaceUuid,
        .header = &DeviceInterface::gInterface.header,
    },
    {
        .uuid = ConfigInterface::kInterfaceUuid,
        .header = &ConfigInterface::gInterface.header,
    },
    {
        .uuid = BusInterface::kInterfaceUuid,
        .header = &BusInterface::gInterface.header,
    },
    {
        .uuid = ClockSourceInterface::kInterfaceUuid,
        .header = &ClockSourceInterface::gInterface.header,
    },
    {
        .uuid = SchedulerInterface::kInterfaceUuid,
        .header = &SchedulerInterface::gInterface.header,
    },
    {
        .uuid = TraceManagerInterface::kInterfaceUuid,
        .header = &TraceManagerInterface::gInterface.header,
    },
}};
