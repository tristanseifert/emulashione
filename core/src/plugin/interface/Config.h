#ifndef EMULASHIONE_PLUGIN_INTERFACE_CONFIG_H
#define EMULASHIONE_PLUGIN_INTERFACE_CONFIG_H

#include "Common.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

struct emulashione_plugin_context;
struct emulashione_interface_header;

struct emulashione_config_object;

/**
 * Interface that plugins can use to query the configuration of the system; more generally, it
 * allows exchange of arbitrary "dictionary" type data between plugins.
 *
 * @brief Methods for manipulating config objects.
 */
struct emulashione_config_interface {
    /// Standard interface header
    struct emulashione_interface_header header;

    /**
     * Retrieves a plugin's configuration object, if it exists. The origin of this configuration
     * is determined by the emulator core.
     *
     * @note The returned config object 
     *
     * @param Plugin whose configuration to query
     *
     * @return Configuration object, or `null` if no config for this plugin
     */
    struct emulashione_config_object* (*configForPlugin)(struct emulashione_plugin_context *);

    /**
     * Allocates an empty config object.
     *
     * @return An empty configuration object, or `null` if error.
     */
    struct emulashione_config_object* (*newConfig)();

    /**
     * Deallocates a previously allocated config object.
     *
     * @param Config object to deallocate
     */
    void (*freeConfig)(struct emulashione_config_object *);

    /**
     * Checks if the particular key exists.
     *
     * @param Config object to query
     * @param Config key to test
     *
     * @return Whether the key exists
     */
    bool (*keyExists)(struct emulashione_config_object *, const char *);

    /**
     * Retrieves a string value from the config, copying it into the specified temporary buffer.
     *
     * @param Config object to query
     * @param Config key to query
     * @param Output buffer to receive a copy of the string
     * @param Max size of the string buffer, in bytes
     *
     * @return Whether the key was found and could be converted
     */
    bool (*getString)(struct emulashione_config_object *, const char *, char *, const size_t);

    /**
     * Retrieves the length of the string in the config.
     *
     * @param Config object to query
     * @param Config key to query
     * @param Size of the string, in bytes
     *
     * @return Whether the key exists and it is a string.
     */
    bool (*getStringLength)(struct emulashione_config_object *, const char *, size_t *);

    /**
     * Retrieves a signed value from the config.
     *
     * @param Config object to query
     * @param Config key to query
     * @param Where to store the signed value
     *
     * @return Whether the key was found and could be converted
     */
    bool (*getSigned)(struct emulashione_config_object *, const char *, int64_t *);

    /**
     * Retrieves an unsigned value from the config.
     *
     * @param Config object to query
     * @param Config key to query
     * @param Where to store the unsigned value
     *
     * @return Whether the key was found and could be converted
     */
    bool (*getUnsigned)(struct emulashione_config_object *, const char *, uint64_t *);

    /**
     * Retrieves a floating point value from the config.
     *
     * @param Config object to query
     * @param Config key to query
     * @param Where to store the floating point value
     *
     * @return Whether the key was found and could be converted
     */
    bool (*getDouble)(struct emulashione_config_object *, const char *, double *);

    /**
     * Retrieves a boolean value from the config.
     *
     * @param Config object to query
     * @param Config key to query
     * @param Where to store the boolean value
     *
     * @return Whether the key was found and could be converted
     */
    bool (*getBool)(struct emulashione_config_object *, const char *, bool *);


    /**
     * Sets a string value in the config object.
     *
     * @param Config object to modify
     * @param Key whose value to set
     * @param String value to set
     */
    void (*setString)(struct emulashione_config_object *, const char *, const char *);
    /**
     * Sets a signed value for a config key
     *
     * @param Config object to modify
     * @param Key whose value to set
     * @param Signed value to set
     */
    void (*setSigned)(struct emulashione_config_object *, const char *, const int64_t);
    /**
     * Sets an unsigned value for a config key
     *
     * @param Config object to modify
     * @param Key whose value to set
     * @param Signed value to set
     */
    void (*setUnsigned)(struct emulashione_config_object *, const char *, const uint64_t);
    /**
     * Sets a double value for a config key
     *
     * @param Config object to modify
     * @param Key whose value to set
     * @param Signed value to set
     */
    void (*setDouble)(struct emulashione_config_object *, const char *, const double);
    /**
     * Sets a boolean value for a config key
     *
     * @param Config object to modify
     * @param Key whose value to set
     * @param Signed value to set
     */
    void (*setBool)(struct emulashione_config_object *, const char *, const bool);
};

#endif
