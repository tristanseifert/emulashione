#ifndef EMULASHIONE_PLUGIN_INTERFACE_DEVICE_H
#define EMULASHIONE_PLUGIN_INTERFACE_DEVICE_H

#include "Common.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

struct emulashione_plugin_context;
struct emulashione_interface_header;
struct emulashione_config_object;
struct emulashione_device_descriptor;
struct emulashione_device_instance_header;
struct emulashione_bus_instance;
struct emulashione_clock_source_instance;

/**
 * Interface that allows plugins to register devices they wish to export to the rest of the
 * library, to be combined into a system as a whole.
 *
 * Devices are defined by device structures, which are constant info blocks that have identifying
 * information about the plugin, callbacks for initialization and other life cycle events,
 * notification handlers, and more.
 *
 * @brief Methods for registering and retrieving device classes
 */
struct emulashione_device_interface {
    /// Standard interface header
    struct emulashione_interface_header header;

    /**
     * Gets the device descriptor for a device with the provided UUID.
     *
     * @param UUID of device to search for
     *
     * @return Pointer to device descriptor, or `nullptr` if not found.
     */
    const struct emulashione_device_descriptor *(*getDevice)(const uint8_t *uuid);

    /**
     * Searches the device registry for all devices matching the specified criteria, and provides
     * the address of each of the matching devices' info descriptors.
     *
     * @note Constraints are specified in string format; it is very similar to the NSPredicate
     *       string format on Apple platforms. That is, the string is made up of atoms of the
     *       form "(key == value)", which can stand by itself, and be combined via AND/OR
     *       operators.
     *
     * @param Where to output matching device descriptor pointers; if `null`, no descriptors are
     *        fetched, but the total number of results that matched the query are returned.
     * @param Maximum number of device descriptors to return
     * @param A string of constraints to apply against each device; you may use printf-style
     *        placeholders that are sequentially substituted as the constraint is parsed.
     * @param Arguments to be substituted in the constraint string.
     *
     * @return Number of devices found (up to maximum requested), or a negative error code. Zero
     *         indicates no devices were found matching the descriptor.
     */
    int (*getMatchingDevices)(const struct emulashione_device_descriptor **, const size_t,
            const char *, ...);

    /**
     * Registers a device.
     *
     * @param Plugin that is exporting this device; this same plugin must later call the
     *        deregistration method when shutting down.
     * @param Pointer to the device descriptor
     * @param Size of the device descriptor, in bytes.
     *
     * @return 0 if device was successfully registered, error code otherwise.
     */
    int (*registerDevice)(struct emulashione_plugin_context *,
            const struct emulashione_device_descriptor *, const size_t);

    /**
     * Unregisters a device.
     *
     * @param Plugin that originally registered the device
     * @param UUID of the registered device
     *
     * @return 0 if the device was successfully unregistered, error code otherwise.
     */
    int (*unregisterDevice)(struct emulashione_plugin_context *, const uint8_t *);

    /**
     * Gets the device descriptor that is associated with the given device instance.
     *
     * @return Device descriptor, or `NULL` if error.
     */
    const struct emulashione_device_descriptor* (*getDescriptor)(struct emulashione_device_instance_header *);

    /**
     * Indicate that the specified device has encountered an abnormal condition and the emulation
     * should be aborted.
     *
     * @param device Device that caused the error; used to look up the system
     * @param status An optional status code to indicate what failed
     *
     * @return 0 on success or a negative error code
     */
    int (*abort)(struct emulashione_device_instance_header *device, const int status);
};

#define kEmulashioneDeviceMagic 0x44657669636501A4ULL

/**
 * Callbacks related to the lifecycle of a device; this includes load/unload as well as the
 * designated instance initialization and deallocation methods.
 *
 * @brief Device callbacks to initialize and free new device instances
 */
struct emulashione_device_lifecycle_callbacks {
    /**
     * Invoked immediately after the device type has been registered.
     *
     * @return 0 on success, or a negative error code to abort the registration.
     */
    int (*didLoad)();

    /**
     * The device type is about to be removed from the registry; any clean up actions should be
     * performed at this point.
     *
     * @return 0 on success, or an error code.
     */
    int (*willUnload)();

    /**
     * Allocates a new instance of the device. The provided options are passed to the constructor
     * of the device to affect its behavior for various options.
     *
     * @param name Short name for this device (for display)
     * @param cfg A key/value list of options
     *
     * @return Pointer to a device instance, or `null` if failed
     */
    struct emulashione_device_instance_header* (*alloc)(const char *name,
            struct emulashione_config_object *cfg);

    /**
     * Deallocates a previously allocated device.
     *
     * @param Pointer to the device that was allocated.
     *
     * @return 0 on success, negative error code otherwise.
     */
    int (*free)(struct emulashione_device_instance_header *);
};

/**
 * These callbacks allow devices to provide a somewhat standardized interface to expose information
 * to the rest of the system -- whether that is other devices or an emulation frontend.
 *
 * @brief Device callbacks to query device information
 */
struct emulashione_device_info_callbacks {
    /**
     * Test whether the device implements a particular interface.
     *
     * @param device Device to query
     * @param uuid Interface UUID to query
     * @param uuidBytes Length of the uuid buffer, in bytes
     */
    bool (*supportsInterface)(struct emulashione_device_instance_header *device,
            const uint8_t *uuid, const size_t uuidBytes);

    /**
     * Read information out of the device.
     *
     * @param device Device to query
     * @param key Key to read information for; most of these will be unique to the device type.
     * @param outBuf A buffer to hold the information
     * @param outBufSize Maximum number of bytes that can be written to this buffer
     *
     * @return Actual number of bytes read, or a negative error code
     */
    int (*getInfo)(struct emulashione_device_instance_header *device, const char *key,
            void *outBuf, const size_t outBufSize);

    /**
     * Write information to the device.
     *
     * @param device Device to query
     * @param key Key to write information to; most of these will be unique to the device type.
     * @param data Pointer to the buffer to copy into this key's storage
     * @param dataSize Number of bytes to copy from the key storage; a value of 0 truncates the
     *        current contents of the key.
     *
     * @remark Not all keys that are readable can be written to.
     *
     * @return 0 on success, or a negative error code
     */
    int (*setInfo)(struct emulashione_device_instance_header *device, const char *key,
            const void *data, const size_t dataSize);
};

/**
 * Callbacks for the overall flow of emulation.
 *
 * @brief Device callbacks for emulation state changes
 */
struct emulashione_device_emulation_callbacks {
    /**
     * The system the device is a part of has been powered on and emulation is about to begin.
     *
     * @note This method is optional.
     */
    int (*willStartEmulation)(struct emulashione_device_instance_header *);

    /**
     * The system the device is a part of will stop emulation.
     *
     * @note This method is optional.
     */
    int (*willStopEmulation)(struct emulashione_device_instance_header *);

    /**
     * The system the device is a part of is being suspended; any background work done by the
     * device should be stopped immediately.
     *
     * @note This method is optional.
     */
    int (*willPauseEmulation)(struct emulashione_device_instance_header *);

    /**
     * The system the device is a part of is about to be resumed.
     *
     * @note This method is optional.
     */
    int (*willResumeEmulation)(struct emulashione_device_instance_header *);

    /**
     * The system this device is a part of is being shut down. No more emulation cycles will take
     * place for _any_ device at this point.
     *
     * To prevent deadlocks when the device's `free()` method is invoked, if the device has any
     * background workers, or allows callers to block on it (for example, for the strobe waiting
     * facilities) it should signal its own workers to terminate (and thus, release the lock) and
     * signal any waiting threads with an error condition.
     *
     * @note This method is optional.
     */
    int (*willShutDown)(struct emulashione_device_instance_header *);

    /**
     * The system the device is in is being reset. Any internal state should be reset as it would
     * on the physical hardware.
     *
     * @param Whether the reset is a hard reset
     *
     * @note This method is optional.
     */
    int (*reset)(struct emulashione_device_instance_header *, const bool);

    /**
     * Commits all changes up to the given system time. This indicates that all devices have been
     * run up until this timepoint, and there will be no requests from external devices with
     * timestamps past this point.
     *
     * What this means for the device is that it may discard all rollback state data for before
     * this time point, and convert that data to its output buffer format.
     *
     * @param System clock time, in nanoseconds, that all devices have been synced to
     */
    int (*commitTo)(struct emulashione_device_instance_header *, const double);
};

/**
 * Callbacks for performing emulation of a device via time slices. Some of these methods are
 * optional; they are explicitly marked as such and may be `NULL`.
 *
 * @brief Device callbacks for time slice based emulation
 */
struct emulashione_device_slice_callbacks {
    /**
     * A time step is about to begin; no device has yet begun executing it.
     *
     * @note This method is optional.
     *
     * @param Current system clock, in nanoseconds
     * @param Length of the time slice, in nanoseconds; this may be zero.
     */
    int (*timeSliceWillBegin)(struct emulashione_device_instance_header *, const double,
            const double);

    /**
     * Execute a time slice of the given duration. The device should use as much of the time slice
     * as possible; any remainder should be consumed in the next time slice.
     *
     * @param Current system clock, in nanoseconds
     * @param Length of the time slice, in nanoseconds.
     */
    int (*executeTimeSlice)(struct emulashione_device_instance_header *, const double,
            const double);

    /**
     * A time step has been executed by all devices.
     *
     * @note This method is optional.
     *
     * @param Current system clock, in nanoseconds
     */
    int (*timeSliceDidEnd)(struct emulashione_device_instance_header *, const double);

    /**
     * Returns the device's current progress into the time slice, in nanoseconds.
     *
     * @param Device to query
     *
     * @return Nanoseconds of the time slice that have been processed already; NaN if not executing.
     */
    double (*getTimeSliceProgress)(struct emulashione_device_instance_header *);
};

/**
 * Callbacks for step based emulation.
 *
 * @brief Device callbacks for step based emulation
 */
struct emulashione_device_step_callbacks {
    /**
     * Executes a single step of the device.
     *
     * @param Device to execute
     * @param System time at the start of the time step, in nanoseconds
     * @param Storage for the total number of nanoseconds executed in this step
     *
     * @return 0 on success, or a negative error code to abort emulation.
     */
    int (*step)(struct emulashione_device_instance_header *, double, double *);
};

/**
 * Context received by a cothread based device allowing it to interact with the scheduler that is
 * in charge of the system it is being emulated in. Each device instance has its own scheduler
 * context, but the actual callbacks and scheduler may be the same.
 *
 * @brief Information structure defining how a device interacts with the cooperative scheduler
 */
struct emulashione_device_scheduler_context {
    /**
     * Pointer to the scheduler interface; this is a convenience to avoid having to look up the
     * interface manually. It is the same interface pointer that would be returned from the
     * registry.
     */
    const struct emulashione_scheduler_interface *interface;

    /**
     * Scheduler responsible for emulating this device. This may be shared with other devices in
     * the system.
     */
    struct emulashione_scheduler_instance *scheduler;
};

/**
 * Callbacks for cooperative multithreading based emulation. In this mode, the device has a
 * cooperative thread spawned for it (see the
 * [cothread library docs](https://libcommunism.blraaz.me/docs/doxygen/) for more information) on
 * which it performs its emulation. The system exposes methods into the scheduler, which manages
 * the device's current time.
 *
 * The device can then simply perform calls to indicate to the system it's executed `n` clock
 * cycles, and based on the configured scheduling roles, either another device will be emulated or
 * the device will continue to be emulated.
 *
 * An advantage of this approach is that it simplifies the emulation of complex devices, as what
 * would previously have been a mess of nested state machines can now simply be a series of
 * function calls, with the call stack of the thread (including function arguments) serving as the
 * state of the device.
 *
 * @brief Device callbacks for cooperative multithreading based emulation
 */
struct emulashione_device_cothread_callbacks {
    /**
     * Called during initialization to associate a scheduler instance, and associated context, with
     * a particular device.
     *
     * @param device Device instance
     * @param context Scheduler information for the device; this pointer will be valid until the
     *        device is destroyed.
     */
    int (*prepare)(struct emulashione_device_instance_header *device,
            struct emulashione_device_scheduler_context *context);

    /**
     * Entry point for the device's thread. It may interact with the scheduler via the callbacks
     * provided in the sceduler callback struct.
     *
     * @param Device to execute
     * @param Scheduler information
     *
     * @return 0 if the device terminated successfully, an error code otherwise.
     */
    int (*main)(struct emulashione_device_instance_header *,
            struct emulashione_device_scheduler_context *);
};

/**
 * Flags for a memory access
 *
 * @brief Device memory access modifiers
 */
enum emulashione_device_memory_flags {
    /// Mask for the access width
    kAccessWidthMask                    = (0x07 << 0),

    /// Access is byte wide
    kAccessWidthByte                    = (0x01 << 0),
    /// Access is word (2 bytes) wide
    kAccessWidthWord                    = (0x02 << 0),
    /// Access is long word (4 bytes) wide
    kAccessWidthLongWord                = (0x03 << 0),
    /// Access is quad word (8 bytes) wide
    kAccessWidthQuadWord                = (0x04 << 0),
};

/**
 * Defines a memory access to a device. This contains all the parameters for an access and will
 * hold information about it once completed.
 *
 * @brief Information describing a device memory access
 */
struct emulashione_device_memory_access {
    /// Payload of the access (inputs for reads, outputs for writes)
    union {
         uint8_t d8;
        uint16_t d16;
        uint32_t d32;
        uint64_t d64;
    };

    /**
     * Mask of which bits are driven by the device. This is used to emulate the case in which a
     * device is only connected to part of the data bus, and some bits are read as open bus.
     *
     * If the value is 0, it's assumed that the entire bus is being driven.
     */
    union {
         uint8_t mask8;
        uint16_t mask16;
        uint32_t mask32;
        uint64_t mask64;
    };

    /**
     * Total time, in nanoseconds, that the access took. This is used by the caller to
     * appropriately advance its emulation step.
     */
    double time;

    /**
     * Memory space to issue the request against. Typically, this value is `0` corresponding to the
     * standard memory space. Platforms may define alternate memory spaces to support processor
     * specific features such as IO address spaces or special processor cycles.
     */
    uint16_t addressSpace;

    /**
     * Flags that affect how this transfer is performed. You must at least specify one of the
     * access width flags.
     */
    enum emulashione_device_memory_flags flags;
};

/**
 * Callbacks for performing memory reads/writes against the device.
 *
 * @brief Device callbacks for memory accesses
 */
struct emulashione_device_memory_callbacks {
    /**
     * Copies out all address spaces supported by the device. The only mandatory address space for
     * a device supporting memory accesses is `0`.
     *
     * @param Device to query
     * @param Buffer to copy the supported address space IDs into
     * @param Maximum number of address space IDs to output
     *
     * @return Number of address space IDs copied out (≤ max) or an error code.
     */
    int (*getSupportedAddressSpaces)(struct emulashione_device_instance_header *, uint16_t *,
            const size_t);

    /**
     * Invoked by another device on the bus to perform a read from this device.
     *
     * If the device has not yet executed to the specified time point, the call will block until
     * that takes place and data is available. If it has executed past this time, the state of the
     * device is rolled back to this time point and the read is then executed.
     *
     * @note This method will be invoked from other threads. Any communication with the device
     * itself and its emulation thread, and its internal state, must be appropriately
     * synchronized.
     *
     * @param Device to perform the read against
     * @param Device that issued the read request
     * @param Memory address to access
     * @param Absolute system time at which this access takes place
     * @param Access descriptor; see its definition for description of its contents
     *
     * @return 0 on success, negative error code.
     */
    int (*read)(struct emulashione_device_instance_header *,
            struct emulashione_device_instance_header *, const uint64_t,
            const double, struct emulashione_device_memory_access *);

    /**
     * Invoked by another device on the bus to perform a write to this device.
     *
     * If the device has not yet executed to the time point, the call may either block (if the
     * write may have timing-related side effects such as a DMA) or simply enqueue the write for
     * later time to be processed when the device emulation reaches the appropriate point. That
     * behavior can vary from write to write, for example, when a FIFO becomes full.
     *
     * Alternatively, if the device has executed past the specified time point, it is rolled back
     * to the time point and then the write is processed.
     *
     * @note This method will be invoked from other threads. Any communication with the device
     * itself and its emulation thread, and its internal state, must be appropriately
     * synchronized.
     *
     * @param Device to perform the write against
     * @param Device that issued the write request
     * @param Memory address to access
     * @param Absolute system time at which this access takes place
     * @param Access descriptor; see its definition for description of its contents
     *
     * @return 0 on success, negative error code.
     */
    int (*write)(struct emulashione_device_instance_header *,
            struct emulashione_device_instance_header *, const uint64_t,
            const double, struct emulashione_device_memory_access *);
};

/**
 * In addition to the "simplified" memory style bus access interface above, devices can expose
 * various strobes that are used to signal state that would be handled by dedicated bus control
 * lines on a real system; for example, for interrupt status, bus requests, etc.
 *
 * Strobes are identified in the context of this device by an integer; globally, they are identified
 * by a name string. The bus to which the device is attached will consume strobes by name, and
 * connect them together for all devices.
 *
 * IDs must start at 1, up to some maximum value. 0 is not a valid line ID. They should be the
 * same for all instances of the same device class.
 *
 * It is possible for a strobe to be wider than one bit: this can be used for auxiliary bus control
 * signals like interrupt priorities or access tags.
 *
 * @brief Device callbacks for strobe signaling
 */
struct emulashione_device_strobe_callbacks {
    /**
     * Gets the maximum ID supported by the device.
     */
    unsigned int (*maxId)(struct emulashione_device_instance_header *);

    /**
     * Gets the string name for the given ID.
     *
     * @param Device instance
     * @param ID
     *
     * @return String name for the ID, or `NULL`
     */
    const char* (*nameFor)(struct emulashione_device_instance_header *, const unsigned int);

    /**
     * Converts a string name to a strobe ID.
     *
     * @param Device instance
     * @param Strobe name
     *
     * @return Strobe ID, 0 if not found, or negative error code.
     */
    int (*idFor)(struct emulashione_device_instance_header *, const char *);

    /**
     * Gets the width (number of bits) for a given strobe. In most cases, this should be 1.
     *
     * @param Device instance
     * @param Strobe ID
     *
     * @return Width of strobe, or a negative error code; 0 indicates strobe was not found.
     */
    int (*widthFor)(struct emulashione_device_instance_header *, const unsigned int);

    /**
     * Sets the status of the given strobe. Only the lowest `n` bits, where `n` is the width of the
     * strobe, of the data parameter are considered.
     *
     * @note This does not make any differentation between active low and active high signals. That
     *       is an implementation detail of the underlying platform, and (famous last words…)
     *       should not be something an emulator really has to care about. So, all strobes are
     *       considered such that "1" = active unless specified otherwise.
     *
     * @note So that we can more easily emulate the behavior of real bus signals (and the
     *       associated side effects) there is support for multiple devices driving the strobes at
     *       once. Unless otherwise specified, the signals are driven on the strobes until a
     *       subsequent call to `stopDriving`.
     *
     * @param Device instance to set the strobe on
     * @param Device that is updating the strobes
     * @param Strobe ID
     * @param New value for strobe
     * @param System time point at which the change is made
     *
     * @return 0 on success, an error code otherwise.
     */
    int (*setState)(struct emulashione_device_instance_header *,
            struct emulashione_device_instance_header *, const unsigned int, const uint64_t,
            const double);

    /**
     * Indicates a particular device has stopped driving a particular strobe.
     *
     * @note The arguments should match a previous call to `setState`.
     *
     * @param Device instance whose strobe to control
     * @param Device that is updating the strobes
     * @param Strobe ID
     * @param New value for strobe (?????)
     * @param System time point at which the change is made
     *
     * @return 0 on success, an error code otherwise.
     */
    int (*stopDriving)(struct emulashione_device_instance_header *,
            struct emulashione_device_instance_header *, const unsigned int, const uint64_t,
            const double);

    /**
     * Waits for a particular strobe to change to the specified value, optionally with a mask to
     * apply against the strobe. The caller will be blocked until the strobe reaches the specified
     * value; it will be woken up from the recipient device's emulation loop.
     *
     * Typical uses for this include simulating behaviors such as bus requests.
     *
     * @param Device instance we want to wait for a strobe change on
     * @param Device that is making this call, e.g. the one waiting for the strobe to change
     * @param Strobe ID to observe
     * @param Value we're waiting for on the strobe
     * @param Mask to apply between the strobe and value; 0 means no mask is used.
     * @param System time point at which this access is made
     *
     * @return 1 if the device was executed until the line changed; 0 if not; or a negative error
     * code.
     */
    int (*waitForChange)(struct emulashione_device_instance_header *,
            struct emulashione_device_instance_header *, const unsigned int, const uint64_t,
            const uint64_t, const double);
};

/**
 * Allows several devices to be interconnected; for example, a bus controller may have several
 * busses connected to it or an IO port may have devices connected to it.
 *
 * @brief Device callbacks for connections to busses, clock sources and other devices
 */
struct emulashione_device_connection_callbacks {
    /**
     * Connects the given bus to this device.
     *
     * @param Device whose connections to operate on
     * @param Identifier of the connection slot
     * @param Bus to connect
     *
     * @return 0 on success, or a negative error code.
     */
    int (*addBus)(struct emulashione_device_instance_header *, const char *,
            struct emulashione_bus_instance *);

    /**
     * Removes a previously connected bus.
     *
     * @param Device whose connections to operate on
     * @param Slot which to disconnect
     *
     * @return 0 if bus connection was severed, an error code otherwise.
     */
    int (*removeBus)(struct emulashione_device_instance_header *, const char *);

    /**
     * Connects the given clock source to a named slot on this device.
     *
     * @param Device whose connections to operate on
     * @param Identifier of the connection slot
     * @param Clock source to be connected
     *
     * @return 0 on success, or a negative error code.
     */
    int (*addClockSource)(struct emulashione_device_instance_header *, const char *,
            struct emulashione_clock_source_instance *);

    /**
     * Removes a previously connected clock source.
     *
     * @param Device whose connections to operate on
     * @param Slot which to disconnect
     *
     * @return 0 if clock source connection was severed, an error code otherwise.
     */
    int (*removeClockSource)(struct emulashione_device_instance_header *, const char *);

    /**
     * Connects the given device to this one. The connection is identified by the provided string;
     * the plugin should use standardized (as far as a system goes) names here, but there is no
     * requirement on the format of these.
     *
     * @param Device whose connections to operate on
     * @param Identifier of the connection slot
     * @param Device as the source of the connection
     *
     * @return 0 on success, or a negative error code.
     */
    int (*addDevice)(struct emulashione_device_instance_header *, const char *,
            struct emulashione_device_instance_header *);

    /**
     * Removes a previously connected device.
     *
     * @param Device whose connections to operate on
     * @param Slot which to disconnect
     *
     * @return 0 if device connection was severed, an error code otherwise.
     */
    int (*removeDevice)(struct emulashione_device_instance_header *, const char *);
};

/**
 * Various flags that indicate the threading mode of a device.
 *
 * @brief Various flags defining how a device is emulated
 */
enum emulashione_device_threading_mode {
    /// The device supports being executed in its own thread per instance.
    kSupportsPerInstanceThread                  = (1 << 0),
    /**
     * Each instance of the device should be run in its own thread. This serves as a hint that the
     * device is relatively processor intensive for each time step.
     */
    kWantsPerInstanceThread                     = (1 << 1),

    /// Mask defining the emulation mode of the device
    kEmulationModeMask                          = (0x07 << 4),

    /// This device does not require any active emulation steps.
    kEmulationModePassive                       = (0x00 << 4),
    /**
     * The device is emulated by allowing the device code to run in one chunk; the device should
     * use as much of the time slice as possible without going over, and keep track of accumulated
     * error and use it on the next invocation.
     *
     * This mode is well suited to devices such as video processors that generate a continuous
     * discrete output.
     */
    kEmulationModeTimeSlice                     = (0x01 << 4),
    /**
     * The device is to be emulated by stepping it individually.
     *
     * In this mode, the core emulator library handles emulating the time slice and keeping track
     * of the elapsed. This is well suited to processors whose operation is easily divisible into
     * small atomic sections such as microinstructions.
     */
    kEmulationModeStep                          = (0x02 << 4),
    /**
     * Cooperative threading is used to emulate the device.
     *
     * This is similar to the stepped mode, with the difference being that instead of always
     * returning out of the step method (and thus, having to store all device state explicitly
     * somewhere) the state of the device becomes implicit, as it is captured as part of its stack
     * and all stack frames built up; when it wants to advance time, it simply calls into the
     * scheduler and notifies it that it wants to yield a certain amount of clock cycles.
     *
     * It is then the responsibility of the scheduler to ensure all devices get some processor
     * time to run. When there's no other devices to advance (or there's no need to yet, as they
     * may not be far enough out of sync to warrant resynchronization) the call may also return
     * immediately.
     */
    kEmulationModeCoThread                      = (0x03 << 4),
};

/**
 * A bit field that indicates which methods are supported by a particular device.
 *
 * @remark Supported emulation interfaces are determined based on the threading mode enum.
 *
 * @brief Flags indicating which sets of methods are supported by the device
 */
enum emulashione_device_supported_methods {
    /// Device implements the strobe interface
    kSupportsStrobe                             = (1 << 0),
    /// Device supports the memory bus callbacks
    kSupportsMemory                             = (1 << 1),
};

/**
 * Defines a device. This consists of some identifying information, as well as various mandatory
 * callbacks and optional interfaces.
 *
 * @brief Information structure for a particular device class, including all its callbacks
 */
struct emulashione_device_descriptor {
    /// Magic value: this must be `kEmulashioneDeviceMagic`
    uint64_t magic;
    /// Structure version; current is 1
    uint32_t infoVersion;
    uint32_t reserved;

    /// UUID of device; must be unique!
    uint8_t uuid[16];

    /// Various identification strings
    struct {
        /// Long name for display in user interface
        const char *displayName;
        /// Short name; used for matching against the device from system descriptions
        const char *shortName;
        /// Indicates who developed the device
        const char *authors;
        /// (Optional) Long form description for the device; Markdown syntax is supported
        const char *description;
        /// (Optional) An URL that provides more information about the device
        const char *infoUrl;
        /// Version string
        const char *version;
    } strings;

    /// Bit field indicating which device interfaces are supported.
    enum emulashione_device_supported_methods supported;
    /**
     * Flags that define the threading mode of the device; this specifies whether it is executed
     * in its own thread, as well as how the steps are done.
     *
     * @note This value should be constant for the device's lifespan.
     */
    enum emulashione_device_threading_mode threadingMode;

    struct emulashione_device_lifecycle_callbacks lifecycle;
    struct emulashione_device_info_callbacks info;

    struct emulashione_device_emulation_callbacks emulation;

    struct emulashione_device_slice_callbacks slice;
    struct emulashione_device_step_callbacks step;
    struct emulashione_device_cothread_callbacks cothread;
    struct emulashione_device_memory_callbacks memory;
    struct emulashione_device_strobe_callbacks strobe;
    struct emulashione_device_connection_callbacks connections;
};

/**
 * Header for device instance objects; the descriptor pointer should point to the descriptor for
 * the device that we have an instance of; it allows us to always look up the correct functions to
 * call.
 *
 * @brief Common header for all device instances
 */
struct emulashione_device_instance_header {
    const struct emulashione_device_descriptor *descriptor; 
};

#endif
