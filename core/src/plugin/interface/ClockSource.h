#ifndef EMULASHIONE_PLUGIN_INTERFACE_CLOCKSOURCE_H
#define EMULASHIONE_PLUGIN_INTERFACE_CLOCKSOURCE_H

#include "Common.h"

#include <stddef.h>
#include <stdint.h>

struct emulashione_clock_source_instance;

/**
 * Interface used to interface with emulated clock sources.
 *
 * All clock sources are implemented internally to the emulation library. Callers do not need to
 * care about the type (primary vs. derived) of a clock.
 *
 * @brief Methods to interface with clock sources
 */
struct emulashione_clock_source_interface {
    /// Standard interface header
    struct emulashione_interface_header header;

    /**
     * Get the name that this clock source is known as.
     *
     * @param Clock source to query
     *
     * @param Name of clock source, or `NULL` if error.
     */
    const char* (*getName)(struct emulashione_clock_source_instance *);

    /**
     * Get the frequency of the clock, in Hz.
     *
     * @param Clock source to query
     */
    double (*getFrequency)(struct emulashione_clock_source_instance *);

    /**
     * Get the period of the clock, in nanoseconds.
     *
     * @param Clock source to query
     */
    double (*getPeriod)(struct emulashione_clock_source_instance *);
};
#endif
