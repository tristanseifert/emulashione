#ifndef EMULASHIONE_PLUGIN_INTERFACE_SCHEDULER_H
#define EMULASHIONE_PLUGIN_INTERFACE_SCHEDULER_H

#include "Common.h"

#include <stddef.h>
#include <stdint.h>

struct emulashione_clock_source_instance;
struct emulashione_device_instance_header;
struct emulashione_scheduler_instance;

/**
 * Devices that use the cooperative threading model of execution have to interact with a scheduler
 * to give up their processor time shares. These schedulers manage the current time point of those
 * devices, according to their clocks. Synchronization with other devices in the system is
 * achieved by mmeans of the rules defined in the system definition.
 *
 * @brief Methods to interface with device schedulers
 */
struct emulashione_scheduler_interface {
    /// Standard interface header
    struct emulashione_interface_header header;

    /**
     * Registers the specified clock as the reference clock used by the device to keep track of
     * elapsed time.
     *
     * @param sched Scheduler instance to make this registration in
     * @param device Device instance to associate the clock with
     * @param clock Clock instance to associate with this device; value of `NULL` removes the
     *        clock association
     *
     * @return 0 on success or a negative error code
     */
    int (*setRefClock)(struct emulashione_scheduler_instance *sched,
            struct emulashione_device_instance_header *device,
            struct emulashione_clock_source_instance *clock);

    /**
     * Notifies the scheduler that the device has advanced a certain number of clock ticks, with
     * the clock being defined by a previous call.
     *
     * @remark You must set a reference clock for the device before calling this method.
     *
     * @param sched Scheduler instance this call operates on
     * @param device Device instance that is being processed
     * @param ticks Number of clock ticks the device has advanced.
     * @param outTime System time at which the device regains control. May be `NULL`
     */
    void (*yieldTicks)(struct emulashione_scheduler_instance *sched,
            struct emulashione_device_instance_header *device, const size_t ticks,
            double *outTime);
};

#endif
