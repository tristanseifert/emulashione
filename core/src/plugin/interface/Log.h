#ifndef EMULASHIONE_PLUGIN_INTERFACE_LOG_H
#define EMULASHIONE_PLUGIN_INTERFACE_LOG_H

#include "Common.h"

#include <stddef.h>
#include <stdint.h>

struct emulashione_logger_handle;

/**
 * Exposes methods for logging messages to the shared emulator log. Each plugin has a logger
 * created during initialization and assigned to the plugin context.
 *
 * @brief Methods to allow plugins to output messages to the emulator log
 */
struct emulashione_logger_interface {
    /// Standard interface header
    struct emulashione_interface_header header;

    /**
     * Sets the minimum log level that the logger will output. Log levels range from 0 (trace) to
     * 5 (critical).
     *
     * @param Logger whose state to change
     * @param Minimum log level to output. Loggers default to the emulation library's verbosity
     * level, unless the user has specified an override on the logging verbosity.
     *
     * @return 0 on success, error code otherwise
     */
    int (*setLogLevel)(struct emulashione_logger_handle *, unsigned int);

    /**
     * Get the current minimum log level.
     *
     * @return Log level, or a negative error code.
     */
    int (*getLogLevel)(struct emulashione_logger_handle *);

    /**
     * Prints a trace level message.
     *
     * @param Message string to output; this will be copied by the logger before returning.
     */
    void (*logTrace)(struct emulashione_logger_handle *, const char *);

    /**
     * Prints a debug level message.
     *
     * @param Message string to output; this will be copied by the logger before returning.
     */
    void (*logDebug)(struct emulashione_logger_handle *, const char *);

    /**
     * Prints an info level message.
     *
     * @param Message string to output; this will be copied by the logger before returning.
     */
    void (*logInfo)(struct emulashione_logger_handle *, const char *);

    /**
     * Prints a warning level message.
     *
     * @param Message string to output; this will be copied by the logger before returning.
     */
    void (*logWarning)(struct emulashione_logger_handle *, const char *);

    /**
     * Prints an error level message.
     *
     * @param Message string to output; this will be copied by the logger before returning.
     */
    void (*logError)(struct emulashione_logger_handle *, const char *);

    /**
     * Prints a critical level message.
     *
     * @param Message string to output; this will be copied by the logger before returning.
     */
    void (*logCritical)(struct emulashione_logger_handle *, const char *);
};

#endif
