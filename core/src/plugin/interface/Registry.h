#ifndef EMULASHIONE_PLUGIN_INTERFACE_REGISTRY_H
#define EMULASHIONE_PLUGIN_INTERFACE_REGISTRY_H

#include "Common.h"

#include <stddef.h>
#include <stdint.h>

struct emulashione_plugin_context;
struct emulashione_interface_header;

/**
 * Allows callers to look up other interfaces by name. This is one of the interfaces provided in
 * the plugin context structure to all plugins.
 *
 * Additionally, it allows plugins to register interfaces they export.
 *
 * @brief Register and retrieve interfaces via their UUID
 */
struct emulashione_registry_interface {
    /// Standard interface header
    struct emulashione_interface_header header;

    /**
     * Look up an interface, given its UUID.
     *
     * @param uuid Pointer to 16 bytes of UUID data.
     * @param flags Determine how the interface is searched for; currently unused, pass 0.
     *
     * @return Interface header, or `NULL` if not found.
     */
    const struct emulashione_interface_header *(*getInterface)(const uint8_t *uuid,
            const uint32_t flags);

    /**
     * Registers an interface.
     *
     * @param plugin The plugin in which the interface is defined
     * @param interface Pointer to the header of the interface
     * @param interfaceSize Total number of bytes in the interface object
     *
     * @return 0 on success, error code otherwise.
     */
    int (*registerInterface)(struct emulashione_plugin_context *,
            const struct emulashione_interface_header *, const size_t);

    /**
     * Unregisters an interface. The caller must be the same as the one that registered the
     * interface originally.
     *
     * @note This only prevents new lookups of the interface. It does not magically invalidate any
     * references to the interface held by other code; you should ensure that all instances are
     * shut down before unregistering an interface.
     *
     * @param plugin Plugin which originally registered the interface
     * @param uuid Pointer to 16 bytes of UUID data identifying the interface
     *
     * @return 0 on success (interface unregistered), or an error code
     */
    int (*unregisterInterface)(struct emulashione_plugin_context *, const uint8_t *uuid);
};

#endif
