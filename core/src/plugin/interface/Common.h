#ifndef EMULASHIONE_PLUGIN_INTERFACE_COMMON_H
#define EMULASHIONE_PLUGIN_INTERFACE_COMMON_H

#include <stddef.h>
#include <stdint.h>

/**
 * All interface structures must have this header as their first member; it identifies what the
 * interface is. Note that the format of interfaces past the last member of this header is not
 * guaranteed, and is up to the individual interface.
 *
 * @brief Common header for all exported interfaces containing their UUID
 */
struct emulashione_interface_header {
    /// UUID for the interface
    uint8_t uuid[16];
    /// Interface revision
    uint32_t revision;
};

#endif
