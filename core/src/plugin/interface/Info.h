#ifndef EMULASHIONE_PLUGIN_INTERFACE_INFO_H
#define EMULASHIONE_PLUGIN_INTERFACE_INFO_H

#include <stddef.h>
#include <stdint.h>

#define kEmulashionePluginMagic 0x506C7567496E01A4ULL

/**
 * Defines static information about the plugin, its required features and a few methods required
 * to handle more complicated dynamic discovery.
 *
 * @brief Mandatory information structure exported by plugin modules
 */
struct emulashione_plugin_info {
    /// Magic value: this must be `kEmulashionePluginMagic`
    uint64_t magic;
    /// Structure version; current is 1
    uint32_t infoVersion;
    /// Minimum required plugin API version
    uint32_t minPluginApi;

    /// Unique identifier of this plugin (should be a valid UUID)
    uint8_t uuid[16];

    /// Information strings for display
    struct {
        /// Long name for display in user interface
        const char *displayName;
        /// Short name; used for logging, etc.
        const char *shortName;
        /// Indicates who developed the plugin
        const char *authors;
        /// (Optional) Long form description for the plugin; Markdown syntax is supported
        const char *description;
        /// (Optional) An URL that provides more information about the plugin
        const char *infoUrl;
        /// Version string
        const char *version;
        /// License the plugin is available under
        const char *license;
    } strings;

    /**
     * Initialization callback; this is invoked by the plugin loader, with a reference to the
     * plugin data structure that the plugin should hold on to for all further calls into the core
     * library.
     *
     * The context structure provided is valid until the shutdown callback returns.
     */
    int (*initCallback)(struct emulashione_plugin_context *);

    /**
     * Shutdown callback; any resources allocated by the plugin should be released, as it is about
     * to be unloaded. Access into the core library should NOT be performed after this method
     * returns.
     */
    int (*shutdownCallback)(void);
};

/**
 * Plugin contexts hold per plugin information, and provides some default interfaces exported by
 * the core library. All interfaces must be called with a valid plugin context.
 *
 * These structures are allocated and filled by the library. Plugins should never attempt to
 * allocate this structure, nor modify any fields in the structure unless explicitly specified
 * otherwise.
 *
 * @brief Information passed to a newly loaded plugin to bootstrap it
 */
struct emulashione_plugin_context {
    /// Plugin context version; current is 1
    uint32_t contextVersion;

    /// internal plugin identifier (used by core)
    uint32_t identifier;

    /// pointer to the plugin info that this context corresponds to
    const struct emulashione_plugin_info *info;

    /**
     * Logger instance allocated by the core library as part of the plugin load process; this is
     * provided in the context so that plugins can begin logging as early as possible during
     * initialization in case of errors.
     */
    struct emulashione_logger_handle *logger;
    /// logger interface used for the provided logger
    const struct emulashione_logger_interface *loggerInterface;

    /// discovery interface; used to get references to other interfaces by their UUIDs
    const struct emulashione_registry_interface *registryInterface;
};

#endif
