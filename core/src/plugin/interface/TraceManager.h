#ifndef EMULASHIONE_PLUGIN_INTERFACE_TRACEMANAGER_H
#define EMULASHIONE_PLUGIN_INTERFACE_TRACEMANAGER_H

#include "Common.h"
#include "Device.h"

#include <stddef.h>
#include <stdint.h>

struct emulashione_trace_manager_instance;
struct emulashione_trace_manager_mem_output;

/**
 * Exposes an interface to the built in TraceManager class, which allows devices to output an
 * execution trace of their internal state for debugging purposes.
 *
 * @brief Methods for tracing internal state of devices
 */
struct emulashione_trace_manager_interface {
    /// Standard interface header
    struct emulashione_interface_header header;

    /**
     * Allocate a new trace manager for the given device.
     *
     * @remark The allocated trace manager must be deallocated by the device when it's done with
     *         using it.
     *
     * @param messageNamespace Default namespace for trace messages
     * @param name Short descriptive name for the trace manager
     * @param maxPayloadSize Size of the maximum trace message payload, or 0 to disable payloads
     * @param out Variable to hold the allocated trace manager instance
     *
     * @return 0 on success or a negative error code
     */
    int (*alloc)(const uint16_t messageNamespace, const char *name, const size_t maxPayloadSize,
            struct emulashione_trace_manager_instance **out);

    /**
     * Deallocates a trace manager. This will flush any pending data to the output plugins.
     *
     * @param tm Trace manager instance to deallocate
     */
    void (*free)(struct emulashione_trace_manager_instance *tm);



    /**
     * Adds a memory backed output to this trace manager, which will receive trace messages.
     *
     * @remark The memory held by the output (and the output itself) is only valid for as long as
     *         the trace manager itself. It is automatically deallocated when the trace manager is
     *         released.
     *
     * @param tm Trace manager to add the output to
     * @param maxChunks Maximum number of chunks to allocate, or 0 for no limit
     * @param chunkSize Size of a storage chunk, in bytes
     * @param outPtr A location to receive the initialized output (to later retrieve records)
     *
     * @return 0 on success or a negative error code
     */
    int (*addMemoryOutput)(struct emulashione_trace_manager_instance *tm, const size_t maxChunks,
            const size_t chunkSize, struct emulashione_trace_manager_mem_output **outPtr);



    /**
     * Write a message to all outputs associated with the given trace manager.
     *
     * @param tm Trace manager to receive the message
     * @param time Current system time to associate with this message
     * @param id A 48-bit message identifier to identify this event
     * @param payload Location of an optional payload to associate with this event
     * @param payloadBytes If `payload` is non-NULL, the number of bytes of payload data to be
     *        associated with the message. It may not be more than the maximum specified when the
     *        trace manager was initialized.
     *
     * @remark Any payload data is copied and need not remain valid once this call returns; note
     *         that this does imply a not insigificant performance impact with large payloads.
     *
     * @remark Messages sent to the trace system must be in chronological order. That is, once a
     *         particular timestamp is associated with a message, no messages with timestamps that
     *         were prior to that value are allowed. Failure to follow this requirement will result
     *         in strange behavior when serializing and accessing records.
     */
    void (*putMessage)(struct emulashione_trace_manager_instance *tm, const double time,
            const uint64_t id, void *payload, const size_t payloadBytes);

    /**
     * Waits until the trace manager has processed all messages it has received up until this
     * point in time. The caller will be blocked until that point.
     *
     * @param tm Trace manager to wait on
     *
     * @return 0 on success, or a negative error code.
     */
    int (*waitForBacklogDrained)(struct emulashione_trace_manager_instance *tm);

    /**
     * Get the total number of records stored in the memory output.
     *
     * @param out Memory output instance
     * @param outNumRecords Variable to receive the number of records
     *
     * @return 0 on success or a negative error code
     */
    int (*memGetNumRecords)(struct emulashione_trace_manager_mem_output *out,
            size_t *outNumRecords);

    /**
     * Iterate over all records written to the memory output.
     *
     * @param out Memory output instance to iterate over
     * @param callback Method to invoke for each record; return `false` to terminate iteration. It
     *        is called with these arguments:
     *        @param time Timestamp of this record
     *        @param ns Namespace for the record's message id
     *        @param id Message identifier
     *        @param payload If non-NULL, pointer to payload associated with message
     *        @param payloadSz Number of bytes of payload provided
     *        @param ctx Context passed into the method
     * @param inCtx Context pointer to pass to the callback function
     *
     * @return Total number of records iterated over, or -1 if error
     */
    size_t (*memIterateRecords)(struct emulashione_trace_manager_mem_output *out,
            bool(*callback)(double time, uint16_t ns, uint64_t id, const void *payload,
                size_t payloadSz, void *ctx), void *inCtx);
};

#endif
