#ifndef EMULASHIONE_PLUGIN_INTERFACE_BUS_H
#define EMULASHIONE_PLUGIN_INTERFACE_BUS_H

#include "Common.h"
#include "Device.h"

#include <stddef.h>
#include <stdint.h>

struct emulashione_bus_instance;
struct emulashione_device_instance_header;
struct emulashione_device_memory_access;

/**
 * Interface used to interface with emulated busses.
 *
 * Currently, these are implemented entirely within the core emulation library, so this interface
 * can be retrieved via the standard registry methods.
 *
 * @brief Methods to interface with busses
 */
struct emulashione_bus_interface {
    /// Standard interface header
    struct emulashione_interface_header header;

    /**
     * Get the name that this bus is known as.
     *
     * @param Bus to query
     *
     * @param Name of bus, or `NULL` if error.
     */
    const char* (*getName)(struct emulashione_bus_instance *);

    /**
     * Returns the width of the data bus, in bits.
     *
     * @param Bus whose width to get
     *
     * @return Width of data bus, in bits; or an error code if negative. Zero is an error.
     */
    int (*getDataWidth)(struct emulashione_bus_instance *);

    /**
     * Returns the width of the address bus, in bits.
     *
     * @param Bus whose width to get
     *
     * @return Width of address bus, in bits; or an error code if negative. Zero is an error.
     */
    int (*getAddressWidth)(struct emulashione_bus_instance *);



    /**
     * Perform a read from a device on the bus.
     *
     * Internally, the bus consults its address map to determine which device shall handle this
     * access, then forwards the access to that device.
     *
     * @remark The access descriptor must be prepopulated with the adress space and access flags.
     *
     * @param bus Bus object to perform the IO on
     * @param device Device performing the read
     * @param address Physical address to access
     * @param info Memory access descriptor; this is identical to what devices use.
     *
     * @return 0 on success, negative error code.
     */
    int (*read)(struct emulashione_bus_instance *bus,
            struct emulashione_device_instance_header *device, const uint64_t address,
            struct emulashione_device_memory_access *info);

    /**
     * Perform a write to a device on the bus.
     *
     * Internally, the bus consults its address map to determine which device shall handle this
     * access, then forwards the access to that device.
     *
     * @remark The access descriptor must be prepopulated with the data, mask, address space and
     *         access flags.
     *
     * @param bus Bus object to perform the IO on
     * @param device Device performing the write
     * @param address Physical address to access
     * @param info Memory access descriptor; this is identical to what devices use.
     *
     * @return 0 on success, negative error code.
     */
    int (*write)(struct emulashione_bus_instance *bus,
            struct emulashione_device_instance_header *device, const uint64_t address,
            struct emulashione_device_memory_access *info);
};

#endif
