#include "Loader.h"
#include "Manager.h"
#include "Plugin.h"

#include "config/Config.h"
#include "log/Logger.h"
#include "platform/platform.h"

#include <filesystem>

using namespace emulashione::core::plugin;

/**
 * Enumerates the default plugin directories, including those specified via configuration, and
 * loads all plugins found there.
 */
void Loader::LoadFromDefaultPaths() {
    if(!Config::HasKey("plugin.directory")) return;

    // check the directory in config
    auto plugDir = Config::Get<std::string>("plugin.directory");
    if(!plugDir.empty()) {
        LoadFromDirectory(std::filesystem::path(plugDir));
    }

    // how many plugins were loaded?
    Logger::Info("Loaded {} plugins", Manager::The()->numPlugins());
}

/**
 * Loads all plugins in the given directory.
 *
 * @param path Path to the directory to enumerate for plugins (with the correct platform specific
 * extension will be loaded)
 */
void Loader::LoadFromDirectory(const std::filesystem::path &path) {
    Logger::Trace("Enumerating plugins in '{}'", path.string());

    // ensure it exists
    if(!std::filesystem::exists(path)) {
        Logger::Warn("Plugin directory '{}' does not exist", path.string());
        return;
    }

    // iterate directory
    for(const auto &dent: std::filesystem::directory_iterator{path}) {
        auto ext = dent.path().extension();

        // ensure it's a plugin file
        if((!dent.is_regular_file() && !dent.is_symlink()) || ext.empty() ||
               ext.string() != platform::Dylib::gFileExtension) continue;

        // try to load it
        Load(dent.path());
    }
}

/**
 * Loads a plugin from the given path.
 *
 * @param path Location of a plugin file
 *
 * @throw Plugin::LoadError An error occurred while loading the plugin information
 * @throw std::exception Something went wrong while initializing the plugin
 *
 * TODO: Check if we've already loaded it (via path or UUID match)
 */
void Loader::Load(const std::filesystem::path &path) {
    std::shared_ptr<Plugin> plug;
    Logger::Trace("Attempting to load plugin from '{}'", path.string());

    try {
        plug = std::make_shared<Plugin>(path);
    } catch(const Plugin::LoadError &e) {
        Logger::Warn("Failed to load plugin at '{}': {}", path.string(), e.what());
        throw;
    }

    // register plugin with manager
    RegisterPlugin(plug);

    // invoke the initializers
    try {
        plug->invokeInitializers();
    } catch(const std::exception &e) {
        UnregisterPlugin(plug);

        Logger::Warn("Failed to initialize plugin '{}': {}", plug->getDisplayName(), e.what());
        throw;
    }
}

/**
 * Loads a plugin from its info already in memory.
 *
 * @param info Pointer to plugin information structure
 *
 * @throw Plugin::LoadError An error occurred while loading the plugin information
 * @throw std::exception Something went wrong while initializing the plugin
 */
void Loader::Load(const struct emulashione_plugin_info *info) {
    std::shared_ptr<Plugin> plug;
    //NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    Logger::Trace("Attempting to load plugin from '{}'", reinterpret_cast<const void *>(info));

    try {
        plug = std::make_shared<Plugin>(info);
    } catch(const Plugin::LoadError &e) {
    //NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        Logger::Warn("Failed to load in-memory '{}': {}", reinterpret_cast<const void *>(info),
                e.what());
        throw;
    }

    // register plugin with manager
    RegisterPlugin(plug);

    // invoke the initializers
    try {
        plug->invokeInitializers();
    } catch(const std::exception &e) {
        UnregisterPlugin(plug);

        Logger::Warn("Failed to initialize plugin '{}': {}", plug->getDisplayName(), e.what());
        throw;
    }
}

/**
 * Registers the provided plugin with the plugin manager.
 *
 * @param plugin Plugin instance; the manager will keep a strong reference.
 */
void Loader::RegisterPlugin(const std::shared_ptr<Plugin> &plug) {
    Manager::The()->registerPlugin(plug);
}

/**
 * Unegisters the provided plugin with the plugin manager.
 *
 * @param plugin Plugin instance; the manager will keep a strong reference.
 */
void Loader::UnregisterPlugin(const std::shared_ptr<Plugin> &plug) {
    Manager::The()->unregisterPlugin(plug);
}
