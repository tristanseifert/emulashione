#ifndef PLUGIN_LOADER_H
#define PLUGIN_LOADER_H

#include <filesystem>
#include <memory>
#include <string_view>

struct emulashione_plugin_info;

namespace emulashione::core::plugin {
class Plugin;
/**
 * Handles loading of plugins from disk, by either loading an individual file or enumerating the
 * contents of a directory and loading all plugins within.
 *
 * @brief Helpers for loading plugins from directories, files and memory
 */
class Loader {
    public:
        /// Loads plugins from default directories.
        static void LoadFromDefaultPaths();
        /// Loads all plugins in the given directory.
        static void LoadFromDirectory(const std::filesystem::path &path);

        /// Loads a plugin from the provided path.
        static void Load(const std::filesystem::path &path);
        /// Attempt to load a plugin that's already been loaded in memory.
        static void Load(const struct emulashione_plugin_info *info);

    private:
        static void UnregisterPlugin(const std::shared_ptr<Plugin> &);
        static void RegisterPlugin(const std::shared_ptr<Plugin> &);
};
}

#endif
