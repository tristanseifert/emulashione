#ifndef LIBEMULASHIONECORE_VERSION_H
#define LIBEMULASHIONECORE_VERSION_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Gets the short version string for the loaded library.
 *
 * @note The returned string may be a constant in the library's data segment. If you need to modify
 * the string, make a copy of it. Do not attempt to pass it to `free()` or similar calls.
 *
 * @return Short version string, in SemVer format
 */
EMULASHIONECORE_EXPORT const char *libemulashione_version();

/**
 * Gets the long version string for the loaded library.
 *
 * @note The returned string may be a constant in the library's data segment. If you need to modify
 * the string, make a copy of it. Do not attempt to pass it to `free()` or similar calls.
 *
 * @return Human-readable long version string
 */
EMULASHIONECORE_EXPORT const char *libemulashione_version_long();

#ifdef __cplusplus
}
#endif
#endif
