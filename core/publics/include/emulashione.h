#ifndef LIBEMULASHIONECORE_EMULASHIONE_H
#define LIBEMULASHIONECORE_EMULASHIONE_H

#ifndef EMULASHIONE_TYPES_ONLY

#include <emulashionecore_export.h>

#endif

////////////////////////////////////////////////////////////////////////////////
/// Error and status codes used by the library.
////////////////////////////////////////////////////////////////////////////////
typedef enum emulashione_status {
    /// Operation completed successfully
    kEmulashioneSuccess                 = 0,
    /// An unknown error occurred during the operation
    kEmulashioneUnknownError            = -1,
    /// Library is not initialized
    kEmulashioneNotInitialized          = -2,
    /// Library has already been initialized
    kEmulashioneAlreadyInitialized      = -3,
    /// API misuse detected; check the provided parameters
    kEmulashioneApiMisuse               = -4,
    /// The specified file could be read, but is invalid
    kEmulashioneParseError              = -5,
    /// Specified file does not exist
    kEmulashioneNonexistentFile         = -6,
    /// Plugin is invalid
    kEmulashioneInvalidPlugin           = -7,
    /// Provided interface does not exist
    kEmulashioneNoSuchInterface         = -8,
    /// Interface has already been registered
    kEmulashioneInterfaceExists         = -9,
    /// Interface was registered by a different plugin
    kEmulashioneInterfaceOwnerMismatch  = -10,
    /// A required device could not be found
    kEmulashioneNonexistentDevice       = -11,
    /// This device has already been registered
    kEmulashioneDeviceExists            = -12,
    /// Device was registered by a different plugin
    kEmulashioneDeviceOwnerMismatch     = -13,
    /// Device is invalid
    kEmulashioneInvalidDevice           = -14,
    /// System is in an invalid state for this call
    kEmulashioneInvalidSystemState      = -15,
    /// System is invalid
    kEmulashioneInvalidSystem           = -16,
    /// Provided key is unknown or unsupported in this context
    kEmulashioneUnknownKey              = -17,
    /// Information key is not writeable
    kEmulashioneReadOnlyKey             = -18,
    /// No device with the given short name exists on this system
    kEmulashioneNoSuchDevice            = -19,
} emulashione_status_t;

#ifndef EMULASHIONE_TYPES_ONLY

////////////////////////////////////////////////////////////////////////////////
// Functions defined in files included below here, and up to the subsequent
// comment, can be called safely without initializing the library.
////////////////////////////////////////////////////////////////////////////////
#include <version.h>
#include <init.h>

////////////////////////////////////////////////////////////////////////////////
// All functions defined in files included below MUST NOT be called before the
// library is initialized, or behavior is undefined.
////////////////////////////////////////////////////////////////////////////////
#include <interfaces.h>
#include <plugin.h>
#include <system.h>

#endif

#endif
