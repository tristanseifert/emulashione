#ifndef LIBEMULASHIONECORE_INTERFACES_H
#define LIBEMULASHIONECORE_INTERFACES_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

struct emulashione_interface_header;

/**
 * Looks up the interface corresponding to the provided id.
 *
 * @param id Location of interface id
 * @param idBytes Number of bytes of id data provided; this should be 16 as all current interfaces
 *        are identified by UUIDs.
 * @param outInterface A variable to receive the found interface descriptor
 */
EMULASHIONECORE_EXPORT
    emulashione_status_t libemulashione_interface_get(const void *id, const size_t idBytes,
            const struct emulashione_interface_header **outInterface);

#ifdef __cplusplus
}
#endif
#endif
