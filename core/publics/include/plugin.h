#ifndef LIBEMULASHIONECORE_PLUGIN_H
#define LIBEMULASHIONECORE_PLUGIN_H

struct emulashione_plugin_info;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Attempts to load all valid plugins in the provided directory. Any plugins that have already been
 * loaded (whether that is because of an exact path match, or because of an UUID match) are
 * ignored.
 *
 * @note This function only fails if a valid plugin file (e.g. it has the plugin information
 * structure) is invalid. Files that aren't plugins are ignored.
 *
 * @param directoryPath Path to a directory containing plugin files
 * @return kEmulashioneSuccess, or an error code indicating why plugin loading failed.
 */
EMULASHIONECORE_EXPORT emulashione_status_t libemulashione_plugin_load_dir(const char *directoryPath);

/**
 * Attempts to load a plugin from the specified file.
 *
 * @param path Path tothe plugin file.
 * @return kEmulashioneSuccess if the plugin was loaded (or is already loaded), or an error code.
 */
EMULASHIONECORE_EXPORT emulashione_status_t libemulashione_plugin_load_file(const char *path);

/**
 * Register a plugin which has already been loaded. This can be built in to the calling application
 * or loaded through other means.
 *
 * @note The library will NOT perform management of the underlying code. This means you are solely
 * responsible for ensuring all code and data used by this plugin remains accessible until you call
 * `libemulashione_deinit()`.
 *
 * @param info Pointer to a plugin information structure
 * @return kEmulashioneSuccess if the plugin was loaded, error code otherwise.
 */
EMULASHIONECORE_EXPORT emulashione_status_t libemulashione_plugin_load(
        const struct emulashione_plugin_info *info);

#ifdef __cplusplus
}
#endif
#endif
