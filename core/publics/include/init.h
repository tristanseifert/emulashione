#ifndef LIBEMULASHIONECORE_INIT_H
#define LIBEMULASHIONECORE_INIT_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Configuration for the emulator library.
 *
 * This is an optional object that can be passed to the extended initialization function, and
 * allows the client to override much of the configuration without providing a custom config file.
 *
 * It is designed so that when zero initialized, the default configuration values are used; that is
 * specifying a struct that's zeroed will result in identical behavior as if the struct had been
 * specified as `NULL` instead, unless otherwise specified.
 */
typedef struct {
    /**
     * Log level to use.
     *
     * A value of 0 uses the default level. Values from 1 (trace level logs) to 6 (critical
     * error messages only) can be specified.
     */
    int logLevel;
} emulashione_init_params_t;

/**
 * Initializes the emulator library.
 *
 * @note It is undefined behavior to repeatedly invoke this method without a corresponding call
 * to `libemulashione_deinit()`.
 *
 * @param configFilePath Path to the library configuration file, or NULL to read from the default
 * configuration.
 * @param params If not `NULL`, a structure defining various overrides for configurations of the
 *        emulator library.
 *
 * @return A status code indicating whether the library was initialized
 */
EMULASHIONECORE_EXPORT emulashione_status_t libemulashione_init(const char *configFilePath,
        const emulashione_init_params_t *params);

/**
 * Shuts down the emulator library, and deallocates any resources we deallocated.
 *
 * @note After this call is invoked, any pointers provided by the library should be assumed to be
 * invalid, and should no longer be accessed.
 *
 * @return A status code indicating whether the library was deinitialized
 */
EMULASHIONECORE_EXPORT emulashione_status_t libemulashione_deinit();

#ifdef __cplusplus
}
#endif
#endif
