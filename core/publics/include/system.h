#ifndef LIBEMULASHIONECORE_SYSTEM_H
#define LIBEMULASHIONECORE_SYSTEM_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

struct emulashione_system;
struct emulashione_device_instance_header;

/**
 * Allocates a new system; this serves as an individual instance of the emulator. There may be
 * multiple concurrent systems, each running independently.
 *
 * @note This system does not have any devices associated with it. Attempts to emulate it will fail
 *
 * @return An allocated system object, or `NULL` if an error occurred.
 */
EMULASHIONECORE_EXPORT
    struct emulashione_system *libemulashione_system_alloc();

/**
 * Releases resources used by a previously allocated system instance. Any devices allocated, as
 * well as other resources used by these devices are deallocated.
 *
 * @param Device instance to free
 *
 * @return Status code of the free operation
 */
EMULASHIONECORE_EXPORT
    emulashione_status_t libemulashione_system_free(struct emulashione_system *);

/**
 * Instantiates the devices, and makes the connections indicated in the system description at the
 * specified path.
 *
 * @param System to apply the configuration against
 * @param Path to the system definition
 *
 * @return Whether the definition was successfully loaded
 */
EMULASHIONECORE_EXPORT
    emulashione_status_t libemulashione_system_load_definition(struct emulashione_system *,
            const char *);

/**
 * Instantiates the devices, and makes the connections indicated in the system description already
 * loaded into memory.
 *
 * @param sys System to apply the configuration against
 * @param definition A char buffer containing the contents of a system definition file. This buffer
 *        should be a standard NULL-terminated C string, and must remain valid until this method
 *        returns.
 *
 * @return Whether the definition was successfully loaded
 */
EMULASHIONECORE_EXPORT
    emulashione_status_t libemulashione_system_parse_definition(struct emulashione_system *sys,
            const char *definition);

/**
 * Pauses a currently running system.
 *
 * @param System to pause
 *
 * @return Whether the system was succesfully paused.
 */
EMULASHIONECORE_EXPORT
    emulashione_status_t libemulashione_system_pause(struct emulashione_system *);

/**
 * Resumes a previously paused system.
 *
 * @param System to resume
 *
 * @return Whether the system was successfully resumed.
 */
EMULASHIONECORE_EXPORT
    emulashione_status_t libemulashione_system_resume(struct emulashione_system *);

/**
 * Resets the system, and all devices within.
 *
 * @param System to reset
 * @param Whether to perform a hard (`true`) or soft (`false`) reset
 *
 * @return Whether the system was successfully reset.
 */
EMULASHIONECORE_EXPORT
    emulashione_status_t libemulashione_system_reset(struct emulashione_system *, const bool);

/**
 * Executes the system. The calling thread becomes the execution manager for this emulation
 * system and won't return from this call until the emulation is paused or terminated.
 *
 * @param System to execute
 *
 * @return Reason the execution ended; negative indicates abnormal terminations.
 */
EMULASHIONECORE_EXPORT
    int libemulashione_system_run(struct emulashione_system *);

/**
 * Aborts the emulation of a running system; this can be used to force a call to the run method to
 * return.
 *
 * @param system The system to abort
 * @param status A status code indicating why emulation was aborted
 *
 * @return Status code indicating whether the abort was sent
 */
EMULASHIONECORE_EXPORT
    emulashione_status_t libemulashione_system_abort(struct emulashione_system *system,
            const int status);

/**
 * Get a reference to a device object on the system, to directly call methods on the device.
 *
 * @param system The system to query for this device
 * @param name Short name of the device, from system definition
 * @param outDevice Location of a pointer to hold the device object
 *
 * @return 0 on success or a negative error code.
 */
EMULASHIONECORE_EXPORT
    emulashione_status_t libemulashione_system_get_device(struct emulashione_system *system,
            const char *name, struct emulashione_device_instance_header **outDevice);

#ifdef __cplusplus
}
#endif
#endif
