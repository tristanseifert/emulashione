# Emulashione Core
This library contains the core of the emulator logic. It ties all systems together, provides some synchronization facilities, and channels input/output between the driver and various devices in the system.

Drivers can make use of this library through the C interface exported via the headers in `publics/include`. Internally, much of the library is written in C++. We don't export C++ classes directly due to ABI stability problems, which are worse on some platforms than others.

